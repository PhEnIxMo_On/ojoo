<?php
	return array(
		"exampleRoute" => array(
			"sub" => "CallSub",
			"mod" => "CallMod",
			"act" => "callAct",
			"url" => "example-url-(.+)",
			"vars" => "exampleVar"
		)
	)
?>