<?php
	return array(
		"outils" => array(
			'url' => 'outils',
			"sub" => "EM",
			"mod" => "global",
			"act" => "accueil",
		),
		"maintenanteCompte" => array(
			"url" => "maintenance-compte",
			"sub" => "EM",
			"prefix" => "manager",
			"mod" => "global",
			"act" => "accountChange"
		),
		"pnj251" => array(
			"url" => "pnj251",
			"sub" => "EM",
			"prefix" => "manager",
			"mod" => "global",
			"act" => "pnj"
		),
		"accueilManager" => array(
			"url" => "accueil",
			"prefix" => "manager",
			"sub" => "manager",
			"mod" => "global",
			"act" => "accueil"
		),
		"editConfig" => array(
			"url" => "config-edit",
			"prefix" => "manager",
			"sub" => "EM",
			"mod" => "global",
			"act" => "editConfig"
		)
	);
?>