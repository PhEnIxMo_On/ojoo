<?php
	class WrongParametersEventException extends Exception {
		public $code = '2';
		public $message = '';
		public $nom = 'WrongParametersEvent';
		
		public function __construct($message = null,$code = null) {
			if ($message != null) $m = $message;
			else				  $m = $this->message;
			
			if ($code != null) 	 $c = $code;
			else				 $c = $this->code;
			
			parent::__construct($m,$c);
		}
	}
?>