<?php
	class StringRightNotFoundException extends Exception {
		public $code = '#O11';
		public $message = '';
		public $nom = 'StringRightNotFound';
		
		public function __construct($message = null,$code = null) {
			if ($message != null) $m = $message;
			else				  $m = $this->message;
			
			if ($code != null) 	 $c = $code;
			else				 $c = $this->code;
			
			// parent::__construct($m,$c);
		}
	}
?>