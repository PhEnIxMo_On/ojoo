<?php
	class AppException extends Exception {
		protected $code = '9';
		protected $appId;
		protected $appCode;
		protected $message = '';
		protected $nom = 'AppException';
		
		public function __construct($appId,$message = null,$code = null,$appCode = APP_ERROR_UNKNOW) {
			if ($message != null) $m = $message;
			else				  $m = $this->message;
			
			if ($code != null) 	 $c = $code;
			else				 $c = $this->code;

			$this->appId = $appId;
			$this->appCode = $appCode;
			
			parent::__construct($m,$c);
		}
	}
?>