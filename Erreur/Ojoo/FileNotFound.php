<?php
class FileNotFoundException extends Exception {
	public $code = "1";
	public $message = "";
	public $name = "FileNotFound";
	
	public function __construct($message = null,$code = null) {
		if ($message != null) $m = $message;
		else				  $m = $this->message;
		
		if ($code != null) 	 $c = $code;
		else				 $c = $this->code;
		
		parent::__construct($m,$c);
	}
	
}
?>