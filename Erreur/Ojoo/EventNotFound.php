<?php
	class EventNotFoundException extends Exception {
		public $code = '9';
		public $message = '';
		public $nom = 'EventNotFound';
		
		public function __construct($message = null,$code = null) {
			if ($message != null) $m = $message;
			else				  $m = $this->message;
			
			if ($code != null) 	 $c = $code;
			else				 $c = $this->code;
			
			parent::__construct($m,$c);
		}
	}
?>