<?php
	// Commande line : module -create -sub CP -name Coucou
	if (isset ($command['args']['create'])) {
		if (isset ($command['args']['sub'])) {
			if (isset ($command['args']['name'])) {
				if (is_dir('Web/Sub/' . $command['args']['sub'])) {
					if (!is_dir('Web/Sub/' . $command['args']['sub'] . '/' . $command['args']['name'])) {
						// Okay' on passe aux choses sérieuses :P
						// Création du dossier du module : 
						mkdir('Web/Sub/' . $command['args']['sub'] . '/' . $command['args']['name']);

						// Création du dossier Act : 
						mkdir('Web/Sub/' . $command['args']['sub'] . '/' . $command['args']['name'] . '/Act');

						// Création du dossier View : 
						mkdir('Web/Sub/' . $command['args']['sub'] . '/' . $command['args']['name'] . '/View');

						consoleMessage("OCL[Module]",6,"The module " . $command['args']['name'] . 'have been successfully created into ' . $command['args']['sub']);

					} else consoleMessage("OCL[Module]",5,"The indicated mod already exist !");
				} else consoleMessage("OCL[Module]",4,"The indicated sub doesn't exist.");
			} else consoleMessage("OCL[Module]",3,"Module must have a name to be created.");
		} else consoleMessage("OCL[Module]",2,"Module must have a sub name to be created.");
	} else {
		consoleMessage("OCL[Module]",1,"Module implement one function : create. Commande line : module -create -sub CP -name Coucou");
	}
?>