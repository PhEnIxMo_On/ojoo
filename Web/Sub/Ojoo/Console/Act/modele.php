<?php
	// Command Line : modele -table lolilol -bdd trololol	

	$_POST['table'] = $command['args']['table'];
	$_POST['mvc_bdd'] = TRUE;
	$_POST['bdd'] = $command['args']['db'];	


	if (isset ($_POST['mvc_bdd'])) $extends = ' extends mvc_bdd ';
	else						   $extends = ' ';
	

	$id = $O->modele->information_schema->get_ref_id($_POST['table']);
	$donneesIdRef = $id->fetch();
	//$donneesIdRef['COLUMN_NAME'] = 'id';
	if ($donneesIdRef['COLUMN_NAME'] != null) {
	$select_id = 
"
		public function select_id(\$id) {
			return \$this->select('" . $_POST['table'] . "','" . $donneesIdRef['COLUMN_NAME'] . "=' . \$id);
		}
";
	$delete_id = 
"
		public function delete_id(\$id) {
			return \$this->delete(array('" . $donneesIdRef['COLUMN_NAME'] . "' => \$id));
		}
";
	} else { $select_id = ""; $delete_id = "";}
	
	$reponse = $O->modele->information_schema->get_columns($_POST['table']);
	$attributs = "";
	$set = "";
	$select = "";
	while ($donnees = $reponse->fetch()) {
		$attributs .= "public \$" . $donnees['COLUMN_NAME'] . ";
		";
if ($donneesIdRef['COLUMN_NAME'] != null) {
$set .= 
"
		public function set_" . $donnees['COLUMN_NAME'] . "(\$set_value,\$id) {
			\$this->edit(array('" . $donnees['COLUMN_NAME'] . "' => \$set_value),array('" . $donneesIdRef['COLUMN_NAME'] . "' => \$id ));
		}
";

$select .=
"
		public function select_" . $donnees['COLUMN_NAME'] . "(\$name) {
			return \$this->select('" . $_POST['table'] . "','" . $donnees['COLUMN_NAME'] . "=\"' . \$name . '\"');
		}
";
}
	}

	
	
	$modele = 
"<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : " . date_fran(time()) . "
	****************************************************/
	class " . $_POST['table'] . $extends . " {
		" . $attributs . "
		public \$BDD = '" . $_POST['bdd'] . "';
		public \$where;
		public  \$liens = array();
		public \$actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
" . $set . "
		
		// Fonction de sélection :
		public function select_all() {
			return \$this->select('" . $_POST['table'] . "');
		}
		
" . $select . "

	// Fonction de suppression : 	
" . $delete_id . "
	}
?>";
	
	$O->file->set_file('Web/Modele/' . $_POST['table'] . '.php','a+');
	$O->file->write($modele);
	$O->file->close();

	consoleMessage("Modele",1,"Le modele a été créé avec succès !");
?>