<?php
	$filter = 0;
	if (isset ($_POST['FILTER_APP'])) {
		if ($_POST['FILTER_APP'] != 'false') {
			$filter |= $O->console->get_mask(Console::APP);
		}
		if ($_POST['FILTER_ROUTING'] != 'false') {
			$filter |= $O->console->get_mask(Console::ROUTING);
		}
		if ($_POST['FILTER_CONTROL'] != 'false') {
			$filter |= $O->console->get_mask(Console::CONTROL);
		}
		if ($_POST['FILTER_PHAR'] != 'false') {
			$filter |= $O->console->get_mask(Console::PHAR);
		}		
		$_SESSION['filterConsole'] = $filter;	
	}
?>
{{filter.view.php}}
