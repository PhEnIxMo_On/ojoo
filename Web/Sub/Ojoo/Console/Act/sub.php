<?php
	// Command line : sub -create -name Lol
	if (isset ($command['args']['create'])) {
		if (isset ($command['args']['name'])) {
			if (!is_dir('Web/Sub/' . $command['args']['name'])) {
				mkdir('Web/Sub/' . $command['args']['name']);
				consoleMessage("OCL[Sub]",3,"Sub " . $command['args']['name'] . ' have been successfully created.');
			} else consoleMessage("OCL[Sub]",4,"Sub " . $command['args']['name'] . ' already exist.');
		} else consoleMessage("OCL[Sub]",2,"Command sub need a name.");
	} else consoleMessage("OCL[Sub]",1,"Command sub just handle create for the moment.");
?>