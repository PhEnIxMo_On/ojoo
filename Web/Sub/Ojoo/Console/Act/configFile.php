<?php
 // Command line : configFile -create -sub cp -mod forum -right utilisateur
 $needArgs = array(
	"create" => "configFile can only be create.",
	"sub" => "configFile need a sub name",
	"mod" => "configFile need a mod name",
	"right" => "configFile need a default right"
 );
 if ($O->console->checkArgs($command,$needArgs)) {
 	$O->file->set_file('Web/Sub/' . $command['args']['sub'] . '/' . $command['args']['mod'] . '/' . $command['args']['mod'] . '.conf.yml','a+');
	$content =
"
---
droits: " . $command['args']['right'] . "
name: " . $command['args']['mod'] . "
description:
icone: Aucune
maintenance:
  design:
  status: false
  allow:
  reason:
  start: Mardi 16 Octobre 2012
  end:
  access_key:

";
	$O->file->write($content);
	$O->file->close();
	consoleMessage("configFile",1,"ConfigFile have been successfully created.");
 }
?>