<br />
<div class="newConsoleFondTitre">
	<div class="newConsoleIconTitle"><span class="icon">&#225;</span></div><span class="newConsoleTitre"> Filtrer : </span>
	<div style="clear: both;"></div>
</div>
<br />
<form action="" method="POST" id="filter">
	<table style="margin-left: 15px;">
		<tr>
			<td><div class="newConsoleType" style="background-color: #<?php echo $O->console->get_color(Console::APP); ?>"> <?php echo Console::APP; ?></td>
			<td><input type="checkbox" name="FILTER_APP" <?php if ($O->console->filter(Console::APP)) echo 'checked="checked"'; ?>/></td>
		</tr>
		<tr>
			<td colspan="2" style="height: 20px"></td>
		</tr>
		<tr>
			<td><div class="newConsoleType" style="background-color: #<?php echo $O->console->get_color(Console::CONTROL); ?>"> <?php echo Console::CONTROL; ?></td>
			<td><input type="checkbox" name="FILTER_CONTROL" <?php if ($O->console->filter(Console::CONTROL)) echo 'checked="checked"'; ?>/></td>
		</tr>
		<tr>
			<td colspan="2" style="height: 20px"></td>
		</tr>	
		<tr>
			<td><div class="newConsoleType" style="background-color: #<?php echo $O->console->get_color(Console::PHAR); ?>"> <?php echo Console::PHAR; ?></td>
			<td><input type="checkbox" name="FILTER_PHAR" <?php if ($O->console->filter(Console::PHAR)) echo 'checked="checked"'; ?>/></td>
		</tr>
		<tr>
			<td colspan="2" style="height: 20px"></td>
		</tr>	
		<tr>
			<td><div class="newConsoleType" style="background-color: #<?php echo $O->console->get_color(Console::ROUTING); ?>"> <?php echo Console::ROUTING; ?></td>
			<td><input type="checkbox" name="FILTER_ROUTING" <?php if ($O->console->filter(Console::ROUTING)) echo 'checked="checked"'; ?>/></td>
		</tr>
		<tr>
			<td colspan="2" style="height: 20px"></td>
		</tr>		
		<tr>
			<td colspan="2"><input style="margin-left: 10px;" type="button" class="btn btn-success" value="Filter" onClick="javascript:ojooForm.loadWithPostData('console/filter','filter','changeOnglet','','OjooConsole');"/></td>
		</tr>	
	</table>
</form>

