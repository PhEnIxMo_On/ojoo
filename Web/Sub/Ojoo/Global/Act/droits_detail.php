<?php
	$O->folder->set_folder('Web/Sub/' . $_POST['sub2'] . '/');
?>
<h3>{{img:Ojoo/Design/icones/icone_titre.jpg}} Sub - Droits : <?php echo $O->folder->name; ?></h3>
<div id="optionTitre">
	&nbsp;&nbsp;&nbsp;Droit actuel : <?php $O->sub->set_sub($O->folder->name); echo $O->sub->config['droit']; ?> <br />
	&nbsp;&nbsp;&nbsp;<a style="cursor: pointer;" onClick="formSubDroit();">{{img:Ojoo/Design/valid_button.jpg}}</a>
</div>
<br />
<form>
	<input type="hidden" name="sub_name" value="<?php echo $_POST['sub2']; ?>" />
	<table width="98%">
		<tr>
			<td class="titre_table" colspan="3"> Droits </td>
		</tr>
		<tr>
			<td class="sous_titre_table"> Nom : </td>
			<td class="sous_titre_table"> Rang :</td>
			<td class="sous_titre_table_no_border"> Action : </td>
		</tr>
		<?php
			foreach ($O->config['Rang'] as $key => $rang) {
				?>
					<tr>
						<td class="ligne_table" valign="middle" height="15"><?php echo $key; ?></td>
						<td class="ligne_table" valign="middle"><?php echo $rang; ?></td>
						<td class="ligne_table_no_border" valign="middle">{{checkbox name="<?php echo $key; ?>" content="Activer"}}</td>
					</tr>
				<?php
			}
		?>
	</table>
</form>