<?php 
if (isset ($_POST['utf8_decode_ajax']))    $ajax = true;
else $ajax = false;
if (isset ($_POST['DB_NAMES_UTF8']))    $db_names = true;
else $db_names = false;
if (isset ($_POST['utf8_decode_page']))    $utf8_page = true;
else $utf8_page = false;
if (isset ($_POST['maintenance'])) $maintenance = true;
else $maintenance = false;

$site_name = $_POST['nom_site'];
$fondateur = $_POST['fondateur'];
$redirection = $_POST['404'];

$config = $O->config;
$config["Template"]['utf8_decode_page'] = $utf8_page;
$config["Template"]['utf8_decode_ajax'] = $ajax;
$config['DB_NAMES_UTF8'] = $db_names;

$config['site_name'] = $site_name;
$config['fondateur'] = $fondateur;
$config['redirection_404'] = $redirection;
$config['maintenance'] = $maintenance;
$dump = yaml_dump($config);
unlink('Ojoo/Ojoo.conf.yml');
$handle = fopen('Ojoo/Ojoo.conf.yml','a+');
fwrite($handle,$dump);
fclose($handle);
?>