<h3>{{img:Ojoo/Design/icones/icone_titre.jpg}} Liste des exceptions : </h3>
<br />
<div class="bouton" onClick="ojooPanel.load('sub=Ojoo&mod=Outils&act=add_exception');">{*TABLE_ADD*} Ajouter </div>
<table width="98%">
	<tr>
		<td class="sous_titre_table"> Nom : </td>
		<td class="sous_titre_table_no_border" width="70"> Action : </td>
	</tr>
	<?php
		$O->folder->set_folder('Erreur/');
		foreach ($O->folder->folders as $folder) {
			?>
			<tr>
				<td class="titre_table" colspan="3"> <?php echo $folder->name; ?> - Fichier de configuration erreur </td>
			</tr>
			<?php
			foreach ($O->folder->folders[$folder->name]->files as $file) {
				?>
				<tr>
					<td class="ligne_table" valign="middle" height="15"><?php echo $file->name; ?></td>
					<td class="ligne_table_no_border" valign="middle"><a style="cursor: pointer;" onClick="ojooPanel.load('sub=Ojoo&mod=Outils&act=exceptions_config_look&path=<?Php echo $file->path; ?>');">{*TABLE_LOOK*}</a> {*TABLE_EDIT*} {*TABLE_DELETE*} </td>
				</tr>
				<?php
			}
			?>
			<tr>
				<td class="titre_table" colspan="3"> <?php echo $folder->name; ?> - Exceptions </td>
			</tr>
			<?php
			if (is_dir('Erreur/' . $folder->name . '/Exception')) {
				foreach ($O->folder->folders[$folder->name]->folders['Exception']->files as $file) {
					?>
					<tr>
						<td class="ligne_table" valign="middle" height="15"><?php echo $file->name; ?></td>
						<td class="ligne_table_no_border" valign="middle">{*TABLE_LOOK*} {*TABLE_EDIT*} {*TABLE_DELETE*} </td>
					</tr>
					<?php
				}
			}
		}
	?>
</table>