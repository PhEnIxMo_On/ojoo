<?php
	if (@mkdir('Web/Sub/' . $_POST['sub_name'])) {
		if (isset ($_POST['sub_maintenance_status'])) $status = true;
		else										  $status = false;
		$config = array(
			"droit" => $_POST['sub_droits'],
			"name" => $_POST['sub_name'],
			"design" => $_POST['sub_design'],
			"description" => $_POST['sub_desc'],
			"icone" => $_POST['sub_icone'],
			"maintenance" => array(
				"design" => $_POST['sub_maintenance_design'],
				"status" => $status,
				"allow" => "",
				"reason" => $_POST['sub_maintenance_raison'],
				"start" => $_POST['sub_maintenance_start'],
				"end" => $_POST['sub_maintenance_end'],
				"access_key" => $_POST['sub_maintenance_cle']
			)
		);
		$content = yaml_dump($config);
		$O->file->set_file('Web/Sub/' . $_POST['sub_name'] . '/' . $_POST['sub_name'] . '.conf.yml','a+');
		$O->file->write($content);
		$O->file->close();
	}
?>