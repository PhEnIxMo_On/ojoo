<?php
	$donneesT = $O->modele->event_template->select_id($_POST['id'])->fetch();
	$reponse = $O->modele->onevent->select_event_id($donneesT['id']);
?>
<h3>{{img:Ojoo/Design/icones/icone_titre.jpg}} <?php echo $donneesT['nom']; ?> : </h3>
<br />
<div class="bouton" onClick="ojooPanel.load('sub=Ojoo&mod=Outils&act=add_event&id=<?php echo $donneesT['id']; ?>');">{*TABLE_ADD*} Ajouter </div>
<br />
<table width="98%">
	<tr>
		<td class="titre_table" colspan="4"> Event :</td>
	</tr>
	<tr>
		<td class="sous_titre_table"> Id : </td>
		<td class="sous_titre_table"> Event ID : </td>
		<td class="sous_titre_table"> Script path : </td>
		<td class="sous_titre_table_no_border" width="70"> Action : </td>
	</tr>
<?php
	while ($donnees = $reponse->fetch()) {
		?>
			<tr>
				<td class="ligne_table" valign="middle" height="15"><?php echo $donnees['id']; ?></td>
				<td class="ligne_table" valign="middle" height="15"><?php echo $donnees['event_id']; ?></td>
				<td class="ligne_table" valign="middle" height="15"><?php echo $donnees['script_path']; ?></td>
				<td class="ligne_table_no_border" valign="middle">
					<a style="cursor: pointer;" onClick="ojooPanel.load('sub=Ojoo&mod=Outils&act=add_event&id=<?php echo $donnees['id']; ?>');">{*TABLE_ADD*}</a>
					<a style="cursor: pointer;" onClick="ojooPanel.load('sub=Ojoo&mod=Outils&act=edit_event&id=<?php echo $donnees['id']; ?>');">{*TABLE_EDIT*}</a>
					<a style="cursor: pointer;" onClick="ojooPanel.load('sub=Ojoo&mod=Outils&act=delete_event&id=<?php echo $donnees['id']; ?>','true');">{*TABLE_DELETE*}</a>
				</td>
			</tr>
		<?php
	}
?>