<h3>{{img:Ojoo/Design/icones/icone_titre.jpg}} Sub </h3>
<br />
<div class="bouton" onClick="ojooPanel.load('sub=Ojoo&mod=Outils&act=add_sub');">{*TABLE_ADD*} Ajouter </div>
<br />
<table width="98%">
	<tr>
		<td class="titre_table" colspan="3"> Sub :</td>
	</tr>
	<tr>
		<td class="sous_titre_table"> Nom : </td>
		<td class="sous_titre_table"> Description : </td>
		<td class="sous_titre_table_no_border" width="70"> Action : </td>
	</tr>
<?php
	$O->folder->set_folder('Web/Sub/',true);
	foreach ($O->folder->folders as $folder) {
		$name = $folder->name;
		$O->sub->set_sub($name);
		?>
			<tr>
				<td class="ligne_table" valign="middle" height="15"><?php echo $folder->name; ?></td>
				<td class="ligne_table" valign="middle" height="15"><?php echo $O->sub->config['description']; ?></td>
				<td class="ligne_table_no_border" valign="middle">
					<a style="cursor: pointer;" onClick="ojooPanel.load('sub=Ojoo&mod=Outils&act=add_mod&sub2=<?php echo $folder->name; ?>');">{*TABLE_ADD*}</a>
					<a style="cursor: pointer;" onClick="ojooPanel.load('sub=Ojoo&mod=Outils&act=edit_event&id=');">{*TABLE_EDIT*}</a>
					<a style="cursor: pointer;" onClick="ojooPanel.load('sub=Ojoo&mod=Outils&act=delete_event&id=','true');">{*TABLE_DELETE*}</a>
				</td>
			</tr>
		<?php
	}
?>