<h3>{{img:Ojoo/Design/icones/icone_titre.jpg}} Ajouter une exception : </h3>
<br />
<a style="cursor: pointer;" onClick="ojooPanel.load('sub=Ojoo&mod=Outils&act=exceptions_liste');">{{img:Ojoo/Design/exceptions.jpg}}</a>
<br />
<br />
<form id="test_form">
	<fieldset>
		<legend> Exception : </legend>
		<div id="icone_global">
		</div>
		<label> Nom </label> <input type="text" name="exception_name" id="exception_name"/><span> Le nom de l'exception </span><br />
		<label> Code </label> <input type="text" name="exception_code" id="exception_code"/><span> Le code de l'exception </span>
	</fieldset>
	<br />
	<fieldset>
		<legend> Fichier d'erreur : </legend>
		<label> titre </label> <input type="text" name="exception_titre" /><span> Le titre de l'erreur </span><br />
		<label> Code </label> <input type="text" name="exception_config_code" id="exception_config_code"/><span> Il doit �tre le m�me que celui de l'exception </span><br />
		<label> Log </label>{{checkbox name="exception_log" content="Loguer"}}<span> Si l'exception est logu�e </span><br />
		<label> Contenu </label> <input type="text" name="exception_contenu" /><span> Le message de l'erreur </span><br />
		<label> Template </label> <input type="text" name="exception_template" /><span> Le template de l'erreur </span><br />
		<label> Design </label> <input type="text" name="exception_design_template" /><span> Le template du design dans lequel sera ins�r� l'erreur (si aucun laissez vide) </span>
	</fieldset>
	<br />
	<fieldset>
		<legend> Replace tags : </legend>
		<label> Titre </label> <input type="text" name="replace_tag_titre" value="{{TITRE}}"/><span> Le tag de remplace du titre </span><br />
		<label> Contenu </label> <input type="text" name="replace_tag_contenu"  value="{{CONTENU}}"/><span> Le tag de remplace du contenu </span><br />
		<label> Code </label> <input type="text" name="replace_tag_code"  value="{{CODE}}"/><span> Le tag de remplace du code </span><br />
		<label> Ligne </label> <input type="text" name="replace_tag_ligne"  value="{{LIGNE}}"/><span> Le tag de remplace de la ligne </span><br />
		<label> Fichier </label> <input type="text" name="replace_tag_fichier"  value="{{FICHIER}}"/><span> Le tag de remplace du fichier </span>
		<input type="button" class="submit_button" onClick="formOutilsAddException()"/>
	</fieldset>
</form>