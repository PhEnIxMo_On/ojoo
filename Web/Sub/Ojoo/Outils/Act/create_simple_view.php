<?php
	$reponse = $O->modele->information_schema->get_columns($_POST['table']);
?>
<h3>{{img:Ojoo/Design/icones/icone_titre.jpg}} Intitul� des champs : </h3>
<br />
<form id="test_form">
	<table width="98%">
		<tr>
			<td class="titre_table" colspan="6"><?php echo $_POST['table']; ?></td>
		</tr>
		<tr>
			<td class="sous_titre_table"> Nom du champ : </td>
			<td class="sous_titre_table"> Intitul� : </td>
			<td class="sous_titre_table"> Utiliser : </td>
			<td class="sous_titre_table"> Input : </td>
			<td class="sous_titre_table"> R�f�rence : </td>
		</tr>
	<?php
		while ($donnees = $reponse->fetch()) {
			?>
				<tr>
					<td class="ligne_table" valign="middle" height="15"><?php echo $donnees['COLUMN_NAME']; ?></td>
					<td class="ligne_table" valign="middle" height="15"><input type="text" name="intitule[<?php echo $donnees['COLUMN_NAME']; ?>]" /></td>
					<td class="ligne_table" valign="middle" height="15"><input type="checkbox" name="utiliser[<?php echo $donnees['COLUMN_NAME']; ?>]" /></td>
					<td class="ligne_table" valign="middle" height="15">
						<select name="input[<?php echo $donnees['COLUMN_NAME']; ?>]">
							<option value="text"> Texte </option>
							<option value="checkbox"> Checkbox </option>
							<option value="select" onClick="ojooPanel.selectChoice('select_<?php echo $donnees['COLUMN_NAME']; ?>','/');"> Select </option>
						</select>
					</td>
					<input type="hidden" id="select_<?php echo $donnees['COLUMN_NAME']; ?>" name="selectChoice[<?php echo $donnees['COLUMN_NAME']; ?>]" value="none" />
					<td class="ligne_table" valign="middle" height="15"><input type="radio" name="ref" value="<?php echo $donnees['COLUMN_NAME']; ?>"/></td>
				</tr>
			<?php
		}
	?>
	</table>
	<?php
		$explode = explode('_',$_POST['mod2']);
		$sub = $explode[1];
		$mod = $explode[0];
		if (isset ($_POST['liste'])) echo '<input type="hidden" name="liste" value="ok" />';
		if (isset ($_POST['edition'])) echo '<input type="hidden" name="edit" value="ok" />';
		if (isset ($_POST['suppression'])) echo '<input type="hidden" name="delete" value="ok" />';
		if (isset ($_POST['ajout'])) echo '<input type="hidden" name="add" value="ok" />';
		if (isset ($_POST['href'])) echo '<input type="hidden" name="href" value="ok" />';
	?>
	<input type="hidden" name="sub2" value="<?php echo $sub; ?>" />
	<input type="hidden" name="mod2" value="<?php echo $mod; ?>" />
	<input type="hidden" name="onClick" value="<?php echo $_POST['onClick']; ?>" />
	<input type="hidden" name="mod" value="<?php echo $_POST['mod']; ?>" />
	<input type="hidden" name="table" value="<?php echo $_POST['table']; ?>" />
	<input type="hidden" name="bdd" value="<?php echo $_POST['bdd']; ?>" />
</form>
<br />
<div class="bouton" onClick="ojooPanel.load('sub=Ojoo&mod=Outils&act=generate_simple_view','false',true);">{*TABLE_ADD*} Envoyer </div>