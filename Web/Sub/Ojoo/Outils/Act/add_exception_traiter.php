<?php
	// On va construire le fichier d'exception :
	$exception_file_titre = $_POST['exception_name'];


	$exception_file = 
"<?php
	class " . $exception_file_titre . "Exception extends Exception {
		public \$code = '" . $_POST['exception_code'] . "';
		public \$message = '';
		public \$nom = '" . $exception_file_titre . "';
		
		public function __construct(\$message = null,\$code = null) {
			if (\$message != null) \$m = \$message;
			else				  \$m = \$this->message;
			
			if (\$code != null) 	 \$c = \$code;
			else				 \$c = \$this->code;
			
			parent::__construct(\$m,\$c);
		}
	}
?>";


	$O->file->set_file('Erreur/Ojoo/Exception/' . $exception_file_titre . '.php','a+');
	$O->file->write($exception_file);
	$O->file->close();
	
	
	// On construit maintenant le fichier de configuration : 
	// On va le faire via le SpicYAMLDump, on sera s�r de notre coup :D
	$config = array();
	$config["titre"] = $_POST['exception_titre'];
	$config["code"] = $_POST['exception_config_code'];
	if (isset ($_POST['exception_log']))
		$config["log"] = true;
	else
		$config["log"] = false;
	$config["report"] = false;
	$config["type"] = "Exception";
	$config["contenu"] = $_POST['exception_contenu'];
	$config["template"] = $_POST['exception_template'];
	$config["templateDesign"] = $_POST['exception_design_template'];
	$config["replace_tags"] = array(
		"titre" => $_POST['replace_tag_titre'],
		"code" => $_POST['replace_tag_code'],
		"ligne" =>  $_POST['replace_tag_ligne'],
		"fichier" =>  $_POST['replace_tag_fichier'],
		"contenu" =>  $_POST['replace_tag_contenu']
	);
	
	$content = yaml_dump($config);
	$O->file->set_file('Erreur/Ojoo/#O' . $_POST['exception_config_code'] . '.yml','a+');
	$O->file->write(utf8_decode($content));
	$O->file->close();
?>
<h3>{{img:Ojoo/Design/icones/icone_titre.jpg}} Code de l'exception g�n�r�e : </h3><br /> 
<div style="width: 800px; height: auto; background-color: white; border: 1px solid black;">
	<?php echo highlight_string($exception_file); ?>
</div><br /> <br />
<h3>{{img:Ojoo/Design/icones/icone_titre.jpg}} Code du fichier configuration g�n�r� : </h3> <br />
<div style="width: 800px; height: auto; background-color: white; border: 1px solid black;">
	<?php echo var_dump($config); ?>
</div>