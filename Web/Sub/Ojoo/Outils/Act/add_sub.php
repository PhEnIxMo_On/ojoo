<h3>{{img:Ojoo/Design/icones/icone_titre.jpg}} Sub </h3>
<br />
<form>
	<fieldset>
		<legend> Ajouter un sub </legend>
		<label> Nom : </label><input type="text" name="sub_name" /><span> Le nom du sub </span><br />
		<label> Description : </label><input type="text" name="sub_desc" /><span> Courte description du sub </span><br />
		<label> Droits : </label><input type="text" name="sub_droits" /><span> Droits du sub sous la forme 0.0.0.1.1/1/2/3  </span><br />
		<label> Design : </label><input type="text" name="sub_design" /><span> Le design utilis� par le sub  </span><br />
		<label> Ic�ne : </label><input type="text" name="sub_icone" /><span> L'ic�ne du sub </span><br />
	</fieldset>
	<br />
	<fieldset> 
		<legend> Maintenance : </legend>
		<label> Design : </label><input type="text" name="sub_maintenance_design" /><span> Le design de la maintenance </span><br />
		<label> Status : </label>{{checkbox name="sub_maintenance_status" content="Activer"}} <span> Status de la maintenance </span><br />
		<label> Cl� d'acc�s : </label><input type="text" name="sub_maintenance_cle" /><span> Cl� d'acc�s au sub en maintenance  </span><br />
		<label> Raison : </label><input type="text" name="sub_maintenance_raison" /><span> Raison de la maintenance  </span><br />
		<label> D�but : </label><input type="text" name="sub_maintenance_start" value="<?php echo date_fran(time()); ?>"/><span> D�but de la maintenance </span><br />
		<label> Fin : </label><input type="text" name="sub_maintenance_end" /><span> Fin de la maintenance </span><br />
		<input type="button" class="submit_button" onClick="formAddSub()"/>
	</fieldset>
</form>