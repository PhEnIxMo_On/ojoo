	function getWindowWidth() {
    var w = 0;
    if (typeof(window.innerWidth) == 'number') { // Netscape
        w = window.innerWidth;
    } else if (document.documentElement && document.documentElement.clientWidth) {
        w = document.documentElement.clientWidth;
    } else if (document.body && document.body.offsetWidth) { //client
        w = document.body.offsetWidth;
    }
    return w;
}
function callbackCompUser(params,msg) {
    if (params['avancement'] <= 100) {
        $("#dialog" + params['id']).dialog('close');
        $("#bar" + params['id']).css('width', params['avancement'] + '%');
        var texteBar = params['langage'] + ' : ' + params['avancement'] + '%';
        if (typeof (params['maitre']) != 'undefined')
            texteBar += "&nbsp;&nbsp;" + '<span style="color: red;"> Accepte les padawans </span>';
        $("#bar" + params['id']).html(texteBar);
    } else alert("Vous devez entrer un avancement comprit entre 0% et 100%");
} 
function deleteComp(params,msg) {
    $("#table" + params['id']).fadeOut();
}
function addComp(params,lastId) {
    if (params['avancement'] <= 100) {
        var maitre = "";
        var checked = "";

        $("dialogAdd").dialog('close');

        if (typeof (params['maitre']) != 'undefined') {
            maitre = '<span style="color: red;"> Accepte les padawan </span>';
            checked = 'checked="checked"';
        }
        var htmlComp = '<table style="width:  100%;" id="table' + lastId + '"><tr><td style="width:  50px;">&nbsp;<a style="cursor:  pointer;" onclick="if (confirm(\'Vous allez supprimer cette compétence sans moyen de retour, en êtes vous sûr ? \')) { ojooForm.loadPage(\'sub=cp&mod=user&act=deleteComp&id=' + lastId + '\',\'deleteComp\',{id: \'' + lastId + '\'}) };"><img src="Web/Images/icons/16x16/delete.png" alt="Editer" /></a>&nbsp;<a style="cursor:  pointer;" onclick="$(\'#dialog' + lastId + '\').dialog({width: \'60%\'});"><img src="Web/Images/icons/16x16/pencil.png" alt="Editer" /></a></td><td><div class="progress progress-success progress-striped"><div class="bar" id="bar' + lastId + '" style="width: ' + params['avancement'] + '%; color: black;"> ' + params['langage'] + ' : ' + params['avancement'] + '% &nbsp;&nbsp;&nbsp;&nbsp;' + maitre + '</div>&nbsp;</div></td></tr></table>';
        $("#competences").prepend(htmlComp);

        var htmlEdit = '<div id="dialog' + lastId + '" title="Editer" style="display:  none;"><form id="' + lastId + '" name="test"><div class="well"><div class="progress progress-success progress-striped"><div class="bar" style="width: ' + params['avancement'] + '%; color: black;"> ' + params['langage'] + ' : ' + params['avancement'] + '% &nbsp;&nbsp;&nbsp;&nbsp;' + maitre + '</div>&nbsp;</div></div><div class="well"><table><tr><td><img src="Web/Images/icons/16x16/file_extension_log.png"  style="vertical-align:  middle;" alt="Image langage" />&nbsp;<input type="text" name="langage" id="langage' + lastId + '" value="' + params['langage'] + '" /></td><td><span class="little color-help form-help"> Indiquer le nom du langage maîtrisé. </span></td></tr><tr><td><img src="Web/Images/icons/16x16/user_c3po.png" alt="Image langage" /> &nbsp;Je suis prêt ! <input type="checkbox" name="maitre" id="maitre' + lastId + '" ' + checked + ' /></td><td><span class="little color-help form-help"> Êtes vous prêt à coacher quelqu\'un ? (Cf. info-bulle plus haut) </span></td></tr><tr><td><img src="Web/Images/icons/16x16/barchart.png" alt="Image langage"  style="vertical-align:  middle;"/><input type="text" name="avancement" id="avancement' + lastId + '" value="' + params['avancement'] + '" /></td><td><span class="little color-help form-help"> L\'avancement dans le langage en question. </span></td></tr></table></div><hr /><div class="well"><input type="button" class="btn btn-primary" onclick="ojooForm.loadWithPostData(\'sub=cp&mod=user&act=majComp&idComp=' + lastId + '\',\'' + lastId + '\',\'callbackCompUser\',{langage: $(\'#langage' + lastId + '\').val(), maitre: $(\'#maitre' + lastId + '\').attr(\'checked\'),avancement: $(\'#avancement' + lastId + '\').val(),id: \'' + lastId + '\' });" value="Mettre à jour !"></div></form></div>';
        $("#editDialog").prepend(htmlEdit);
    }
}
function callBackUserTeam(msg) {
	alert(msg);
}

function notification() {
	
}

notification.hide = function () {
	$("#notificationText").hide();
	$("#notificationBlock").animate({ width: '0', 'margin-left': '548px' });
}

notification.show = function () {
	$("#notificationText").show();
	if ($("#notificationBlock").css('display') == 'none')
		$("#notificationBlock").fadeIn();
	else
		$("#notificationBlock").animate({ width: '548px', 'margin-left': '144px' });
}

notification.feed = function (title, content) {
	$("#notificationTitle").html(title);
	$("#notificationContent").html(content);
}

function notificationRefresh() {
 	$.ajax({
 		type: "POST",
 		url: "notification-refresh",
 		success: function (msg) {
 			if (msg != 'null') {
  				$("#notificationBlock").html(msg);
 				notification.show();				
 			}

 		}
 	});
}
function suspendreProjet(id) {
	if (confirm("Êtes vous sûr de vouloir susprendre votre projet ? Vous pourrez bien entendu le réactiver par la suite dans l'édition de votre projet.")) {
		$.ajax({
			type: "POST",
			url: "projet-suspendre-" + id,
			success: function (msg) {
				alert("Votre projet a bien été suspendu. Les équipes rattachées à ce projet ont été notifiées.");
			}
		});		
	}
}