function formGlobal() {
	var test = true;
	
	var sitename = $("#nom_site").val();
	var fondateur = $("#fondateur").val();
	var redirection404 = $("#404").val();

	
	if (sitename == '') {
		$("#nom_site").attr('class','error_form');
		$("#nom_site").next().html('<font color="red">Vous devez entrer un nom de site</font>');
		test = false;
	} else { $("#nom_site").attr('class','valid_form'); }
	if (fondateur == '') {
		$("#fondateur").attr('class','error_form');
		test = false;
	} else { $("#fondateur").attr('class','valid_form');}
	if (redirection404 == '') {
		$("#404").attr('class','error_form');
		test = false; 
	} else { $("#404").attr('class','valid_form');}
	
		ojooPanel.load('sub=ojoo&mod=Global&act=resume_traiter','true',true);
	if (test)
		ojooPanel.ok("Les informations ont bien �t� �dit�es");
	else
		ojooPanel.error("Erreur lors de la validation du formulaire");
	
	return test;
}
function formSubDroit() {
	ojooPanel.ok("Les droits ont bien �t� �dit�s.");

	ojooPanel.load('sub=Ojoo&mod=Global&act=sub_droits_traiter','true',false);
}
function formOutilsAddException() {
	var exception_name = $("#exception_name").val();
	var exception_code = $("#exception_code").val();
	var exception_config_code = $("#exception_config_code").val();
	var test = true;
	
	if (exception_name == '') {
		test = false;
		$("#exception_name").attr('class','error_form');
		$("#exception_name").next().html('<font color="red"> Vous devez entrer un titre d\'exception</font>');
	}
	
	if (exception_code == '') {
		test = false;
		$("#exception_code").attr('class','error_form');
		$("#exception_code").next().html('<font color="red"> Vous devez entrer un code d\'exception</font>');
	}
	
	if (exception_config_code == '') {
		test = false;
		$("#exception_config_code").attr('class','error_form');
		$("#exception_config_code").next().html('<font color="red"> Vous devez entrer un code d\'exception</font>');
	}
	
	if (test) {
		if (exception_config_code == exception_code) {
			ojooPanel.load('sub=Ojoo&mod=Outils&act=add_exception_traiter','false',true);
			ojooPanel.ok("Les fichiers ont correctement �t� g�n�r�");
		}else {
			$("#exception_name").attr('class','valid_form');
		
			$("#exception_config_code").attr('class','error_form');
			$("#exception_config_code").next().html('<font color="red"> Le code entr� doit �tre le m�me que celui de l\'exception.</font>');
		}
	}
}
function formModeleAdd() {
	ojooPanel.load('sub=Ojoo&mod=Outils&act=add_table_modele_4','false',true);
	ojooPanel.ok("Les fichiers ont correctement �t� g�n�r�.");
}
function formAddEvent() {
	ojooPanel.load('sub=Ojoo&mod=Outils&act=add_event_traiter','true',true);
	ojooPanel.ok("L'event a correctement �t� ajout�.");
}
function formEventAddTemplate() {
	ojooPanel.load('sub=Ojoo&mod=Outils&act=add_event_template_traiter','false',true);
	ojooPanel.ok("L'event a correctement �t� ajout�.");
}
function formAddSub() {
	ojooPanel.load('sub=Ojoo&mod=Outils&act=add_sub_traiter','false',true);
	ojooPanel.ok("Le sub a correctement �t� g�n�r�.");
}
function formAddMod() {
	ojooPanel.load('sub=Ojoo&mod=Outils&act=add_mod_traiter','false',true);
	ojooPanel.ok("Le sub a correctement �t� g�n�r�.");
}
function formCreateView() {
	ojooPanel.load('sub=Ojoo&mod=Outils&act=create_simple_view','false',true);
}

function ojooForm() {
    
}
ojooForm.console = function (url) {
    $.ajax({
        type: "POST",
        url: url,
        data: "commandLine=" + $("#OCLInput").val(),
        success: function (msg) {
                var html2 = $("#contentConsole").html() + msg;
                $("#contentConsole").html(html2);
        }
    });
}
ojooForm.loadWithPostData = function (url, form, callback, paramsCallback, object) {
	var params = "";
	var input = $("#" + form).find("input");
	input.each(function (index, domEle) {
		if (typeof ($(this).attr('name')) == 'undefined')
			name = $(this).attr('id');
		else
			name = $(this).attr('name');
		if ($(this).attr('type') == 'checkbox') {
			if ($(this).is(':checked')) {
				params += "&" + name + "=true";
			} else params += "&" + name + "=false";
		} else {
			if ($(this).attr('type') != 'button' && $(this).attr('type') != 'radio')
				params += "&" + name + "=" + encodeURIComponent($(this).val()) + "";
			if ($(this).attr('type') == 'radio') {
				if (typeof ($(this).attr('name')) == 'undefined') {
					params += "&" + name + "=" + encodeURIComponent($('input[type=radio][id=' + $(this).attr('id') + ']:checked').attr('value')) + "";
				} else {
					params += "&" + name + "=" + encodeURIComponent($('input[type=radio][name=' + $(this).attr('name') + ']:checked').attr('value')) + "";
				}
			}
		}
	});

	var textarea = $("#" + form).find("textarea");
	textarea.each(function (index, domEle) {
		if (typeof ($(this).attr('name')) == 'undefined')
			name = $(this).attr('id');
		else
			name = $(this).attr('name');
		params += "&" + name + "=" + encodeURIComponent($(this).val()) + "";
	});
	var select = $("#" + form).find("select");
	select.each(function (index, domEle) {
		if (typeof ($(this).attr('name')) == 'undefined')
			name = $(this).attr('id');
		else
			name = $(this).attr('name');
		params += "&" + name + "=" + encodeURIComponent($(this).val()) + "";
	});

	$.ajax({
		type: "POST",
		url: url,
		data: params,
		success: function (msg) {
			if (typeof (callback) != 'undefined') {
            	if (typeof(object) != 'undefined')
                	window[object][callback](msg);
                else if (typeof (paramsCallback) != 'undefined')
                    window[callback](paramsCallback, msg);
                else
                    window[callback](msg);
			}
		}
	});
}

ojooForm.loadPage = function (url, callback, paramsCallback,object) {
    $.ajax({
        type: "POST",
        url: url,
        success: function (msg) {
            if (typeof (callback) != 'undefined') {
            	if (typeof(object) != 'undefined')
                	window[object][callback](msg);
                else if (typeof (paramsCallback) != 'undefined')
                    window[callback](paramsCallback, msg);
                else
                    window[callback](msg);
            }
            
        }
    });
}

function ongletOxygen() {
    
}
ongletOxygen.change = function (onglet) {
    var id = $(onglet).attr('title');
    var idHide;
    var onglets = $("#ongletContent").find("div");
    var classOnglet;
    onglets.each(function(index,domEle) {
        if ($(this).attr('class') == 'onglet active') {
            // Onglet actif donc on en change :            
            $(this).attr('class','onglet');
            $(onglet).attr('class','onglet active');
            idHide = $(this).attr('title');
            $('#' + idHide).hide();
            $('#' + id).show();
        }
    });
}
$(function (){
   $('a').tooltip();
   $('.ckeditor').CKEDITOR();
});