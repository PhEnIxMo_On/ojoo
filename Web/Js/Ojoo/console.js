

/*
 *  OBJET JAVASCRIPT CONSOLE 
 *  Créé par Hellbender
 *  Le : 7 Octobre 2013
 */

function OjooConsole() {
	var test = true;
}

OjooConsole.onglet = function(page,data,search) {
	// (String) page : Page PHP à charger 
	// (String) data : données à envoyer à la page.
	//alert('Page : ' + page + ' & Data : ' + data + ' & Search : ' + search);
	if (typeof(page) == 'undefined' && typeof(search) != 'undefined' && typeof(data) != 'undefined') {
		if (data == 'google') 
			var win = window.open('https://www.google.fr/#q=' + search,'_blank');
		else
			var win = window.open('http://fr2.php.net/manual-lookup.php?pattern=' + search + '&scope=quickref');
		win.focus();
	} 
	if (typeof(page) != 'undefined') {
		ojooForm.loadPage(page,'changeOnglet','undefined','OjooConsole');
	}
}

OjooConsole.addMessage = function(language,type,message,vars) {
	
}

OjooConsole.changeOnglet = function(msg) {
	$("#newConsoleContent").html(msg);
}
/*
 * FIN DE LA CONSOLE
 */







/*
 * Handlers console :
 */
$( "#consoleGoogle" ).submit(function( event ) {
	OjooConsole.onglet('undefined','google',$("#googleInput").val());
	event.preventDefault();
});

$( "#consolePHP" ).submit(function( event ) {
	OjooConsole.onglet('undefined','php',$("#PHPInput").val());
	event.preventDefault();
});

/*
 * Fin handlers console
 */