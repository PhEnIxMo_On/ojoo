function ojooPanel() {
}
ojooPanel.checkbox = function(div) {
	if ($(div).attr('title') == 'check') {
		$(div).css('background',"url('Web/Images/Ojoo/Design/wait_checkbox.jpg')");
		$(div).attr('title','no');
		$(div).next().remove();
	} else {
		$(div).css('background',"url('Web/Images/Ojoo/Design/ok_checkbox.jpg')");
		$(div).attr('title','check');
		$(div).after('<input type="checkbox" style="display: none;" name="' + $(div).attr('name') + '" />');
	}
}

ojooPanel.wait = function() {
	$("#footer").css('background',"url('Web/Images/Ojoo/Design/footer_wait.jpg')");
	$("#footer span.error").html('<img src="Web/Images/Ojoo/Design/load.gif" style="margin-top: 5px;"/>');
	
	setTimeout('ojooPanel.wait2()',2000);
}
ojooPanel.wait2 = function() {
	$("#footer").css('background',"url('Web/Images/Ojoo/Design/footer_success.jpg')");
	$("#footer span.error").html("La page a bien �t� charg�e.");
}

ojooPanel.error = function(message) {
	$("#footer").css('background',"url('Web/Images/Ojoo/Design/footer.jpg')");
	$("#footer span.error").html(message);
	$("#footer").animate({'margin-left': '50px'},500);
	$("#footer").animate({'margin-left': '0px'},500);
}
ojooPanel.ok = function(message) {
	$("#footer").css('background',"url('Web/Images/Ojoo/Design/footer_success.jpg')");
	$("#footer span.error").html(message);
	$("#footer").animate({'margin-left': '50px'},500);
	$("#footer").animate({'margin-left': '0px'},500);
}

ojooPanel.changeDiv = function() {
	if ($("#content2").css('display') == 'none') {
		$("#content2").css('display','block')
		$("#content1").slideUp('normal',function() {
			$("#content1").html('');
		});
	} else {
		$("#content1").slideDown('normal',function() {
			$("#content2").css('display','none');
			$("#content2").html('');
		});
	}
}
ojooPanel.load = function(params,reload,form) {
	ojooPanel.wait();
	if (typeof(form) != 'undefined') {
		var input = $("form").find("input");
		input.each(function(index,domEle) {
			if (typeof($(this).attr('name')) == 'undefined')
				name = $(this).attr('id');
			else
				name = $(this).attr('name');
			if ($(this).attr('type') == 'checkbox') {
				if ($(this).attr('type') != 'button')
					params += "&" + name + "=true"
			} else {
				if ($(this).attr('type') != 'button' && $(this).attr('type') != 'radio')
					params += "&" + name + "=" + encodeURIComponent($(this).val()) + "";
				if ($(this).attr('type') == 'radio') {
					if (typeof($(this).attr('name')) == 'undefined') {
						params += "&" + name + "=" + encodeURIComponent($('input[type=radio][id=' + $(this).attr('id') + ']:checked').attr('value')) + ""; 
					} else {
						params += "&" + name + "=" + encodeURIComponent($('input[type=radio][name=' + $(this).attr('name') + ']:checked').attr('value')) + ""; 
					}
				}
			}
		});
		var textarea = $("form").find("textarea");
		textarea.each(function(index,domEle) {
			if (typeof($(this).attr('name')) == 'undefined')
				name = $(this).attr('id');
			else
				name = $(this).attr('name');
			params += "&" + name + "=" + encodeURIComponent($(this).val()) + "";
		});
		var select = $("form").find("select");
		select.each(function(index,domEle) {
			if (typeof($(this).attr('name')) == 'undefined')
				name = $(this).attr('id');
			else
				name = $(this).attr('name');
			params += "&" + name + "=" + encodeURIComponent($(this).val()) + "";
		});
	}
	$.ajax({
	   type: "POST",
	   url: "index.php",
	   data: params,
	   success: function(msg){
			var test = false;
			if (typeof(reload) == 'undefined') test = true;
			else if (reload != 'true') test = true;
			if (test) {
				if ($("#content2").css('display') == 'none') {
					$("#content2").html(msg);
				} else {
					$("#content1").html(msg);
				}
				ojooPanel.changeDiv();
				$('#scrollbar1').tinyscrollbar();
			}
	   }
	});
}
ojooPanel.loadMenu = function(tpl) {
	 $.ajax({
	   type: "POST",
	   url: "index.php",
	   data: "tpl=" + tpl,
	   success: function(msg){
		 $("#fond_menu").html(msg);
	   }
	 });
}
ojooPanel.validForm = function() {
	$("#error").attr('class','error_form');
	$("#ok").attr('class','valid_form');
}
ojooPanel.selectChoice = function(inputID,separator) {
	var text;
	var reponse;
	while (reponse != 'fin/') {
		reponse = prompt("Entrez les valeurs possibles, pour finir entrez 'fin'") + separator;
		if (reponse != 'fin/')
			text += reponse;
	}
	$("#" + inputId).val(text);
}
