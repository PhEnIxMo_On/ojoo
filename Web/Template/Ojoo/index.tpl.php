<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<base href="<?php echo $O->config["SitePath"]; ?>" />
	<title><?php echo $O->config["site_name"]; ?> - <?php echo $O->route->getPageTitle(); ?></title>
	<meta charset="utf-8">
	{{Ojoo/animations.css}}
	{{Ojoo/reset.css}}
	{{Ojoo/miseEnPage.css}}
	{{bootstrap.min.js}}
	{{Ojoo/documentation.css}}
	{{bootstrap.min.css}}
	{{jquery.js}}
	{{jquerui.min.js}}
	{{jqueryui.custom.css}}
	{{form.js}}
	{{ckeditor/ckeditor.js}}
</head>
<body>
<div class="navbar">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-responsive-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="#"><?php echo $O->config["site_name"]; ?></a>
      <div class="nav-collapse collapse navbar-responsive-collapse">
        <ul class="nav">
          <li class="active"><a href="#">Accueil</a></li>
          <li><a href="#">Code</a></li>
          <li><a href="#">Tables</a></li>
          <li><a href="#">Configuration</a></li>
          <li><a href="#">Contribuer</a></li>
        </ul>
        <form class="navbar-search pull-left" action="">
          <input type="text" class="search-query span2" placeholder="Rechercher">
        </form>
      </div><!-- /.nav-collapse -->
    </div>
  </div><!-- /navbar-inner -->
</div><!-- /navbar -->
<br />
<span class="row-fluid" style="margin-left: 15px">
	<span class="span1"></span>
	<span class="span2">
	    <ul class="nav nav-tabs nav-stacked">
	    	<li><a href="">Chapitre 1 </a></li>
	    	<li><a href="">Chapitre 1 </a></li>
	   	    <li><a href="">Chapitre 1 </a></li>
    	</ul>
	</span>
	<span class="span8 well">
		{{CONTENT}}
	</span>
</span>
</body>
</html>
<script type="text/javascript">
	$(".ckeditor").CKEDITOR();
</script>

