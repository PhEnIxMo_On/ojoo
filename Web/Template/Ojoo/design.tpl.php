<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<title><?php echo $O->config["site_name"]; ?></title>
	<meta charset="iso-8859-2">
	{{Ojoo/reset.css}}
	{{Ojoo/style.css}}
	{{Ojoo/design.js}}
	{{jquery.js}}
	{{jquerui.min.js}}
	{{jqueryui.custom.css}}
	{{form.js}}
	{{filtres.js}}
	{{scrollBar.js}}
</head>
<body>
	<div id="ojoo_panel">
		<div id="fond_header">
			<ul>
				<li class="first"><div id="bt_global" onClick="ojooPanel.loadMenu('Ojoo/Menu/Global.tpl.php');"></div></li>
				<li><div id="bt_rapport" onClick="ojooPanel.loadMenu('Ojoo/Menu/Rapports.tpl.php');"></div></li>
				<li><div id="bt_styles" onClick="ojooPanel.loadMenu('Ojoo/Menu/Styles.tpl.php');"></div></li>
				<li><div id="bt_outil" onClick="ojooPanel.loadMenu('Ojoo/Menu/Outil.tpl.php');"></div></li>
				<li><div id="bt_save" onClick="ojooPanel.loadMenu('Ojoo/Menu/Sauvegarde.tpl.php');"></div></li>
				<li><div id="bt_secu" onClick="ojooPanel.loadMenu('Ojoo/Menu/Securite.tpl.php');"></div></li>
			</ul>
		</div>
		<div class="clear">
		</div>
		<div id="fond_menu">
			<div id="menu1">
				<div id="titre_menu">{{img:Ojoo/Design/icones/global_menu.jpg}} <span class="titre_menu"> Global </span></div>
				<span class="separateur_menu">{{img:Ojoo/Design/separateur_menu.jpg}}</span>
				<ul>
					<li> Configuration </li>
					<li> Documentation </li>
					<li onClick="ojooPanel.load('sub=Ojoo&mod=Global&act=ojoo');"><font color="orange"> Ojoo </font></li>
					<li onClick="ojooPanel.load('sub=Ojoo&mod=Global&act=droits');"> Droits </li>
					<li onClick="ojooPanel.load('sub=Ojoo&mod=Global&act=accueil');"> R�sum� </li>
					<li onClick="ojooPanel.load('sub=Ojoo&mod=Global&act=statistique');"> Statistiques </li>
				</ul>
			</div>
		</div>
		<div id="fond_corps">
			<div id="scrollbar1">
				<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
				<div class="viewport">
					<div class="overview">
						<?php
							$O = $GLOBALS['O'];
							if ($O->request_type == 'ajax') {
						?>
								<div id="content1">				
									<h3>{{img:Ojoo/Design/icones/icone_titre.jpg}} Conteneur n�1 </h3>
									<div id="optionTitre">
								</div>
								<br />
								<div class="bouton">{{img:Ojoo/Design/icones/test_edit.png}} Editer </div></div>
								<div id="content2" style="display: none;">{{CONTENT}}</div>
						<?php
							} else {
								?>
									<div id="content1"> {{CONTENT}} </div>
									<div id="content2" style="display: none;"></div>
								<?php
							}
						?>
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			<span class="error">
				Le rapport a correctement �t� supprim�.
			</span>
		</div>
	</div>
</body>
</html>