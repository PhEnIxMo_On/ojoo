<div id="newConsole">
	<div id="newConsoleMenu">
		<ul>
			<li>&#094;<span> Paramètres </span></li>
			<li>&#0046;<span> Session </span></li>
			<li>&#242;<span><form id="consoleGoogle"><input id="googleInput" type="text" size="3" /></form></span></li>
			<li onClick="OjooConsole.onglet('console/filter');">&#226;<span> Filtrer </span></li>
			<li>&#0095;<span><form id="consolePHP"><input id="PHPInput" type="text" size="3" /></form></span></li>
			<li onClick="location.reload();">&#0101;<span> Console </span></li>
			<li>&#0105;<span> Ojoo </span></li>
		</ul>
		<ul class="left">
			<li class="leftIcons">&#0092;<span> Erreur(s)</span></li>
		</ul>
	</div>
	<div id="newConsoleContent">
		{{CONTENT_CONSOLE}}
	</div>
</div>
	{{Ojoo/console.css}}	
	{{Ojoo/console.js}}