<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<title> Ojoo beta - Index</title>
	<meta charset="iso-8859-2">
	{{Ojoo/design.js}}
	{{Ojoo/style_erreur.css}}
	{{jquery.js}}
	{{jquerui.min.js}}
	{{jqueryui.custom.css}}
</head>
<body>
	<div id="fond_erreur">
		<table>
			<tr>
				<td width="45"></td><td height="49"></td>
			</tr>
			<tr>
				<td></td><td> Ojoo a lev� une exception : {{TITRE}}</td>
			</tr>
		</table>
		<table width="100%">
			<tr>
				<td width="20"></td><td height="10"  colspan="3"></td>
			</tr>
			<tr>
				<td></td>
				<td  colspan="3">
					T'as tout cass� ! <br /> <br />
					<span class="erreur"> Erreur : </span> &nbsp;{{CONTENU}} <br /> <br />
					Elle s'est produite � la ligne {{LIGNE}} du fichier {{FICHIER}}, le code de l'erreur est : {{CODE}} <br />
					<?php
						$O = $GLOBALS['O'];
						if ($O->erreur->report) $rapport = "Un rapport a �t� automatiquement cr��";
						else					 $rapport = "Aucun rapport n'a �t� cr��";
						if ($O->erreur->log) $log = " et celle-ci a �t� logu�e.";
						else				 $log = " et celle-ci n'a pas �t� logu�e.";
						
						echo $rapport . $log;
					?>
					<br /><br /><br />
				</td>
			</tr>
			<tr>
				<td width="20"></td><td colspan="3"></td>
			</tr>
		</table>
	</div>
</body>
</html>