<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mardi 2 Avril 2013
	****************************************************/
	class link_recup extends mvc_bdd  {
		public $guid;
		public $type;
		public $subtype;
		public $numparam;
		public $txtparam;
		
		public $BDD = 'char';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
		
		public function updateFinishRecup() {
			$O = getOjoo();
			$recups = $O->bdd->char->query("SELECT guid FROM link_recup WHERE type=8 AND (numparam=1 OR numparam=2)")->fetchAll();
			foreach ($recups as $recup) {
				$enteteId = $O->modele->web_entete_recup->select_charGuid($recup['guid'])->fetch();
				if ($enteteId != false)
					$O->bdd->manager->query("UPDATE web_entete_recup SET state=5 WHERE id=" . $enteteId['id'] . "");
			}
		}

		public function set_guid($set_value,$id) {
			$this->edit(array('guid' => $set_value),array('id' => $id ));
		}

		public function set_type($set_value,$id) {
			$this->edit(array('type' => $set_value),array('id' => $id ));
		}

		public function set_subtype($set_value,$id) {
			$this->edit(array('subtype' => $set_value),array('id' => $id ));
		}

		public function set_numparam($set_value,$id) {
			$this->edit(array('numparam' => $set_value),array('id' => $id ));
		}

		public function set_txtparam($set_value,$id) {
			$this->edit(array('txtparam' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('link_recup');
		}
		

		public function select_guid($name) {
			return $this->select('link_recup','guid="' . $name . '"');
		}

		public function select_type($name) {
			return $this->select('link_recup','type="' . $name . '"');
		}

		public function select_subtype($name) {
			return $this->select('link_recup','subtype="' . $name . '"');
		}

		public function select_numparam($name) {
			return $this->select('link_recup','numparam="' . $name . '"');
		}

		public function select_txtparam($name) {
			return $this->select('link_recup','txtparam="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>