<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Samedi 30 Mars 2013
	****************************************************/
	class web_history_recup extends mvc_bdd  {
		public $id;
		public $enteteId;
		public $com;
		public $accountId;
		public $accountMj;
		public $recupState;
		public $date;
		
		public $BDD = 'manager';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_enteteId($set_value,$id) {
			$this->edit(array('enteteId' => $set_value),array('id' => $id ));
		}

		public function set_com($set_value,$id) {
			$this->edit(array('com' => $set_value),array('id' => $id ));
		}

		public function set_accountId($set_value,$id) {
			$this->edit(array('accountId' => $set_value),array('id' => $id ));
		}

		public function set_accountMj($set_value,$id) {
			$this->edit(array('accountMj' => $set_value),array('id' => $id ));
		}

		public function set_recupState($set_value,$id) {
			$this->edit(array('recupState' => $set_value),array('id' => $id ));
		}

		public function set_date($set_value,$id) {
			$this->edit(array('date' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('web_history_recup');
		}
		

		public function select_id($name) {
			return $this->select('web_history_recup','id="' . $name . '"');
		}

		public function select_enteteId($name) {
			return $this->select('web_history_recup','enteteId="' . $name . '"');
		}

		public function select_com($name) {
			return $this->select('web_history_recup','com="' . $name . '"');
		}

		public function select_accountId($name) {
			return $this->select('web_history_recup','accountId="' . $name . '"');
		}

		public function select_accountMj($name) {
			return $this->select('web_history_recup','accountMj="' . $name . '"');
		}

		public function select_recupState($name) {
			return $this->select('web_history_recup','recupState="' . $name . '"');
		}

		public function select_date($name) {
			return $this->select('web_history_recup','date="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>