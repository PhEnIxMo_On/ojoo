<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Jeudi 28 Mars 2013
	****************************************************/
	class web_content_recup extends mvc_bdd  {
		public $id;
		public $enteteId;
		public $urlScreen;
		public $type;
		public $state;
		public $com;
		public $optionnalParam1;
		public $optionnalParam2;
		public $lastChange;
		public $lastChangeIp;
		public $lastChangeAccount;
		
		public $BDD = 'manager';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
                
				
                public function selectScreen($type,$enteteId) {
                    $O = getOjoo();
                    $screen = $O->bdd->manager->query("SELECT * FROM web_content_recup WHERE type=" . intval($type) . " AND enteteId='" . intval($enteteId) . "'")->fetch();
                    if ($screen != null) return $screen;
                    else             return false;
                }
                
                public function selectMetier($enteteId) {
                    $O = getOjoo();
                    $screen = $O->bdd->manager->query("SELECT * FROM web_content_recup WHERE (type=28 OR type=29) AND enteteId='" . intval($enteteId) . "'")->fetchAll();
                    if ($screen != null) return $screen;
                    else             return false;
                }
                
                public function selectReput($enteteId) {
                    $O = getOjoo();
                    $screen = $O->bdd->manager->query("SELECT * FROM web_content_recup WHERE type=27 AND enteteId='" . intval($enteteId) . "'")->fetchAll();
                    if ($screen != null) return $screen;
                    else             return false;
                }                  
                
                public function getNameByType($type) {
                    switch ($type) {
                        case 1:
                            $name =  "/played";
                            $icon = "picture.png";
                            break;
                        
                        case 2:
                            $name =  "page de choix de personnage";
                            $icon = "picture.png";
                            break;
                        
                        case 3:
                            $name =  "/qui";
                            $icon = "picture.png";
                            break;
                        
                        case 4:
                            $name =  "recup Oxygen + date";
                            $icon = "picture.png";
                            break;
                        
                        case 5:
                            $name =  "calendrier en jeu";
                            $icon = "picture.png";
                            break;
                        
                        case 6:
                            $name =  "screen attestant du nombre de pièces d'or";
                            $icon = "picture.png";
                            break;
                        
                        case 7:
                            $name =  "tête";
                            $icon = "INV_empty_head.png";
                            break;
                        
                        case 8:
                            $name =  "coup";
                            $icon = "INV_empty_neck.png";
                            break;
                        
                        case 9:
                            $name =  "épaules";
                            $icon = "INV_empty_shoulder.png";
                            break;
                        
                        case 10:
                            $name =  "dos";
                            $icon = "INV_empty_chest_back.png";
                            break;
                        
                        case 11:
                            $name =  "torse";
                            $icon = "INV_empty_chest_back.png";
                            break;
                        
                        case 12:
                            $name =  "chemise";
                            $icon = "INV_empty_shirt.png";
                            break;
                        
                        case 13:
                            $name =  "tabard";
                            $icon = "INV_empty_tabard.png";
                            break;
                        
                        case 14:
                            $name =  "poignet";
                            $icon = "INV_empty_wrist.png";
                            break;
                        
                        case 15:
                            $name =  "arme principale";
                            $icon = "INV_empty_main_hand.png";
                            break;
                        
                        case 16:
                            $name =  "arme secondaire";
                            $icon = "INV_empty_off_hand.png";
                            break;
                        
                        case 17:
                            $name =  "arme à distance";
                            $icon = "INV_empty_ranged.png";
                            break;
                        
                        case 18:
                            $name =  "munitions";
                            $icon = "INV_empty_ranged.png";
                            break;
                        
                        case 19:
                            $name =  "gants";
                            $icon = "INV_empty_gloves.png";
                            break;
                        
                        case 20:
                            $name =  "ceinture";
                            $icon = "INV_empty_waist.png";
                            break;  
                        
                        case 21:
                            $name =  "jambières";
                            $icon = "INV_empty_legs.png";
                            break;
                        
                        case 22:
                            $name =  "bottes";
                            $icon = "INV_empty_feet.png";
                            break;
                        
                        case 23:
                            $name =  "anneau 1";
                            $icon = "INV_empty_finger.png";
                            break;
                        
                        case 24:
                            $name =  "anneau 2";
                            $icon = "INV_empty_finger.png";
                            break;
                        
                        case 25:
                            $name =  "bijou 1";
                            $icon = "INV_empty_trinket.png";
                            break;
                        
                        case 26:
                            $name =  "bijou 2";
                            $icon = "INV_empty_trinket.png";
                            break;
                        
                        case 27:
                            $name =  "réputation";
                            $icon = "picture.png";
                            break;
                        
                        case 28:
                            $name =  "métier";
                            $icon = "picture.png";
                            break;
                        
                        case 29:
                            $name =  "compétence d'arme";
                            $icon = "picture.png";
                            break;
                        
                        case 30:
                            $name =  "screen des sacs + banque";
                            $icon = "picture.png";
                            break;
                        
                        case 31:
                            $name =  "screen double spé";
                            $icon = "picture.png";
                            break; 
                        
                        case 32:
                            $name =  "scren vol par temps froid";
                            $icon = "picture.png";
                            break;
                        
                        case 33:
                            $name =  "screen compétence de monte";
                            $icon = "picture.png";
                            break;
                        
                        case 34:
                            $name =  "montant PO";
                            $icon = null;
                            break;
                        
                        case 35:
                            $name =  "ilvl max de stuff";
                            $icon = null;
                            break;

						case 36:
							$name = "Lien armurerie";
							$icon = 'picture.png';
							break;
                    }
                    
                    return array('name' => $name, 'icon' => $icon);
                }

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_enteteId($set_value,$id) {
			$this->edit(array('enteteId' => $set_value),array('id' => $id ));
		}

		public function set_urlScreen($set_value,$id) {
			$this->edit(array('urlScreen' => $set_value),array('id' => $id ));
		}

		public function set_type($set_value,$id) {
			$this->edit(array('type' => $set_value),array('id' => $id ));
		}

		public function set_state($set_value,$id) {
			$this->edit(array('state' => $set_value),array('id' => $id ));
		}

		public function set_com($set_value,$id) {
			$this->edit(array('com' => $set_value),array('id' => $id ));
		}

		public function set_optionnalParam1($set_value,$id) {
			$this->edit(array('optionnalParam1' => $set_value),array('id' => $id ));
		}

		public function set_optionnalParam2($set_value,$id) {
			$this->edit(array('optionnalParam2' => $set_value),array('id' => $id ));
		}

		public function set_lastChange($set_value,$id) {
			$this->edit(array('lastChange' => $set_value),array('id' => $id ));
		}

		public function set_lastChangeIp($set_value,$id) {
			$this->edit(array('lastChangeIp' => $set_value),array('id' => $id ));
		}

		public function set_lastChangeAccount($set_value,$id) {
			$this->edit(array('lastChangeAccount' => $set_value),array('id' => $id ));
		}
                
                public function set_where($val) {
                    $this->where = $val;
                }

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('web_content_recup');
		}
		

		public function select_id($name) {
			return $this->select('web_content_recup','id="' . $name . '"');
		}

		public function select_enteteId($name) {
			return $this->select('web_content_recup','enteteId="' . $name . '"');
		}

		public function select_urlScreen($name) {
			return $this->select('web_content_recup','urlScreen="' . $name . '"');
		}

		public function select_type($name) {
			return $this->select('web_content_recup','type="' . $name . '"');
		}

		public function select_state($name) {
			return $this->select('web_content_recup','state="' . $name . '"');
		}

		public function select_com($name) {
			return $this->select('web_content_recup','com="' . $name . '"');
		}

		public function select_optionnalParam1($name) {
			return $this->select('web_content_recup','optionnalParam1="' . $name . '"');
		}

		public function select_optionnalParam2($name) {
			return $this->select('web_content_recup','optionnalParam2="' . $name . '"');
		}

		public function select_lastChange($name) {
			return $this->select('web_content_recup','lastChange="' . $name . '"');
		}

		public function select_lastChangeIp($name) {
			return $this->select('web_content_recup','lastChangeIp="' . $name . '"');
		}

		public function select_lastChangeAccount($name) {
			return $this->select('web_content_recup','lastChangeAccount="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>