<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mercredi 27 Mars 2013
	****************************************************/
	class dbc_faction extends mvc_bdd  {
		public $id;
		public $field_1;
		public $field_2;
		public $field_3;
		public $field_4;
		public $field_5;
		public $field_6;
		public $field_7;
		public $field_8;
		public $field_9;
		public $field_10;
		public $field_11;
		public $field_12;
		public $field_13;
		public $field_14;
		public $field_15;
		public $field_16;
		public $field_17;
		public $field_18;
		public $field_19;
		public $field_20;
		public $field_21;
		public $field_22;
		public $field_23;
		public $field_24;
		public $field_25;
		public $field_26;
		public $field_27;
		public $field_28;
		public $field_29;
		public $field_30;
		public $field_31;
		public $field_32;
		public $field_33;
		public $field_34;
		public $field_35;
		public $field_36;
		public $field_37;
		public $field_38;
		public $field_39;
		public $field_40;
		public $field_41;
		public $field_42;
		public $field_43;
		public $field_44;
		public $field_45;
		public $field_46;
		public $field_47;
		public $field_48;
		public $field_49;
		public $field_50;
		public $field_51;
		public $field_52;
		public $field_53;
		public $field_54;
		public $field_55;
		public $field_56;
		
		public $BDD = '';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
                public function getFactions() {
                    $O = getOjoo();
                    return $O->bdd->mmfpm->query("SELECT * FROM dbc_faction");
                }
                
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_field_1($set_value,$id) {
			$this->edit(array('field_1' => $set_value),array('id' => $id ));
		}

		public function set_field_2($set_value,$id) {
			$this->edit(array('field_2' => $set_value),array('id' => $id ));
		}

		public function set_field_3($set_value,$id) {
			$this->edit(array('field_3' => $set_value),array('id' => $id ));
		}

		public function set_field_4($set_value,$id) {
			$this->edit(array('field_4' => $set_value),array('id' => $id ));
		}

		public function set_field_5($set_value,$id) {
			$this->edit(array('field_5' => $set_value),array('id' => $id ));
		}

		public function set_field_6($set_value,$id) {
			$this->edit(array('field_6' => $set_value),array('id' => $id ));
		}

		public function set_field_7($set_value,$id) {
			$this->edit(array('field_7' => $set_value),array('id' => $id ));
		}

		public function set_field_8($set_value,$id) {
			$this->edit(array('field_8' => $set_value),array('id' => $id ));
		}

		public function set_field_9($set_value,$id) {
			$this->edit(array('field_9' => $set_value),array('id' => $id ));
		}

		public function set_field_10($set_value,$id) {
			$this->edit(array('field_10' => $set_value),array('id' => $id ));
		}

		public function set_field_11($set_value,$id) {
			$this->edit(array('field_11' => $set_value),array('id' => $id ));
		}

		public function set_field_12($set_value,$id) {
			$this->edit(array('field_12' => $set_value),array('id' => $id ));
		}

		public function set_field_13($set_value,$id) {
			$this->edit(array('field_13' => $set_value),array('id' => $id ));
		}

		public function set_field_14($set_value,$id) {
			$this->edit(array('field_14' => $set_value),array('id' => $id ));
		}

		public function set_field_15($set_value,$id) {
			$this->edit(array('field_15' => $set_value),array('id' => $id ));
		}

		public function set_field_16($set_value,$id) {
			$this->edit(array('field_16' => $set_value),array('id' => $id ));
		}

		public function set_field_17($set_value,$id) {
			$this->edit(array('field_17' => $set_value),array('id' => $id ));
		}

		public function set_field_18($set_value,$id) {
			$this->edit(array('field_18' => $set_value),array('id' => $id ));
		}

		public function set_field_19($set_value,$id) {
			$this->edit(array('field_19' => $set_value),array('id' => $id ));
		}

		public function set_field_20($set_value,$id) {
			$this->edit(array('field_20' => $set_value),array('id' => $id ));
		}

		public function set_field_21($set_value,$id) {
			$this->edit(array('field_21' => $set_value),array('id' => $id ));
		}

		public function set_field_22($set_value,$id) {
			$this->edit(array('field_22' => $set_value),array('id' => $id ));
		}

		public function set_field_23($set_value,$id) {
			$this->edit(array('field_23' => $set_value),array('id' => $id ));
		}

		public function set_field_24($set_value,$id) {
			$this->edit(array('field_24' => $set_value),array('id' => $id ));
		}

		public function set_field_25($set_value,$id) {
			$this->edit(array('field_25' => $set_value),array('id' => $id ));
		}

		public function set_field_26($set_value,$id) {
			$this->edit(array('field_26' => $set_value),array('id' => $id ));
		}

		public function set_field_27($set_value,$id) {
			$this->edit(array('field_27' => $set_value),array('id' => $id ));
		}

		public function set_field_28($set_value,$id) {
			$this->edit(array('field_28' => $set_value),array('id' => $id ));
		}

		public function set_field_29($set_value,$id) {
			$this->edit(array('field_29' => $set_value),array('id' => $id ));
		}

		public function set_field_30($set_value,$id) {
			$this->edit(array('field_30' => $set_value),array('id' => $id ));
		}

		public function set_field_31($set_value,$id) {
			$this->edit(array('field_31' => $set_value),array('id' => $id ));
		}

		public function set_field_32($set_value,$id) {
			$this->edit(array('field_32' => $set_value),array('id' => $id ));
		}

		public function set_field_33($set_value,$id) {
			$this->edit(array('field_33' => $set_value),array('id' => $id ));
		}

		public function set_field_34($set_value,$id) {
			$this->edit(array('field_34' => $set_value),array('id' => $id ));
		}

		public function set_field_35($set_value,$id) {
			$this->edit(array('field_35' => $set_value),array('id' => $id ));
		}

		public function set_field_36($set_value,$id) {
			$this->edit(array('field_36' => $set_value),array('id' => $id ));
		}

		public function set_field_37($set_value,$id) {
			$this->edit(array('field_37' => $set_value),array('id' => $id ));
		}

		public function set_field_38($set_value,$id) {
			$this->edit(array('field_38' => $set_value),array('id' => $id ));
		}

		public function set_field_39($set_value,$id) {
			$this->edit(array('field_39' => $set_value),array('id' => $id ));
		}

		public function set_field_40($set_value,$id) {
			$this->edit(array('field_40' => $set_value),array('id' => $id ));
		}

		public function set_field_41($set_value,$id) {
			$this->edit(array('field_41' => $set_value),array('id' => $id ));
		}

		public function set_field_42($set_value,$id) {
			$this->edit(array('field_42' => $set_value),array('id' => $id ));
		}

		public function set_field_43($set_value,$id) {
			$this->edit(array('field_43' => $set_value),array('id' => $id ));
		}

		public function set_field_44($set_value,$id) {
			$this->edit(array('field_44' => $set_value),array('id' => $id ));
		}

		public function set_field_45($set_value,$id) {
			$this->edit(array('field_45' => $set_value),array('id' => $id ));
		}

		public function set_field_46($set_value,$id) {
			$this->edit(array('field_46' => $set_value),array('id' => $id ));
		}

		public function set_field_47($set_value,$id) {
			$this->edit(array('field_47' => $set_value),array('id' => $id ));
		}

		public function set_field_48($set_value,$id) {
			$this->edit(array('field_48' => $set_value),array('id' => $id ));
		}

		public function set_field_49($set_value,$id) {
			$this->edit(array('field_49' => $set_value),array('id' => $id ));
		}

		public function set_field_50($set_value,$id) {
			$this->edit(array('field_50' => $set_value),array('id' => $id ));
		}

		public function set_field_51($set_value,$id) {
			$this->edit(array('field_51' => $set_value),array('id' => $id ));
		}

		public function set_field_52($set_value,$id) {
			$this->edit(array('field_52' => $set_value),array('id' => $id ));
		}

		public function set_field_53($set_value,$id) {
			$this->edit(array('field_53' => $set_value),array('id' => $id ));
		}

		public function set_field_54($set_value,$id) {
			$this->edit(array('field_54' => $set_value),array('id' => $id ));
		}

		public function set_field_55($set_value,$id) {
			$this->edit(array('field_55' => $set_value),array('id' => $id ));
		}

		public function set_field_56($set_value,$id) {
			$this->edit(array('field_56' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('dbc_faction');
		}
		

		public function select_id($name) {
			return $this->select('dbc_faction','id="' . $name . '"');
		}

		public function select_field_1($name) {
			return $this->select('dbc_faction','field_1="' . $name . '"');
		}

		public function select_field_2($name) {
			return $this->select('dbc_faction','field_2="' . $name . '"');
		}

		public function select_field_3($name) {
			return $this->select('dbc_faction','field_3="' . $name . '"');
		}

		public function select_field_4($name) {
			return $this->select('dbc_faction','field_4="' . $name . '"');
		}

		public function select_field_5($name) {
			return $this->select('dbc_faction','field_5="' . $name . '"');
		}

		public function select_field_6($name) {
			return $this->select('dbc_faction','field_6="' . $name . '"');
		}

		public function select_field_7($name) {
			return $this->select('dbc_faction','field_7="' . $name . '"');
		}

		public function select_field_8($name) {
			return $this->select('dbc_faction','field_8="' . $name . '"');
		}

		public function select_field_9($name) {
			return $this->select('dbc_faction','field_9="' . $name . '"');
		}

		public function select_field_10($name) {
			return $this->select('dbc_faction','field_10="' . $name . '"');
		}

		public function select_field_11($name) {
			return $this->select('dbc_faction','field_11="' . $name . '"');
		}

		public function select_field_12($name) {
			return $this->select('dbc_faction','field_12="' . $name . '"');
		}

		public function select_field_13($name) {
			return $this->select('dbc_faction','field_13="' . $name . '"');
		}

		public function select_field_14($name) {
			return $this->select('dbc_faction','field_14="' . $name . '"');
		}

		public function select_field_15($name) {
			return $this->select('dbc_faction','field_15="' . $name . '"');
		}

		public function select_field_16($name) {
			return $this->select('dbc_faction','field_16="' . $name . '"');
		}

		public function select_field_17($name) {
			return $this->select('dbc_faction','field_17="' . $name . '"');
		}

		public function select_field_18($name) {
			return $this->select('dbc_faction','field_18="' . $name . '"');
		}

		public function select_field_19($name) {
			return $this->select('dbc_faction','field_19="' . $name . '"');
		}

		public function select_field_20($name) {
			return $this->select('dbc_faction','field_20="' . $name . '"');
		}

		public function select_field_21($name) {
			return $this->select('dbc_faction','field_21="' . $name . '"');
		}

		public function select_field_22($name) {
			return $this->select('dbc_faction','field_22="' . $name . '"');
		}

		public function select_field_23($name) {
			return $this->select('dbc_faction','field_23="' . $name . '"');
		}

		public function select_field_24($name) {
			return $this->select('dbc_faction','field_24="' . $name . '"');
		}

		public function select_field_25($name) {
			return $this->select('dbc_faction','field_25="' . $name . '"');
		}

		public function select_field_26($name) {
			return $this->select('dbc_faction','field_26="' . $name . '"');
		}

		public function select_field_27($name) {
			return $this->select('dbc_faction','field_27="' . $name . '"');
		}

		public function select_field_28($name) {
			return $this->select('dbc_faction','field_28="' . $name . '"');
		}

		public function select_field_29($name) {
			return $this->select('dbc_faction','field_29="' . $name . '"');
		}

		public function select_field_30($name) {
			return $this->select('dbc_faction','field_30="' . $name . '"');
		}

		public function select_field_31($name) {
			return $this->select('dbc_faction','field_31="' . $name . '"');
		}

		public function select_field_32($name) {
			return $this->select('dbc_faction','field_32="' . $name . '"');
		}

		public function select_field_33($name) {
			return $this->select('dbc_faction','field_33="' . $name . '"');
		}

		public function select_field_34($name) {
			return $this->select('dbc_faction','field_34="' . $name . '"');
		}

		public function select_field_35($name) {
			return $this->select('dbc_faction','field_35="' . $name . '"');
		}

		public function select_field_36($name) {
			return $this->select('dbc_faction','field_36="' . $name . '"');
		}

		public function select_field_37($name) {
			return $this->select('dbc_faction','field_37="' . $name . '"');
		}

		public function select_field_38($name) {
			return $this->select('dbc_faction','field_38="' . $name . '"');
		}

		public function select_field_39($name) {
			return $this->select('dbc_faction','field_39="' . $name . '"');
		}

		public function select_field_40($name) {
			return $this->select('dbc_faction','field_40="' . $name . '"');
		}

		public function select_field_41($name) {
			return $this->select('dbc_faction','field_41="' . $name . '"');
		}

		public function select_field_42($name) {
			return $this->select('dbc_faction','field_42="' . $name . '"');
		}

		public function select_field_43($name) {
			return $this->select('dbc_faction','field_43="' . $name . '"');
		}

		public function select_field_44($name) {
			return $this->select('dbc_faction','field_44="' . $name . '"');
		}

		public function select_field_45($name) {
			return $this->select('dbc_faction','field_45="' . $name . '"');
		}

		public function select_field_46($name) {
			return $this->select('dbc_faction','field_46="' . $name . '"');
		}

		public function select_field_47($name) {
			return $this->select('dbc_faction','field_47="' . $name . '"');
		}

		public function select_field_48($name) {
			return $this->select('dbc_faction','field_48="' . $name . '"');
		}

		public function select_field_49($name) {
			return $this->select('dbc_faction','field_49="' . $name . '"');
		}

		public function select_field_50($name) {
			return $this->select('dbc_faction','field_50="' . $name . '"');
		}

		public function select_field_51($name) {
			return $this->select('dbc_faction','field_51="' . $name . '"');
		}

		public function select_field_52($name) {
			return $this->select('dbc_faction','field_52="' . $name . '"');
		}

		public function select_field_53($name) {
			return $this->select('dbc_faction','field_53="' . $name . '"');
		}

		public function select_field_54($name) {
			return $this->select('dbc_faction','field_54="' . $name . '"');
		}

		public function select_field_55($name) {
			return $this->select('dbc_faction','field_55="' . $name . '"');
		}

		public function select_field_56($name) {
			return $this->select('dbc_faction','field_56="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>