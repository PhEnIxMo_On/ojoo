<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mercredi 24 Avril 2013
	****************************************************/
	class phpbb_topics extends mvc_bdd  {
		public $topic_id;
		public $forum_id;
		public $icon_id;
		public $topic_attachment;
		public $topic_approved;
		public $topic_reported;
		public $topic_title;
		public $topic_poster;
		public $topic_time;
		public $topic_time_limit;
		public $topic_views;
		public $topic_replies;
		public $topic_replies_real;
		public $topic_status;
		public $topic_type;
		public $topic_first_post_id;
		public $topic_first_poster_name;
		public $topic_first_poster_colour;
		public $topic_last_post_id;
		public $topic_last_poster_id;
		public $topic_last_poster_name;
		public $topic_last_poster_colour;
		public $topic_last_post_subject;
		public $topic_last_post_time;
		public $topic_last_view_time;
		public $topic_moved_id;
		public $topic_bumped;
		public $topic_bumper;
		public $poll_title;
		public $poll_start;
		public $poll_length;
		public $poll_max_options;
		public $poll_last_vote;
		public $poll_vote_change;
		
		public $BDD = 'forum';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
		
		public function getAnnounceTopic() {
			$O = getOjoo();
			$topics = $O->bdd->forum->query("SELECT topic_id,topic_title,topic_first_poster_name,topic_time,topic_replies FROM phpbb_topics WHERE forum_id=5 ORDER BY topic_time DESC LIMIT 2")->fetchAll();
			return $topics;
		}

		public function set_topic_id($set_value,$id) {
			$this->edit(array('topic_id' => $set_value),array('id' => $id ));
		}

		public function set_forum_id($set_value,$id) {
			$this->edit(array('forum_id' => $set_value),array('id' => $id ));
		}

		public function set_icon_id($set_value,$id) {
			$this->edit(array('icon_id' => $set_value),array('id' => $id ));
		}

		public function set_topic_attachment($set_value,$id) {
			$this->edit(array('topic_attachment' => $set_value),array('id' => $id ));
		}

		public function set_topic_approved($set_value,$id) {
			$this->edit(array('topic_approved' => $set_value),array('id' => $id ));
		}

		public function set_topic_reported($set_value,$id) {
			$this->edit(array('topic_reported' => $set_value),array('id' => $id ));
		}

		public function set_topic_title($set_value,$id) {
			$this->edit(array('topic_title' => $set_value),array('id' => $id ));
		}

		public function set_topic_poster($set_value,$id) {
			$this->edit(array('topic_poster' => $set_value),array('id' => $id ));
		}

		public function set_topic_time($set_value,$id) {
			$this->edit(array('topic_time' => $set_value),array('id' => $id ));
		}

		public function set_topic_time_limit($set_value,$id) {
			$this->edit(array('topic_time_limit' => $set_value),array('id' => $id ));
		}

		public function set_topic_views($set_value,$id) {
			$this->edit(array('topic_views' => $set_value),array('id' => $id ));
		}

		public function set_topic_replies($set_value,$id) {
			$this->edit(array('topic_replies' => $set_value),array('id' => $id ));
		}

		public function set_topic_replies_real($set_value,$id) {
			$this->edit(array('topic_replies_real' => $set_value),array('id' => $id ));
		}

		public function set_topic_status($set_value,$id) {
			$this->edit(array('topic_status' => $set_value),array('id' => $id ));
		}

		public function set_topic_type($set_value,$id) {
			$this->edit(array('topic_type' => $set_value),array('id' => $id ));
		}

		public function set_topic_first_post_id($set_value,$id) {
			$this->edit(array('topic_first_post_id' => $set_value),array('id' => $id ));
		}

		public function set_topic_first_poster_name($set_value,$id) {
			$this->edit(array('topic_first_poster_name' => $set_value),array('id' => $id ));
		}

		public function set_topic_first_poster_colour($set_value,$id) {
			$this->edit(array('topic_first_poster_colour' => $set_value),array('id' => $id ));
		}

		public function set_topic_last_post_id($set_value,$id) {
			$this->edit(array('topic_last_post_id' => $set_value),array('id' => $id ));
		}

		public function set_topic_last_poster_id($set_value,$id) {
			$this->edit(array('topic_last_poster_id' => $set_value),array('id' => $id ));
		}

		public function set_topic_last_poster_name($set_value,$id) {
			$this->edit(array('topic_last_poster_name' => $set_value),array('id' => $id ));
		}

		public function set_topic_last_poster_colour($set_value,$id) {
			$this->edit(array('topic_last_poster_colour' => $set_value),array('id' => $id ));
		}

		public function set_topic_last_post_subject($set_value,$id) {
			$this->edit(array('topic_last_post_subject' => $set_value),array('id' => $id ));
		}

		public function set_topic_last_post_time($set_value,$id) {
			$this->edit(array('topic_last_post_time' => $set_value),array('id' => $id ));
		}

		public function set_topic_last_view_time($set_value,$id) {
			$this->edit(array('topic_last_view_time' => $set_value),array('id' => $id ));
		}

		public function set_topic_moved_id($set_value,$id) {
			$this->edit(array('topic_moved_id' => $set_value),array('id' => $id ));
		}

		public function set_topic_bumped($set_value,$id) {
			$this->edit(array('topic_bumped' => $set_value),array('id' => $id ));
		}

		public function set_topic_bumper($set_value,$id) {
			$this->edit(array('topic_bumper' => $set_value),array('id' => $id ));
		}

		public function set_poll_title($set_value,$id) {
			$this->edit(array('poll_title' => $set_value),array('id' => $id ));
		}

		public function set_poll_start($set_value,$id) {
			$this->edit(array('poll_start' => $set_value),array('id' => $id ));
		}

		public function set_poll_length($set_value,$id) {
			$this->edit(array('poll_length' => $set_value),array('id' => $id ));
		}

		public function set_poll_max_options($set_value,$id) {
			$this->edit(array('poll_max_options' => $set_value),array('id' => $id ));
		}

		public function set_poll_last_vote($set_value,$id) {
			$this->edit(array('poll_last_vote' => $set_value),array('id' => $id ));
		}

		public function set_poll_vote_change($set_value,$id) {
			$this->edit(array('poll_vote_change' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('phpbb_topics');
		}
		

		public function select_topic_id($name) {
			return $this->select('phpbb_topics','topic_id="' . $name . '"');
		}

		public function select_forum_id($name) {
			return $this->select('phpbb_topics','forum_id="' . $name . '"');
		}

		public function select_icon_id($name) {
			return $this->select('phpbb_topics','icon_id="' . $name . '"');
		}

		public function select_topic_attachment($name) {
			return $this->select('phpbb_topics','topic_attachment="' . $name . '"');
		}

		public function select_topic_approved($name) {
			return $this->select('phpbb_topics','topic_approved="' . $name . '"');
		}

		public function select_topic_reported($name) {
			return $this->select('phpbb_topics','topic_reported="' . $name . '"');
		}

		public function select_topic_title($name) {
			return $this->select('phpbb_topics','topic_title="' . $name . '"');
		}

		public function select_topic_poster($name) {
			return $this->select('phpbb_topics','topic_poster="' . $name . '"');
		}

		public function select_topic_time($name) {
			return $this->select('phpbb_topics','topic_time="' . $name . '"');
		}

		public function select_topic_time_limit($name) {
			return $this->select('phpbb_topics','topic_time_limit="' . $name . '"');
		}

		public function select_topic_views($name) {
			return $this->select('phpbb_topics','topic_views="' . $name . '"');
		}

		public function select_topic_replies($name) {
			return $this->select('phpbb_topics','topic_replies="' . $name . '"');
		}

		public function select_topic_replies_real($name) {
			return $this->select('phpbb_topics','topic_replies_real="' . $name . '"');
		}

		public function select_topic_status($name) {
			return $this->select('phpbb_topics','topic_status="' . $name . '"');
		}

		public function select_topic_type($name) {
			return $this->select('phpbb_topics','topic_type="' . $name . '"');
		}

		public function select_topic_first_post_id($name) {
			return $this->select('phpbb_topics','topic_first_post_id="' . $name . '"');
		}

		public function select_topic_first_poster_name($name) {
			return $this->select('phpbb_topics','topic_first_poster_name="' . $name . '"');
		}

		public function select_topic_first_poster_colour($name) {
			return $this->select('phpbb_topics','topic_first_poster_colour="' . $name . '"');
		}

		public function select_topic_last_post_id($name) {
			return $this->select('phpbb_topics','topic_last_post_id="' . $name . '"');
		}

		public function select_topic_last_poster_id($name) {
			return $this->select('phpbb_topics','topic_last_poster_id="' . $name . '"');
		}

		public function select_topic_last_poster_name($name) {
			return $this->select('phpbb_topics','topic_last_poster_name="' . $name . '"');
		}

		public function select_topic_last_poster_colour($name) {
			return $this->select('phpbb_topics','topic_last_poster_colour="' . $name . '"');
		}

		public function select_topic_last_post_subject($name) {
			return $this->select('phpbb_topics','topic_last_post_subject="' . $name . '"');
		}

		public function select_topic_last_post_time($name) {
			return $this->select('phpbb_topics','topic_last_post_time="' . $name . '"');
		}

		public function select_topic_last_view_time($name) {
			return $this->select('phpbb_topics','topic_last_view_time="' . $name . '"');
		}

		public function select_topic_moved_id($name) {
			return $this->select('phpbb_topics','topic_moved_id="' . $name . '"');
		}

		public function select_topic_bumped($name) {
			return $this->select('phpbb_topics','topic_bumped="' . $name . '"');
		}

		public function select_topic_bumper($name) {
			return $this->select('phpbb_topics','topic_bumper="' . $name . '"');
		}

		public function select_poll_title($name) {
			return $this->select('phpbb_topics','poll_title="' . $name . '"');
		}

		public function select_poll_start($name) {
			return $this->select('phpbb_topics','poll_start="' . $name . '"');
		}

		public function select_poll_length($name) {
			return $this->select('phpbb_topics','poll_length="' . $name . '"');
		}

		public function select_poll_max_options($name) {
			return $this->select('phpbb_topics','poll_max_options="' . $name . '"');
		}

		public function select_poll_last_vote($name) {
			return $this->select('phpbb_topics','poll_last_vote="' . $name . '"');
		}

		public function select_poll_vote_change($name) {
			return $this->select('phpbb_topics','poll_vote_change="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>