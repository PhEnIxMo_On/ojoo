<?php

	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mercredi 28 Novembre 2012
	****************************************************/
	class forum_categories extends mvc_bdd  {
		public $id;
		public $nom;
		public $description;
		public $nbTopic;
		public $nbMessages;
		public $idCat;
		public $idLastPost;
		
		public $BDD = 'cp';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_nom($set_value,$id) {
			$this->edit(array('nom' => $set_value),array('id' => $id ));
		}

		public function set_description($set_value,$id) {
			$this->edit(array('description' => $set_value),array('id' => $id ));
		}

		public function set_nbTopic($set_value,$id) {
			$this->edit(array('nbTopic' => $set_value),array('id' => $id ));
		}

		public function set_nbMessages($set_value,$id) {
			$this->edit(array('nbMessages' => $set_value),array('id' => $id ));
		}

		public function set_idCat($set_value,$id) {
			$this->edit(array('idCat' => $set_value),array('id' => $id ));
		}

		public function set_idLastPost($set_value,$id) {
			$this->edit(array('idLastPost' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('forum_categories');
		}
		

		public function select_id($name) {
			return $this->select('forum_categories','id="' . $name . '"');
		}

		public function select_nom($name) {
			return $this->select('forum_categories','nom="' . $name . '"');
		}

		public function select_description($name) {
			return $this->select('forum_categories','description="' . $name . '"');
		}

		public function select_nbTopic($name) {
			return $this->select('forum_categories','nbTopic="' . $name . '"');
		}

		public function select_nbMessages($name) {
			return $this->select('forum_categories','nbMessages="' . $name . '"');
		}

		public function select_idCat($name) {
			return $this->select('forum_categories','idCat="' . $name . '"');
		}

		public function select_idLastPost($name) {
			return $this->select('forum_categories','idLastPost="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>