<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Jeudi 20 Juin 2013
	****************************************************/
	class money extends mvc_bdd  {
		public $id;
		public $idCompte;
		public $nbMoney;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
	
		public function setMoney($set_value,$idCompte) {
			$O = getOjoo();
			$money = $O->bdd->site->query("UPDATE money SET nbMoney='" . $set_value . "' WHERE idCompte =" . $idCompte . "") or die (" Erreur lors de la modification des votes");
		}
		
		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_idCompte($set_value,$id) {
			$this->edit(array('idCompte' => $set_value),array('id' => $id ));
		}
		
		public function crediter($nbPoints,$idCompte) {
			$O = getOjoo();
			$O->bdd->site->query("UPDATE money SET nbMoney=nbMoney+" . $nbPoints . " WHERE idCompte=" . $idCompte);
		}

		public function set_nbMoney($set_value,$id) {
			$O = getOjoo();
			$money = $O->bdd->site->query("UPDATE money SET nbMoney='" . $set_value . "' WHERE id=" . $id . "") or die (" Erreur lors de la modification de la money");
			$this->edit(array('nbMoney' => $set_value),array('id' => $id ));
		}

		public function getNbMoney() {
			return $this->nbMoney;
		}
		
		public function setNbMoney($value) {
			
			$this->nbMoney = $value;
			
		}
		
		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('money');
		}
		

		public function select_id($name) {
			return $this->select('money','id="' . $name . '"');
		}

		public function select_idCompte($name) {
			return $this->select('money','idCompte="' . $name . '"');
		}

		public function select_nbMoney($name) {
			return $this->select('money','nbMoney="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>