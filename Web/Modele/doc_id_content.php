<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Lundi 11 Fevrier 2013
	****************************************************/
	class doc_id_content extends mvc_bdd  {
		public $id;
		public $docId;
		public $wowId;
		public $type;
		public $autre;
		public $sure;
		
		public $BDD = 'world';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_docId($set_value,$id) {
			$this->edit(array('docId' => $set_value),array('id' => $id ));
		}

		public function set_wowId($set_value,$id) {
			$this->edit(array('wowId' => $set_value),array('id' => $id ));
		}

		public function set_type($set_value,$id) {
			$this->edit(array('type' => $set_value),array('id' => $id ));
		}

		public function set_autre($set_value,$id) {
			$this->edit(array('autre' => $set_value),array('id' => $id ));
		}

		public function set_sure($set_value,$id) {
			$this->edit(array('sure' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('doc_id_content');
		}
		

		public function select_id($name) {
			return $this->select('doc_id_content','id="' . $name . '"');
		}

		public function select_docId($name) {
			return $this->select('doc_id_content','docId="' . $name . '"');
		}

		public function select_wowId($name) {
			return $this->select('doc_id_content','wowId="' . $name . '"');
		}

		public function select_type($name) {
			return $this->select('doc_id_content','type="' . $name . '"');
		}

		public function select_autre($name) {
			return $this->select('doc_id_content','autre="' . $name . '"');
		}

		public function select_sure($name) {
			return $this->select('doc_id_content','sure="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>