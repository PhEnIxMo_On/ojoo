<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Lundi 10 Juin 2013
	****************************************************/
	class web_boutique_cat extends mvc_bdd  {
		const IS_OPEN = 0x20; 				// 100000 = 32
		const IS_CLOSE = 0x10; 				// 010000 = 16
		const IS_ALLOPASS = 0x08; 			// 001000 = 8
		const IS_PAYPAL = 0x04; 			// 000100 = 4
		const IS_STARPASS = 0x02; 			// 000010 = 2
		const IS_VOTE = 0x01; 				// 000001 = 1
		const IS_REAL_MONEY = 0xE; 			// 001110 = 14
		const IS_FREE_OR_REAL_MONEY = 0xF;	// 001111 = 15
				
		public $id;
		public $id_cat;
		public $name;
		public $description;
		public $state;
		public $icon_name;
		public $ordre;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
		
		public function deleteCat($id) {
			$O = getOjoo();
			$cat = $O->bdd->site->query("SELECT * FROM web_boutique_cat WHERE id='" . $id . "'")->fetch();
			if($cat != null) {
				$O->bdd->site->query("UPDATE web_boutique_cat SET ordre=ordre-1 WHERE ordre > " . $cat['ordre']);
				$this->delete_id($id);
			}
		}
		
		public function getMaxOrder() {
			$O = getOjoo();
			$test = $O->bdd->site->query("SELECT ordre FROM web_boutique_cat ORDER BY ordre DESC LIMIT 1")->fetch();
			//$O->console->debug('Ordre : ' . $test['ordre'],'[ORDRE]');
			return $test['ordre'];
		}
		
		public function getSCat() {
			$O = getOjoo();
			$sCat = $O->bdd->site->query("SELECT * FROM web_boutique_cat WHERE id_cat > 0 ORDER BY ordre DESC");
			return $sCat->fetchAll();
		}
		
		public function updateOrder($id,$type) {
			$O = getOjoo();
			$ordreFetch = $O->bdd->site->query("SELECT ordre FROM web_boutique_cat WHERE id=" . $id)->fetch();
			$ordre = $ordreFetch['ordre'];
			
			if ($type == 'up')
				$ordre3 = $ordre + 1;
			else
				$ordre3 = $ordre - 1;
			if ($ordre3 > 0) {
				$test = $O->bdd->site->query("SELECT id FROM web_boutique_cat WHERE ordre='" . $ordre3 . "'")->fetch();
				if ($test != false) {
					$id2 = $O->bdd->site->query("SELECT id FROM web_boutique_cat WHERE ordre=" . $ordre3)->fetch();
					
					$O->bdd->site->query("UPDATE web_boutique_cat SET ordre=" . $ordre3 . " WHERE id=" . $id);
					$O->bdd->site->query("UPDATE web_boutique_cat SET ordre=" . $ordre . " WHERE id=" . $id2['id']);					
				}
			}					
		}

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_id_cat($set_value,$id) {
			$this->edit(array('id_cat' => $set_value),array('id' => $id ));
		}

		public function set_name($set_value,$id) {
			$this->edit(array('name' => $set_value),array('id' => $id ));
		}

		public function set_description($set_value,$id) {
			$this->edit(array('description' => $set_value),array('id' => $id ));
		}

		public function set_state($set_value,$id) {
			$this->edit(array('state' => $set_value),array('id' => $id ));
		}

		
		public function selectByOrdre($type = 'ASC') {
			$O = getOjoo();
			$data = $O->bdd->site->query("SELECT * FROM web_boutique_cat WHERE id_cat=0 OR id_cat=-1 ORDER BY ordre " . $type)->fetchAll();
			return $data;
		}
		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('web_boutique_cat');
		}
		

		public function select_id($name) {
			return $this->select('web_boutique_cat','id="' . $name . '"');
		}

		public function select_id_cat($name) {
			return $this->select('web_boutique_cat','id_cat="' . $name . '"');
		}

		public function select_name($name) {
			return $this->select('web_boutique_cat','name="' . $name . '"');
		}

		public function select_description($name) {
			return $this->select('web_boutique_cat','description="' . $name . '"');
		}

		public function select_state($name) {
			return $this->select('web_boutique_cat','state="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>