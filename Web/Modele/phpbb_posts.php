<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mercredi 24 Avril 2013
	****************************************************/
	class phpbb_posts extends mvc_bdd  {
		public $post_id;
		public $topic_id;
		public $forum_id;
		public $poster_id;
		public $icon_id;
		public $poster_ip;
		public $post_time;
		public $post_approved;
		public $post_reported;
		public $enable_bbcode;
		public $enable_smilies;
		public $enable_magic_url;
		public $enable_sig;
		public $post_username;
		public $post_subject;
		public $post_text;
		public $post_checksum;
		public $post_attachment;
		public $bbcode_bitfield;
		public $bbcode_uid;
		public $post_postcount;
		public $post_edit_time;
		public $post_edit_reason;
		public $post_edit_user;
		public $post_edit_count;
		public $post_edit_locked;
		
		public $BDD = 'forum';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
		
		public function getAnnouces() {
			$O = getOjoo();
			$topics = $O->modele->phpbb_topics->getAnnounceTopic();
			$posts = array();
			$infosAnnounce = array();
			foreach ($topics as $topic) {
				$posts[$topic['topic_id']] = $O->bdd->forum->query("SELECT * FROM phpbb_posts WHERE topic_id=" . $topic['topic_id'] . " ORDER BY post_time ASC")->fetch();
				$infosAnnounce[$topic['topic_id']]["topic_id"] = $topic['topic_id'];
				$infosAnnounce[$topic['topic_id']]["topic_title"] = $topic['topic_title'];
				$infosAnnounce[$topic['topic_id']]['topic_time'] = $topic['topic_time'];
				$infosAnnounce[$topic['topic_id']]['message'] = $posts[$topic['topic_id']]['post_text'];
				$infosAnnounce[$topic['topic_id']]['post_id'] = $posts[$topic['topic_id']]['post_id'];
				$infosAnnounce[$topic['topic_id']]['nbCom'] = $topic['topic_replies'];
				$infosAnnounce[$topic['topic_id']]['bbcode_uid'] = $posts[$topic['topic_id']]['bbcode_uid'];
			}			
			
			return $infosAnnounce;
		}

		public function set_post_id($set_value,$id) {
			$this->edit(array('post_id' => $set_value),array('id' => $id ));
		}

		public function set_topic_id($set_value,$id) {
			$this->edit(array('topic_id' => $set_value),array('id' => $id ));
		}

		public function set_forum_id($set_value,$id) {
			$this->edit(array('forum_id' => $set_value),array('id' => $id ));
		}

		public function set_poster_id($set_value,$id) {
			$this->edit(array('poster_id' => $set_value),array('id' => $id ));
		}

		public function set_icon_id($set_value,$id) {
			$this->edit(array('icon_id' => $set_value),array('id' => $id ));
		}

		public function set_poster_ip($set_value,$id) {
			$this->edit(array('poster_ip' => $set_value),array('id' => $id ));
		}

		public function set_post_time($set_value,$id) {
			$this->edit(array('post_time' => $set_value),array('id' => $id ));
		}

		public function set_post_approved($set_value,$id) {
			$this->edit(array('post_approved' => $set_value),array('id' => $id ));
		}

		public function set_post_reported($set_value,$id) {
			$this->edit(array('post_reported' => $set_value),array('id' => $id ));
		}

		public function set_enable_bbcode($set_value,$id) {
			$this->edit(array('enable_bbcode' => $set_value),array('id' => $id ));
		}

		public function set_enable_smilies($set_value,$id) {
			$this->edit(array('enable_smilies' => $set_value),array('id' => $id ));
		}

		public function set_enable_magic_url($set_value,$id) {
			$this->edit(array('enable_magic_url' => $set_value),array('id' => $id ));
		}

		public function set_enable_sig($set_value,$id) {
			$this->edit(array('enable_sig' => $set_value),array('id' => $id ));
		}

		public function set_post_username($set_value,$id) {
			$this->edit(array('post_username' => $set_value),array('id' => $id ));
		}

		public function set_post_subject($set_value,$id) {
			$this->edit(array('post_subject' => $set_value),array('id' => $id ));
		}

		public function set_post_text($set_value,$id) {
			$this->edit(array('post_text' => $set_value),array('id' => $id ));
		}

		public function set_post_checksum($set_value,$id) {
			$this->edit(array('post_checksum' => $set_value),array('id' => $id ));
		}

		public function set_post_attachment($set_value,$id) {
			$this->edit(array('post_attachment' => $set_value),array('id' => $id ));
		}

		public function set_bbcode_bitfield($set_value,$id) {
			$this->edit(array('bbcode_bitfield' => $set_value),array('id' => $id ));
		}

		public function set_bbcode_uid($set_value,$id) {
			$this->edit(array('bbcode_uid' => $set_value),array('id' => $id ));
		}

		public function set_post_postcount($set_value,$id) {
			$this->edit(array('post_postcount' => $set_value),array('id' => $id ));
		}

		public function set_post_edit_time($set_value,$id) {
			$this->edit(array('post_edit_time' => $set_value),array('id' => $id ));
		}

		public function set_post_edit_reason($set_value,$id) {
			$this->edit(array('post_edit_reason' => $set_value),array('id' => $id ));
		}

		public function set_post_edit_user($set_value,$id) {
			$this->edit(array('post_edit_user' => $set_value),array('id' => $id ));
		}

		public function set_post_edit_count($set_value,$id) {
			$this->edit(array('post_edit_count' => $set_value),array('id' => $id ));
		}

		public function set_post_edit_locked($set_value,$id) {
			$this->edit(array('post_edit_locked' => $set_value),array('id' => $id ));
		}
		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('phpbb_posts');
		}
		

		public function select_post_id($name) {
			return $this->select('phpbb_posts','post_id="' . $name . '"');
		}

		public function select_topic_id($name) {
			return $this->select('phpbb_posts','topic_id="' . $name . '"');
		}

		public function select_forum_id($name) {
			return $this->select('phpbb_posts','forum_id="' . $name . '"');
		}

		public function select_poster_id($name) {
			return $this->select('phpbb_posts','poster_id="' . $name . '"');
		}

		public function select_icon_id($name) {
			return $this->select('phpbb_posts','icon_id="' . $name . '"');
		}

		public function select_poster_ip($name) {
			return $this->select('phpbb_posts','poster_ip="' . $name . '"');
		}

		public function select_post_time($name) {
			return $this->select('phpbb_posts','post_time="' . $name . '"');
		}

		public function select_post_approved($name) {
			return $this->select('phpbb_posts','post_approved="' . $name . '"');
		}

		public function select_post_reported($name) {
			return $this->select('phpbb_posts','post_reported="' . $name . '"');
		}

		public function select_enable_bbcode($name) {
			return $this->select('phpbb_posts','enable_bbcode="' . $name . '"');
		}

		public function select_enable_smilies($name) {
			return $this->select('phpbb_posts','enable_smilies="' . $name . '"');
		}

		public function select_enable_magic_url($name) {
			return $this->select('phpbb_posts','enable_magic_url="' . $name . '"');
		}

		public function select_enable_sig($name) {
			return $this->select('phpbb_posts','enable_sig="' . $name . '"');
		}

		public function select_post_username($name) {
			return $this->select('phpbb_posts','post_username="' . $name . '"');
		}

		public function select_post_subject($name) {
			return $this->select('phpbb_posts','post_subject="' . $name . '"');
		}

		public function select_post_text($name) {
			return $this->select('phpbb_posts','post_text="' . $name . '"');
		}

		public function select_post_checksum($name) {
			return $this->select('phpbb_posts','post_checksum="' . $name . '"');
		}

		public function select_post_attachment($name) {
			return $this->select('phpbb_posts','post_attachment="' . $name . '"');
		}

		public function select_bbcode_bitfield($name) {
			return $this->select('phpbb_posts','bbcode_bitfield="' . $name . '"');
		}

		public function select_bbcode_uid($name) {
			return $this->select('phpbb_posts','bbcode_uid="' . $name . '"');
		}

		public function select_post_postcount($name) {
			return $this->select('phpbb_posts','post_postcount="' . $name . '"');
		}

		public function select_post_edit_time($name) {
			return $this->select('phpbb_posts','post_edit_time="' . $name . '"');
		}

		public function select_post_edit_reason($name) {
			return $this->select('phpbb_posts','post_edit_reason="' . $name . '"');
		}

		public function select_post_edit_user($name) {
			return $this->select('phpbb_posts','post_edit_user="' . $name . '"');
		}

		public function select_post_edit_count($name) {
			return $this->select('phpbb_posts','post_edit_count="' . $name . '"');
		}

		public function select_post_edit_locked($name) {
			return $this->select('phpbb_posts','post_edit_locked="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>