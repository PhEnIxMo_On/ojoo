<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Jeudi 25 Juillet 2013
	****************************************************/
	class arena_team extends mvc_bdd  {
		public $arenaTeamId;
		public $name;
		public $captainGuid;
		public $type;
		public $rating;
		public $seasonGames;
		public $seasonWins;
		public $weekGames;
		public $weekWins;
		public $rank;
		public $backgroundColor;
		public $emblemStyle;
		public $emblemColor;
		public $borderStyle;
		public $borderColor;
		
		public $BDD = 'char';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		
		// Fonction de sélection :
		public function select_topX($valueType, $valueTop) {
			$O = getOjoo();
			$data = $O->bdd->char->query("SELECT * FROM arena_team WHERE type=" . $valueType ." ORDER BY rating DESC LIMIT 0," . $valueTop ."")->fetchAll();
			return $data;
		}
		
		public function select_all() {
			return $this->select('arena_team');
		}
		


	// Fonction de suppression : 	

	}
?>