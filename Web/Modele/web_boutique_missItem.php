<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Jeudi 18 Juillet 2013
	****************************************************/
	class web_boutique_missItem extends mvc_bdd  {
		
		const MISSING = 0;
		const REPAIR = 1;
		const DELETE = 2;
		
		public $id;
		public $idItem;
		public $idWowhead;
		public $state;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_idItem($set_value,$id) {
			$this->edit(array('idItem' => $set_value),array('id' => $id ));
		}

		public function set_idWowhead($set_value,$id) {
			$this->edit(array('idWowhead' => $set_value),array('id' => $id ));
		}

		public function set_state($set_value,$id) {
			$this->edit(array('state' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('web_boutique_missItem');
		}
		

		public function select_id($name) {
			return $this->select('web_boutique_missItem','id="' . $name . '"');
		}

		public function select_idItem($name) {
			return $this->select('web_boutique_missItem','idItem="' . $name . '"');
		}

		public function select_idWowhead($name) {
			return $this->select('web_boutique_missItem','idWowhead="' . $name . '"');
		}

		public function select_state($name) {
			return $this->select('web_boutique_missItem','state="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>