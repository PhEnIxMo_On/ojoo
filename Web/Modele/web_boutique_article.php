<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Dimanche 16 Juin 2013
	****************************************************/
	class web_boutique_article extends mvc_bdd  {
		const IS_OPEN = 0x20; 				// 100000 = 32
		const IS_CLOSE = 0x10; 				// 010000 = 16
		const IS_ALLOPASS = 0x08; 			// 001000 = 8
		const IS_PAYPAL = 0x04; 			// 000100 = 4
		const IS_STARPASS = 0x02; 			// 000010 = 2
		const IS_VOTE = 0x01; 				// 000001 = 1
		const IS_REAL_MONEY = 0xE; 			// 001110 = 14
		const IS_FREE_OR_REAL_MONEY = 0xF;	// 001111 = 15
		
		public $id;
		public $idCat;
		public $name;
		public $description;
		public $idWowhead;
		public $state;
		public $prixVote;
		public $prixMoney;
		public $icon_name;
		public $nbCharge;
		public $sticky;
		public $stock;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_idCat($set_value,$id) {
			$this->edit(array('idCat' => $set_value),array('id' => $id ));
		}

		public function set_name($set_value,$id) {
			$this->edit(array('name' => $set_value),array('id' => $id ));
		}

		public function set_description($set_value,$id) {
			$this->edit(array('description' => $set_value),array('id' => $id ));
		}
		
		public function set_idWowhead($set_value,$id) {
			$this->edit(array('idWowhead' => $set_value),array('id' => $id ));
		}

		public function set_state($set_value,$id) {
			$this->edit(array('state' => $set_value),array('id' => $id ));
		}

		public function set_prixVote($set_value,$id) {
			$this->edit(array('prixVote' => $set_value),array('id' => $id ));
		}

		public function set_prixMoney($set_value,$id) {
			$this->edit(array('prixMoney' => $set_value),array('id' => $id ));
		}

		public function set_icon_name($set_value,$id) {
			$this->edit(array('icon_name' => $set_value),array('id' => $id ));
		}
		
		public function set_nbCharge($set_value,$id) {
			$this->edit(array('nbCharge' => $set_value),array('id' => $id ));
		}
		
		public function set_sticky($set_value,$id) {
			$this->edit(array('sticky' => $set_value),array('id' => $id ));
		}
		
		public function set_stock($set_value,$id) {
			$this->edit(array('stock' => $set_value),array('id' => $id ));
		}
		
		public function downStock($id, $qte) {
			$newStock = $this->getStock($id)-$qte;
			if($newStock<0)$newStock=0;
			$this->edit(array('stock' => $newStock),array('id' => $id ));
		}

		public function select_between_idCat($value, $valueMin, $valueMax) {
			$O = getOjoo();
			$data = $O->bdd->site->query("SELECT * FROM web_boutique_article WHERE idCat=" . $value ." OR idCat BETWEEN " . $valueMin . " AND " . $valueMax . "")->fetchAll();
			return $data;
		}
		
		// Fonction de sélection :
		public function selectAllOrderByName() {
			$O = getOjoo();
			$data = $O->bdd->site->query("SELECT * FROM web_boutique_article ORDER BY name")->fetchAll();
			return $data;
		}
		
		public function selectAllOrderByNameWhere($idCat) {
			$O = getOjoo();
			$data = $O->bdd->site->query("SELECT * FROM web_boutique_article WHERE idCat=" . $idCat . " ORDER BY name")->fetchAll();
			return $data;
		}
		
		public function getStock($idArticle) {
			$O = getOjoo();
			$data = $O->bdd->site->query("SELECT * FROM web_boutique_article WHERE id=" . $idArticle ."")->fetchAll();
			$stock =0;
			foreach($data as $dataStock) {
				$stock = $dataStock['stock'];
			}
			return $stock;
		}
		
		public function select_all() {
			return $this->select('web_boutique_article');
		}
		

		public function select_id($name) {
			return $this->select('web_boutique_article','id="' . $name . '"');
		}

		public function select_idCat($name) {
			return $this->select('web_boutique_article','idCat="' . $name . '"');
		}

		
		public function select_name($name) {
			return $this->select('web_boutique_article','name="' . $name . '"');
		}

		public function select_description($name) {
			return $this->select('web_boutique_article','description="' . $name . '"');
		}
		
		public function select_idWowhead($name) {
			return $this->select('web_boutique_article','idWowhead="' . $name . '"');
		}

		public function select_state($name) {
			return $this->select('web_boutique_article','state="' . $name . '"');
		}

		public function select_prixVote($name) {
			return $this->select('web_boutique_article','prixVote="' . $name . '"');
		}

		public function select_prixMoney($name) {
			return $this->select('web_boutique_article','prixMoney="' . $name . '"');
		}

		public function select_icon_name($name) {
			return $this->select('web_boutique_article','icon_name="' . $name . '"');
		}
		
		public function select_nbCharge($name) {
			return $this->select('web_boutique_article','nbCharge="' . $name . '"');
		}
		
		public function select_sticky($name) {
			return $this->select('web_boutique_article','sticky="' . $name . '"');
		}
		
		public function select_stock($name) {
			return $this->select('web_boutique_article','stock="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>