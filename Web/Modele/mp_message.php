<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mercredi 22 Mai 2013
	****************************************************/
	class mp_message extends mvc_bdd  {
		public $id;
		public $accountId;
		public $convId;
		public $message;
		public $dateCreate;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_accountId($set_value,$id) {
			$this->edit(array('accountId' => $set_value),array('id' => $id ));
		}

		public function set_convId($set_value,$id) {
			$this->edit(array('convId' => $set_value),array('id' => $id ));
		}

		public function set_message($set_value,$id) {
			$this->edit(array('message' => $set_value),array('id' => $id ));
		}

		public function set_dateCreate($set_value,$id) {
			$this->edit(array('dateCreate' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('mp_message');
		}
		

		public function select_id($name) {
			return $this->select('mp_message','id="' . $name . '"');
		}

		public function select_accountId($name) {
			return $this->select('mp_message','accountId="' . $name . '"');
		}

		public function select_convId($name) {
			return $this->select('mp_message','convId="' . $name . '"');
		}

		public function select_message($name) {
			return $this->select('mp_message','message="' . $name . '"');
		}

		public function select_dateCreate($name) {
			return $this->select('mp_message','dateCreate="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>