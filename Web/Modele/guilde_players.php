<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Jeudi 23 Mai 2013
	****************************************************/
	class guilde_players extends mvc_bdd  {
                const WAIT = 1;
                const VALID = 2;
                const EXPIRE = 3;
                const HOST_INVITE = 4;
                const PLAYER_INVITE = 5;
                const REFUSE = 6;
                
                const CHAR_MAIN = 1;
                const CHAR_REROLL = 2;
                const CHAR_WAIT = 3;
            
            
		public $id;
		public $guildeId;
		public $accountId;
		public $charGuid;
		public $originalName;
                public $charType;
		public $state;
		public $code;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}
                
                public function getCharType($type) {
                    switch ($type) {
                        case self::CHAR_MAIN:
                            return "Personnage principal";
                            break;
                        
                        case self::CHAR_REROLL;
                            return "Personnage secondaire";
                            break;
                        
                        case self::CHAR_WAIT:
                            return "En attente";
                            break;
                    }
                }
                
                public function canAccept($charGuid,$guildeId,$checkMain = false) {
                    $O = getOjoo();
                    $char = $O->modele->characters->select_guid($charGuid)->fetch();
                    if ($char != false) {
                        $accountId = $char['account'];
                        $guid = $char['guid'];                                                            
                        if ($checkMain) {
                            $test3 = $O->bdd->site->query("SELECT COUNT(id) AS nb FROM guilde_players WHERE accountId=" . $accountId . " AND guildeId=" . $guildeId . " AND charType=1")->fetch();
                            if ($test3['nb'] >= 1) {
                                $O->console->debug("Personnage invalide déjà un main en DB","[CHECK_CAN_INVITE]");
                                return false;
                            } else return true;
                        }
                        $O->console->debug("Personnage validé","[CHECK_CAN_INVITE]");
                        return true;
                    } else 
                        $O->console->debug("Personange non existant","[CHECK_CAN_INVITE]");
                                           
                }
                
                public function canInvite($charGuid,$guildeId,$checkMain = false) {
                    $O = getOjoo();
                    $char = $O->modele->characters->select_guid($charGuid)->fetch();
                    if ($char != false) {
                        $accountId = $char['account'];
                        $guid = $char['guid'];                        
                        $test1 = $O->bdd->site->query("SELECT id FROM guilde_players WHERE accountId=" . $accountId . " AND guildeId=" . $guildeId)->fetch();
                        if ($test1 != false) {                                           
                            $test2 = $O->bdd->site->query("SELECT id FROM guilde_players WHERE charGuid=" . $guid . " AND guildeId=" . $guildeId)->fetch();
                            if($test2 != false) {
                               $O->console->debug("Personange déjà en table","[CHECK_CAN_INVITE]");
                                return false;
                            }
                            else  {
                                if ($checkMain) {
                                    $test3 = $O->bdd->site->query("SELECT COUNT(id) AS nb FROM guilde_players WHERE accountId=" . $accountId . " AND guildeId=" . $guildeId . " AND charType=1")->fetch();
                                    if ($test3['nb'] >= 1) {
                                        $O->console->debug("Personnage invalide déjà un main en DB","[CHECK_CAN_INVITE]");
                                        return false;
                                    } else return true;
                                }
                                $O->console->debug("Personnage validé","[CHECK_CAN_INVITE]");
                                return true;
                            }
                        } else {
                            $O->console->debug("Personange validé","[CHECK_CAN_INVITE]");
                            return true;
                        }
                    } else {
                        $O->console->debug("Personange non existant","[CHECK_CAN_INVITE]");
                        return false;
                    }
                }                
                
                public function getState($state) {
                    switch ($state) {
                        case self::WAIT:
                            return "En attente";
                            break;
                        
                        case self::VALID:
                            return "Validé";
                            break;
                        
                        case self::EXPIRE:
                            return "Expiré";
                            break;
                        
                        case self::HOST_INVITE:
                            return "En attente de réponse";
                            break;
                        
                        case self::PLAYER_INVITE:
                            return "Souhaite rejoindre la guilde";
                            break;
                        
                        case self::REFUSE:
                            return "<span style=\"color: red; font-weight: bold;\">Invitation refusée</font>";
                            break;
                    }
                }

		public function set_guildeId($set_value,$id) {
			$this->edit(array('guildeId' => $set_value),array('id' => $id ));
		}

		public function set_accountId($set_value,$id) {
			$this->edit(array('accountId' => $set_value),array('id' => $id ));
		}

		public function set_charGuid($set_value,$id) {
			$this->edit(array('charGuid' => $set_value),array('id' => $id ));
		}

		public function set_originalName($set_value,$id) {
			$this->edit(array('originalName' => $set_value),array('id' => $id ));
		}

		public function set_state($set_value,$id) {
			$this->edit(array('state' => $set_value),array('id' => $id ));
		}

		public function set_code($set_value,$id) {
			$this->edit(array('code' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('guilde_players');
		}
		

		public function select_id($name) {
			return $this->select('guilde_players','id="' . $name . '"');
		}

		public function select_guildeId($name) {
			return $this->select('guilde_players','guildeId="' . $name . '"');
		}

		public function select_accountId($name) {
			return $this->select('guilde_players','accountId="' . $name . '"');
		}

		public function select_charGuid($name) {
			return $this->select('guilde_players','charGuid="' . $name . '"');
		}

		public function select_originalName($name) {
			return $this->select('guilde_players','originalName="' . $name . '"');
		}

		public function select_state($name) {
			return $this->select('guilde_players','state="' . $name . '"');
		}

		public function select_code($name) {
			return $this->select('guilde_players','code="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>