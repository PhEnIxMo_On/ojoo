<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Samedi 19 Janvier 2013
	****************************************************/
	class account_banned extends mvc_bdd  {
		public $id;
		public $bandate;
		public $unbandate;
		public $bannedby;
		public $banreason;
		public $active;
		
		public $BDD = 'auth';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_bandate($set_value,$id) {
			$this->edit(array('bandate' => $set_value),array('id' => $id ));
		}

		public function set_unbandate($set_value,$id) {
			$this->edit(array('unbandate' => $set_value),array('id' => $id ));
		}

		public function set_bannedby($set_value,$id) {
			$this->edit(array('bannedby' => $set_value),array('id' => $id ));
		}

		public function set_banreason($set_value,$id) {
			$this->edit(array('banreason' => $set_value),array('id' => $id ));
		}

		public function set_active($set_value,$id) {
			$this->edit(array('active' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('account_banned');
		}
		

		public function select_id($name) {
			return $this->select('account_banned','id="' . $name . '"');
		}

		public function select_bandate($name) {
			return $this->select('account_banned','bandate="' . $name . '"');
		}

		public function select_unbandate($name) {
			return $this->select('account_banned','unbandate="' . $name . '"');
		}

		public function select_bannedby($name) {
			return $this->select('account_banned','bannedby="' . $name . '"');
		}

		public function select_banreason($name) {
			return $this->select('account_banned','banreason="' . $name . '"');
		}

		public function select_active($name) {
			return $this->select('account_banned','active="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>