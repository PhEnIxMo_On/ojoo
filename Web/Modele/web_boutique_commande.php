<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mardi 25 Juin 2013
	****************************************************/
	class web_boutique_commande extends mvc_bdd  {
		public $id;
		public $idCompte;
		public $guidPerso;
		public $idWowhead;
		public $nbArticle;
		public $date;
		public $state;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function ajoutCommande($add_idCompte, $add_guidPerso, $add_idWowhead, $add_nbArticle, $add_date, $add_state, $idEntete) {
			$O = getOjoo();
			$O->bdd->site->query("INSERT INTO web_boutique_commande VALUES('', '" . $add_idCompte . "', '" . $add_idWowhead . "', '" . $add_nbArticle . "', '" . $add_date . "', '" . $add_state . "'
			,'" . $idEntete . "','" . $add_guidPerso . "')");
		}
		
		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_idCompte($set_value,$id) {
			$this->edit(array('idCompte' => $set_value),array('id' => $id ));
		}
		
		public function set_guidPerso($set_value,$id) {
			$this->edit(array('guidPerso' => $set_value),array('id' => $id ));
		}

		public function set_idWowhead($set_value,$id) {
			$this->edit(array('idWowhead' => $set_value),array('id' => $id ));
		}

		public function set_nbArticle($set_value,$id) {
			$this->edit(array('nbArticle' => $set_value),array('id' => $id ));
		}

		public function set_date($set_value,$id) {
			$this->edit(array('date' => $set_value),array('id' => $id ));
		}

		public function set_state($set_value,$id) {
			$this->edit(array('state' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('web_boutique_commande');
		}
		

		public function select_id($name) {
			return $this->select('web_boutique_commande','id="' . $name . '"');
		}

		public function select_idCompte($name) {
			return $this->select('web_boutique_commande','idCompte="' . $name . '"');
		}
		
		public function select_guidPerso($name) {
			return $this->select('web_boutique_commande','guidPerso="' . $name . '"');
		}

		public function select_idWowhead($name) {
			return $this->select('web_boutique_commande','idWowhead="' . $name . '"');
		}

		public function select_nbArticle($name) {
			return $this->select('web_boutique_commande','nbArticle="' . $name . '"');
		}

		public function select_date($name) {
			return $this->select('web_boutique_commande','date="' . $name . '"');
		}

		public function select_state($name) {
			return $this->select('web_boutique_commande','state="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>