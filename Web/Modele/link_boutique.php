<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Vendredi 26 Juillet 2013
	****************************************************/
	class link_boutique extends mvc_bdd  {
		public $id;
		public $idCompte;
		public $idWowhead;
		public $nbArticle;
		public $date;
		public $state;
		public $idEntete;
		public $guidPersonnage;
		
		public $BDD = 'auth';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
		
		public function ajoutCommande($accountId,$guidPerso,$idWoWhead,$qte,$date,$state,$idE) {
			$O = getOjoo();
			$O->bdd->auth->query("INSERT INTO link_boutique VALUES('','" . $accountId . "','" . $idWoWhead . "','" . $qte . "','" . $date . "',0,'" . $idE . "','" . $guidPerso . "')");
			return true;
		}

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_idCompte($set_value,$id) {
			$this->edit(array('idCompte' => $set_value),array('id' => $id ));
		}

		public function set_idWowhead($set_value,$id) {
			$this->edit(array('idWowhead' => $set_value),array('id' => $id ));
		}

		public function set_nbArticle($set_value,$id) {
			$this->edit(array('nbArticle' => $set_value),array('id' => $id ));
		}

		public function set_date($set_value,$id) {
			$this->edit(array('date' => $set_value),array('id' => $id ));
		}

		public function set_state($set_value,$id) {
			$this->edit(array('state' => $set_value),array('id' => $id ));
		}

		public function set_idEntete($set_value,$id) {
			$this->edit(array('idEntete' => $set_value),array('id' => $id ));
		}

		public function set_guidPersonnage($set_value,$id) {
			$this->edit(array('guidPersonnage' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('link_boutique');
		}
		

		public function select_id($name) {
			return $this->select('link_boutique','id="' . $name . '"');
		}

		public function select_idCompte($name) {
			return $this->select('link_boutique','idCompte="' . $name . '"');
		}

		public function select_idWowhead($name) {
			return $this->select('link_boutique','idWowhead="' . $name . '"');
		}

		public function select_nbArticle($name) {
			return $this->select('link_boutique','nbArticle="' . $name . '"');
		}

		public function select_date($name) {
			return $this->select('link_boutique','date="' . $name . '"');
		}

		public function select_state($name) {
			return $this->select('link_boutique','state="' . $name . '"');
		}

		public function select_idEntete($name) {
			return $this->select('link_boutique','idEntete="' . $name . '"');
		}

		public function select_guidPersonnage($name) {
			return $this->select('link_boutique','guidPersonnage="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>