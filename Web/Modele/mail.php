<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Lundi 29 Juillet 2013
	****************************************************/
	class mail extends mvc_bdd  {
		const MAIL_GM = 61;
		const MAIL_NORMAL = 41;
		const MESSAGE_NORMAL = 0;
		const MESSAGE_AUCTION = 3;
		const MAIL_TEMPLATE_ID = 0;
		
		public $id;
		public $messageType;
		public $stationery;
		public $mailTemplateId;
		public $sender;
		public $receiver;
		public $subject;
		public $body;
		public $has_items;
		public $expire_time;
		public $deliver_time;
		public $money;
		public $cod;
		public $checked;
		
		public $BDD = 'characters';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('mail');
		}
		
		public function getNextId() {
			$O = getOjoo();
			$id = $O->bdd->char->query("SELECT id FROM mail ORDER BY id DESC LIMIT 1")->fetch();
			return $id['id'] + 1;
		}
		
		public function createMail($type,$stationnery,$sender,$receiver,$subject,$body,$item = 0,$expire_time = 0,$deliver_time = 0,$money = 0,$cod = 0,$checked = 16) {
			$O = getOjoo();
			$O->bdd->char->query("INSERT INTO mail VALUES(" . $this->getNextId() . "," . $type . "," . $stationnery . "," . self::MAIL_TEMPLATE_ID . "," . $sender . "," . $receiver . ",'" . $subject . "','" . $body . "'," . $item . "," . $expire_time . "," . $deliver_time . "," . $money . "," . $cod . "," . $checked . ")");
			return true;
		}
		


	// Fonction de suppression : 	

	}
?>