<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Samedi 19 Janvier 2013
	****************************************************/
	class gm_tickets extends mvc_bdd  {
		public $ticketId;
		public $guid;
		public $name;
		public $message;
		public $createTime;
		public $mapId;
		public $posX;
		public $posY;
		public $posZ;
		public $lastModifiedTime;
		public $closedBy;
		public $assignedTo;
		public $comment;
		public $completed;
		public $escalated;
		public $viewed;
		
		public $BDD = 'char';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
		
		public function searchTicket(Sort $sort ) {
			$O = getOjoo();
                        //echo "SELECT * FROM gm_tickets " . $sort->sqlByActiveFilter();
			return $O->bdd->char->query("SELECT * FROM gm_tickets " . $sort->sqlByActiveFilter())->fetchAll();
		}

		public function set_ticketId($set_value,$id) {
			$this->edit(array('ticketId' => $set_value),array('ticketId' => $id ));
		}

		public function set_guid($set_value,$id) {
			$this->edit(array('guid' => $set_value),array('ticketId' => $id ));
		}

		public function set_name($set_value,$id) {
			$this->edit(array('name' => $set_value),array('ticketId' => $id ));
		}

		public function set_message($set_value,$id) {
			$this->edit(array('message' => $set_value),array('ticketId' => $id ));
		}

		public function set_createTime($set_value,$id) {
			$this->edit(array('createTime' => $set_value),array('ticketId' => $id ));
		}

		public function set_mapId($set_value,$id) {
			$this->edit(array('mapId' => $set_value),array('ticketId' => $id ));
		}

		public function set_posX($set_value,$id) {
			$this->edit(array('posX' => $set_value),array('ticketId' => $id ));
		}

		public function set_posY($set_value,$id) {
			$this->edit(array('posY' => $set_value),array('ticketId' => $id ));
		}

		public function set_posZ($set_value,$id) {
			$this->edit(array('posZ' => $set_value),array('ticketId' => $id ));
		}

		public function set_lastModifiedTime($set_value,$id) {
			$this->edit(array('lastModifiedTime' => $set_value),array('ticketId' => $id ));
		}

		public function set_closedBy($set_value,$id) {
			$this->edit(array('closedBy' => $set_value),array('ticketId' => $id ));
		}

		public function set_assignedTo($set_value,$id) {
			$this->edit(array('assignedTo' => $set_value),array('ticketId' => $id ));
		}

		public function set_comment($set_value,$id) {
			$this->edit(array('comment' => $set_value),array('ticketId' => $id ));
		}

		public function set_completed($set_value,$id) {
			$this->edit(array('completed' => $set_value),array('ticketId' => $id ));
		}

		public function set_escalated($set_value,$id) {
			$this->edit(array('escalated' => $set_value),array('ticketId' => $id ));
		}

		public function set_viewed($set_value,$id) {
			$this->edit(array('viewed' => $set_value),array('ticketId' => $id ));
		}

		
		// Fonction de sélection :
		
        public function selectTickets() {
            $O = getOjoo();
            return $O->bdd->char->query("SELECT * FROM gm_tickets WHERE closedBy=0 ORDER BY createTime")->fetchAll();
        }
                
		public function selectLastTickets($start, $end) {
			$O = getOjoo();
			return $O->bdd->char->query("SELECT * FROM gm_tickets ORDER BY createTime ASC LIMIT " .$start . "," .$end . "")->fetchAll();
		}	
		
		public function select_all() {
			return $this->select('gm_tickets');
		}
		

		public function select_ticketId($name) {
			return $this->select('gm_tickets','ticketId="' . $name . '"');
		}

		public function select_guid($name) {
			return $this->select('gm_tickets','guid="' . $name . '"');
		}

		public function select_name($name) {
			return $this->select('gm_tickets','name="' . $name . '"');
		}

		public function select_message($name) {
			return $this->select('gm_tickets','message="' . $name . '"');
		}

		public function select_createTime($name) {
			return $this->select('gm_tickets','createTime="' . $name . '"');
		}

		public function select_mapId($name) {
			return $this->select('gm_tickets','mapId="' . $name . '"');
		}

		public function select_posX($name) {
			return $this->select('gm_tickets','posX="' . $name . '"');
		}

		public function select_posY($name) {
			return $this->select('gm_tickets','posY="' . $name . '"');
		}

		public function select_posZ($name) {
			return $this->select('gm_tickets','posZ="' . $name . '"');
		}

		public function select_lastModifiedTime($name) {
			return $this->select('gm_tickets','lastModifiedTime="' . $name . '"');
		}

		public function select_closedBy($name) {
			return $this->select('gm_tickets','closedBy="' . $name . '"');
		}

		public function select_assignedTo($name) {
			return $this->select('gm_tickets','assignedTo="' . $name . '"');
		}

		public function select_comment($name) {
			return $this->select('gm_tickets','comment="' . $name . '"');
		}

		public function select_completed($name) {
			return $this->select('gm_tickets','completed="' . $name . '"');
		}

		public function select_escalated($name) {
			return $this->select('gm_tickets','escalated="' . $name . '"');
		}

		public function select_viewed($name) {
			return $this->select('gm_tickets','viewed="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function countAll() {
			$O = getOjoo();
			$nb = $O->bdd->char->query("SELECT COUNT(ticketId) AS nb FROM gm_tickets")->fetch();
			return $nb['nb'];
		}
	
		public function delete_id($id) {
			return $this->delete(array('ticketId' => $id));
		}

	}
?>