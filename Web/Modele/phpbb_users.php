<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mardi 23 Avril 2013
	****************************************************/
	class phpbb_users extends mvc_bdd  {
		public $user_id;
		public $user_type;
		public $group_id;
		public $user_permissions;
		public $user_perm_from;
		public $user_ip;
		public $user_regdate;
		public $username;
		public $username_clean;
		public $user_password;
		public $user_passchg;
		public $user_pass_convert;
		public $user_email;
		public $user_email_hash;
		public $user_birthday;
		public $user_lastvisit;
		public $user_lastmark;
		public $user_lastpost_time;
		public $user_lastpage;
		public $user_last_confirm_key;
		public $user_last_search;
		public $user_warnings;
		public $user_last_warning;
		public $user_login_attempts;
		public $user_inactive_reason;
		public $user_inactive_time;
		public $user_posts;
		public $user_lang;
		public $user_timezone;
		public $user_dst;
		public $user_dateformat;
		public $user_style;
		public $user_rank;
		public $user_colour;
		public $user_new_privmsg;
		public $user_unread_privmsg;
		public $user_last_privmsg;
		public $user_message_rules;
		public $user_full_folder;
		public $user_emailtime;
		public $user_topic_show_days;
		public $user_topic_sortby_type;
		public $user_topic_sortby_dir;
		public $user_post_show_days;
		public $user_post_sortby_type;
		public $user_post_sortby_dir;
		public $user_notify;
		public $user_notify_pm;
		public $user_notify_type;
		public $user_allow_pm;
		public $user_allow_viewonline;
		public $user_allow_viewemail;
		public $user_allow_massemail;
		public $user_options;
		public $user_avatar;
		public $user_avatar_type;
		public $user_avatar_width;
		public $user_avatar_height;
		public $user_sig;
		public $user_sig_bbcode_uid;
		public $user_sig_bbcode_bitfield;
		public $user_from;
		public $user_icq;
		public $user_aim;
		public $user_yim;
		public $user_msnm;
		public $user_jabber;
		public $user_website;
		public $user_occ;
		public $user_interests;
		public $user_actkey;
		public $user_newpasswd;
		public $user_form_salt;
		public $user_new;
		public $user_reminded;
		public $user_reminded_time;
		
		public $BDD = 'forum';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
                
                public function createWowAccount($username,$password) {
                    $O = getOjoo();
                    $testPseudo = $O->bdd->forum->query("SELECT COUNT(user_id) AS nb FROM phpbb_users WHERE username_clean='" . strtolower(trim($username)) . "'")->fetch();
                    if ($testPseudo['nb'] > 0) { return FALSE; }                    
                    $O->bdd->forum->query("INSERT INTO phpbb_users(user_type,group_id,user_ip,user_regdate,username,username_clean,user_password) VALUES(0,2,'" . $_SERVER['REMOTE_ADDR'] . "'," . time() . ",'" . $username . "','" . strtolower($username) . "','" . $password . "')");
                    $O->bdd->forum->query("INSERT INTO phpbb_user_group VALUES(2," . $O->bdd->forum->last_id() . ",0,0)");
                    return true;
                }
		
		// Fonction de mise à jour :

		public function set_user_id($set_value,$id) {
			$this->edit(array('user_id' => $set_value),array('id' => $id ));
		}

		public function set_user_type($set_value,$id) {
			$this->edit(array('user_type' => $set_value),array('id' => $id ));
		}

		public function set_group_id($set_value,$id) {
			$this->edit(array('group_id' => $set_value),array('id' => $id ));
		}

		public function set_user_permissions($set_value,$id) {
			$this->edit(array('user_permissions' => $set_value),array('id' => $id ));
		}

		public function set_user_perm_from($set_value,$id) {
			$this->edit(array('user_perm_from' => $set_value),array('id' => $id ));
		}

		public function set_user_ip($set_value,$id) {
			$this->edit(array('user_ip' => $set_value),array('id' => $id ));
		}

		public function set_user_regdate($set_value,$id) {
			$this->edit(array('user_regdate' => $set_value),array('id' => $id ));
		}

		public function set_username($set_value,$id) {
			$this->edit(array('username' => $set_value),array('id' => $id ));
		}

		public function set_username_clean($set_value,$id) {
			$this->edit(array('username_clean' => $set_value),array('id' => $id ));
		}

		public function set_user_password($set_value,$id) {
			$this->edit(array('user_password' => $set_value),array('id' => $id ));
		}

		public function set_user_passchg($set_value,$id) {
			$this->edit(array('user_passchg' => $set_value),array('id' => $id ));
		}

		public function set_user_pass_convert($set_value,$id) {
			$this->edit(array('user_pass_convert' => $set_value),array('id' => $id ));
		}

		public function set_user_email($set_value,$id) {
			$this->edit(array('user_email' => $set_value),array('id' => $id ));
		}

		public function set_user_email_hash($set_value,$id) {
			$this->edit(array('user_email_hash' => $set_value),array('id' => $id ));
		}

		public function set_user_birthday($set_value,$id) {
			$this->edit(array('user_birthday' => $set_value),array('id' => $id ));
		}

		public function set_user_lastvisit($set_value,$id) {
			$this->edit(array('user_lastvisit' => $set_value),array('id' => $id ));
		}

		public function set_user_lastmark($set_value,$id) {
			$this->edit(array('user_lastmark' => $set_value),array('id' => $id ));
		}

		public function set_user_lastpost_time($set_value,$id) {
			$this->edit(array('user_lastpost_time' => $set_value),array('id' => $id ));
		}

		public function set_user_lastpage($set_value,$id) {
			$this->edit(array('user_lastpage' => $set_value),array('id' => $id ));
		}

		public function set_user_last_confirm_key($set_value,$id) {
			$this->edit(array('user_last_confirm_key' => $set_value),array('id' => $id ));
		}

		public function set_user_last_search($set_value,$id) {
			$this->edit(array('user_last_search' => $set_value),array('id' => $id ));
		}

		public function set_user_warnings($set_value,$id) {
			$this->edit(array('user_warnings' => $set_value),array('id' => $id ));
		}

		public function set_user_last_warning($set_value,$id) {
			$this->edit(array('user_last_warning' => $set_value),array('id' => $id ));
		}

		public function set_user_login_attempts($set_value,$id) {
			$this->edit(array('user_login_attempts' => $set_value),array('id' => $id ));
		}

		public function set_user_inactive_reason($set_value,$id) {
			$this->edit(array('user_inactive_reason' => $set_value),array('id' => $id ));
		}

		public function set_user_inactive_time($set_value,$id) {
			$this->edit(array('user_inactive_time' => $set_value),array('id' => $id ));
		}

		public function set_user_posts($set_value,$id) {
			$this->edit(array('user_posts' => $set_value),array('id' => $id ));
		}

		public function set_user_lang($set_value,$id) {
			$this->edit(array('user_lang' => $set_value),array('id' => $id ));
		}

		public function set_user_timezone($set_value,$id) {
			$this->edit(array('user_timezone' => $set_value),array('id' => $id ));
		}

		public function set_user_dst($set_value,$id) {
			$this->edit(array('user_dst' => $set_value),array('id' => $id ));
		}

		public function set_user_dateformat($set_value,$id) {
			$this->edit(array('user_dateformat' => $set_value),array('id' => $id ));
		}

		public function set_user_style($set_value,$id) {
			$this->edit(array('user_style' => $set_value),array('id' => $id ));
		}

		public function set_user_rank($set_value,$id) {
			$this->edit(array('user_rank' => $set_value),array('id' => $id ));
		}

		public function set_user_colour($set_value,$id) {
			$this->edit(array('user_colour' => $set_value),array('id' => $id ));
		}

		public function set_user_new_privmsg($set_value,$id) {
			$this->edit(array('user_new_privmsg' => $set_value),array('id' => $id ));
		}

		public function set_user_unread_privmsg($set_value,$id) {
			$this->edit(array('user_unread_privmsg' => $set_value),array('id' => $id ));
		}

		public function set_user_last_privmsg($set_value,$id) {
			$this->edit(array('user_last_privmsg' => $set_value),array('id' => $id ));
		}

		public function set_user_message_rules($set_value,$id) {
			$this->edit(array('user_message_rules' => $set_value),array('id' => $id ));
		}

		public function set_user_full_folder($set_value,$id) {
			$this->edit(array('user_full_folder' => $set_value),array('id' => $id ));
		}

		public function set_user_emailtime($set_value,$id) {
			$this->edit(array('user_emailtime' => $set_value),array('id' => $id ));
		}

		public function set_user_topic_show_days($set_value,$id) {
			$this->edit(array('user_topic_show_days' => $set_value),array('id' => $id ));
		}

		public function set_user_topic_sortby_type($set_value,$id) {
			$this->edit(array('user_topic_sortby_type' => $set_value),array('id' => $id ));
		}

		public function set_user_topic_sortby_dir($set_value,$id) {
			$this->edit(array('user_topic_sortby_dir' => $set_value),array('id' => $id ));
		}

		public function set_user_post_show_days($set_value,$id) {
			$this->edit(array('user_post_show_days' => $set_value),array('id' => $id ));
		}

		public function set_user_post_sortby_type($set_value,$id) {
			$this->edit(array('user_post_sortby_type' => $set_value),array('id' => $id ));
		}

		public function set_user_post_sortby_dir($set_value,$id) {
			$this->edit(array('user_post_sortby_dir' => $set_value),array('id' => $id ));
		}

		public function set_user_notify($set_value,$id) {
			$this->edit(array('user_notify' => $set_value),array('id' => $id ));
		}

		public function set_user_notify_pm($set_value,$id) {
			$this->edit(array('user_notify_pm' => $set_value),array('id' => $id ));
		}

		public function set_user_notify_type($set_value,$id) {
			$this->edit(array('user_notify_type' => $set_value),array('id' => $id ));
		}

		public function set_user_allow_pm($set_value,$id) {
			$this->edit(array('user_allow_pm' => $set_value),array('id' => $id ));
		}

		public function set_user_allow_viewonline($set_value,$id) {
			$this->edit(array('user_allow_viewonline' => $set_value),array('id' => $id ));
		}

		public function set_user_allow_viewemail($set_value,$id) {
			$this->edit(array('user_allow_viewemail' => $set_value),array('id' => $id ));
		}

		public function set_user_allow_massemail($set_value,$id) {
			$this->edit(array('user_allow_massemail' => $set_value),array('id' => $id ));
		}

		public function set_user_options($set_value,$id) {
			$this->edit(array('user_options' => $set_value),array('id' => $id ));
		}

		public function set_user_avatar($set_value,$id) {
			$this->edit(array('user_avatar' => $set_value),array('id' => $id ));
		}

		public function set_user_avatar_type($set_value,$id) {
			$this->edit(array('user_avatar_type' => $set_value),array('id' => $id ));
		}

		public function set_user_avatar_width($set_value,$id) {
			$this->edit(array('user_avatar_width' => $set_value),array('id' => $id ));
		}

		public function set_user_avatar_height($set_value,$id) {
			$this->edit(array('user_avatar_height' => $set_value),array('id' => $id ));
		}

		public function set_user_sig($set_value,$id) {
			$this->edit(array('user_sig' => $set_value),array('id' => $id ));
		}

		public function set_user_sig_bbcode_uid($set_value,$id) {
			$this->edit(array('user_sig_bbcode_uid' => $set_value),array('id' => $id ));
		}

		public function set_user_sig_bbcode_bitfield($set_value,$id) {
			$this->edit(array('user_sig_bbcode_bitfield' => $set_value),array('id' => $id ));
		}

		public function set_user_from($set_value,$id) {
			$this->edit(array('user_from' => $set_value),array('id' => $id ));
		}

		public function set_user_icq($set_value,$id) {
			$this->edit(array('user_icq' => $set_value),array('id' => $id ));
		}

		public function set_user_aim($set_value,$id) {
			$this->edit(array('user_aim' => $set_value),array('id' => $id ));
		}

		public function set_user_yim($set_value,$id) {
			$this->edit(array('user_yim' => $set_value),array('id' => $id ));
		}

		public function set_user_msnm($set_value,$id) {
			$this->edit(array('user_msnm' => $set_value),array('id' => $id ));
		}

		public function set_user_jabber($set_value,$id) {
			$this->edit(array('user_jabber' => $set_value),array('id' => $id ));
		}

		public function set_user_website($set_value,$id) {
			$this->edit(array('user_website' => $set_value),array('id' => $id ));
		}

		public function set_user_occ($set_value,$id) {
			$this->edit(array('user_occ' => $set_value),array('id' => $id ));
		}

		public function set_user_interests($set_value,$id) {
			$this->edit(array('user_interests' => $set_value),array('id' => $id ));
		}

		public function set_user_actkey($set_value,$id) {
			$this->edit(array('user_actkey' => $set_value),array('id' => $id ));
		}

		public function set_user_newpasswd($set_value,$id) {
			$this->edit(array('user_newpasswd' => $set_value),array('id' => $id ));
		}

		public function set_user_form_salt($set_value,$id) {
			$this->edit(array('user_form_salt' => $set_value),array('id' => $id ));
		}

		public function set_user_new($set_value,$id) {
			$this->edit(array('user_new' => $set_value),array('id' => $id ));
		}

		public function set_user_reminded($set_value,$id) {
			$this->edit(array('user_reminded' => $set_value),array('id' => $id ));
		}

		public function set_user_reminded_time($set_value,$id) {
			$this->edit(array('user_reminded_time' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('phpbb_users');
		}
		

		public function select_user_id($name) {
			return $this->select('phpbb_users','user_id="' . $name . '"');
		}

		public function select_user_type($name) {
			return $this->select('phpbb_users','user_type="' . $name . '"');
		}

		public function select_group_id($name) {
			return $this->select('phpbb_users','group_id="' . $name . '"');
		}

		public function select_user_permissions($name) {
			return $this->select('phpbb_users','user_permissions="' . $name . '"');
		}

		public function select_user_perm_from($name) {
			return $this->select('phpbb_users','user_perm_from="' . $name . '"');
		}

		public function select_user_ip($name) {
			return $this->select('phpbb_users','user_ip="' . $name . '"');
		}

		public function select_user_regdate($name) {
			return $this->select('phpbb_users','user_regdate="' . $name . '"');
		}

		public function select_username($name) {
			return $this->select('phpbb_users','username="' . $name . '"');
		}

		public function select_username_clean($name) {
			return $this->select('phpbb_users','username_clean="' . $name . '"');
		}

		public function select_user_password($name) {
			return $this->select('phpbb_users','user_password="' . $name . '"');
		}

		public function select_user_passchg($name) {
			return $this->select('phpbb_users','user_passchg="' . $name . '"');
		}

		public function select_user_pass_convert($name) {
			return $this->select('phpbb_users','user_pass_convert="' . $name . '"');
		}

		public function select_user_email($name) {
			return $this->select('phpbb_users','user_email="' . $name . '"');
		}

		public function select_user_email_hash($name) {
			return $this->select('phpbb_users','user_email_hash="' . $name . '"');
		}

		public function select_user_birthday($name) {
			return $this->select('phpbb_users','user_birthday="' . $name . '"');
		}

		public function select_user_lastvisit($name) {
			return $this->select('phpbb_users','user_lastvisit="' . $name . '"');
		}

		public function select_user_lastmark($name) {
			return $this->select('phpbb_users','user_lastmark="' . $name . '"');
		}

		public function select_user_lastpost_time($name) {
			return $this->select('phpbb_users','user_lastpost_time="' . $name . '"');
		}

		public function select_user_lastpage($name) {
			return $this->select('phpbb_users','user_lastpage="' . $name . '"');
		}

		public function select_user_last_confirm_key($name) {
			return $this->select('phpbb_users','user_last_confirm_key="' . $name . '"');
		}

		public function select_user_last_search($name) {
			return $this->select('phpbb_users','user_last_search="' . $name . '"');
		}

		public function select_user_warnings($name) {
			return $this->select('phpbb_users','user_warnings="' . $name . '"');
		}

		public function select_user_last_warning($name) {
			return $this->select('phpbb_users','user_last_warning="' . $name . '"');
		}

		public function select_user_login_attempts($name) {
			return $this->select('phpbb_users','user_login_attempts="' . $name . '"');
		}

		public function select_user_inactive_reason($name) {
			return $this->select('phpbb_users','user_inactive_reason="' . $name . '"');
		}

		public function select_user_inactive_time($name) {
			return $this->select('phpbb_users','user_inactive_time="' . $name . '"');
		}

		public function select_user_posts($name) {
			return $this->select('phpbb_users','user_posts="' . $name . '"');
		}

		public function select_user_lang($name) {
			return $this->select('phpbb_users','user_lang="' . $name . '"');
		}

		public function select_user_timezone($name) {
			return $this->select('phpbb_users','user_timezone="' . $name . '"');
		}

		public function select_user_dst($name) {
			return $this->select('phpbb_users','user_dst="' . $name . '"');
		}

		public function select_user_dateformat($name) {
			return $this->select('phpbb_users','user_dateformat="' . $name . '"');
		}

		public function select_user_style($name) {
			return $this->select('phpbb_users','user_style="' . $name . '"');
		}

		public function select_user_rank($name) {
			return $this->select('phpbb_users','user_rank="' . $name . '"');
		}

		public function select_user_colour($name) {
			return $this->select('phpbb_users','user_colour="' . $name . '"');
		}

		public function select_user_new_privmsg($name) {
			return $this->select('phpbb_users','user_new_privmsg="' . $name . '"');
		}

		public function select_user_unread_privmsg($name) {
			return $this->select('phpbb_users','user_unread_privmsg="' . $name . '"');
		}

		public function select_user_last_privmsg($name) {
			return $this->select('phpbb_users','user_last_privmsg="' . $name . '"');
		}

		public function select_user_message_rules($name) {
			return $this->select('phpbb_users','user_message_rules="' . $name . '"');
		}

		public function select_user_full_folder($name) {
			return $this->select('phpbb_users','user_full_folder="' . $name . '"');
		}

		public function select_user_emailtime($name) {
			return $this->select('phpbb_users','user_emailtime="' . $name . '"');
		}

		public function select_user_topic_show_days($name) {
			return $this->select('phpbb_users','user_topic_show_days="' . $name . '"');
		}

		public function select_user_topic_sortby_type($name) {
			return $this->select('phpbb_users','user_topic_sortby_type="' . $name . '"');
		}

		public function select_user_topic_sortby_dir($name) {
			return $this->select('phpbb_users','user_topic_sortby_dir="' . $name . '"');
		}

		public function select_user_post_show_days($name) {
			return $this->select('phpbb_users','user_post_show_days="' . $name . '"');
		}

		public function select_user_post_sortby_type($name) {
			return $this->select('phpbb_users','user_post_sortby_type="' . $name . '"');
		}

		public function select_user_post_sortby_dir($name) {
			return $this->select('phpbb_users','user_post_sortby_dir="' . $name . '"');
		}

		public function select_user_notify($name) {
			return $this->select('phpbb_users','user_notify="' . $name . '"');
		}

		public function select_user_notify_pm($name) {
			return $this->select('phpbb_users','user_notify_pm="' . $name . '"');
		}

		public function select_user_notify_type($name) {
			return $this->select('phpbb_users','user_notify_type="' . $name . '"');
		}

		public function select_user_allow_pm($name) {
			return $this->select('phpbb_users','user_allow_pm="' . $name . '"');
		}

		public function select_user_allow_viewonline($name) {
			return $this->select('phpbb_users','user_allow_viewonline="' . $name . '"');
		}

		public function select_user_allow_viewemail($name) {
			return $this->select('phpbb_users','user_allow_viewemail="' . $name . '"');
		}

		public function select_user_allow_massemail($name) {
			return $this->select('phpbb_users','user_allow_massemail="' . $name . '"');
		}

		public function select_user_options($name) {
			return $this->select('phpbb_users','user_options="' . $name . '"');
		}

		public function select_user_avatar($name) {
			return $this->select('phpbb_users','user_avatar="' . $name . '"');
		}

		public function select_user_avatar_type($name) {
			return $this->select('phpbb_users','user_avatar_type="' . $name . '"');
		}

		public function select_user_avatar_width($name) {
			return $this->select('phpbb_users','user_avatar_width="' . $name . '"');
		}

		public function select_user_avatar_height($name) {
			return $this->select('phpbb_users','user_avatar_height="' . $name . '"');
		}

		public function select_user_sig($name) {
			return $this->select('phpbb_users','user_sig="' . $name . '"');
		}

		public function select_user_sig_bbcode_uid($name) {
			return $this->select('phpbb_users','user_sig_bbcode_uid="' . $name . '"');
		}

		public function select_user_sig_bbcode_bitfield($name) {
			return $this->select('phpbb_users','user_sig_bbcode_bitfield="' . $name . '"');
		}

		public function select_user_from($name) {
			return $this->select('phpbb_users','user_from="' . $name . '"');
		}

		public function select_user_icq($name) {
			return $this->select('phpbb_users','user_icq="' . $name . '"');
		}

		public function select_user_aim($name) {
			return $this->select('phpbb_users','user_aim="' . $name . '"');
		}

		public function select_user_yim($name) {
			return $this->select('phpbb_users','user_yim="' . $name . '"');
		}

		public function select_user_msnm($name) {
			return $this->select('phpbb_users','user_msnm="' . $name . '"');
		}

		public function select_user_jabber($name) {
			return $this->select('phpbb_users','user_jabber="' . $name . '"');
		}

		public function select_user_website($name) {
			return $this->select('phpbb_users','user_website="' . $name . '"');
		}

		public function select_user_occ($name) {
			return $this->select('phpbb_users','user_occ="' . $name . '"');
		}

		public function select_user_interests($name) {
			return $this->select('phpbb_users','user_interests="' . $name . '"');
		}

		public function select_user_actkey($name) {
			return $this->select('phpbb_users','user_actkey="' . $name . '"');
		}

		public function select_user_newpasswd($name) {
			return $this->select('phpbb_users','user_newpasswd="' . $name . '"');
		}

		public function select_user_form_salt($name) {
			return $this->select('phpbb_users','user_form_salt="' . $name . '"');
		}

		public function select_user_new($name) {
			return $this->select('phpbb_users','user_new="' . $name . '"');
		}

		public function select_user_reminded($name) {
			return $this->select('phpbb_users','user_reminded="' . $name . '"');
		}

		public function select_user_reminded_time($name) {
			return $this->select('phpbb_users','user_reminded_time="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>