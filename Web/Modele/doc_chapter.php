<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Lundi 22 Juillet 2013
	****************************************************/
	class doc_chapter extends mvc_bdd  {
		public $id;
		public $id_cat;
		public $name;
		public $description;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_id_cat($set_value,$id) {
			$this->edit(array('id_cat' => $set_value),array('id' => $id ));
		}

		public function set_name($set_value,$id) {
			$this->edit(array('name' => $set_value),array('id' => $id ));
		}

		public function set_description($set_value,$id) {
			$this->edit(array('description' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('doc_chapter');
		}
		

		public function select_id($name) {
			return $this->select('doc_chapter','id="' . $name . '"');
		}

		public function select_id_cat($name) {
			return $this->select('doc_chapter','id_cat="' . $name . '"');
		}

		public function select_name($name) {
			return $this->select('doc_chapter','name="' . $name . '"');
		}

		public function select_description($name) {
			return $this->select('doc_chapter','description="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>