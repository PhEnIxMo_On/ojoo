<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mardi 12 Fevrier 2013
	****************************************************/
	class task extends mvc_bdd  {
		public $id;
		public $userId;
		public $title;
		public $description;
		public $assignedTo;
		public $state;
		public $dateCrea;
		public $dateEnd;
		public $realDateEnd;
		
		public $BDD = 'world';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_userId($set_value,$id) {
			$this->edit(array('userId' => $set_value),array('id' => $id ));
		}

		public function set_title($set_value,$id) {
			$this->edit(array('title' => $set_value),array('id' => $id ));
		}

		public function set_description($set_value,$id) {
			$this->edit(array('description' => $set_value),array('id' => $id ));
		}

		public function set_assignedTo($set_value,$id) {
			$this->edit(array('assignedTo' => $set_value),array('id' => $id ));
		}

		public function set_state($set_value,$id) {
			$this->edit(array('state' => $set_value),array('id' => $id ));
		}

		public function set_dateCrea($set_value,$id) {
			$this->edit(array('dateCrea' => $set_value),array('id' => $id ));
		}

		public function set_dateEnd($set_value,$id) {
			$this->edit(array('dateEnd' => $set_value),array('id' => $id ));
		}

		public function set_realDateEnd($set_value,$id) {
			$this->edit(array('realDateEnd' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('task');
		}
		

		public function select_id($name) {
			return $this->select('task','id="' . $name . '"');
		}

		public function select_userId($name) {
			return $this->select('task','userId="' . $name . '"');
		}

		public function select_title($name) {
			return $this->select('task','title="' . $name . '"');
		}

		public function select_description($name) {
			return $this->select('task','description="' . $name . '"');
		}

		public function select_assignedTo($name) {
			return $this->select('task','assignedTo="' . $name . '"');
		}

		public function select_state($name) {
			return $this->select('task','state="' . $name . '"');
		}

		public function select_dateCrea($name) {
			return $this->select('task','dateCrea="' . $name . '"');
		}

		public function select_dateEnd($name) {
			return $this->select('task','dateEnd="' . $name . '"');
		}

		public function select_realDateEnd($name) {
			return $this->select('task','realDateEnd="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>