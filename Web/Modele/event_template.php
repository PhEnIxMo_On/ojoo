<?php
	/***************************************************
	* Modele g�n�r� via Ojoo.
	* Le : Mercredi 2 Mai 2012
	****************************************************/
	class event_template extends mvc_bdd  {
		public $id;
		public $nom;
		public $is_trig;
		public $context;
		public $repeat2;
		public $article;
		public $liens = array(
		);
		public $actionsLiens = array(
			"onDelete" => false,
			"onSelect" => false
		);
		
		public $BDD = 'ojoo';
		
		// Fonction de mise � jour :
		public function selectOnLinks() {
			var_dump($this->select('event_template'));
		}
		
		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_nom($set_value,$id) {
			$this->edit(array('nom' => $set_value),array('id' => $id ));
		}

		public function set_is_trig($set_value,$id) {
			$this->edit(array('is_trig' => $set_value),array('id' => $id ));
		}

		public function set_context($set_value,$id) {
			$this->edit(array('context' => $set_value),array('id' => $id ));
		}

		public function set_repeat2($set_value,$id) {
			$this->edit(array('repeat2' => $set_value),array('id' => $id ));
		}
		public function set_all_trig() {
			$O = $GLOBALS['O'];
			$O->Bdd->ojoo->query("UDPATE event_template SET is_trig=0");
		}

		
		// Fonction de s�lection :
		public function select_all() {
			return $this->select('event_template');
		}
		

		public function select_id($name) {
			return $this->select('event_template','id="' . $name . '"');
		}

		public function select_nom($name) {
			return $this->select('event_template','nom="' . $name . '"');
		}

		public function select_is_trig($name) {
			return $this->select('event_template','is_trig="' . $name . '"');
		}

		public function select_context($name) {
			return $this->select('event_template','context="' . $name . '"');
		}

		public function select_repeat2($name) {
			return $this->select('event_template','repeat2="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>