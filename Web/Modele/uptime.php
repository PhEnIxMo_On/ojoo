<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mercredi 24 Avril 2013
	****************************************************/
	class uptime extends mvc_bdd  {
		public $realmid;
		public $starttime;
		public $uptime;
		public $maxplayers;
		public $revision;

		
		public $BDD = 'auth';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

                public function getUptime() {
                    $O = getOjoo();
                    $uptime = $O->bdd->auth->query("SELECT starttime FROM uptime ORDER BY starttime DESC LIMIT 0,1")->fetch();
                    $time = time() - $uptime['starttime'];
                    return $time;
                }
                
		public function set_realmid($set_value,$id) {
			$this->edit(array('realmid' => $set_value),array('id' => $id ));
		}

		public function set_starttime($set_value,$id) {
			$this->edit(array('starttime' => $set_value),array('id' => $id ));
		}

		public function set_uptime($set_value,$id) {
			$this->edit(array('uptime' => $set_value),array('id' => $id ));
		}

		public function set_maxplayers($set_value,$id) {
			$this->edit(array('maxplayers' => $set_value),array('id' => $id ));
		}

		public function set_revision($set_value,$id) {
			$this->edit(array('revision' => $set_value),array('id' => $id ));
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>