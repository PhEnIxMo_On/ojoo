<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Jeudi 18 Juillet 2013
	****************************************************/
	class item_template extends mvc_bdd  {
		public $entry;
		public $class;
		public $subclass;
		public $unk0;
		public $name;
		public $displayid;
		public $Quality;
		public $Flags;
		public $FlagsExtra;
		public $BuyCount;
		public $BuyPrice;
		public $SellPrice;
		public $InventoryType;
		public $AllowableClass;
		public $AllowableRace;
		public $ItemLevel;
		public $RequiredLevel;
		public $RequiredSkill;
		public $RequiredSkillRank;
		public $requiredspell;
		public $requiredhonorrank;
		public $RequiredCityRank;
		public $RequiredReputationFaction;
		public $RequiredReputationRank;
		public $maxcount;
		public $stackable;
		public $ContainerSlots;
		public $StatsCount;
		public $stat_type1;
		public $stat_value1;
		public $stat_type2;
		public $stat_value2;
		public $stat_type3;
		public $stat_value3;
		public $stat_type4;
		public $stat_value4;
		public $stat_type5;
		public $stat_value5;
		public $stat_type6;
		public $stat_value6;
		public $stat_type7;
		public $stat_value7;
		public $stat_type8;
		public $stat_value8;
		public $stat_type9;
		public $stat_value9;
		public $stat_type10;
		public $stat_value10;
		public $ScalingStatDistribution;
		public $ScalingStatValue;
		public $dmg_min1;
		public $dmg_max1;
		public $dmg_type1;
		public $dmg_min2;
		public $dmg_max2;
		public $dmg_type2;
		public $armor;
		public $holy_res;
		public $fire_res;
		public $nature_res;
		public $frost_res;
		public $shadow_res;
		public $arcane_res;
		public $delay;
		public $ammo_type;
		public $RangedModRange;
		public $spellid_1;
		public $spelltrigger_1;
		public $spellcharges_1;
		public $spellppmRate_1;
		public $spellcooldown_1;
		public $spellcategory_1;
		public $spellcategorycooldown_1;
		public $spellid_2;
		public $spelltrigger_2;
		public $spellcharges_2;
		public $spellppmRate_2;
		public $spellcooldown_2;
		public $spellcategory_2;
		public $spellcategorycooldown_2;
		public $spellid_3;
		public $spelltrigger_3;
		public $spellcharges_3;
		public $spellppmRate_3;
		public $spellcooldown_3;
		public $spellcategory_3;
		public $spellcategorycooldown_3;
		public $spellid_4;
		public $spelltrigger_4;
		public $spellcharges_4;
		public $spellppmRate_4;
		public $spellcooldown_4;
		public $spellcategory_4;
		public $spellcategorycooldown_4;
		public $spellid_5;
		public $spelltrigger_5;
		public $spellcharges_5;
		public $spellppmRate_5;
		public $spellcooldown_5;
		public $spellcategory_5;
		public $spellcategorycooldown_5;
		public $bonding;
		public $description;
		public $PageText;
		public $LanguageID;
		public $PageMaterial;
		public $startquest;
		public $lockid;
		public $Material;
		public $sheath;
		public $RandomProperty;
		public $RandomSuffix;
		public $block;
		public $itemset;
		public $MaxDurability;
		public $area;
		public $Map;
		public $BagFamily;
		public $TotemCategory;
		public $socketColor_1;
		public $socketContent_1;
		public $socketColor_2;
		public $socketContent_2;
		public $socketColor_3;
		public $socketContent_3;
		public $socketBonus;
		public $GemProperties;
		public $RequiredDisenchantSkill;
		public $ArmorDamageModifier;
		public $Duration;
		public $ItemLimitCategory;
		public $HolidayId;
		public $ScriptName;
		public $DisenchantID;
		public $FoodType;
		public $minMoneyLoot;
		public $maxMoneyLoot;
		public $WDBVerified;
		
		public $BDD = 'world';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
		
		public function getStack($entry) {
			$O = getOjoo();
			$item = $O->bdd->world->query("SELECT stackable FROM item_template WHERE entry=" . $entry)->fetch();
			if ($item != null) {
				return $item['stackable'];
			} else return false;
		}

		public function set_entry($set_value,$id) {
			$this->edit(array('entry' => $set_value),array('entry' => $id ));
		}

		public function set_class($set_value,$id) {
			$this->edit(array('class' => $set_value),array('entry' => $id ));
		}

		public function set_subclass($set_value,$id) {
			$this->edit(array('subclass' => $set_value),array('entry' => $id ));
		}

		public function set_unk0($set_value,$id) {
			$this->edit(array('unk0' => $set_value),array('entry' => $id ));
		}

		public function set_name($set_value,$id) {
			$this->edit(array('name' => $set_value),array('entry' => $id ));
		}

		public function set_displayid($set_value,$id) {
			$this->edit(array('displayid' => $set_value),array('entry' => $id ));
		}

		public function set_Quality($set_value,$id) {
			$this->edit(array('Quality' => $set_value),array('entry' => $id ));
		}

		public function set_Flags($set_value,$id) {
			$this->edit(array('Flags' => $set_value),array('entry' => $id ));
		}

		public function set_FlagsExtra($set_value,$id) {
			$this->edit(array('FlagsExtra' => $set_value),array('entry' => $id ));
		}

		public function set_BuyCount($set_value,$id) {
			$this->edit(array('BuyCount' => $set_value),array('entry' => $id ));
		}

		public function set_BuyPrice($set_value,$id) {
			$this->edit(array('BuyPrice' => $set_value),array('entry' => $id ));
		}

		public function set_SellPrice($set_value,$id) {
			$this->edit(array('SellPrice' => $set_value),array('entry' => $id ));
		}

		public function set_InventoryType($set_value,$id) {
			$this->edit(array('InventoryType' => $set_value),array('entry' => $id ));
		}

		public function set_AllowableClass($set_value,$id) {
			$this->edit(array('AllowableClass' => $set_value),array('entry' => $id ));
		}

		public function set_AllowableRace($set_value,$id) {
			$this->edit(array('AllowableRace' => $set_value),array('entry' => $id ));
		}

		public function set_ItemLevel($set_value,$id) {
			$this->edit(array('ItemLevel' => $set_value),array('entry' => $id ));
		}

		public function set_RequiredLevel($set_value,$id) {
			$this->edit(array('RequiredLevel' => $set_value),array('entry' => $id ));
		}

		public function set_RequiredSkill($set_value,$id) {
			$this->edit(array('RequiredSkill' => $set_value),array('entry' => $id ));
		}

		public function set_RequiredSkillRank($set_value,$id) {
			$this->edit(array('RequiredSkillRank' => $set_value),array('entry' => $id ));
		}

		public function set_requiredspell($set_value,$id) {
			$this->edit(array('requiredspell' => $set_value),array('entry' => $id ));
		}

		public function set_requiredhonorrank($set_value,$id) {
			$this->edit(array('requiredhonorrank' => $set_value),array('entry' => $id ));
		}

		public function set_RequiredCityRank($set_value,$id) {
			$this->edit(array('RequiredCityRank' => $set_value),array('entry' => $id ));
		}

		public function set_RequiredReputationFaction($set_value,$id) {
			$this->edit(array('RequiredReputationFaction' => $set_value),array('entry' => $id ));
		}

		public function set_RequiredReputationRank($set_value,$id) {
			$this->edit(array('RequiredReputationRank' => $set_value),array('entry' => $id ));
		}

		public function set_maxcount($set_value,$id) {
			$this->edit(array('maxcount' => $set_value),array('entry' => $id ));
		}

		public function set_stackable($set_value,$id) {
			$this->edit(array('stackable' => $set_value),array('entry' => $id ));
		}

		public function set_ContainerSlots($set_value,$id) {
			$this->edit(array('ContainerSlots' => $set_value),array('entry' => $id ));
		}

		public function set_StatsCount($set_value,$id) {
			$this->edit(array('StatsCount' => $set_value),array('entry' => $id ));
		}

		public function set_stat_type1($set_value,$id) {
			$this->edit(array('stat_type1' => $set_value),array('entry' => $id ));
		}

		public function set_stat_value1($set_value,$id) {
			$this->edit(array('stat_value1' => $set_value),array('entry' => $id ));
		}

		public function set_stat_type2($set_value,$id) {
			$this->edit(array('stat_type2' => $set_value),array('entry' => $id ));
		}

		public function set_stat_value2($set_value,$id) {
			$this->edit(array('stat_value2' => $set_value),array('entry' => $id ));
		}

		public function set_stat_type3($set_value,$id) {
			$this->edit(array('stat_type3' => $set_value),array('entry' => $id ));
		}

		public function set_stat_value3($set_value,$id) {
			$this->edit(array('stat_value3' => $set_value),array('entry' => $id ));
		}

		public function set_stat_type4($set_value,$id) {
			$this->edit(array('stat_type4' => $set_value),array('entry' => $id ));
		}

		public function set_stat_value4($set_value,$id) {
			$this->edit(array('stat_value4' => $set_value),array('entry' => $id ));
		}

		public function set_stat_type5($set_value,$id) {
			$this->edit(array('stat_type5' => $set_value),array('entry' => $id ));
		}

		public function set_stat_value5($set_value,$id) {
			$this->edit(array('stat_value5' => $set_value),array('entry' => $id ));
		}

		public function set_stat_type6($set_value,$id) {
			$this->edit(array('stat_type6' => $set_value),array('entry' => $id ));
		}

		public function set_stat_value6($set_value,$id) {
			$this->edit(array('stat_value6' => $set_value),array('entry' => $id ));
		}

		public function set_stat_type7($set_value,$id) {
			$this->edit(array('stat_type7' => $set_value),array('entry' => $id ));
		}

		public function set_stat_value7($set_value,$id) {
			$this->edit(array('stat_value7' => $set_value),array('entry' => $id ));
		}

		public function set_stat_type8($set_value,$id) {
			$this->edit(array('stat_type8' => $set_value),array('entry' => $id ));
		}

		public function set_stat_value8($set_value,$id) {
			$this->edit(array('stat_value8' => $set_value),array('entry' => $id ));
		}

		public function set_stat_type9($set_value,$id) {
			$this->edit(array('stat_type9' => $set_value),array('entry' => $id ));
		}

		public function set_stat_value9($set_value,$id) {
			$this->edit(array('stat_value9' => $set_value),array('entry' => $id ));
		}

		public function set_stat_type10($set_value,$id) {
			$this->edit(array('stat_type10' => $set_value),array('entry' => $id ));
		}

		public function set_stat_value10($set_value,$id) {
			$this->edit(array('stat_value10' => $set_value),array('entry' => $id ));
		}

		public function set_ScalingStatDistribution($set_value,$id) {
			$this->edit(array('ScalingStatDistribution' => $set_value),array('entry' => $id ));
		}

		public function set_ScalingStatValue($set_value,$id) {
			$this->edit(array('ScalingStatValue' => $set_value),array('entry' => $id ));
		}

		public function set_dmg_min1($set_value,$id) {
			$this->edit(array('dmg_min1' => $set_value),array('entry' => $id ));
		}

		public function set_dmg_max1($set_value,$id) {
			$this->edit(array('dmg_max1' => $set_value),array('entry' => $id ));
		}

		public function set_dmg_type1($set_value,$id) {
			$this->edit(array('dmg_type1' => $set_value),array('entry' => $id ));
		}

		public function set_dmg_min2($set_value,$id) {
			$this->edit(array('dmg_min2' => $set_value),array('entry' => $id ));
		}

		public function set_dmg_max2($set_value,$id) {
			$this->edit(array('dmg_max2' => $set_value),array('entry' => $id ));
		}

		public function set_dmg_type2($set_value,$id) {
			$this->edit(array('dmg_type2' => $set_value),array('entry' => $id ));
		}

		public function set_armor($set_value,$id) {
			$this->edit(array('armor' => $set_value),array('entry' => $id ));
		}

		public function set_holy_res($set_value,$id) {
			$this->edit(array('holy_res' => $set_value),array('entry' => $id ));
		}

		public function set_fire_res($set_value,$id) {
			$this->edit(array('fire_res' => $set_value),array('entry' => $id ));
		}

		public function set_nature_res($set_value,$id) {
			$this->edit(array('nature_res' => $set_value),array('entry' => $id ));
		}

		public function set_frost_res($set_value,$id) {
			$this->edit(array('frost_res' => $set_value),array('entry' => $id ));
		}

		public function set_shadow_res($set_value,$id) {
			$this->edit(array('shadow_res' => $set_value),array('entry' => $id ));
		}

		public function set_arcane_res($set_value,$id) {
			$this->edit(array('arcane_res' => $set_value),array('entry' => $id ));
		}

		public function set_delay($set_value,$id) {
			$this->edit(array('delay' => $set_value),array('entry' => $id ));
		}

		public function set_ammo_type($set_value,$id) {
			$this->edit(array('ammo_type' => $set_value),array('entry' => $id ));
		}

		public function set_RangedModRange($set_value,$id) {
			$this->edit(array('RangedModRange' => $set_value),array('entry' => $id ));
		}

		public function set_spellid_1($set_value,$id) {
			$this->edit(array('spellid_1' => $set_value),array('entry' => $id ));
		}

		public function set_spelltrigger_1($set_value,$id) {
			$this->edit(array('spelltrigger_1' => $set_value),array('entry' => $id ));
		}

		public function set_spellcharges_1($set_value,$id) {
			$this->edit(array('spellcharges_1' => $set_value),array('entry' => $id ));
		}

		public function set_spellppmRate_1($set_value,$id) {
			$this->edit(array('spellppmRate_1' => $set_value),array('entry' => $id ));
		}

		public function set_spellcooldown_1($set_value,$id) {
			$this->edit(array('spellcooldown_1' => $set_value),array('entry' => $id ));
		}

		public function set_spellcategory_1($set_value,$id) {
			$this->edit(array('spellcategory_1' => $set_value),array('entry' => $id ));
		}

		public function set_spellcategorycooldown_1($set_value,$id) {
			$this->edit(array('spellcategorycooldown_1' => $set_value),array('entry' => $id ));
		}

		public function set_spellid_2($set_value,$id) {
			$this->edit(array('spellid_2' => $set_value),array('entry' => $id ));
		}

		public function set_spelltrigger_2($set_value,$id) {
			$this->edit(array('spelltrigger_2' => $set_value),array('entry' => $id ));
		}

		public function set_spellcharges_2($set_value,$id) {
			$this->edit(array('spellcharges_2' => $set_value),array('entry' => $id ));
		}

		public function set_spellppmRate_2($set_value,$id) {
			$this->edit(array('spellppmRate_2' => $set_value),array('entry' => $id ));
		}

		public function set_spellcooldown_2($set_value,$id) {
			$this->edit(array('spellcooldown_2' => $set_value),array('entry' => $id ));
		}

		public function set_spellcategory_2($set_value,$id) {
			$this->edit(array('spellcategory_2' => $set_value),array('entry' => $id ));
		}

		public function set_spellcategorycooldown_2($set_value,$id) {
			$this->edit(array('spellcategorycooldown_2' => $set_value),array('entry' => $id ));
		}

		public function set_spellid_3($set_value,$id) {
			$this->edit(array('spellid_3' => $set_value),array('entry' => $id ));
		}

		public function set_spelltrigger_3($set_value,$id) {
			$this->edit(array('spelltrigger_3' => $set_value),array('entry' => $id ));
		}

		public function set_spellcharges_3($set_value,$id) {
			$this->edit(array('spellcharges_3' => $set_value),array('entry' => $id ));
		}

		public function set_spellppmRate_3($set_value,$id) {
			$this->edit(array('spellppmRate_3' => $set_value),array('entry' => $id ));
		}

		public function set_spellcooldown_3($set_value,$id) {
			$this->edit(array('spellcooldown_3' => $set_value),array('entry' => $id ));
		}

		public function set_spellcategory_3($set_value,$id) {
			$this->edit(array('spellcategory_3' => $set_value),array('entry' => $id ));
		}

		public function set_spellcategorycooldown_3($set_value,$id) {
			$this->edit(array('spellcategorycooldown_3' => $set_value),array('entry' => $id ));
		}

		public function set_spellid_4($set_value,$id) {
			$this->edit(array('spellid_4' => $set_value),array('entry' => $id ));
		}

		public function set_spelltrigger_4($set_value,$id) {
			$this->edit(array('spelltrigger_4' => $set_value),array('entry' => $id ));
		}

		public function set_spellcharges_4($set_value,$id) {
			$this->edit(array('spellcharges_4' => $set_value),array('entry' => $id ));
		}

		public function set_spellppmRate_4($set_value,$id) {
			$this->edit(array('spellppmRate_4' => $set_value),array('entry' => $id ));
		}

		public function set_spellcooldown_4($set_value,$id) {
			$this->edit(array('spellcooldown_4' => $set_value),array('entry' => $id ));
		}

		public function set_spellcategory_4($set_value,$id) {
			$this->edit(array('spellcategory_4' => $set_value),array('entry' => $id ));
		}

		public function set_spellcategorycooldown_4($set_value,$id) {
			$this->edit(array('spellcategorycooldown_4' => $set_value),array('entry' => $id ));
		}

		public function set_spellid_5($set_value,$id) {
			$this->edit(array('spellid_5' => $set_value),array('entry' => $id ));
		}

		public function set_spelltrigger_5($set_value,$id) {
			$this->edit(array('spelltrigger_5' => $set_value),array('entry' => $id ));
		}

		public function set_spellcharges_5($set_value,$id) {
			$this->edit(array('spellcharges_5' => $set_value),array('entry' => $id ));
		}

		public function set_spellppmRate_5($set_value,$id) {
			$this->edit(array('spellppmRate_5' => $set_value),array('entry' => $id ));
		}

		public function set_spellcooldown_5($set_value,$id) {
			$this->edit(array('spellcooldown_5' => $set_value),array('entry' => $id ));
		}

		public function set_spellcategory_5($set_value,$id) {
			$this->edit(array('spellcategory_5' => $set_value),array('entry' => $id ));
		}

		public function set_spellcategorycooldown_5($set_value,$id) {
			$this->edit(array('spellcategorycooldown_5' => $set_value),array('entry' => $id ));
		}

		public function set_bonding($set_value,$id) {
			$this->edit(array('bonding' => $set_value),array('entry' => $id ));
		}

		public function set_description($set_value,$id) {
			$this->edit(array('description' => $set_value),array('entry' => $id ));
		}

		public function set_PageText($set_value,$id) {
			$this->edit(array('PageText' => $set_value),array('entry' => $id ));
		}

		public function set_LanguageID($set_value,$id) {
			$this->edit(array('LanguageID' => $set_value),array('entry' => $id ));
		}

		public function set_PageMaterial($set_value,$id) {
			$this->edit(array('PageMaterial' => $set_value),array('entry' => $id ));
		}

		public function set_startquest($set_value,$id) {
			$this->edit(array('startquest' => $set_value),array('entry' => $id ));
		}

		public function set_lockid($set_value,$id) {
			$this->edit(array('lockid' => $set_value),array('entry' => $id ));
		}

		public function set_Material($set_value,$id) {
			$this->edit(array('Material' => $set_value),array('entry' => $id ));
		}

		public function set_sheath($set_value,$id) {
			$this->edit(array('sheath' => $set_value),array('entry' => $id ));
		}

		public function set_RandomProperty($set_value,$id) {
			$this->edit(array('RandomProperty' => $set_value),array('entry' => $id ));
		}

		public function set_RandomSuffix($set_value,$id) {
			$this->edit(array('RandomSuffix' => $set_value),array('entry' => $id ));
		}

		public function set_block($set_value,$id) {
			$this->edit(array('block' => $set_value),array('entry' => $id ));
		}

		public function set_itemset($set_value,$id) {
			$this->edit(array('itemset' => $set_value),array('entry' => $id ));
		}

		public function set_MaxDurability($set_value,$id) {
			$this->edit(array('MaxDurability' => $set_value),array('entry' => $id ));
		}

		public function set_area($set_value,$id) {
			$this->edit(array('area' => $set_value),array('entry' => $id ));
		}

		public function set_Map($set_value,$id) {
			$this->edit(array('Map' => $set_value),array('entry' => $id ));
		}

		public function set_BagFamily($set_value,$id) {
			$this->edit(array('BagFamily' => $set_value),array('entry' => $id ));
		}

		public function set_TotemCategory($set_value,$id) {
			$this->edit(array('TotemCategory' => $set_value),array('entry' => $id ));
		}

		public function set_socketColor_1($set_value,$id) {
			$this->edit(array('socketColor_1' => $set_value),array('entry' => $id ));
		}

		public function set_socketContent_1($set_value,$id) {
			$this->edit(array('socketContent_1' => $set_value),array('entry' => $id ));
		}

		public function set_socketColor_2($set_value,$id) {
			$this->edit(array('socketColor_2' => $set_value),array('entry' => $id ));
		}

		public function set_socketContent_2($set_value,$id) {
			$this->edit(array('socketContent_2' => $set_value),array('entry' => $id ));
		}

		public function set_socketColor_3($set_value,$id) {
			$this->edit(array('socketColor_3' => $set_value),array('entry' => $id ));
		}

		public function set_socketContent_3($set_value,$id) {
			$this->edit(array('socketContent_3' => $set_value),array('entry' => $id ));
		}

		public function set_socketBonus($set_value,$id) {
			$this->edit(array('socketBonus' => $set_value),array('entry' => $id ));
		}

		public function set_GemProperties($set_value,$id) {
			$this->edit(array('GemProperties' => $set_value),array('entry' => $id ));
		}

		public function set_RequiredDisenchantSkill($set_value,$id) {
			$this->edit(array('RequiredDisenchantSkill' => $set_value),array('entry' => $id ));
		}

		public function set_ArmorDamageModifier($set_value,$id) {
			$this->edit(array('ArmorDamageModifier' => $set_value),array('entry' => $id ));
		}

		public function set_Duration($set_value,$id) {
			$this->edit(array('Duration' => $set_value),array('entry' => $id ));
		}

		public function set_ItemLimitCategory($set_value,$id) {
			$this->edit(array('ItemLimitCategory' => $set_value),array('entry' => $id ));
		}

		public function set_HolidayId($set_value,$id) {
			$this->edit(array('HolidayId' => $set_value),array('entry' => $id ));
		}

		public function set_ScriptName($set_value,$id) {
			$this->edit(array('ScriptName' => $set_value),array('entry' => $id ));
		}

		public function set_DisenchantID($set_value,$id) {
			$this->edit(array('DisenchantID' => $set_value),array('entry' => $id ));
		}

		public function set_FoodType($set_value,$id) {
			$this->edit(array('FoodType' => $set_value),array('entry' => $id ));
		}

		public function set_minMoneyLoot($set_value,$id) {
			$this->edit(array('minMoneyLoot' => $set_value),array('entry' => $id ));
		}

		public function set_maxMoneyLoot($set_value,$id) {
			$this->edit(array('maxMoneyLoot' => $set_value),array('entry' => $id ));
		}

		public function set_WDBVerified($set_value,$id) {
			$this->edit(array('WDBVerified' => $set_value),array('entry' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('item_template');
		}
		

		public function select_entry($name) {
			return $this->select('item_template','entry="' . $name . '"');
		}

		public function select_class($name) {
			return $this->select('item_template','class="' . $name . '"');
		}

		public function select_subclass($name) {
			return $this->select('item_template','subclass="' . $name . '"');
		}

		public function select_unk0($name) {
			return $this->select('item_template','unk0="' . $name . '"');
		}

		public function select_name($name) {
			return $this->select('item_template','name="' . $name . '"');
		}

		public function select_displayid($name) {
			return $this->select('item_template','displayid="' . $name . '"');
		}

		public function select_Quality($name) {
			return $this->select('item_template','Quality="' . $name . '"');
		}

		public function select_Flags($name) {
			return $this->select('item_template','Flags="' . $name . '"');
		}

		public function select_FlagsExtra($name) {
			return $this->select('item_template','FlagsExtra="' . $name . '"');
		}

		public function select_BuyCount($name) {
			return $this->select('item_template','BuyCount="' . $name . '"');
		}

		public function select_BuyPrice($name) {
			return $this->select('item_template','BuyPrice="' . $name . '"');
		}

		public function select_SellPrice($name) {
			return $this->select('item_template','SellPrice="' . $name . '"');
		}

		public function select_InventoryType($name) {
			return $this->select('item_template','InventoryType="' . $name . '"');
		}

		public function select_AllowableClass($name) {
			return $this->select('item_template','AllowableClass="' . $name . '"');
		}

		public function select_AllowableRace($name) {
			return $this->select('item_template','AllowableRace="' . $name . '"');
		}

		public function select_ItemLevel($name) {
			return $this->select('item_template','ItemLevel="' . $name . '"');
		}

		public function select_RequiredLevel($name) {
			return $this->select('item_template','RequiredLevel="' . $name . '"');
		}

		public function select_RequiredSkill($name) {
			return $this->select('item_template','RequiredSkill="' . $name . '"');
		}

		public function select_RequiredSkillRank($name) {
			return $this->select('item_template','RequiredSkillRank="' . $name . '"');
		}

		public function select_requiredspell($name) {
			return $this->select('item_template','requiredspell="' . $name . '"');
		}

		public function select_requiredhonorrank($name) {
			return $this->select('item_template','requiredhonorrank="' . $name . '"');
		}

		public function select_RequiredCityRank($name) {
			return $this->select('item_template','RequiredCityRank="' . $name . '"');
		}

		public function select_RequiredReputationFaction($name) {
			return $this->select('item_template','RequiredReputationFaction="' . $name . '"');
		}

		public function select_RequiredReputationRank($name) {
			return $this->select('item_template','RequiredReputationRank="' . $name . '"');
		}

		public function select_maxcount($name) {
			return $this->select('item_template','maxcount="' . $name . '"');
		}

		public function select_stackable($name) {
			return $this->select('item_template','stackable="' . $name . '"');
		}

		public function select_ContainerSlots($name) {
			return $this->select('item_template','ContainerSlots="' . $name . '"');
		}

		public function select_StatsCount($name) {
			return $this->select('item_template','StatsCount="' . $name . '"');
		}

		public function select_stat_type1($name) {
			return $this->select('item_template','stat_type1="' . $name . '"');
		}

		public function select_stat_value1($name) {
			return $this->select('item_template','stat_value1="' . $name . '"');
		}

		public function select_stat_type2($name) {
			return $this->select('item_template','stat_type2="' . $name . '"');
		}

		public function select_stat_value2($name) {
			return $this->select('item_template','stat_value2="' . $name . '"');
		}

		public function select_stat_type3($name) {
			return $this->select('item_template','stat_type3="' . $name . '"');
		}

		public function select_stat_value3($name) {
			return $this->select('item_template','stat_value3="' . $name . '"');
		}

		public function select_stat_type4($name) {
			return $this->select('item_template','stat_type4="' . $name . '"');
		}

		public function select_stat_value4($name) {
			return $this->select('item_template','stat_value4="' . $name . '"');
		}

		public function select_stat_type5($name) {
			return $this->select('item_template','stat_type5="' . $name . '"');
		}

		public function select_stat_value5($name) {
			return $this->select('item_template','stat_value5="' . $name . '"');
		}

		public function select_stat_type6($name) {
			return $this->select('item_template','stat_type6="' . $name . '"');
		}

		public function select_stat_value6($name) {
			return $this->select('item_template','stat_value6="' . $name . '"');
		}

		public function select_stat_type7($name) {
			return $this->select('item_template','stat_type7="' . $name . '"');
		}

		public function select_stat_value7($name) {
			return $this->select('item_template','stat_value7="' . $name . '"');
		}

		public function select_stat_type8($name) {
			return $this->select('item_template','stat_type8="' . $name . '"');
		}

		public function select_stat_value8($name) {
			return $this->select('item_template','stat_value8="' . $name . '"');
		}

		public function select_stat_type9($name) {
			return $this->select('item_template','stat_type9="' . $name . '"');
		}

		public function select_stat_value9($name) {
			return $this->select('item_template','stat_value9="' . $name . '"');
		}

		public function select_stat_type10($name) {
			return $this->select('item_template','stat_type10="' . $name . '"');
		}

		public function select_stat_value10($name) {
			return $this->select('item_template','stat_value10="' . $name . '"');
		}

		public function select_ScalingStatDistribution($name) {
			return $this->select('item_template','ScalingStatDistribution="' . $name . '"');
		}

		public function select_ScalingStatValue($name) {
			return $this->select('item_template','ScalingStatValue="' . $name . '"');
		}

		public function select_dmg_min1($name) {
			return $this->select('item_template','dmg_min1="' . $name . '"');
		}

		public function select_dmg_max1($name) {
			return $this->select('item_template','dmg_max1="' . $name . '"');
		}

		public function select_dmg_type1($name) {
			return $this->select('item_template','dmg_type1="' . $name . '"');
		}

		public function select_dmg_min2($name) {
			return $this->select('item_template','dmg_min2="' . $name . '"');
		}

		public function select_dmg_max2($name) {
			return $this->select('item_template','dmg_max2="' . $name . '"');
		}

		public function select_dmg_type2($name) {
			return $this->select('item_template','dmg_type2="' . $name . '"');
		}

		public function select_armor($name) {
			return $this->select('item_template','armor="' . $name . '"');
		}

		public function select_holy_res($name) {
			return $this->select('item_template','holy_res="' . $name . '"');
		}

		public function select_fire_res($name) {
			return $this->select('item_template','fire_res="' . $name . '"');
		}

		public function select_nature_res($name) {
			return $this->select('item_template','nature_res="' . $name . '"');
		}

		public function select_frost_res($name) {
			return $this->select('item_template','frost_res="' . $name . '"');
		}

		public function select_shadow_res($name) {
			return $this->select('item_template','shadow_res="' . $name . '"');
		}

		public function select_arcane_res($name) {
			return $this->select('item_template','arcane_res="' . $name . '"');
		}

		public function select_delay($name) {
			return $this->select('item_template','delay="' . $name . '"');
		}

		public function select_ammo_type($name) {
			return $this->select('item_template','ammo_type="' . $name . '"');
		}

		public function select_RangedModRange($name) {
			return $this->select('item_template','RangedModRange="' . $name . '"');
		}

		public function select_spellid_1($name) {
			return $this->select('item_template','spellid_1="' . $name . '"');
		}

		public function select_spelltrigger_1($name) {
			return $this->select('item_template','spelltrigger_1="' . $name . '"');
		}

		public function select_spellcharges_1($name) {
			return $this->select('item_template','spellcharges_1="' . $name . '"');
		}

		public function select_spellppmRate_1($name) {
			return $this->select('item_template','spellppmRate_1="' . $name . '"');
		}

		public function select_spellcooldown_1($name) {
			return $this->select('item_template','spellcooldown_1="' . $name . '"');
		}

		public function select_spellcategory_1($name) {
			return $this->select('item_template','spellcategory_1="' . $name . '"');
		}

		public function select_spellcategorycooldown_1($name) {
			return $this->select('item_template','spellcategorycooldown_1="' . $name . '"');
		}

		public function select_spellid_2($name) {
			return $this->select('item_template','spellid_2="' . $name . '"');
		}

		public function select_spelltrigger_2($name) {
			return $this->select('item_template','spelltrigger_2="' . $name . '"');
		}

		public function select_spellcharges_2($name) {
			return $this->select('item_template','spellcharges_2="' . $name . '"');
		}

		public function select_spellppmRate_2($name) {
			return $this->select('item_template','spellppmRate_2="' . $name . '"');
		}

		public function select_spellcooldown_2($name) {
			return $this->select('item_template','spellcooldown_2="' . $name . '"');
		}

		public function select_spellcategory_2($name) {
			return $this->select('item_template','spellcategory_2="' . $name . '"');
		}

		public function select_spellcategorycooldown_2($name) {
			return $this->select('item_template','spellcategorycooldown_2="' . $name . '"');
		}

		public function select_spellid_3($name) {
			return $this->select('item_template','spellid_3="' . $name . '"');
		}

		public function select_spelltrigger_3($name) {
			return $this->select('item_template','spelltrigger_3="' . $name . '"');
		}

		public function select_spellcharges_3($name) {
			return $this->select('item_template','spellcharges_3="' . $name . '"');
		}

		public function select_spellppmRate_3($name) {
			return $this->select('item_template','spellppmRate_3="' . $name . '"');
		}

		public function select_spellcooldown_3($name) {
			return $this->select('item_template','spellcooldown_3="' . $name . '"');
		}

		public function select_spellcategory_3($name) {
			return $this->select('item_template','spellcategory_3="' . $name . '"');
		}

		public function select_spellcategorycooldown_3($name) {
			return $this->select('item_template','spellcategorycooldown_3="' . $name . '"');
		}

		public function select_spellid_4($name) {
			return $this->select('item_template','spellid_4="' . $name . '"');
		}

		public function select_spelltrigger_4($name) {
			return $this->select('item_template','spelltrigger_4="' . $name . '"');
		}

		public function select_spellcharges_4($name) {
			return $this->select('item_template','spellcharges_4="' . $name . '"');
		}

		public function select_spellppmRate_4($name) {
			return $this->select('item_template','spellppmRate_4="' . $name . '"');
		}

		public function select_spellcooldown_4($name) {
			return $this->select('item_template','spellcooldown_4="' . $name . '"');
		}

		public function select_spellcategory_4($name) {
			return $this->select('item_template','spellcategory_4="' . $name . '"');
		}

		public function select_spellcategorycooldown_4($name) {
			return $this->select('item_template','spellcategorycooldown_4="' . $name . '"');
		}

		public function select_spellid_5($name) {
			return $this->select('item_template','spellid_5="' . $name . '"');
		}

		public function select_spelltrigger_5($name) {
			return $this->select('item_template','spelltrigger_5="' . $name . '"');
		}

		public function select_spellcharges_5($name) {
			return $this->select('item_template','spellcharges_5="' . $name . '"');
		}

		public function select_spellppmRate_5($name) {
			return $this->select('item_template','spellppmRate_5="' . $name . '"');
		}

		public function select_spellcooldown_5($name) {
			return $this->select('item_template','spellcooldown_5="' . $name . '"');
		}

		public function select_spellcategory_5($name) {
			return $this->select('item_template','spellcategory_5="' . $name . '"');
		}

		public function select_spellcategorycooldown_5($name) {
			return $this->select('item_template','spellcategorycooldown_5="' . $name . '"');
		}

		public function select_bonding($name) {
			return $this->select('item_template','bonding="' . $name . '"');
		}

		public function select_description($name) {
			return $this->select('item_template','description="' . $name . '"');
		}

		public function select_PageText($name) {
			return $this->select('item_template','PageText="' . $name . '"');
		}

		public function select_LanguageID($name) {
			return $this->select('item_template','LanguageID="' . $name . '"');
		}

		public function select_PageMaterial($name) {
			return $this->select('item_template','PageMaterial="' . $name . '"');
		}

		public function select_startquest($name) {
			return $this->select('item_template','startquest="' . $name . '"');
		}

		public function select_lockid($name) {
			return $this->select('item_template','lockid="' . $name . '"');
		}

		public function select_Material($name) {
			return $this->select('item_template','Material="' . $name . '"');
		}

		public function select_sheath($name) {
			return $this->select('item_template','sheath="' . $name . '"');
		}

		public function select_RandomProperty($name) {
			return $this->select('item_template','RandomProperty="' . $name . '"');
		}

		public function select_RandomSuffix($name) {
			return $this->select('item_template','RandomSuffix="' . $name . '"');
		}

		public function select_block($name) {
			return $this->select('item_template','block="' . $name . '"');
		}

		public function select_itemset($name) {
			return $this->select('item_template','itemset="' . $name . '"');
		}

		public function select_MaxDurability($name) {
			return $this->select('item_template','MaxDurability="' . $name . '"');
		}

		public function select_area($name) {
			return $this->select('item_template','area="' . $name . '"');
		}

		public function select_Map($name) {
			return $this->select('item_template','Map="' . $name . '"');
		}

		public function select_BagFamily($name) {
			return $this->select('item_template','BagFamily="' . $name . '"');
		}

		public function select_TotemCategory($name) {
			return $this->select('item_template','TotemCategory="' . $name . '"');
		}

		public function select_socketColor_1($name) {
			return $this->select('item_template','socketColor_1="' . $name . '"');
		}

		public function select_socketContent_1($name) {
			return $this->select('item_template','socketContent_1="' . $name . '"');
		}

		public function select_socketColor_2($name) {
			return $this->select('item_template','socketColor_2="' . $name . '"');
		}

		public function select_socketContent_2($name) {
			return $this->select('item_template','socketContent_2="' . $name . '"');
		}

		public function select_socketColor_3($name) {
			return $this->select('item_template','socketColor_3="' . $name . '"');
		}

		public function select_socketContent_3($name) {
			return $this->select('item_template','socketContent_3="' . $name . '"');
		}

		public function select_socketBonus($name) {
			return $this->select('item_template','socketBonus="' . $name . '"');
		}

		public function select_GemProperties($name) {
			return $this->select('item_template','GemProperties="' . $name . '"');
		}

		public function select_RequiredDisenchantSkill($name) {
			return $this->select('item_template','RequiredDisenchantSkill="' . $name . '"');
		}

		public function select_ArmorDamageModifier($name) {
			return $this->select('item_template','ArmorDamageModifier="' . $name . '"');
		}

		public function select_Duration($name) {
			return $this->select('item_template','Duration="' . $name . '"');
		}

		public function select_ItemLimitCategory($name) {
			return $this->select('item_template','ItemLimitCategory="' . $name . '"');
		}

		public function select_HolidayId($name) {
			return $this->select('item_template','HolidayId="' . $name . '"');
		}

		public function select_ScriptName($name) {
			return $this->select('item_template','ScriptName="' . $name . '"');
		}

		public function select_DisenchantID($name) {
			return $this->select('item_template','DisenchantID="' . $name . '"');
		}

		public function select_FoodType($name) {
			return $this->select('item_template','FoodType="' . $name . '"');
		}

		public function select_minMoneyLoot($name) {
			return $this->select('item_template','minMoneyLoot="' . $name . '"');
		}

		public function select_maxMoneyLoot($name) {
			return $this->select('item_template','maxMoneyLoot="' . $name . '"');
		}

		public function select_WDBVerified($name) {
			return $this->select('item_template','WDBVerified="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('entry' => $id));
		}

	}
?>