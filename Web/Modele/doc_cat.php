<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Lundi 22 Juillet 2013
	****************************************************/
	class doc_cat extends mvc_bdd  {
		public $id;
		public $name;
		public $description;
		public $id_cat;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_name($set_value,$id) {
			$this->edit(array('name' => $set_value),array('id' => $id ));
		}

		public function set_description($set_value,$id) {
			$this->edit(array('description' => $set_value),array('id' => $id ));
		}

		public function set_id_cat($set_value,$id) {
			$this->edit(array('id_cat' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('doc_cat');
		}
		

		public function select_id($name) {
			return $this->select('doc_cat','id="' . $name . '"');
		}

		public function select_name($name) {
			return $this->select('doc_cat','name="' . $name . '"');
		}

		public function select_description($name) {
			return $this->select('doc_cat','description="' . $name . '"');
		}

		public function select_id_cat($name) {
			return $this->select('doc_cat','id_cat="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>