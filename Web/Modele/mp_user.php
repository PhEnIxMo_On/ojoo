<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mercredi 22 Mai 2013
	****************************************************/
	class mp_user extends mvc_bdd  {
		public $id;
		public $convId;
		public $accountId;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
                
                public function isUserConv($accountId,$convId) {
                    $O = getOjoo();
                    $test = $O->bdd->site->query("SELECT id FROM mp_user WHERE accountId=" . $accountId . " AND convId=" . $convId)->fetch();
                    if ($test != false) return true;
                    else                return false;
                }
                
                public function deleteUserConv($accountId,$convId) {
                    $O = getOjoo();
                    $O->bdd->site->query("DELETE FROM mp_user WHERE convId=" . $convId . " AND accountId=" . $accountId);
                }

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_convId($set_value,$id) {
			$this->edit(array('convId' => $set_value),array('id' => $id ));
		}

		public function set_accountId($set_value,$id) {
			$this->edit(array('accountId' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('mp_user');
		}
		

		public function select_id($name) {
			return $this->select('mp_user','id="' . $name . '"');
		}

		public function select_convId($name) {
			return $this->select('mp_user','convId="' . $name . '"');
		}

		public function select_accountId($name) {
			return $this->select('mp_user','accountId="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>