<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mardi 21 Mai 2013
	****************************************************/
	class guilde_recup extends mvc_bdd  {
                const INTEREST_PVP = 1;
                const INTEREST_PVE = 2;
                const INTEREST_COMMUNITY = 3;
                
                const FACTION_ALLY = 1;
                const FACTION_HORDE = 2;
            
            
		public $id;
		public $accountId;
		public $server;
		public $originalGuildName;
		public $oxygenGuildName;
		public $originalNbUser;
		public $oxygenNbUser;
		public $leaveReason;
		public $interest;
                public $com;
                public $reason;
                public $faction;
		public $accountMj;
		public $lastIp;
		public $lastIpMj;
		public $state;
		public $conditions;
		public $dateCreate;
		public $dateValid;
		public $lastMaj;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
                
                public function getIcone($state) {
                    switch ($state) {
                        case 1:
                            return "guildeAttente.png";
                            break;
                        
                        case 2:
                            return "guildeExa.png";
                            break;
                        
                        case 3:
                            return "guildeValide.png";
                            break;
                        
                        case 4:
                            return "guildeRefusee.png";
                            break;
                    }
                }

                public function getInterest($interest) {
                    switch ($interest) {
                        case guilde_recup::INTEREST_PVP:
                            return "PvP";
                            break;
                        
                        case guilde_recup::INTEREST_PVE:
                            return "PvE";
                            break;
                        
                        case guilde_recup::INTEREST_COMMUNITY:
                            return "Communauté";
                            break;
                    }
                }
                
                public function canEdit($state) {
                    if ($state == 1) return true;
                    else             return false;
                }
                
                public function displayState($state) {
                    $html = "<fieldset><legend>{{TITRE}}</legend>{{CONTENT}}</fieldset>";
                    switch ($state) {
                        case 1:
                            $titre = "En attente";
                            $content = "Votre demande est en attente. Cela ne veut pas dire qu'elle n'est pas actuellement étudiée, soyez patient ! ;)";
                            break;
                        
                        case 2:
                            $titre = "En cours d'examination";
                            $content = "Encore un peu de patience, votre demande est examinée actuellement. ";
                            break;
                        
                        case 3:
                            $titre = "Validée";
                            $content = $this->conditions;
                            break;
                        
                        case 4:
                            $titre = "Refusée";
                            $content = $this->reason;
                            break;
                    }
                    $html = str_replace('{{TITRE}}',$titre,$html);
                    $html = str_replace('{{CONTENT}}','<span style="color: white;">' . $content  .'</span>',$html);
                    return $html;
                }                

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_accountId($set_value,$id) {
			$this->edit(array('accountId' => $set_value),array('id' => $id ));
		}

		public function set_server($set_value,$id) {
			$this->edit(array('server' => $set_value),array('id' => $id ));
		}

		public function set_originalGuildName($set_value,$id) {
			$this->edit(array('originalGuildName' => $set_value),array('id' => $id ));
		}

		public function set_oxygenGuildName($set_value,$id) {
			$this->edit(array('oxygenGuildName' => $set_value),array('id' => $id ));
		}

		public function set_originalNbUser($set_value,$id) {
			$this->edit(array('originalNbUser' => $set_value),array('id' => $id ));
		}

		public function set_oxygenNbUser($set_value,$id) {
			$this->edit(array('oxygenNbUser' => $set_value),array('id' => $id ));
		}

		public function set_leaveReason($set_value,$id) {
			$this->edit(array('leaveReason' => $set_value),array('id' => $id ));
		}

		public function set_interest($set_value,$id) {
			$this->edit(array('interest' => $set_value),array('id' => $id ));
		}

		public function set_accountMj($set_value,$id) {
			$this->edit(array('accountMj' => $set_value),array('id' => $id ));
		}

		public function set_lastIp($set_value,$id) {
			$this->edit(array('lastIp' => $set_value),array('id' => $id ));
		}

		public function set_lastIpMj($set_value,$id) {
			$this->edit(array('lastIpMj' => $set_value),array('id' => $id ));
		}

		public function set_state($set_value,$id) {
			$this->edit(array('state' => $set_value),array('id' => $id ));
		}

		public function set_conditions($set_value,$id) {
			$this->edit(array('conditions' => $set_value),array('id' => $id ));
		}

		public function set_dateCreate($set_value,$id) {
			$this->edit(array('dateCreate' => $set_value),array('id' => $id ));
		}

		public function set_dateValid($set_value,$id) {
			$this->edit(array('dateValid' => $set_value),array('id' => $id ));
		}

		public function set_lastMaj($set_value,$id) {
			$this->edit(array('lastMaj' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('guilde_recup');
		}
		

		public function select_id($name) {
			return $this->select('guilde_recup','id="' . $name . '"');
		}
                
		public function select_faction($name) {
			return $this->select('guilde_recup','faction="' . $name . '"');
		}                

		public function select_accountId($name) {
			return $this->select('guilde_recup','accountId="' . $name . '"');
		}

		public function select_server($name) {
			return $this->select('guilde_recup','server="' . $name . '"');
		}

		public function select_originalGuildName($name) {
			return $this->select('guilde_recup','originalGuildName="' . $name . '"');
		}

		public function select_oxygenGuildName($name) {
			return $this->select('guilde_recup','oxygenGuildName="' . $name . '"');
		}

		public function select_originalNbUser($name) {
			return $this->select('guilde_recup','originalNbUser="' . $name . '"');
		}

		public function select_oxygenNbUser($name) {
			return $this->select('guilde_recup','oxygenNbUser="' . $name . '"');
		}

		public function select_leaveReason($name) {
			return $this->select('guilde_recup','leaveReason="' . $name . '"');
		}

		public function select_interest($name) {
			return $this->select('guilde_recup','interest="' . $name . '"');
		}

		public function select_accountMj($name) {
			return $this->select('guilde_recup','accountMj="' . $name . '"');
		}

		public function select_lastIp($name) {
			return $this->select('guilde_recup','lastIp="' . $name . '"');
		}

		public function select_lastIpMj($name) {
			return $this->select('guilde_recup','lastIpMj="' . $name . '"');
		}

		public function select_state($name) {
			return $this->select('guilde_recup','state="' . $name . '"');
		}

		public function select_conditions($name) {
			return $this->select('guilde_recup','conditions="' . $name . '"');
		}

		public function select_dateCreate($name) {
			return $this->select('guilde_recup','dateCreate="' . $name . '"');
		}

		public function select_dateValid($name) {
			return $this->select('guilde_recup','dateValid="' . $name . '"');
		}

		public function select_lastMaj($name) {
			return $this->select('guilde_recup','lastMaj="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>