<?php
class information_schema extends mvc_bdd {
	public $BDD = "information_schema";
	
	public function get_tables($bdd) {
		return $this->select('TABLES','table_schema="' . $bdd . '"');
	}
	
	public function get_columns($table,$bdd = null) {
		if ($bdd != null)
			return $this->select('COLUMNS','TABLE_NAME="' . $table . '" AND TABLE_SCHEMA="' . $bdd . '"');
		else
			return $this->select('COLUMNS','TABLE_NAME="' . $table . '"');
			
	}
	
	public function get_bdd() {
		return $this->select('SCHEMATA');
	}
	
	public function get_ref_id($table,$bdd = null) {
		if ($bdd == null)
			return $this->select('COLUMNS','TABLE_NAME="' . $table . '" AND COLUMN_KEY="PRI" AND EXTRA="auto_increment"');
		else
			return $this->select('COLUMNS','TABLE_NAME="' . $table . '" AND COLUMN_KEY="PRI" AND EXTRA="auto_increment" AND TABLE_SCHEMA="' . $bdd . '"');
	}
}
?>