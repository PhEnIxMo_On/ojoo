<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mardi 23 Avril 2013
	****************************************************/
	class votes extends mvc_bdd  {
		public $date;
		public $idCompte;
		public $ip;
		public $points;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
                
               
                
		public function userVote($id) {
			$O = getOjoo();
			$vote = $O->bdd->site->query("SELECT * FROM votes WHERE idCompte=" . $id)->fetch();
			if ($vote != false) {
				$possibleVote = $vote['date'] + 60*60*3;
				if ($possibleVote <= time()) {
					$this->insertUpdateVote($id);
				} else return false;   
			} else $this->insertUpdateVote($id);
		}
		
		public function insertUpdateVote($id) {
			$O = getOjoo();
			$vote = $O->bdd->site->query("SELECT * FROM votes WHERE idCompte=" . $id)->fetch();
			if ($vote != false) {
				$nbVote = $O->wowUser->nbVote + $O->config['pointVote'];
				echo "UPDATE votes SET ip='" . $_SERVER['REMOTE_ADDR'] . "',points=" . $nbVote . ",date=" . time() . " WHERE idCompte=" . $id;
				$O->bdd->site->query("UPDATE votes SET ip='" . $_SERVER['REMOTE_ADDR'] . "',points=" . $nbVote . ",date=" . time() . " WHERE idCompte=" . $id);
			} else {
				 $O->bdd->site->query("INSERT INTO votes VALUES(" . time() . "," . $id . ",'" . $_SERVER['REMOTE_ADDR'] . "'," . $O->config['pointVote'] . ")");
			}
		}
		
		public function setVote($set_value,$idCompte) {
			$O = getOjoo();
			$money = $O->bdd->site->query("UPDATE votes SET points='" . $set_value . "' WHERE idCompte =" . $idCompte . "") or die (" Erreur lors de la modification des votes");
		}
		

		public function set_date($set_value,$id) {
			$this->edit(array('date' => $set_value),array('id' => $id ));
		}

		public function set_idCompte($set_value,$id) {
			$this->edit(array('idCompte' => $set_value),array('id' => $id ));
		}

		public function set_ip($set_value,$id) {
			$this->edit(array('ip' => $set_value),array('id' => $id ));
		}

		public function set_points($set_value,$id) {
			$this->edit(array('points' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('votes');
		}
		

		public function select_date($name) {
			return $this->select('votes','date="' . $name . '"');
		}

		public function select_idCompte($name) {
			return $this->select('votes','idCompte="' . $name . '"');
		}

		public function select_ip($name) {
			return $this->select('votes','ip="' . $name . '"');
		}

		public function select_points($name) {
			return $this->select('votes','points="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>