<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Jeudi 28 Mars 2013
	****************************************************/
	class factions extends mvc_bdd  {
		public $id;
		public $nom;
		
		public $BDD = 'manager';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_nom($set_value,$id) {
			$this->edit(array('nom' => $set_value),array('id' => $id ));
		}
                
		public function getFactions() {
			$O = getOjoo();
			return $O->bdd->manager->query("SELECT * FROM factions ORDER BY nom")->fetchAll();
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('factions');
		}
		

		public function select_id($name) {
			return $this->select('factions','id="' . $name . '"');
		}

		public function select_nom($name) {
			return $this->select('factions','nom="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}
		
		public function getMetierName($id) {
			switch($id) {
				case 2259:
					return "Alchimie";
					break;
					
				case 45357:
					return "Caligraphie";
					break;					
					
				case 3908:
					return "Couture";
					break;		

				case 2366:
					return "Herboristerie";
					break;

				case 8613:
					return "Dépeçage";
					break;	

				case 7411:
					return "Enchantement";
					break;	

				case 2018:
					return "Forge";
					break;	
					
				case 204:
					return "Défense";
					break;

				case 4036:
					return "Ingénierie";
					break;			

				case 25229:
					return "Joaillerie";
					break;
					
				case 2575:
					return "Minage";
					break;		

				case 2108:
					return "Travail du cuir";
					break;		

				case 7731:
					return "Pêche";
					break;		

				case 2550:
					return "Cuisine";
					break;				

				case 3273:
					return "Secourisme";
					break;			

				case 1180:
					return "Dague";
					break;	

				case 201:
					return "Epée";
					break;	

				case 202:
					return "Epée à deux mains";
					break;					
					
				case 196:
					return "Hache";
					break;		

				case 197:
					return "Hache à deux mains";
					break;	

				case 198:
					return "Masse";
					break;		

				case 199:
					return "Masse à deux mains";
					break;					
					
				case 203:
					return "Mains nues";
					break;	

				case 200:
					return "Arme d'hast";
					break;		

				case 5009:
					return "Baguette";
					break;			

				case 227:
					return "Bâton";
					break;		

				case 2567:
					return "Arme de jet";
					break;			

				case 5011:
					return "Arbalète";
					break;			

				case 264:
					return "Arc";
					break;		

				case 266:
					return "Arme à feu";
					break;					
			}
		}

	}
?>