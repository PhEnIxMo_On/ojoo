<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Jeudi 18 Juillet 2013
	****************************************************/
	class web_boutique_entete extends mvc_bdd  {
		const WAIT = 0;
		const ERROR = 1;
		const OK = 2;
		
		public $id;
		public $accountId;
		public $charGuid;
		public $prixVote;
		public $prixMoney;
		public $state;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_accountId($set_value,$id) {
			$this->edit(array('accountId' => $set_value),array('id' => $id ));
		}

		public function set_charGuid($set_value,$id) {
			$this->edit(array('charGuid' => $set_value),array('id' => $id ));
		}

		public function set_prixVote($set_value,$id) {
			$this->edit(array('prixVote' => $set_value),array('id' => $id ));
		}

		public function set_prixMoney($set_value,$id) {
			$this->edit(array('prixMoney' => $set_value),array('id' => $id ));
		}

		public function set_state($set_value,$id) {
			$this->edit(array('state' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('web_boutique_entete');
		}
		

		public function select_id($name) {
			return $this->select('web_boutique_entete','id="' . $name . '"');
		}

		public function select_accountId($name) {
			return $this->select('web_boutique_entete','accountId="' . $name . '"');
		}

		public function select_charGuid($name) {
			return $this->select('web_boutique_entete','charGuid="' . $name . '"');
		}

		public function select_prixVote($name) {
			return $this->select('web_boutique_entete','prixVote="' . $name . '"');
		}

		public function select_prixMoney($name) {
			return $this->select('web_boutique_entete','prixMoney="' . $name . '"');
		}

		public function select_state($name) {
			return $this->select('web_boutique_entete','state="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>