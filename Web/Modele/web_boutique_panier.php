<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Dimanche 21 Juillet 2013
	****************************************************/
	class web_boutique_panier extends mvc_bdd  {
		public $id;
		public $accountId;
		public $panier;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		public function MAJPanier() {
			$O = getOjoo();
			$panier = addslashes(serialize($O->panier));
			$testInsert = $O->bdd->site->query("SELECT * FROM web_boutique_panier WHERE accountId=" . $O->wowUser->getAccountId())->fetch();
			if ($testInsert != false) {
				$O->bdd->site->query("UPDATE web_boutique_panier SET panier='" . $panier . "' WHERE accountId=" . $O->wowUser->getAccountId());
			} else {
				$O->bdd->site->query("INSERT INTO web_boutique_panier VALUES(''," . $O->wowUser->getAccountId() . ",'" . $panier . "')");
			}			
		}
		
		public function restorePanier() {
			$O = getOjoo();
			$panier = $O->bdd->site->query("SELECT * FROM web_boutique_panier WHERE accountId=" . $O->wowUser->getAccountId())->fetch();
			if ($panier != false) {
				if (is_string($panier['panier']))
					$O->panier = unserialize(stripslashes($panier['panier']));
			}
		}
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_accountId($set_value,$id) {
			$this->edit(array('accountId' => $set_value),array('id' => $id ));
		}

		public function set_panier($set_value,$id) {
			$this->edit(array('panier' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('web_boutique_panier');
		}
		

		public function select_id($name) {
			return $this->select('web_boutique_panier','id="' . $name . '"');
		}

		public function select_accountId($name) {
			return $this->select('web_boutique_panier','accountId="' . $name . '"');
		}

		public function select_panier($name) {
			return $this->select('web_boutique_panier','panier="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>