<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Lundi 5 Ao�t 2013
	****************************************************/
	class npc_vendor extends mvc_bdd  {
		public $entry;
		public $slot;
		public $item;
		public $maxcount;
		public $incrtime;
		public $ExtendedCost;
		
		public $BDD = 'world';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('npc_vendor');
		}
		
		public function getNextSlot($id) {
			$O = getOjoo();
		 	$slot = $O->bdd->world->query("SELECT slot FROM npc_vendor WHERE entry=" . $id . " ORDER BY slot DESC LIMIT 1")->fetch();
			if ($slot != null) {
				$slot2 = $slot['slot'];
				$O->console->debug("SLOT de base : " . $slot2 . " Etape n°1","[SLOT]");
				$slot2++;
				$O->console->debug("SLOT : " . $slot2 . " Etape n°1","[SLOT]"); 	
			 } else {
				$slot2 = $slot['slot'];
				$slot2 = 0;			
				$O->console->debug("SLOT : " . $slot2 . " Etape n°2","[SLOT]"); 	
			 }
			return $slot2;
		}
		
		public function checkItem($entry,$id) {
			$O = getOjoo();
			$test = $O->bdd->world->query("SELECT entry FROM npc_vendor WHERE entry=" . $entry . " AND item=" . $id)->fetch();
			if ($test != null) return false;
			else               return true;
		}
		


	// Fonction de suppression : 	

	}
?>