<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mardi 15 Janvier 2013
	****************************************************/
	class account_access extends mvc_bdd  {
		public $id;
		public $gmlevel;
		public $RealmID;
		
		public $BDD = 'auth';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_gmlevel($set_value,$id) {
			$this->edit(array('gmlevel' => $set_value),array('id' => $id ));
		}

		public function set_RealmID($set_value,$id) {
			$this->edit(array('RealmID' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('account_access');
		}
		

		public function select_id($name) {
			return $this->select('account_access','id="' . $name . '"');
		}

		public function select_gmlevel($name) {
			return $this->select('account_access','gmlevel="' . $name . '"');
		}

		public function select_RealmID($name) {
			return $this->select('account_access','RealmID="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>