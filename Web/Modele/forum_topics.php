<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mercredi 28 Novembre 2012
	****************************************************/
	class forum_topics extends mvc_bdd  {
		public $id;
		public $titre;
		public $idPost;
		public $idCat;
		public $lockTopic;
		
		public $BDD = 'cp';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_titre($set_value,$id) {
			$this->edit(array('titre' => $set_value),array('id' => $id ));
		}

		public function set_idPost($set_value,$id) {
			$this->edit(array('idPost' => $set_value),array('id' => $id ));
		}

		public function set_idCat($set_value,$id) {
			$this->edit(array('idCat' => $set_value),array('id' => $id ));
		}

		public function set_lockTopic($set_value,$id) {
			$this->edit(array('lock' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('forum_topics');
		}
		

		public function select_id($name) {
			return $this->select('forum_topics','id="' . $name . '"');
		}

		public function select_titre($name) {
			return $this->select('forum_topics','titre="' . $name . '"');
		}

		public function select_idPost($name) {
			return $this->select('forum_topics','idPost="' . $name . '"');
		}

		public function select_idCat($name) {
			return $this->select('forum_topics','idCat="' . $name . '"');
		}

		public function select_lockTopic($name) {
			return $this->select('forum_topics','lock="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>