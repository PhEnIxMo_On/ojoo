<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Dimanche 21 Juillet 2013
	****************************************************/
	class log_manager extends mvc_bdd  {
		const ACTION_CHANGE_PASSWD = 1;
		const ACTION_EDIT_ACCOUNT = 2;
		const ACTION_BAN = 3;
		const ACTION_ACCOUNT_MAINTENANCE = 4;
		
		
		public $id;
		public $accountId;
		public $action;
		public $idAction;
		public $date;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function log($texte,$idAction) {
			$O = getOjoo();
			$O->bdd->site->query("INSERT INTO log_manager VALUES('','" . $O->wowUser->getAccountId() . "','" . $texte . "','" . $idAction . "'," . time() . ")");
		}
		
		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_accountId($set_value,$id) {
			$this->edit(array('accountId' => $set_value),array('id' => $id ));
		}

		public function set_action($set_value,$id) {
			$this->edit(array('action' => $set_value),array('id' => $id ));
		}

		public function set_idAction($set_value,$id) {
			$this->edit(array('idAction' => $set_value),array('id' => $id ));
		}

		public function set_date($set_value,$id) {
			$this->edit(array('date' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('log_manager');
		}
		

		public function select_id($name) {
			return $this->select('log_manager','id="' . $name . '"');
		}

		public function select_accountId($name) {
			return $this->select('log_manager','accountId="' . $name . '"');
		}

		public function select_action($name) {
			return $this->select('log_manager','action="' . $name . '"');
		}

		public function select_idAction($name) {
			return $this->select('log_manager','idAction="' . $name . '"');
		}

		public function select_date($name) {
			return $this->select('log_manager','date="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>