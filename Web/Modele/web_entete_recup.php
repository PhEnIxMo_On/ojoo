<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Jeudi 28 Mars 2013
	****************************************************/
	class web_entete_recup extends mvc_bdd  {
		public $id;
		public $accountId;
		public $charGuid;
		public $server;
		public $originalName;
		public $level;
		public $playerIp;
		public $state;
		public $accountMj;
		public $datePost;
		public $dateEnd;
		public $com;
		public $comJoueur;
		
		public $BDD = 'manager';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
		public function checkExistRecupChar($char) {
			$cleanName = cleanCaracteresSpeciaux(strtoupper(trim($char)));
			$O = getOjoo();
			$test = $O->bdd->manager->query("SELECT COUNT(*) AS nb FROM web_entete_recup WHERE UPPER(originalName) LIKE'%" . $cleanName ."%'")->fetch();
			// echo $test['nb'];
			if ($test['nb'] > 1) return true;
			else {
				$test = $O->bdd->manager->query("SELECT COUNT(*) AS nb FROM web_entete_recup WHERE UPPER(originalName) LIKE '%" . strtoupper($char) ."%'")->fetch();
				if ($test['nb'] > 1) return true;
				else  return false;
			}
			
			return false;
		}
		
		public function getExistRecupChar($char) {
			$cleanName = cleanCaracteresSpeciaux(strtoupper(trim($char)));
			$O = getOjoo();
			$test = $O->bdd->manager->query("SELECT * FROM web_entete_recup WHERE UPPER(originalName) LIKE'%" . $cleanName ."%'")->fetchAll();
			if ($test != false) return $test;
			return false;
		}		
		
		public function getDoubleCharClass($accountId) {
			$O = getOjoo();
			$chars = $O->modele->web_entete_recup->select_accountId($accountId)->fetchAll();
			$sameClass = false;
			$count = 0;
			$lastClass = false;
			foreach ($chars as $char) {
				$class = $O->bdd->char->query("SELECT class FROM characters WHERE guid=" . $char['charGuid'])->fetch();
				if ($lastClass != false) {
					if ($class['class'] == $lastClass) {	
						$sameClass = true;
						$count++;
					}					
				}
				// echo "Dernière classe active : " . $lastClass . "<br />";
				// echo "Classe active : " . $class['class'] . "<br />";
				// echo "Compteur : " . $count . "<br />";
				// echo " Etat : " . $sameClass . "<br />";
				$lastClass = $class['class'];
			}
			if ($sameClass) return $count;
			else			return false;
		}
		
		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}
                
                public function isEditable($state) {
                    if ($state == 3) return true;
                    else             return false;
                }
                               

		public function set_accountId($set_value,$id) {
			$this->edit(array('accountId' => $set_value),array('id' => $id ));
		}

		public function set_charGuid($set_value,$id) {
			$this->edit(array('charGuid' => $set_value),array('id' => $id ));
		}

		public function set_server($set_value,$id) {
			$this->edit(array('server' => $set_value),array('id' => $id ));
		}

		public function set_originalName($set_value,$id) {
			$this->edit(array('originalName' => $set_value),array('id' => $id ));
		}

		public function set_playerIp($set_value,$id) {
			$this->edit(array('playerIp' => $set_value),array('id' => $id ));
		}

		public function set_state($set_value,$id) {
			$this->edit(array('state' => $set_value),array('id' => $id ));
		}

		public function set_accountMj($set_value,$id) {
			$this->edit(array('accountMj' => $set_value),array('id' => $id ));
		}

		public function set_datePost($set_value,$id) {
			$this->edit(array('datePost' => $set_value),array('id' => $id ));
		}

		public function set_dateEnd($set_value,$id) {
			$this->edit(array('dateEnd' => $set_value),array('id' => $id ));
		}

		public function set_com($set_value,$id) {
			$this->edit(array('com' => $set_value),array('id' => $id ));
		}
                
                public function set_where($val) {
                    $this->where = $val;
                }

                public function getInfosByState($state) {
                   switch ($state) {
                        case 1:
                            $etat = "En attente";
                            $color = "orange";
                            $img = "hourglass.png";
                            break;

                        case 2:
                            $etat = "Prise en charge";
                            $color = "#435e1f";
                            $img = "restaurant_menu.png";
                            break;

                        case 3:
                            $etat = "En attente de changement";
                            $color = "#9d0d1e";
                            $img = "alarm_bell.png";
                            break;

                        case 4:
                            $etat = "En attente de récupération en jeu";
                            $color = "#435e1f";
                            $img = "hourglass_add.png";
                            break;

                        case 5:
                            $etat = "Récupération validée et terminée.";
                            $color = "#4f9b25";
                            $img = "accept.png";
                            break;

                        case 6:
                            $etat = "Refusée définitivement.";
                            $color = "red";
                            $img = "exclamation.png";
                            break;
                    }
                    
                    return array('etat' => $etat, 'color' => $color, 'img' => $img);
                }
		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('web_entete_recup');
		}
		

		public function select_id($name) {
			return $this->select('web_entete_recup','id="' . $name . '"');
		}

		public function select_accountId($name) {
			return $this->select('web_entete_recup','accountId="' . $name . '"');
		}

		public function select_charGuid($name) {
			return $this->select('web_entete_recup','charGuid="' . $name . '"');
		}

		public function select_server($name) {
			return $this->select('web_entete_recup','server="' . $name . '"');
		}

		public function select_originalName($name) {
			return $this->select('web_entete_recup','originalName="' . $name . '"');
		}

		public function select_playerIp($name) {
			return $this->select('web_entete_recup','playerIp="' . $name . '"');
		}

		public function select_state($name) {
			return $this->select('web_entete_recup','state="' . $name . '"');
		}

		public function select_accountMj($name) {
			return $this->select('web_entete_recup','accountMj="' . $name . '"');
		}

		public function select_datePost($name) {
			return $this->select('web_entete_recup','datePost="' . $name . '"');
		}

		public function select_dateEnd($name) {
			return $this->select('web_entete_recup','dateEnd="' . $name . '"');
		}

		public function select_com($name) {
			return $this->select('web_entete_recup','com="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>