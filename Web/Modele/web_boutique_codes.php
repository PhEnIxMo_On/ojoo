<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Vendredi 26 Juillet 2013
	****************************************************/
	class web_boutique_codes extends mvc_bdd  {
		public $id;
		public $accountId;
		public $charGuid;
		public $code;
		public $date;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_accountId($set_value,$id) {
			$this->edit(array('accountId' => $set_value),array('id' => $id ));
		}

		public function set_charGuid($set_value,$id) {
			$this->edit(array('charGuid' => $set_value),array('id' => $id ));
		}

		public function set_code($set_value,$id) {
			$this->edit(array('code' => $set_value),array('id' => $id ));
		}

		public function set_date($set_value,$id) {
			$this->edit(array('date' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('web_boutique_codes');
		}
		

		public function select_id($name) {
			return $this->select('web_boutique_codes','id="' . $name . '"');
		}

		public function select_accountId($name) {
			return $this->select('web_boutique_codes','accountId="' . $name . '"');
		}

		public function select_charGuid($name) {
			return $this->select('web_boutique_codes','charGuid="' . $name . '"');
		}

		public function select_code($name) {
			return $this->select('web_boutique_codes','code="' . $name . '"');
		}

		public function select_date($name) {
			return $this->select('web_boutique_codes','date="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>