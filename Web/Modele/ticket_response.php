<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Lundi 29 Juillet 2013
	****************************************************/
	class ticket_response extends mvc_bdd  {
		public $id;
		public $ticketId;
		public $accountId;
		public $message;
		public $date;
		public $isGm;
		public $charGuid;
		public $viewMj;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_ticketId($set_value,$id) {
			$this->edit(array('ticketId' => $set_value),array('id' => $id ));
		}

		public function set_accountId($set_value,$id) {
			$this->edit(array('accountId' => $set_value),array('id' => $id ));
		}

		public function set_message($set_value,$id) {
			$this->edit(array('message' => $set_value),array('id' => $id ));
		}

		public function set_date($set_value,$id) {
			$this->edit(array('date' => $set_value),array('id' => $id ));
		}
		
		public function set_viewMj($set_value,$id) {
			$this->edit(array('viewMj' => $set_value),array('id' => $id ));
		}		

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('ticket_response');
		}
		

		public function select_id($name) {
			return $this->select('ticket_response','id="' . $name . '"');
		}

		public function select_ticketId($name) {
			return $this->select('ticket_response','ticketId="' . $name . '"');
		}

		public function select_accountId($name) {
			return $this->select('ticket_response','accountId="' . $name . '"');
		}

		public function select_message($name) {
			return $this->select('ticket_response','message="' . $name . '"');
		}

		public function select_date($name) {
			return $this->select('ticket_response','date="' . $name . '"');
		}


		// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>