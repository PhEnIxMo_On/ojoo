<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Vendredi 2 Ao�t 2013
	****************************************************/
	class notifications extends mvc_bdd  {
		public $id;
		public $sender;
		public $receiver;
		public $title;
		public $content;
		public $flags;
		public $type;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}
		
		public function setAsRead($id) {
			$O = getOjoo();
			$O->bdd->site->query("UPDATE notifications SET flags=(flags & (~ " . OxyNotification::FLAG_IS_READ . ")) WHERE id=" . $id);
			return true;
		}

		public function set_sender($set_value,$id) {
			$this->edit(array('sender' => $set_value),array('id' => $id ));
		}

		public function set_receiver($set_value,$id) {
			$this->edit(array('receiver' => $set_value),array('id' => $id ));
		}

		public function set_title($set_value,$id) {
			$this->edit(array('title' => $set_value),array('id' => $id ));
		}

		public function set_content($set_value,$id) {
			$this->edit(array('content' => $set_value),array('id' => $id ));
		}

		public function set_flags($set_value,$id) {
			$this->edit(array('flags' => $set_value),array('id' => $id ));
		}

		public function set_type($set_value,$id) {
			$this->edit(array('type' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		
		public function getUnreadNotifications() {
			$O = getOjoo();
			return $O->bdd->site->query("SELECT * FROM notifications WHERE receiver=" . $O->wowUser->getAccountId() . " AND flags >= 256")->fetchAll();
		}
		
		public function select_all() {
			return $this->select('notifications');
		}
		

		public function select_id($name) {
			return $this->select('notifications','id="' . $name . '"');
		}

		public function select_sender($name) {
			return $this->select('notifications','sender="' . $name . '"');
		}

		public function select_receiver($name) {
			return $this->select('notifications','receiver="' . $name . '"');
		}

		public function select_title($name) {
			return $this->select('notifications','title="' . $name . '"');
		}

		public function select_content($name) {
			return $this->select('notifications','content="' . $name . '"');
		}

		public function select_flags($name) {
			return $this->select('notifications','flags="' . $name . '"');
		}

		public function select_type($name) {
			return $this->select('notifications','type="' . $name . '"');
		}


		// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>