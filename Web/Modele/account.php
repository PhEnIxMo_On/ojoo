<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Vendredi 11 Janvier 2013
	****************************************************/
	class account extends mvc_bdd  {
		const TIMESTAMP_OXYGEN_CREATION = 1366408799;
		
		public $id;
		public $username;
		public $sha_pass_hash;
		public $sessionkey;
		public $v;
		public $s;
		public $email;
		public $joindate;
		public $last_ip;
		public $failed_logins;
		public $locked;
		public $last_login;
		public $online;
		public $expansion;
		public $mutetime;
		public $locale;
		public $os;
		public $recruiter;
		
		public $BDD = 'auth';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
		
		public function getAccountByGmLevel ($gmlevel) {
			$O = getOjoo();
			return $O->bdd->auth->query("SELECT account.username , account.id FROM account,account_access WHERE account.id=account_access.id AND account_access.gmlevel > " . intval($gmlevel) )->fetchAll();
		}	
		
		public function encryptPass($username,$pass) {
                    return SHA1(strtoupper($username . ':' . $pass));
		}
                
        public function changePassword($username,$password,$accountId,$crypt = true) {
            $O = getOjoo();
            if ($crypt)
                $password = $this->encryptPass($username,$password);
            $O->bdd->auth->query("UPDATE account SET sha_pass_hash='" . $password . "',v=null WHERE id=" . $accountId);
        }
		
		public function isValidPassword($pass,$user) {
			$O = getOjoo();
			$test = $O->bdd->auth->query("SELECT COUNT(id) FROM account WHERE sha_pass_hash='" . $pass . "' AND username='" . $user . "'")->fetch();
			if ($test != false) return true;
			else				return false;
		}

		public function searchAccount(Sort $sort ) {
			$O = getOjoo();
			return $O->bdd->auth->query("SELECT * FROM account " . $sort->sqlByActiveFilter())->fetchAll();
		}
                
		public function searchAccountByUsername($username) {
			$O = getOjoo();
			return $O->bdd->auth->query("SELECT id,username FROM account WHERE username LIKE '%" . $O->security->clean_input_keys($username) . "%'")->fetchAll();
		}

		public function createAccount($username,$pass) {
			$O = getOjoo();
			$testPseudo = $O->bdd->auth->query("SELECT COUNT(id) AS nb FROM account WHERE username='" . strtoupper($O->security->clean_input_keys($username)) . "'")->fetch();
			if ($testPseudo['nb'] > 0) { return FALSE; }
			else {
				$O->bdd->auth->query("INSERT INTO account (username,sha_pass_hash,joindate,expansion) VALUES('" . $O->security->clean_input_keys($username) . "','" . $O->security->clean_input_keys($pass) . "',CURRENT_TIMESTAMP(),2)");
				return TRUE;
			}
		}
		
		public function id_to_pseudo($id) {
			$O = getOjoo();
			$data = $O->bdd->auth->query("SELECT username FROM account WHERE id='" . intval($id) . "'")->fetch();
			return $data['username'];
		}

		public function countAll() {
			$O = getOjoo();
			$count = $O->bdd->auth->query("SELECT COUNT(*) AS nb FROM account")->fetch();
			return $count['nb'];
		}
		
		public function getNewAccount($since) {
			$O = getOjoo();
			$chars = $O->bdd->auth->query("SELECT COUNT(id) AS nb FROM account WHERE UNIX_TIMESTAMP(joindate)>" . $since . "")->fetch();
			return $chars['nb'];
		}

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function getAccountName($id) {
			$O = getOjoo();
			$name = $O->bdd->auth->query("SELECT username FROM account WHERE id=" . $id)->fetch();
			if ($name != 'null') return $name['username'];
			else                 return false;
		}

		public function set_username($set_value,$id) {
			$this->edit(array('username' => $set_value),array('id' => $id ));
		}

		public function set_sha_pass_hash($set_value,$id) {
			$this->edit(array('sha_pass_hash' => $set_value),array('id' => $id ));
		}

		public function set_sessionkey($set_value,$id) {
			$this->edit(array('sessionkey' => $set_value),array('id' => $id ));
		}

		public function set_v($set_value,$id) {
			$this->edit(array('v' => $set_value),array('id' => $id ));
		}

		public function set_s($set_value,$id) {
			$this->edit(array('s' => $set_value),array('id' => $id ));
		}

		public function set_email($set_value,$id) {
			$this->edit(array('email' => $set_value),array('id' => $id ));
		}

		public function set_joindate($set_value,$id) {
			$this->edit(array('joindate' => $set_value),array('id' => $id ));
		}

		public function set_last_ip($set_value,$id) {
			$this->edit(array('last_ip' => $set_value),array('id' => $id ));
		}

		public function set_failed_logins($set_value,$id) {
			$this->edit(array('failed_logins' => $set_value),array('id' => $id ));
		}

		public function set_locked($set_value,$id) {
			$this->edit(array('locked' => $set_value),array('id' => $id ));
		}

		public function set_last_login($set_value,$id) {
			$this->edit(array('last_login' => $set_value),array('id' => $id ));
		}

		public function set_online($set_value,$id) {
			$this->edit(array('online' => $set_value),array('id' => $id ));
		}

		public function set_expansion($set_value,$id) {
			$this->edit(array('expansion' => $set_value),array('id' => $id ));
		}

		public function set_mutetime($set_value,$id) {
			$this->edit(array('mutetime' => $set_value),array('id' => $id ));
		}

		public function set_locale($set_value,$id) {
			$this->edit(array('locale' => $set_value),array('id' => $id ));
		}

		public function set_os($set_value,$id) {
			$this->edit(array('os' => $set_value),array('id' => $id ));
		}

		public function set_recruiter($set_value,$id) {
			$this->edit(array('recruiter' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('account');
		}
		

		public function select_id($name) {
			return $this->select('account','id="' . $name . '"');
		}

		public function select_username($name) {
			return $this->select('account','username="' . $name . '"');
		}

		public function select_sha_pass_hash($name) {
			return $this->select('account','sha_pass_hash="' . $name . '"');
		}

		public function select_sessionkey($name) {
			return $this->select('account','sessionkey="' . $name . '"');
		}

		public function select_v($name) {
			return $this->select('account','v="' . $name . '"');
		}

		public function select_s($name) {
			return $this->select('account','s="' . $name . '"');
		}

		public function select_email($name) {
			return $this->select('account','email="' . $name . '"');
		}

		public function select_joindate($name) {
			return $this->select('account','joindate="' . $name . '"');
		}

		public function select_last_ip($name) {
			return $this->select('account','last_ip="' . $name . '"');
		}

		public function select_failed_logins($name) {
			return $this->select('account','failed_logins="' . $name . '"');
		}

		public function select_locked($name) {
			return $this->select('account','locked="' . $name . '"');
		}

		public function select_last_login($name) {
			return $this->select('account','last_login="' . $name . '"');
		}

		public function select_online($name) {
			return $this->select('account','online="' . $name . '"');
		}

		public function select_expansion($name) {
			return $this->select('account','expansion="' . $name . '"');
		}

		public function select_mutetime($name) {
			return $this->select('account','mutetime="' . $name . '"');
		}

		public function select_locale($name) {
			return $this->select('account','locale="' . $name . '"');
		}

		public function select_os($name) {
			return $this->select('account','os="' . $name . '"');
		}

		public function select_recruiter($name) {
			return $this->select('account','recruiter="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>