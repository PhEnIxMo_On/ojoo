<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Dimanche 10 Fevrier 2013
	****************************************************/
	class doc_id extends mvc_bdd  {
		public $id;
		public $titre;
		public $content;
		public $userId;
		public $date;
		
		public $BDD = 'world';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_titre($set_value,$id) {
			$this->edit(array('titre' => $set_value),array('id' => $id ));
		}

		public function set_content($set_value,$id) {
			$this->edit(array('content' => $set_value),array('id' => $id ));
		}

		public function set_userId($set_value,$id) {
			$this->edit(array('userId' => $set_value),array('id' => $id ));
		}

		public function set_date($set_value,$id) {
			$this->edit(array('date' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('doc_id');
		}
		

		public function select_id($name) {
			return $this->select('doc_id','id="' . $name . '"');
		}

		public function select_titre($name) {
			return $this->select('doc_id','titre="' . $name . '"');
		}

		public function select_content($name) {
			return $this->select('doc_id','content="' . $name . '"');
		}

		public function select_userId($name) {
			return $this->select('doc_id','userId="' . $name . '"');
		}

		public function select_date($name) {
			return $this->select('doc_id','date="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>