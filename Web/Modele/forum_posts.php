<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mercredi 28 Novembre 2012
	****************************************************/
	class forum_posts extends mvc_bdd  {
		public $id;
		public $message;
		public $userId;
		public $topicId;
		public $date;
		
		public $BDD = 'cp';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_message($set_value,$id) {
			$this->edit(array('message' => $set_value),array('id' => $id ));
		}

		public function set_userId($set_value,$id) {
			$this->edit(array('userId' => $set_value),array('id' => $id ));
		}

		public function set_topicId($set_value,$id) {
			$this->edit(array('topicId' => $set_value),array('id' => $id ));
		}

		public function set_date($set_value,$id) {
			$this->edit(array('date' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('forum_posts');
		}
		

		public function select_id($name) {
			return $this->select('forum_posts','id="' . $name . '"');
		}

		public function select_message($name) {
			return $this->select('forum_posts','message="' . $name . '"');
		}

		public function select_userId($name) {
			return $this->select('forum_posts','userId="' . $name . '"');
		}

		public function select_topicId($name) {
			return $this->select('forum_posts','topicId="' . $name . '"');
		}

		public function select_date($name) {
			return $this->select('forum_posts','date="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>