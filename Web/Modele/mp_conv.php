<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Mercredi 22 Mai 2013
	****************************************************/
	class mp_conv extends mvc_bdd  {
                const TYPE_NORMAL = 1;
                const TYPE_MODERATION = 2;
                const TYPE_OFFICIAL = 3;
                const TYPE_RECUP = 4;
                const TYPE_GUILDE = 5;
            
            
		public $id;
		public $accountId;
		public $subject;
		public $dateCreate;
		public $modFlag;
                public $type;
                public $view;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :
                
                public function addMessage($creator,$message,$convId) {
                    $O = getOjoo();                    
                    
                    $O->modele->mp_message->accountId = $creator;
                    $O->modele->mp_message->convId = $convId;
                    $O->modele->mp_message->message = $message;
                    $O->modele->mp_message->dateCreate = time();
                    $O->modele->mp_message->ADD();                    
                }
                
                public function getUnreadConv($accountId) {
                    $O = getOjoo();
                    $nb = $O->bdd->site->query("SELECT COUNT(mv.id) AS nb FROM mp_conv mv,mp_user mu WHERE mv.view=0 AND mv.id=mu.convId AND mu.accountId=" . $accountId)->fetch();
                    return $nb['nb'];
                }
                
                
                public function selectUserConv($accountId) {
                    $O = getOjoo();
                    return $O->bdd->site->query("SELECT mv.* FROM mp_conv mv,mp_user mu WHERE mv.id=mu.convId AND mu.accountId=" . $accountId)->fetchAll();
                }
                
                public function createConv($creator,$destination,$subject,$message,$type = 1) {
                    $O = getOjoo();
                    $O->modele->mp_conv->accountId = $creator;
                    $O->modele->mp_conv->subject = $subject;
                    $O->modele->mp_conv->dateCreate = time();
                    $O->modele->mp_conv->modFlag = 0;
                    $O->modele->mp_conv->type = $type;
                    $O->modele->mp_conv->view = 0;
                    $O->modele->mp_conv->ADD();
                    
                    $convId = $O->bdd->site->last_id();
                    
                    $O->modele->mp_message->accountId = $creator;
                    $O->modele->mp_message->convId = $convId;
                    $O->modele->mp_message->message = $message;
                    $O->modele->mp_message->dateCreate = time();
                    $O->modele->mp_message->ADD();
                    
                    $O->modele->mp_user->convId = $convId;
                    $O->modele->mp_user->accountId = $creator;
                    $O->modele->mp_user->ADD();
                      
                    if (is_array($destination)) {
                        foreach ($destination as $user) {

                            $testMj = $O->modele->account_access->select_id($user)->fetch();
                            if ($testMj == false) $type = 1;
                            else {
                                if ($testMj['gmlevel'] > 0) $O->modele->mp_conv->set_type(3,$convId);
                            }
                            $O->modele->mp_user->convId = $convId;
                            $O->modele->mp_user->accountId = $user;
                            $O->modele->mp_user->ADD();
                        }                            
                    } else $O->console->debug("createConv(\$creator,\$destination,\$subject,\$message,\$type = 1) \$destination doit être un array.","[MP_CONV]");
                }
                
                public function getInfoByType($type) {
                    $infos = array();
                    switch ($type) {
                        case mp_conv::TYPE_NORMAL:
                            $infos['icon'] = '';
                            $infos['label'] = '';
                            break;
                        
                        case mp_conv::TYPE_MODERATION:
                            $infos['icon'] = 'Web/Images/icons/16x16/stop.png';
                            $infos['label'] = ' <span style="color: red; font-weight: bold;">[Modérateurs avertis]</span> ';
                            break;
                        
                        case mp_conv::TYPE_OFFICIAL:
                            $infos['icon'] = 'Web/Images/icons/16x16/gm.gif';
                            $infos['label'] = ' <span style="color: #162428;font-weight: bold;">[STAFF OXYGEN]</span> ';                            
                            break;
                        
                        case mp_conv::TYPE_RECUP:
                            $infos['icon'] = 'Web/Images/icons/16x16/recycle.png';
                            $infos['label'] = ' <span style="color: #162428;font-weight: bold;">[STAFF OXYGEN RECUPERATION]</span> ';                            
                            break;                        
                        
                        case mp_conv::TYPE_GUILDE:
                            $infos['icon'] = 'Web/Images/icons/16x16/user.png';
                            $infos['label'] = ' <span style="color: #162428;font-weight: bold;">[STAFF OXYGEN GUILDE]</span> ';                            
                            break;    
                        
                        
                        default:
                            $infos['icon'] = '';
                            $infos['label'] = '';                            
                        
                    }
                    
                    return $infos;
                }

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_accountId($set_value,$id) {
			$this->edit(array('accountId' => $set_value),array('id' => $id ));
		}

		public function set_subject($set_value,$id) {
			$this->edit(array('subject' => $set_value),array('id' => $id ));
		}

		public function set_dateCreate($set_value,$id) {
			$this->edit(array('dateCreate' => $set_value),array('id' => $id ));
		}

		public function set_modFlag($set_value,$id) {
			$this->edit(array('modFlag' => $set_value),array('id' => $id ));
		}
                
		public function set_type($set_value,$id) {
			$this->edit(array('type' => $set_value),array('id' => $id ));
		}          
                
		public function set_view($set_value,$id) {
			$this->edit(array('view' => $set_value),array('id' => $id ));
		}                
                

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('mp_conv');
		}
		

		public function select_id($name) {
			return $this->select('mp_conv','id="' . $name . '"');
		}

		public function select_accountId($name) {
			return $this->select('mp_conv','accountId="' . $name . '"');
		}

		public function select_subject($name) {
			return $this->select('mp_conv','subject="' . $name . '"');
		}

		public function select_dateCreate($name) {
			return $this->select('mp_conv','dateCreate="' . $name . '"');
		}

		public function select_modFlag($name) {
			return $this->select('mp_conv','modFlag="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>