<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Vendredi 11 Janvier 2013
	****************************************************/
	class characters extends mvc_bdd  {
		const TIMESTAMP_OXYGEN_CREATION = 1366408799;
	
		public $guid;
		public $account;
		public $name;
		public $slot;
		public $race;
		public $class;
		public $gender;
		public $level;
		public $xp;
		public $money;
		public $playerBytes;
		public $playerBytes2;
		public $playerFlags;
		public $position_x;
		public $position_y;
		public $position_z;
		public $map;
		public $instance_id;
		public $instance_mode_mask;
		public $orientation;
		public $taximask;
		public $online;
		public $cinematic;
		public $totaltime;
		public $leveltime;
		public $logout_time;
		public $is_logout_resting;
		public $rest_bonus;
		public $resettalents_cost;
		public $resettalents_time;
		public $talentTree;
		public $trans_x;
		public $trans_y;
		public $trans_z;
		public $trans_o;
		public $transguid;
		public $extra_flags;
		public $stable_slots;
		public $at_login;
		public $zone;
		public $death_expire_time;
		public $taxi_path;
		public $totalKills;
		public $todayKills;
		public $yesterdayKills;
		public $chosenTitle;
		public $watchedFaction;
		public $drunk;
		public $health;
		public $power1;
		public $power2;
		public $power3;
		public $power4;
		public $power5;
		public $latency;
		public $speccount;
		public $activespec;
		public $exploredZones;
		public $equipmentCache;
		public $knownTitles;
		public $actionBars;
		public $grantableLevels;
		public $guildId;
		public $deleteInfos_Account;
		public $deleteInfos_Name;
		public $deleteDate;
		
		public $BDD = 'char';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function countAll() {
			$O = getOjoo();
			$count = $O->bdd->char->query("SELECT COUNT(*) AS nb FROM characters")->fetch();
			return $count['nb'];
		}
                
		public function getCharNameByGuid($guid) {
			$O = getOjoo();
			$nameData = $O->bdd->char->query("SELECT name FROM characters WHERE guid='" . intval($guid) . "'")->fetch();
			if ($nameData != false) return $nameData['name'];
                        else                    return false;
		}
                
        public function getMainChar($accId) {
            $O = getOjoo();
            $mainChar = $O->bdd->char->query("SELECT name FROM characters WHERE account='" . $accId . "' ORDER BY totaltime DESC LIMIT 0,1")->fetch();
            return $mainChar['name'];
        }
                
        public function getMainCharA($accId) {
            $O = getOjoo();
            $mainChar = $O->bdd->char->query("SELECT * FROM characters WHERE account='" . $accId . "' ORDER BY totaltime DESC LIMIT 0,1")->fetch();
            return $mainChar;
        }                
		
		public function getOnlinePlayers() {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE online=1 AND (race=1 OR race=2 OR race=3 OR race=4 OR race=5 OR race=6 OR race=7 OR race=8 OR race=10 OR race=11)")->fetch();
			return $chars['nb'];
			
		}
		
		public function getAccountByGuid($guid) {
			$O = getOjoo();
			$acc = $O->bdd->char->query("SELECT account FROM characters WHERE guid=" . $guid)->fetch();
			return $acc['account'];
		}
		
		public function getAllianceOnlinePlayers() {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE online=1 AND (race=1 OR race=3 OR race=4 OR race=7 OR race=11)")->fetch();
			return $chars['nb'];
		}
		
		public function getHordeOnlinePlayers() {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE online=1 AND (race=2 OR race=5 OR race=6 OR race=8 OR race=10)")->fetch();
			return $chars['nb'];
		}
		
		public function getAlliancePlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE logout_time>" . $since ." AND (race=1 OR race=3 OR race=4 OR race=7 OR race=11)")->fetch();
			return $chars['nb'];
		}
		
		public function getHordePlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE logout_time>" . $since ." AND (race=2 OR race=5 OR race=6 OR race=8 OR race=10)")->fetch();
			return $chars['nb'];
		}
		
		public function getPlayers($since) {
			$nb = $this->getAlliancePlayers($since) + $this->getHordePlayers($since);
			return $nb;
		}
		
		public function getPercentFromAlliance($since, $value) {
			if($this->getAlliancePlayers($since)>0)
				$value = round(($value * 100)/ $this->getAlliancePlayers($since),1);
			else
				$value = round(($value * 100)/ 1,2);
			return $value;
		}
		
		public function getPercentFromHorde($since, $value) {
			if($this->getHordePlayers($since)>0)
				$value = round(($value * 100)/ $this->getHordePlayers($since),1);
			else
				$value = round(($value * 100)/ 1,2);
			return $value;
		}
		
		public function getPercentFromPlayers($since, $value) {
			if($this->getPlayers($since)>0)
				$value = round(($value * 100)/ $this->getPlayers($since),2);
			else
				$value = round(($value * 100)/ 1,2);
			return $value;
		}
		
		public function getLvlPlayers($since, $lvl) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE level='" . $lvl . "' AND logout_time>" . $since ."")->fetch();
			return $chars['nb'];
		}
		
		public function getLvlBetweenPlayers($since, $lvlMin, $lvlMax) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE logout_time>" . $since ." AND level BETWEEN '" . $lvlMin . "' AND '" . $lvlMax . "'")->fetch();
			return $chars['nb'];
		}
		
		
		public function getHumanPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE race=1 AND logout_time>" . $since ."")->fetch();
			return $chars['nb'];
		}
		
		public function getOrcPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE race=2 AND logout_time>" . $since ."")->fetch();
			return $chars['nb'];
		}
		
		public function getDwarfPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE race=3 AND logout_time>" . $since ."")->fetch();
			return $chars['nb'];
		}
		
		public function getNightElfPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE race=4 AND logout_time>" . $since ."")->fetch();
			return $chars['nb'];
		}
		public function getUndeadPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE race=5 AND logout_time>" . $since ."")->fetch();
			return $chars['nb'];
		}
		public function getTaurenPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE race=6 AND logout_time>" . $since ."")->fetch();
			return $chars['nb'];
		}
		public function getGnomePlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE race=7 AND logout_time>" . $since ."")->fetch();
			return $chars['nb'];
		}
		public function getTrollPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE race=8 AND logout_time>" . $since ."")->fetch();
			return $chars['nb'];
		}
		public function getBloodElfPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE race=10 AND logout_time>" . $since ."")->fetch();
			return $chars['nb'];
		}
		
		public function getDraeneiPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE race=11 AND logout_time>" . $since ."")->fetch();
			return $chars['nb'];
		}
		
		public function getAllianceWarPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=1 AND logout_time>" . $since ." AND (race=1 OR race=3 OR race=4 OR race=7 OR race=11)")->fetch();
			return $chars['nb'];
		}
		
		public function getAlliancePaladinPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=3 AND logout_time>" . $since ." AND (race=1 OR race=3 OR race=4 OR race=7 OR race=11)")->fetch();
			return $chars['nb'];
		}
		
		public function getAllianceHunterPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=3 AND logout_time>" . $since ." AND (race=1 OR race=3 OR race=4 OR race=7 OR race=11)")->fetch();
			return $chars['nb'];
		}
		
		public function getAllianceRoguePlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=4 AND logout_time>" . $since ." AND (race=1 OR race=3 OR race=4 OR race=7 OR race=11)")->fetch();
			return $chars['nb'];
		}
		
		public function getAlliancePriestPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=5 AND logout_time>" . $since ." AND (race=1 OR race=3 OR race=4 OR race=7 OR race=11)")->fetch();
			return $chars['nb'];
		}
		
		public function getAllianceDKPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=6 AND logout_time>" . $since ." AND (race=1 OR race=3 OR race=4 OR race=7 OR race=11)")->fetch();
			return $chars['nb'];
		}
		
		public function getAllianceShamanPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=7 AND logout_time>" . $since ." AND (race=1 OR race=3 OR race=4 OR race=7 OR race=11)")->fetch();
			return $chars['nb'];
		}
		
		public function getAllianceMagePlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=8 AND logout_time>" . $since ." AND (race=1 OR race=3 OR race=4 OR race=7 OR race=11)")->fetch();
			return $chars['nb'];
		}
		
		public function getAllianceWarlockPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=9 AND logout_time>" . $since ." AND (race=1 OR race=3 OR race=4 OR race=7 OR race=11)")->fetch();
			return $chars['nb'];
		}
		
		public function getAllianceDruidPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=11 AND logout_time>" . $since ." AND (race=1 OR race=3 OR race=4 OR race=7 OR race=11)")->fetch();
			return $chars['nb'];
		}
		
		public function getHordeWarPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=1 AND logout_time>" . $since ." AND (race=2 OR race=5 OR race=6 OR race=8 OR race=10)")->fetch();
			return $chars['nb'];
		}
		
		public function getHordePaladinPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=3 AND logout_time>" . $since ." AND (race=2 OR race=5 OR race=6 OR race=8 OR race=10)")->fetch();
			return $chars['nb'];
		}
		
		public function getHordeHunterPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=3 AND logout_time>" . $since ." AND (race=2 OR race=5 OR race=6 OR race=8 OR race=10)")->fetch();
			return $chars['nb'];
		}
		
		public function getHordeRoguePlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=4 AND logout_time>" . $since ." AND (race=2 OR race=5 OR race=6 OR race=8 OR race=10)")->fetch();
			return $chars['nb'];
		}
		
		public function getHordePriestPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=5 AND logout_time>" . $since ." AND (race=2 OR race=5 OR race=6 OR race=8 OR race=10)")->fetch();
			return $chars['nb'];
		}
		
		public function getHordeDKPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=6 AND logout_time>" . $since ." AND (race=2 OR race=5 OR race=6 OR race=8 OR race=10)")->fetch();
			return $chars['nb'];
		}
		
		public function getHordeShamanPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=7 AND logout_time>" . $since ." AND (race=2 OR race=5 OR race=6 OR race=8 OR race=10)")->fetch();
			return $chars['nb'];
		}
		
		public function getHordeMagePlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=8 AND logout_time>" . $since ." AND (race=2 OR race=5 OR race=6 OR race=8 OR race=10)")->fetch();
			return $chars['nb'];
		}
		
		public function getHordeWarlockPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=9 AND logout_time>" . $since ." AND (race=2 OR race=5 OR race=6 OR race=8 OR race=10)")->fetch();
			return $chars['nb'];
		}
		
		public function getHordeDruidPlayers($since) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT COUNT(guid) AS nb FROM characters WHERE class=11 AND logout_time>" . $since ." AND (race=2 OR race=5 OR race=6 OR race=8 OR race=10)")->fetch();
			return $chars['nb'];
		}
		
		public function getCharRecup($accountId) {
			$O = getOjoo();
			$chars = $O->bdd->char->query("SELECT name,guid FROM characters WHERE account=" . intval($accountId) . "")->fetchAll();
			return $chars;
		}

		public function set_guid($set_value,$id) {
			$this->edit(array('guid' => $set_value),array('guid' => $id ));
		}
		
		public function getPercentFaction() {
			$divideBy = $this->getOnlinePlayers();
			if($this->getOnlinePlayers()==0)
				$divideBy =1;
			return array("ally" => round((($this->getAllianceOnlinePlayers()*100)/$divideBy),2),"horde" => round((($this->getHordeOnlinePlayers()*100)/$divideBy),2));
		}

		public function set_account($set_value,$id) {
			$this->edit(array('account' => $set_value),array('guid' => $id ));
		}

		public function set_name($set_value,$id) {
			$this->edit(array('name' => $set_value),array('guid' => $id ));
		}

		public function set_slot($set_value,$id) {
			$this->edit(array('slot' => $set_value),array('guid' => $id ));
		}

		public function set_race($set_value,$id) {
			$this->edit(array('race' => $set_value),array('guid' => $id ));
		}

		public function set_class($set_value,$id) {
			$this->edit(array('class' => $set_value),array('guid' => $id ));
		}

		public function set_gender($set_value,$id) {
			$this->edit(array('gender' => $set_value),array('guid' => $id ));
		}

		public function set_level($set_value,$id) {
			$this->edit(array('level' => $set_value),array('guid' => $id ));
		}

		public function set_xp($set_value,$id) {
			$this->edit(array('xp' => $set_value),array('guid' => $id ));
		}

		public function set_money($set_value,$id) {
			$this->edit(array('money' => $set_value),array('guid' => $id ));
		}

		public function set_playerBytes($set_value,$id) {
			$this->edit(array('playerBytes' => $set_value),array('guid' => $id ));
		}

		public function set_playerBytes2($set_value,$id) {
			$this->edit(array('playerBytes2' => $set_value),array('guid' => $id ));
		}

		public function set_playerFlags($set_value,$id) {
			$this->edit(array('playerFlags' => $set_value),array('guid' => $id ));
		}

		public function set_position_x($set_value,$id) {
			$this->edit(array('position_x' => $set_value),array('guid' => $id ));
		}

		public function set_position_y($set_value,$id) {
			$this->edit(array('position_y' => $set_value),array('guid' => $id ));
		}

		public function set_position_z($set_value,$id) {
			$this->edit(array('position_z' => $set_value),array('guid' => $id ));
		}

		public function set_map($set_value,$id) {
			$this->edit(array('map' => $set_value),array('guid' => $id ));
		}

		public function set_instance_id($set_value,$id) {
			$this->edit(array('instance_id' => $set_value),array('guid' => $id ));
		}

		public function set_instance_mode_mask($set_value,$id) {
			$this->edit(array('instance_mode_mask' => $set_value),array('guid' => $id ));
		}

		public function set_orientation($set_value,$id) {
			$this->edit(array('orientation' => $set_value),array('guid' => $id ));
		}

		public function set_taximask($set_value,$id) {
			$this->edit(array('taximask' => $set_value),array('guid' => $id ));
		}

		public function set_online($set_value,$id) {
			$this->edit(array('online' => $set_value),array('guid' => $id ));
		}

		public function set_cinematic($set_value,$id) {
			$this->edit(array('cinematic' => $set_value),array('guid' => $id ));
		}

		public function set_totaltime($set_value,$id) {
			$this->edit(array('totaltime' => $set_value),array('guid' => $id ));
		}

		public function set_leveltime($set_value,$id) {
			$this->edit(array('leveltime' => $set_value),array('guid' => $id ));
		}

		public function set_logout_time($set_value,$id) {
			$this->edit(array('logout_time' => $set_value),array('guid' => $id ));
		}

		public function set_is_logout_resting($set_value,$id) {
			$this->edit(array('is_logout_resting' => $set_value),array('guid' => $id ));
		}

		public function set_rest_bonus($set_value,$id) {
			$this->edit(array('rest_bonus' => $set_value),array('guid' => $id ));
		}

		public function set_resettalents_cost($set_value,$id) {
			$this->edit(array('resettalents_cost' => $set_value),array('guid' => $id ));
		}

		public function set_resettalents_time($set_value,$id) {
			$this->edit(array('resettalents_time' => $set_value),array('guid' => $id ));
		}

		public function set_talentTree($set_value,$id) {
			$this->edit(array('talentTree' => $set_value),array('guid' => $id ));
		}

		public function set_trans_x($set_value,$id) {
			$this->edit(array('trans_x' => $set_value),array('guid' => $id ));
		}

		public function set_trans_y($set_value,$id) {
			$this->edit(array('trans_y' => $set_value),array('guid' => $id ));
		}

		public function set_trans_z($set_value,$id) {
			$this->edit(array('trans_z' => $set_value),array('guid' => $id ));
		}

		public function set_trans_o($set_value,$id) {
			$this->edit(array('trans_o' => $set_value),array('guid' => $id ));
		}

		public function set_transguid($set_value,$id) {
			$this->edit(array('transguid' => $set_value),array('guid' => $id ));
		}

		public function set_extra_flags($set_value,$id) {
			$this->edit(array('extra_flags' => $set_value),array('guid' => $id ));
		}

		public function set_stable_slots($set_value,$id) {
			$this->edit(array('stable_slots' => $set_value),array('guid' => $id ));
		}

		public function set_at_login($set_value,$id) {
			$this->edit(array('at_login' => $set_value),array('guid' => $id ));
		}

		public function set_zone($set_value,$id) {
			$this->edit(array('zone' => $set_value),array('guid' => $id ));
		}

		public function set_death_expire_time($set_value,$id) {
			$this->edit(array('death_expire_time' => $set_value),array('guid' => $id ));
		}

		public function set_taxi_path($set_value,$id) {
			$this->edit(array('taxi_path' => $set_value),array('guid' => $id ));
		}

		public function set_totalKills($set_value,$id) {
			$this->edit(array('totalKills' => $set_value),array('guid' => $id ));
		}

		public function set_todayKills($set_value,$id) {
			$this->edit(array('todayKills' => $set_value),array('guid' => $id ));
		}

		public function set_yesterdayKills($set_value,$id) {
			$this->edit(array('yesterdayKills' => $set_value),array('guid' => $id ));
		}

		public function set_chosenTitle($set_value,$id) {
			$this->edit(array('chosenTitle' => $set_value),array('guid' => $id ));
		}

		public function set_watchedFaction($set_value,$id) {
			$this->edit(array('watchedFaction' => $set_value),array('guid' => $id ));
		}

		public function set_drunk($set_value,$id) {
			$this->edit(array('drunk' => $set_value),array('guid' => $id ));
		}

		public function set_health($set_value,$id) {
			$this->edit(array('health' => $set_value),array('guid' => $id ));
		}

		public function set_power1($set_value,$id) {
			$this->edit(array('power1' => $set_value),array('guid' => $id ));
		}

		public function set_power2($set_value,$id) {
			$this->edit(array('power2' => $set_value),array('guid' => $id ));
		}

		public function set_power3($set_value,$id) {
			$this->edit(array('power3' => $set_value),array('guid' => $id ));
		}

		public function set_power4($set_value,$id) {
			$this->edit(array('power4' => $set_value),array('guid' => $id ));
		}

		public function set_power5($set_value,$id) {
			$this->edit(array('power5' => $set_value),array('guid' => $id ));
		}

		public function set_latency($set_value,$id) {
			$this->edit(array('latency' => $set_value),array('guid' => $id ));
		}

		public function set_speccount($set_value,$id) {
			$this->edit(array('speccount' => $set_value),array('guid' => $id ));
		}

		public function set_activespec($set_value,$id) {
			$this->edit(array('activespec' => $set_value),array('guid' => $id ));
		}

		public function set_exploredZones($set_value,$id) {
			$this->edit(array('exploredZones' => $set_value),array('guid' => $id ));
		}

		public function set_equipmentCache($set_value,$id) {
			$this->edit(array('equipmentCache' => $set_value),array('guid' => $id ));
		}

		public function set_knownTitles($set_value,$id) {
			$this->edit(array('knownTitles' => $set_value),array('guid' => $id ));
		}

		public function set_actionBars($set_value,$id) {
			$this->edit(array('actionBars' => $set_value),array('guid' => $id ));
		}

		public function set_grantableLevels($set_value,$id) {
			$this->edit(array('grantableLevels' => $set_value),array('guid' => $id ));
		}

		public function set_guildId($set_value,$id) {
			$this->edit(array('guildId' => $set_value),array('guid' => $id ));
		}

		public function set_deleteInfos_Account($set_value,$id) {
			$this->edit(array('deleteInfos_Account' => $set_value),array('guid' => $id ));
		}

		public function set_deleteInfos_Name($set_value,$id) {
			$this->edit(array('deleteInfos_Name' => $set_value),array('guid' => $id ));
		}

		public function set_deleteDate($set_value,$id) {
			$this->edit(array('deleteDate' => $set_value),array('guid' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('characters');
		}
		

		public function select_guid($name) {
			return $this->select('characters','guid="' . $name . '"');
		}

		public function select_account($name) {
			return $this->select('characters','account="' . $name . '"');
		}

		public function select_name($name) {
			return $this->select('characters','name="' . $name . '"');
		}

		public function select_slot($name) {
			return $this->select('characters','slot="' . $name . '"');
		}

		public function select_race($name) {
			return $this->select('characters','race="' . $name . '"');
		}

		public function select_class($name) {
			return $this->select('characters','class="' . $name . '"');
		}

		public function select_gender($name) {
			return $this->select('characters','gender="' . $name . '"');
		}

		public function select_level($name) {
			return $this->select('characters','level="' . $name . '"');
		}

		public function select_xp($name) {
			return $this->select('characters','xp="' . $name . '"');
		}

		public function select_money($name) {
			return $this->select('characters','money="' . $name . '"');
		}

		public function select_playerBytes($name) {
			return $this->select('characters','playerBytes="' . $name . '"');
		}

		public function select_playerBytes2($name) {
			return $this->select('characters','playerBytes2="' . $name . '"');
		}

		public function select_playerFlags($name) {
			return $this->select('characters','playerFlags="' . $name . '"');
		}

		public function select_position_x($name) {
			return $this->select('characters','position_x="' . $name . '"');
		}

		public function select_position_y($name) {
			return $this->select('characters','position_y="' . $name . '"');
		}

		public function select_position_z($name) {
			return $this->select('characters','position_z="' . $name . '"');
		}

		public function select_map($name) {
			return $this->select('characters','map="' . $name . '"');
		}

		public function select_instance_id($name) {
			return $this->select('characters','instance_id="' . $name . '"');
		}

		public function select_instance_mode_mask($name) {
			return $this->select('characters','instance_mode_mask="' . $name . '"');
		}

		public function select_orientation($name) {
			return $this->select('characters','orientation="' . $name . '"');
		}

		public function select_taximask($name) {
			return $this->select('characters','taximask="' . $name . '"');
		}

		public function select_online($name) {
			return $this->select('characters','online="' . $name . '"');
		}

		public function select_cinematic($name) {
			return $this->select('characters','cinematic="' . $name . '"');
		}

		public function select_totaltime($name) {
			return $this->select('characters','totaltime="' . $name . '"');
		}

		public function select_leveltime($name) {
			return $this->select('characters','leveltime="' . $name . '"');
		}

		public function select_logout_time($name) {
			return $this->select('characters','logout_time="' . $name . '"');
		}

		public function select_is_logout_resting($name) {
			return $this->select('characters','is_logout_resting="' . $name . '"');
		}

		public function select_rest_bonus($name) {
			return $this->select('characters','rest_bonus="' . $name . '"');
		}

		public function select_resettalents_cost($name) {
			return $this->select('characters','resettalents_cost="' . $name . '"');
		}

		public function select_resettalents_time($name) {
			return $this->select('characters','resettalents_time="' . $name . '"');
		}

		public function select_talentTree($name) {
			return $this->select('characters','talentTree="' . $name . '"');
		}

		public function select_trans_x($name) {
			return $this->select('characters','trans_x="' . $name . '"');
		}

		public function select_trans_y($name) {
			return $this->select('characters','trans_y="' . $name . '"');
		}

		public function select_trans_z($name) {
			return $this->select('characters','trans_z="' . $name . '"');
		}

		public function select_trans_o($name) {
			return $this->select('characters','trans_o="' . $name . '"');
		}

		public function select_transguid($name) {
			return $this->select('characters','transguid="' . $name . '"');
		}

		public function select_extra_flags($name) {
			return $this->select('characters','extra_flags="' . $name . '"');
		}

		public function select_stable_slots($name) {
			return $this->select('characters','stable_slots="' . $name . '"');
		}

		public function select_at_login($name) {
			return $this->select('characters','at_login="' . $name . '"');
		}

		public function select_zone($name) {
			return $this->select('characters','zone="' . $name . '"');
		}

		public function select_death_expire_time($name) {
			return $this->select('characters','death_expire_time="' . $name . '"');
		}

		public function select_taxi_path($name) {
			return $this->select('characters','taxi_path="' . $name . '"');
		}

		public function select_totalKills($name) {
			return $this->select('characters','totalKills="' . $name . '"');
		}

		public function select_todayKills($name) {
			return $this->select('characters','todayKills="' . $name . '"');
		}

		public function select_yesterdayKills($name) {
			return $this->select('characters','yesterdayKills="' . $name . '"');
		}

		public function select_chosenTitle($name) {
			return $this->select('characters','chosenTitle="' . $name . '"');
		}

		public function select_watchedFaction($name) {
			return $this->select('characters','watchedFaction="' . $name . '"');
		}

		public function select_drunk($name) {
			return $this->select('characters','drunk="' . $name . '"');
		}

		public function select_health($name) {
			return $this->select('characters','health="' . $name . '"');
		}

		public function select_power1($name) {
			return $this->select('characters','power1="' . $name . '"');
		}

		public function select_power2($name) {
			return $this->select('characters','power2="' . $name . '"');
		}

		public function select_power3($name) {
			return $this->select('characters','power3="' . $name . '"');
		}

		public function select_power4($name) {
			return $this->select('characters','power4="' . $name . '"');
		}

		public function select_power5($name) {
			return $this->select('characters','power5="' . $name . '"');
		}

		public function select_latency($name) {
			return $this->select('characters','latency="' . $name . '"');
		}

		public function select_speccount($name) {
			return $this->select('characters','speccount="' . $name . '"');
		}

		public function select_activespec($name) {
			return $this->select('characters','activespec="' . $name . '"');
		}

		public function select_exploredZones($name) {
			return $this->select('characters','exploredZones="' . $name . '"');
		}

		public function select_equipmentCache($name) {
			return $this->select('characters','equipmentCache="' . $name . '"');
		}

		public function select_knownTitles($name) {
			return $this->select('characters','knownTitles="' . $name . '"');
		}

		public function select_actionBars($name) {
			return $this->select('characters','actionBars="' . $name . '"');
		}

		public function select_grantableLevels($name) {
			return $this->select('characters','grantableLevels="' . $name . '"');
		}

		public function select_guildId($name) {
			return $this->select('characters','guildId="' . $name . '"');
		}

		public function select_deleteInfos_Account($name) {
			return $this->select('characters','deleteInfos_Account="' . $name . '"');
		}

		public function select_deleteInfos_Name($name) {
			return $this->select('characters','deleteInfos_Name="' . $name . '"');
		}

		public function select_deleteDate($name) {
			return $this->select('characters','deleteDate="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('guid' => $id));
		}

	}
?>