<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Lundi 22 Juillet 2013
	****************************************************/
	class doc_content extends mvc_bdd  {
		public $id;
		public $id_chapter;
		public $description;
		public $content;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_id_chapter($set_value,$id) {
			$this->edit(array('id_chapter' => $set_value),array('id' => $id ));
		}

		public function set_description($set_value,$id) {
			$this->edit(array('description' => $set_value),array('id' => $id ));
		}

		public function set_content($set_value,$id) {
			$this->edit(array('content' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('doc_content');
		}
		

		public function select_id($name) {
			return $this->select('doc_content','id="' . $name . '"');
		}

		public function select_id_chapter($name) {
			return $this->select('doc_content','id_chapter="' . $name . '"');
		}

		public function select_description($name) {
			return $this->select('doc_content','description="' . $name . '"');
		}

		public function select_content($name) {
			return $this->select('doc_content','content="' . $name . '"');
		}


	// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>