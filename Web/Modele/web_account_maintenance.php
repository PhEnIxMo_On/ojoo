<?php
	/***************************************************
	* Modele généré via Ojoo.
	* Le : Vendredi 17 Mai 2013
	****************************************************/
	class web_account_maintenance extends mvc_bdd  {
		public $id;
		public $accountId;
		public $accountUsername;
		public $oldPassword;
		public $newPassword;
		public $accountMj;
		public $endDate;
		
		public $BDD = 'site';
		public $where;
		public  $liens = array();
		public $actionsLiens = array(
			'onDelete' => false,
			'onSelect' => false
		);
		
		// Fonction de mise à jour :

		public function set_id($set_value,$id) {
			$this->edit(array('id' => $set_value),array('id' => $id ));
		}

		public function set_accountId($set_value,$id) {
			$this->edit(array('accountId' => $set_value),array('id' => $id ));
		}

		public function set_accountUsername($set_value,$id) {
			$this->edit(array('accountUsername' => $set_value),array('id' => $id ));
		}

		public function set_oldPassword($set_value,$id) {
			$this->edit(array('oldPassword' => $set_value),array('id' => $id ));
		}

		public function set_newPassword($set_value,$id) {
			$this->edit(array('newPassword' => $set_value),array('id' => $id ));
		}

		public function set_accountMj($set_value,$id) {
			$this->edit(array('accountMj' => $set_value),array('id' => $id ));
		}

		public function set_endDate($set_value,$id) {
			$this->edit(array('endDate' => $set_value),array('id' => $id ));
		}

		
		// Fonction de sélection :
		public function select_all() {
			return $this->select('web_account_maintenance');
		}
		

		public function select_id($name) {
			return $this->select('web_account_maintenance','id="' . $name . '"');
		}

		public function select_accountId($name) {
			return $this->select('web_account_maintenance','accountId="' . $name . '"');
		}

		public function select_accountUsername($name) {
			return $this->select('web_account_maintenance','accountUsername="' . $name . '"');
		}

		public function select_oldPassword($name) {
			return $this->select('web_account_maintenance','oldPassword="' . $name . '"');
		}

		public function select_newPassword($name) {
			return $this->select('web_account_maintenance','newPassword="' . $name . '"');
		}

		public function select_accountMj($name) {
			return $this->select('web_account_maintenance','accountMj="' . $name . '"');
		}

		public function select_endDate($name) {
			return $this->select('web_account_maintenance','endDate="' . $name . '"');
		}


		// Fonction de suppression : 	

		public function delete_id($id) {
			return $this->delete(array('id' => $id));
		}

	}
?>