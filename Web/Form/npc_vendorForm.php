<?php
class npc_vendorForm extends Form {
	public $entry;
	public $slot;
	public $item;
	public $maxcount;
	public $incrtime;
	public $ExtendedCost;		
	
	public function __construct() {
		$this->entry = new Input(array(
			"type" => Input::INPUT_TEXT,
			"name" => "entry",
			"icon" => "Web/Images/icons/16x16/add.png",
			"contrainte" => Contrainte::getC('entry','Vous devez indiquer l\'ID du PNJ cible',Contrainte::INT),
			"placeholder" => "Entry du PNJ",
			"iconClass" => "iconForm"
		));
		$this->slot = new Input(array(
			"type" => Input::INPUT_HIDDEN,
			"name" => "slot"	
		));
		$this->item = new Input(array(
			"type" => Input::INPUT_TEXTAREA,
			"name" => "item",
			"contrainte" => Contrainte::getC('item','Vous devez indiquer l\'ID du PNJ cible',Contrainte::STRING),
			"placeholder" => "Indiquez l'id des items. Séparer les par une virgule.",
			"cols" => "20",
			"rows" => "110"
		));
		$this->ExtendedCost = new Input(array(
			"type" => Input::INPUT_HIDDEN,
			"name" => "ExtendedCost",
			"value" => "190003"
		));		
		$this->maxcount = new Input(array(
			"type" => Input::INPUT_HIDDEN,
			"name" => "maxcount",
			"value" => "0"
		));			
		$this->incrtime = new Input(array(
			"type" => Input::INPUT_HIDDEN,
			"name" => "incrtime",
			"value" => "0"
		));			
	}
	
}
?>