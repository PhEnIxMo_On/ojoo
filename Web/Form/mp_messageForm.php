<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class mp_messageForm extends Form {
    public $id;
    public $convId;
    public $accountId;
    public $message;
    public $dateCreate;
    
    public function __construct() {
        $O = getOjoo();
        
        $this->id = new Input(array(
            "name" => "id",
            "type" => Input::INPUT_HIDDEN,            
        ));
        
        $this->idConv = new Input(array(
            "name" => "idConv",
            "type" => Input::INPUT_HIDDEN
        ));
        
        $this->accountId = new Input(array(
           "name" => "accountId",
            "type" => Input::INPUT_HIDDEN,
            "value" => $O->wowUser->getAccountId()
        ));
        
        $this->message = new Input(array(
            "name" => "message",
            "type" => Input::INPUT_TEXTAREA,
            "placeholder" => "Inscrivez ici votre message",
            "cols" => 110,
            "rows" => 10,
            "contrainte" => Contrainte::getC('message','Vous devez entrer un message',Contrainte::TEXT)
        ));
    }
 }
?>
