<?php
	class web_boutique_articleForm extends Form {
		public $id;
		public $idCat;
		public $name;
		public $description;
		public $idWowhead;
		public $state;
		public $prixVote;
		public $prixMoney;
		public $icon_name;
		public $nbCharge;
		public $sticky;
		public $stock;
		
		public function __construct() {
			$this->id = new Input(array(
				"name" => "id",
				"type" => Input::INPUT_HIDDEN
			));
			
			$this->id_cat = new Input(array(
				"name" => "idCat",
				"type" => Input::INPUT_HIDDEN
			));
			
			$this->name = new Input(array(
				"name" => "name",
				"type" => Input::INPUT_TEXT,
				'icon' => 'Web/Images/icons/16x16/font.png',
				"help" => "Le nom de l'article",
				"iconClass" => "iconForm",
				"helpClass" => "helpForm",
				"contrainte" => Contrainte::getC('name','Vous devez entrer un nom pour l\'article. BAKA !',Contrainte::STRING)
			));
			
			$this->icon_name = new Input(array(
				"name" => "icon_name",
				"type" => Input::INPUT_TEXT,
				"icon" => "Web/Images/icons/16x16/eye.png",
				"iconClass" => "iconForm",
				"helpClass" => "helpForm",
				"help" => "L'icône de l'article",
				"helpClass" => "helpForm",
				"contrainte" => Contrainte::getC('icon_name','Vous devez entrer le nom de l\'icône.',Contrainte::VARCHAR)
			));
			
			$this->description = new Input(array(
				"name" => "description",
				"type" => Input::INPUT_TEXTAREA,
				'cols' => 10,
				'rows' => 110,
				'class' => 'ckeditor',
				"contrainte" => Contrainte::getC('description','Gros blaireau, entre une description',Contrainte::TEXT)
			));
			
			$this->idCat = new Input(array(
				"name" => "idCat",
				"type" => Input::INPUT_TEXT,
				"icon" => "Web/Images/icons/16x16/box.png",
				"helpClass" => "helpForm",
				"iconClass" => "iconForm",
				"help" => "Catégorie mère"
			));
			
			$this->idWowhead = new Input(array(
				"name" => "idWowhead",
				"type" => Input::INPUT_TEXT,
				"helpClass" => "helpForm",
				"help" => "Identifiant de l'objet (le même que sur wowhead)",
				"contrainte" => Contrainte::getC('idWowhead','Vous devez entrer un identifiant wowhead pour l\'article. BAKA !',Contrainte::INT)
			));
			
			$this->prixVote = new Input(array(
				"name" => "prixVote",
				"type" => Input::INPUT_TEXT,
				"helpClass" => "helpForm",
				"help" => "Prix en point de vote",
				"contrainte" => Contrainte::getC('prixVote','Vous devez entrer un prix en points de vote pour l\'article. BAKA !',Contrainte::INT)
			));
			
			$this->prixMoney = new Input(array(
				"name" => "prixMoney",
				"type" => Input::INPUT_TEXT,
				"helpClass" => "helpForm",
				"help" => "Prix en point Oxygen",
				"contrainte" => Contrainte::getC('prixMoney','Vous devez entrer un prix en points Oxygen pour l\'article. BAKA !',Contrainte::INT)
			));
			
			$this->nbCharge = new Input(array(
				"name" => "nbCharge",
				"type" => Input::INPUT_TEXT,
				"helpClass" => "helpForm",
				"help" => "Nombre de charge de l'object",
				"contrainte" => Contrainte::getC('nbCharge','Vous devez entrer un nombre de charge pour l\'article. BAKA !',Contrainte::INT)
			));
			
			$this->sticky = new Input(array(
				"name" => "sticky",
				"type" => Input::INPUT_TEXT,
				"helpClass" => "helpForm",
				"help" => "Mettre à la valeur '1' pour que l'objet soit 'mis en avant' sur la boutique",
				"contrainte" => Contrainte::getC('sticky','Vous devez entrer un sticky en point Oxygen pour l\'article. BAKA !',Contrainte::INT)
			));
			
			$this->stock = new Input(array(
				"name" => "stock",
				"type" => Input::INPUT_TEXT,
				"helpClass" => "helpForm",
				"help" => "Mettre à la valeur '-1' si l'objet est disponible en quantité illimité",
				"contrainte" => Contrainte::getC('stock','Vous devez entrer un stock pour l\'article. BAKA !',Contrainte::INT)
			));
		}
		
	}
?>