<?php
    class guilde_playersForm extends Form {
        public $id;
        public $guildeId;
        public $accountId;
        public $charGuid;
        public $originalName;
        public $state;
        public $code;
        
        public function __construct() {
            $this->accountId = new Input(array(
                "name" => "accountId",
                "type" => Input::INPUT_HIDDEN,
            ));
            
            $this->charGuid = new InputTable(new Input(array(
                "name" => "charGuid[]",
                "placeholder" => "Nom du personnage à inviter",
                "type" => Input::INPUT_TEXT,
                "icon" => "Web/Images/icons/16x16/recycle.png",
                "help" => "Entrer ici le nom du personnage",
                "validClass" => "validInput2",
                "errorClass" => "errorInput2",                
                "contrainte" => Contrainte::getC('charGuid[]','Vous devez entrer le nom du personnage à inviter.',Contrainte::STRING,$this,false)
            )),20);        
            
            $this->originalName = new InputTable(new Input(array(
                "name" => "originalName[]",
                "placeholder" => "Nom du personnage original",
                "type" => Input::INPUT_TEXT,
                "icon" => "Web/Images/icons/16x16/user.png",
                "validClass" => "validInput2",
                "errorClass" => "errorInput2",
                "help" => "Entrer ici le nom du personnage d'origine",
                "contrainte" => Contrainte::getC('originalName[]','Vous devez entrer le nom du personnage original !',Contrainte::STRING,$this)
            )),0);      
            
            $this->state = new Input(array(
                "name" => "state[]",
                "type" => Input::INPUT_SELECT,
                "icon" => "Web/Images/icons/16x16/list.png",
                "options" => array(
                   1 => "En attente",
                   2 => "Validé",
                   3 => "Expiré"
                ),
                "help" => "Entrer ici le nom du personnage d'origine",
                "contrainte" => Contrainte::getC('state[]','Vous devez entrer un nom de personnage !',Contrainte::INT)
            ));      
            
            $this->code = new Input(array(
                "name" => "code[]",
                "type" => Input::INPUT_TEXT,
                "icon" => "Web/Images/icons/16x16/key.png",
                "placeholder" => "Le code lié à ce personnage",
                "help" => "Le code lié au personnage. <b> Ne le modifiez pas sauf sur permission d'un développeur ! </b>",
                "contrainte" => Contrainte::getC('code[]','Vous devez entrer un code !',Contrainte::VARCHAR)
            ));              
        }
        
        public function addFormContrainte() {
            $this->state->setContrainte(null);
            $this->code->setContrainte(null);
        }
        
    }
?>