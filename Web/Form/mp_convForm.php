<?php

    class mp_convForm extends Form {
        public $id;
        public $accountId;
        public $subject;
        public $dateCreate;
        public $modFlag;
        public $type;
        
        public function __construct() {
            $O = getOjoo();
            
            $this->id = new Input(array(
                "name" => "id",
                "type" => Input::INPUT_HIDDEN
            ));
            
            $this->accountId = new Input(array(
               "name" => "accountId",
               "type" => Input::INPUT_HIDDEN,
               "value" => $O->wowUser->getAccountId(),
            ));
            
            $this->dateCreate = new Input(array(
                "name" => "dateCreate",
                "type" => Input::INPUT_HIDDEN,
                "value" => time()
            )); 
            
            $this->modFlag = new Input(array(
                "name" => "modFlag",
                "type" => Input::INPUT_HIDDEN,
                "value" => 0
            ));  
            
            $this->type = new Input(array(
                "name" => "type",
                "type" => Input::INPUT_HIDDEN,
                "value" => 1
            ));            
            
            $this->subject = new Input(array(
                "name" => "subject",
                "type" => Input::INPUT_TEXT,
                "placeholder" => "Le sujet de votre conversation",
                "help" => "Indiquez ici le sujet de votre conversation",
                "icon" => "Web/Images/icons/16x16/font.png",
                "contrainte" => Contrainte::getC('subject','Vous devez entrer un sujet.',Contrainte::STRING)
            ));
            
            
        }
        
    } 
?>
