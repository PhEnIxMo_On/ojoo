<?php
	class ticket_responseForm extends Form {
		public $message;
		public $ticketId;
		public $accountId;
		public $isGm;
		public $date;
		public $charGuid;
		public $viewMj;
		
		public function __construct() {
			$O = getOjoo();
			$this->message = new Input(array(
				"type" => Input::INPUT_TEXTAREA,
				"class" => "ckeditor",
				"name" => 'message',
				'rows' => 10,
				'cols' => 110,
				'contrainte' => Contrainte::getC('message','Vous devez entrer une réponse',Contrainte::TEXT)
			));
			$this->ticketId = new Input(array(
				"type" => Input::INPUT_HIDDEN,
				"name" => "ticketId",
			));
			$this->accountId = new Input(array(
				"type" => Input::INPUT_HIDDEN,
				"name" => "accountId",
				"value" => $O->wowUser->getAccountId()
			));
			$this->date = new Input(array(
				"type" => Input::INPUT_HIDDEN,
				"name" => "date",
				"value" => time()
			));
		}
		
	}
?>