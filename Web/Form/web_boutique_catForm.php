<?php
	class web_boutique_catForm extends Form {
		public $id;
		public $id_cat;
		public $name;
		public $description;
		public $state;
		public $icon_name;
		
		public function __construct() {
			$this->id = new Input(array(
				"name" => "id",
				"type" => Input::INPUT_HIDDEN
			));
			
			$this->id_cat = new Input(array(
				"name" => "id_cat",
				"type" => Input::INPUT_HIDDEN
			));
			
			$this->name = new Input(array(
				"name" => "name",
				"type" => Input::INPUT_TEXT,
				'icon' => 'Web/Images/icons/16x16/font.png',
				"help" => "Le nom de la catégorie",
				"iconClass" => "iconForm",
				"helpClass" => "helpForm",
				"contrainte" => Contrainte::getC('name','Vous devez entrer un nom pour la catégorie. BOULET !',Contrainte::STRING)
			));
			
			$this->icon_name = new Input(array(
				"name" => "icon_name",
				"type" => Input::INPUT_TEXT,
				"icon" => "Web/Images/icons/16x16/eye.png",
				"iconClass" => "iconForm",
				"helpClass" => "helpForm",
				"help" => "L'icône de la catégorie ",
				"helpClass" => "helpForm",
				"contrainte" => Contrainte::getC('icon_name','Vous devez entrer le nom de l\'icône.',Contrainte::VARCHAR)
			));
			
			$this->description = new Input(array(
				"name" => "description",
				"type" => Input::INPUT_TEXTAREA,
				'cols' => 10,
				'rows' => 110,
				'class' => 'ckeditor',
				"contrainte" => Contrainte::getC('description','Gros bouffon, entre une description',Contrainte::TEXT)
			));
			
			$this->id_cat = new Input(array(
				"name" => "id_cat",
				"type" => Input::INPUT_SELECT,
				"icon" => "Web/Images/icons/16x16/box.png",
				"helpClass" => "helpForm",
				"iconClass" => "iconForm",
				"help" => "Catégorie mère"
			));
			
		}
		
	}
?>