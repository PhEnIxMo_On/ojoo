<?php

class guilde_recupForm extends Form {
    public $id;
    public $accountId;
    public $server;
    public $originalGuildName;
    public $oxygenGuildName;
    public $originalNbUser;
    public $oxygenNbUser;
    public $leaveReason;
    public $interest;
    public $com;
    public $reason;
    public $faction;
    public $accountMj;
    public $lastIp;
    public $lastIpMj;
    public $state;
    public $conditions;
    public $dateCreate;
    public $dateValid;
    public $lastMaj;
    
    public function __construct() {
        $O = getOjoo();
        $this->id = new Input(array("name" => 'id',"type" => Input::INPUT_HIDDEN));
        $this->accountId = new Input(
            array(
                "name" => 'accountId',
                "type" => Input::INPUT_HIDDEN,
                "value" => $O->wowUser->getAccountId()
            )
        );
        $this->server = new Input(
            array(
                "name" => 'server',
                "type" => Input::INPUT_TEXT,
                "placeholder" => "Le serveur de provenance",
                "icon" => "Web/Images/icons/16x16/google_map.png",
                "help" => "Le serveur d'ou votre guilde provient",
                "validClass" => "validInput2",
                "errorClass" => "errorInput2",
                "contrainte" => Contrainte::getC('server',"Vous devez indiquer votre serveur de provenance",Contrainte::TEXT,$this)
            )
        );
        $this->originalGuildName = new Input(
            array(
                "name" => 'originalGuildName',
                "type" => Input::INPUT_TEXT,
                "placeholder" => "Nom de guilde",
                "icon" => "Web/Images/icons/16x16/font.png",
                "help" => "Le nom original de votre guilde",
                "validClass" => "validInput2",
                "errorClass" => "errorInput2",                
                "contrainte" => Contrainte::getC('originalGuildName',"Vous devez indiquer le nom original de votre guilde.",Contrainte::TEXT,$this)
            )
        );  
        $this->oxygenGuildName = new Input(
            array(
                "name" => 'oxygenGuildName',
                "type" => Input::INPUT_TEXT,
                "placeholder" => "Nom de guilde",
                "icon" => "Web/Images/icons/16x16/font.png",
                "help" => "Le nom de votre guilde souhaité sur Oxygen",
                "validClass" => "validInput2",
                "errorClass" => "errorInput2",                
                "contrainte" => Contrainte::getC('oxygenGuildName',"Vous devez indiquer le nom souhaité pour votre guilde sur Oxygen",Contrainte::TEXT,$this)
            )
        );   
        $this->originalNbUser = new Input(
            array(
                "name" => 'originalNbUser',
                "type" => Input::INPUT_TEXT,
                "placeholder" => "Nombre de membres",
                "icon" => "Web/Images/icons/16x16/numeric_stepper.png",
                "validClass" => "validInput2",
                "errorClass" => "errorInput2",                
                "help" => "Le nombre de membre composant votre guilde sur votre serveur d'origine",
                "contrainte" => Contrainte::getC('originalNbUser',"Vous devez indiquer le nombre de membre composant originalement votre guilde.",Contrainte::INT,$this)
            )
        );   
        $this->oxygenNbUser = new Input(
            array(
                "name" => 'oxygenNbUser',
                "type" => Input::INPUT_TEXT,
                "placeholder" => "Nombre de migrant",
                "icon" => "Web/Images/icons/16x16/add.png",
                "validClass" => "validInput2",
                "errorClass" => "errorInput2",                
                "help" => "Le nombre de migrant sur Oxygen",
                "contrainte" => Contrainte::getC('oxygenNbUser',"Vous devez indiquer le nombre de membre migrant sur Oxygen",Contrainte::INT,$this)
            )
        );       
        $this->leaveReason = new Input(
            array(
                "name" => 'leaveReason',
                "type" => Input::INPUT_TEXT,
                "placeholder" => "Raison de votre départ",
                "icon" => "Web/Images/icons/16x16/key.png",
                "validClass" => "validInput2",
                "errorClass" => "errorInput2",                
                "help" => "Les raisons qui vous ont fait quitter votre ancien serveur",
                "contrainte" => Contrainte::getC('leaveReason',"Vous devez indiquer les raisons qui vous ont fait quitter votre ancien serveur",Contrainte::TEXT,$this,false)
            )
        ); 
        $this->interest = new Input(
            array(
                "name" => 'interest',
                "type" => Input::INPUT_SELECT,
                "icon" => "Web/Images/icons/16x16/script_palette.png",
                "help" => "Ce que vous recherchez sur Oxygen",
                "options" => array(
                    1 => "PvE",
                    2 => "PvP",
                    3 => "La communauté"
                ),         
                "contrainte" => Contrainte::getC('interest',"Vous devez indiquer ce que vous recherchez sur Oxygen",Contrainte::INT,$this)
            )
        );
        $this->com = new Input(
            array(
                "name" => 'com',
                "type" => Input::INPUT_TEXTAREA,
                "contrainte" => Contrainte::getC('com',"Vous devez indiquer un commentaire",Contrainte::TEXT,$this),
                "cols" => 110,
                "rows" => 10,
                "validClass" => "validInput2",
                "errorClass" => "errorInput2",                
                "placeholder" => "Décrivez ici vos attentes pour votre guilde sur Oxygen"
            )
        );
        
        $this->faction = new Input(array(
            "name" => 'faction',
            "type" => Input::INPUT_SELECT,
            "icon" => "Web/Images/icons/16x16/heart_add.png",
            "help" => "La faction de votre guilde <b> sur Oxygen </b>",
            "options" => array(
                1 => "Alliance",
                2 => "Horde"
            )                       
        ));
    }
    
    public function addFormContrainte() {
        $this->accountId->setContrainte(Contrainte::getC('accountId','Erreur lors de la validation du formulaire.',Contrainte::INT,$this,true,array("not_in" => "site.guilde_recup.accountId.Vous ne pouvez soumettre qu'une seule récupération de guilde par compte.")));
        $this->originalGuildName->setContrainte(Contrainte::getC('originalGuildName',"Vous devez indiquer le nom original de votre guilde.",Contrainte::TEXT,$this,true,array("not_in" => "site.guilde_recup.originalGuildName.Cette guilde a déjà été récupérée")));
    }
    
    public function editFormContrainte() {
        $this->originalGuildName->setContrainte(Contrainte::getC('originalGuildName',"Vous devez indiquer le nom original de votre guilde.",Contrainte::TEXT,$this,true,array("not_in" => "site.guilde_recup.originalGuildName.Cette guilde a déjà été récupérée")));
    }
    
}
?>
