<?php
	class ConsoleMessage {
		private $element;
		private $code;
		private $message;
		private $sessionId;
		private $app;
		private $env;
		private $debugFile;
		private $colors;
		private $method;
		private $passKeys;

		public function __construct($element,$message,$file,$method,$env) {
			$O = $GLOBALS['O'];

			$this->element = $element;
			$this->message = $message;
			$this->sessionId = session_id();
			$this->debugFile = $file;
			$this->env = $env;
			$this->method = $method;
			$this->colors = array(
				"ROUTING" => "103473",
				"APPLICATION" => "04C497",
				"CONTROL" => "C4C404",
				"PHAR" => "C4047E"
			);
			
			// On nettoie l'environnement :
		    $delList=array("HTTP_POST_VARS","HTTP_GET_VARS",
		    "HTTP_COOKIE_VARS","HTTP_SERVER_VARS",
		    "HTTP_ENV_VARS","HTTP_SESSION_VARS",
		    "_ENV","PHPSESSID","SESS_DBUSER",
		    "SESS_DBPASS","HTTP_COOKIE","O","_GET","_POST","_COOKIE","_SERVER","_FILES","_GLOBALS","GLOBALS","_REQUEST","_SESSION");
			
			foreach ($env as $key => $val) {
				if (in_array($key,$delList)) {
					unset($env[$key]);
				}
			}		
			$this->env = $env;
			//var_dump($env);	
		}

		public function getTitre() {
			return $this->titre;
		}
		
		public function genArray($array,$id,$recursive = false) {
			//var_dump($array); echo "<br /> <br />";	
			$html = '<table style="margin-left: 10px;display: none;" id="' . $id . '">';
			foreach ($array as $key => $val) {
				if (is_array($val)) {
					$id2 = uniqid();
					$html .= "<tr><td onClick=\"$('#" . $id2 . "').toggle();\">" . $key . "</td><td>" . $this->genArray($array[$key],$id2) . "</td></tr>";
					unset($array[$key]);
				} else {
					$html .= "<tr><td>" . $key . "</td><td>" . $val . "</td></tr>";
				}
			}
			$html .= "</table>";
			//if (!$recursive) echo "<b> FIN TABLEAU </b><br /><br />";
			return $html;
		}

		public function getFormatMessage() {
			
			$varsHtml = '<table class="detailTable">';
			//var_dump($this->env['routes']); echo "<br /><br />";
			foreach ($this->env as $var => $value) {
				if (!is_array($value)) {					
					$varsHtml .= "<tr><td>" . $var . "</td><td>" . $value . "</td></tr>";
				 } else {
				 	$id = uniqid();
				 	//echo $var . "<br />";
					$varsHtml .= "<tr><td onClick=\"$('#" . $id . "').toggle();\">" . $var . "</td>";
					$varsHtml .= "<td>" . $this->genArray($value,$id) . "</td>";
					$varsHtml .= "</tr>";
				}
			}
			$varsHtml .= "</table>";
			//var_dump($this->passKeys);
			
			$message = '<div class="newConsoleLine">
			<span class="newConsoleTime">' . udate('H:i:s u',microtime(true)) . '(s)</span>
			<div class="newConsoleBar"></div>
			<div class="newConsoleType" style="background-color: #' . $this->colors[$this->element] . '">
				' . $this->element . '
			</div>
			<div class="newConsoleBar"></div>
			<div class="newConsoleText">
				' . $this->message . '
			</div>
			<div style="clear: both;"></div>
			<div class="newConsoleDetail">
				<table>
					<tr>
						<th style="width: 20%" class="informations"> Informations : </th>
						<th style="width: 80%" class="env"> Environnement : </th>
					</tr>
					<tr>
						<td>
							<table class="detailTable">
								<tr>
									<td> Langage </td>
									<td> PHP </td>
								</tr>
								<tr>
									<td> Element </td>
									<td> ' . $this->element . ' </td>
								</tr>
								<tr>
									<td> Méthode </td>
									<td> ' . $this->method . '  </td>
								</tr>
								<tr>
									<td> Fichier </td>
									<td>' . $this->debugFile . '</td>
								</tr>
							</table>
						</td>
						<td  style="text-align: center;">
							' . $varsHtml . '
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div style="clear: both;"></div>';
						return $message;
		}


	}
?>