<?php
	class Request {
		public $url;
		private $method;
		private $mode;
		private $browser;
		private $protocol;
		private $charset;
		private $browserIp;
		private $referer;
		private $host;

		public function __construct() {
			 $this->url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			if (array_key_exists('HTTP_X_REQUESTED_WITH', $_SERVER) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
				$this->mode = 'ajax';
			else
				$this->mode = 'normal';
			$this->method = $_SERVER['REQUEST_METHOD'];
			//$this->charset = $_SERVER['HTTP_ACCEPT_CHARSET'];
			if (isset ($_SERVER['HTTP_REFERER']))
				$this->referer = $_SERVER['HTTP_REFERER'];
			else
				$this->referer = "none";
			$this->browserIp = $_SERVER['REMOTE_ADDR'];
			$this->browser = $_SERVER['HTTP_USER_AGENT'];
		}

		public function getMethod() {
			return $this->method;
		}

		public function getMode() {
			return $this->mode;
		}

		public function getUrl() {
			return $this->url;
		}

	}
?>