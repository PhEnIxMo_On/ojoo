<?php
class View {
	public $mod;
	public $name;
	public $path;
	
	public function __construct($path,$name,$mod) {
		$this->path = $path;
		$this->name = $name;
		$this->mod = $mod;
	}
	
	public function display() {
	}
	
}
?>