<?php
class Folder {
	private $name;
	private $path;
	private $droits = null;
	private $folders = array();
	private $files = array();
	
	public function __construct($path,$flag = false) {
		if ($path != null) {
			$O = $GLOBALS['O'];
			if (is_dir($path)) 
				$folder = opendir($path);
			else {
				$O->erreur->set_erreur('#O7');
				$O->erreur->display();
			}
			if (isset ($folder)) {
				$this->path = $path;
				$name = explode('/',$path);
				$nb = count($name);
				$this->name = $name[$nb - 2];
				if (isset ($O->Config["rang_dossier"][$this->name])) $this->droits = $O->Config["rang_dossier"][$this->name];
				
				if ($this->droit != null) {
					if (isset ($_SESSION['Rang'])) {
						if ($O->Rights->isAllowToRF($O->Config["rang_dossier"][$this->name],$_SESSION['Rang'])) $test = true;
						else																					$test = false;
					} else $test = false;
				} else $test = true;
				if ($test) {
					while ($entry = @readdir($folder)) {
						if ($entry != '.' && $entry != '..') {
							if (is_dir($path . $entry)) {
								$this->folders[$entry] = new Folder($path . $entry . '/',$flag);
							} else {
								$name = explode('.',$entry);
								$this->files[$name[0]] = new File($path . $entry);
							}
						}
					}
				} else {
					$O->erreur->set_erreur("#O8");
					$O->erreur->display();
				}
			}
		}
	}
	
	public function __get($folder) {
		if (is_dir($this->path . '/' . $folder)) { // On veut acc�der � un dossier
			return $this->folders[$folder];
		} else { // On veut acc�der � un fichier
			if (file_exists($this->path . '/' . $folder))
				return $this->files[$folder];
		}
	}
	
	public function set_folder($path,$flag = false) {
		$this->__construct($path,$flag);
	}
	
	public function get_folders() {
		return $this->folders;
	}

	public function get_name() {
      return $this->name;
    }

	public function set_name($name) {
      $this->name = $name;
      return $this;
    }

	public function get_path() {
      return $this->path;
    }

	public function set_path($path) {
      $this->path = $path;
      return $this;
    }
	
	public function get_files() {
		return $this->files;
	}
	
	public function set_files($files) {
		$this->files = $files;
	}

}