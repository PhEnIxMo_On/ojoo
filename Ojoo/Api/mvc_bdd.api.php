<?php
class MVC_BDD {
    
	public function MAJ() {
		$O = $GLOBALS['O'];
		$sql = "UPDATE ";
		$reflection = new ReflectionClass($this);
		$props   = $reflection->getProperties();
		$test = false;
		$test2 = false;
		$sql .= $reflection->getName() . " SET ";
		foreach ($props as $prop) {
			if ($prop->getName() != 'where') {
				if ($prop->getName() != 'BDD' AND $prop->getName() != 'liens' AND $prop->getName() != 'actionsLiens') {
					if (!$test2) {
						if ($prop->getValue($this) != "") {
							$sql .= $prop->getName() . '=\'' . addslashes($prop->getValue($this)) . '\'';
							$test2 = true;
						} else {
						    if (is_int($prop->getValue($this))) {
						       $sql .= $prop->getName() . '=' . intval($prop->getValue($this));
                                $test2 = true;
						    } 
                           
						}
					}
					else {
						if ($prop->getValue($this) != "") {
							$sql .= ',' . $prop->getName() . '=\'' . addslashes($prop->getValue($this)) . '\'';
						} else {
						    if (is_int($prop->getValue($this))) $sql .= ',' . $prop->getName() . '=' . intval($prop->getValue($this));

						}
					}
				}
			}
			else {
				$sql .= " WHERE ";
				$where = $reflection->getProperty("where");
				$where = $where->getValue($this);
				$nb = count($where);
				$i = 1;
				foreach ($where as $champ => $value) {
					if ($i == $nb) 
						$sql .= $champ . "=" . nl2br(addslashes($value)) ;
					else
						$sql .= $champ . "=" . nl2br(addslashes($value)) . " AND ";
					$i++;
				}
			}
		}
		$prop = $reflection->getDefaultProperties();
		$O->bdd->$prop["BDD"]->query($sql);
                 $O->console->debug("Requête SQL : " . $this->colorSQL($sql),"[ORM_OJOO/MAJ]");
	}
	public function ADD() {
		$O = $GLOBALS['O'];
		$sql2 = "";
		$sql = "INSERT INTO ";
		$reflection = new ReflectionClass($this);
		$props   = $reflection->getProperties();
		$test = false;
		$test2 = false;
		$sql .= $reflection->getName() . " (";
		foreach ($props as $prop) {
			if ($prop->getName() != 'where') {
				if ($prop->getName() != 'BDD' AND $prop->getName() != 'liens' AND $prop->getName() != 'actionsLiens') {
					if (!$test2) {
						$sql .= ''. $prop->getName() . '';
						$sql2 .= "'" . addslashes($prop->getValue($this)) . "'";
						$test2 = true;
					}
					else {
						$sql .= ',' . $prop->getName();
						$sql2 .= ",'" . addslashes($prop->getValue($this)) . "'";
					}
				}
			}
			$sql3 = $sql . ")" . " VALUES (" . $sql2 . ")";
		}
		$prop = $reflection->getDefaultProperties();
		$O->bdd->$prop["BDD"]->query($sql3);
                 $O->console->debug("Requête SQL : " . $this->colorSQL($sql3),"[ORM_OJOO/ADD]");
	}
	public function GET($count = false) {
		$O = $GLOBALS['O'];
		$sql = "SELECT ";
		$reflection = new ReflectionClass($this);
		$props   = $reflection->getProperties();
		$test = false;
		$test2 = false;
		//$sql .= $reflection->getName() . " SET ";
		if ($count != false) $sql .= "COUNT(*) as " . $count;
		foreach ($props as $prop) {
			if ($prop->getName() != 'where') {
				if (!$count) {
					if ($prop->getName() != 'BDD' AND $prop->getName() != 'liens' AND $prop->getName() != 'actionsLiens') {
						if (!$test2) {
							$sql .= $prop->getName();
							$test2 = true;
						}
						else {
							$sql .= ',' . $prop->getName();
						}
					}
				}
			}
			else {
				$sql .= " FROM " . $reflection->getName() . " WHERE ";
				$where = $reflection->getProperty("where");
				$where = $where->getValue($this);
				$nb = count($where);
				$i = 1;
				foreach ($where as $champ => $value) {
					if ($i == $nb) 
						$sql .= $champ . "='" . nl2br(addslashes($value)) . "'";
					else
						$sql .= $champ . "='" . nl2br(addslashes($value)) . "' AND ";
					$i++;
				}
			}
		}
		$prop = $reflection->getDefaultProperties();
		$result = $O->bdd->$prop["BDD"]->query($sql);
                $O->console->debug("Requête SQL : " . $this->colorSQL($sql),"[ORM_OJOO/GET]");
                $fetch = $result->fetchAll();
                foreach ($fetch as $key => $value) {
                    foreach ($value as $key2 => $value2) {
                        if (is_string($key2))
                            $this->$key2 = $value2;
                    }
                }
                return $fetch;
	}
        
        public function colorSQL($sql) {
           $sql =  str_replace('SELECT','<font style="color: purple;font-weight: bold;"><em>SELECT</em></font>',$sql);
           $sql =  str_replace('WHERE','<font style="color: purple;font-weight: bold;"><em>WHERE</em></font>',$sql);
           $sql =  str_replace('FROM','<font style="color: purple;font-weight: bold;"><em>FROM</em></font>',$sql);
           $sql =  str_replace('OR','<font style="color: purple;font-weight: bold;"><em>OR</em></font>',$sql);
           $sql =  str_replace('AND','<font style="color: purple;font-weight: bold;"><em>AND</em></font>',$sql);
           $sql =  str_replace('UPDATE','<font style="color: purple;font-weight: bold;"><em>AND</em></font>',$sql);
           $sql =  str_replace('SET','<font style="color: purple;font-weight: bold;"><em>AND</em></font>',$sql);
           $sql =  str_replace('INSERT','<font style="color: purple;font-weight: bold;"><em>INSERT</em></font>',$sql);
           $sql =  str_replace('INTO','<font style="color: purple;font-weight: bold;"><em>INTO</em></font>',$sql);
           return $sql;
        }
	
	public function GET_ALL() {
		$O = $GLOBALS['O'];
		$sql = "SELECT ";
		$reflection = new ReflectionClass($this);
		$sql .= " * FROM " . $reflection->getName();
		
		$prop = $reflection->getDefaultProperties();
		return $O->bdd->$prop["BDD"]->query($sql);
	}
	public function delete($where) {
		$O = $GLOBALS['O'];
		$reflection = new ReflectionClass($this);
		$nomTable = $reflection->getName();
		$BDD = $reflection->getProperty('BDD');
		$BDD = $BDD->getValue($this);
		$sql = "DELETE FROM " . $nomTable . " WHERE ";
		$i = 1;
		foreach ($where as $key => $value) {
			if ($i == 1) {
				$sql .= $key . "=" . $value . " ";
			}
			else {
				$sql .= "AND " . $key . "=" . $value . " ";
			}
			$i++;
		}
		$O->bdd->$BDD->query($sql);
	}
	public function edit($champs,$where){
		$O = $GLOBALS['O'];
		$reflection = new ReflectionClass($this);
		$nomTable = $reflection->getName();
		$BDD = $reflection->getProperty('BDD');
		$BDD = $BDD->getValue($this);
		$sql = "";
		$i = 1;
		$nombre = count($champs);
		$sql .= "UPDATE " . $nomTable . " SET ";
		foreach ($champs as $key => $value) {
			if($i != $nombre) {
				$sql .= $key . "='" . $value . "',";
			} else {
				$sql .= $key . "='" . $value . "' ";
			}
		}
		$i = 1;
		foreach ($where as $key => $value) {
			if ($i == 1) {
				$sql .= "WHERE " . $key . "='" . $value . "' ";
			}
			else {
				$sql .= "AND" . $key . "='" . $value . "' ";
			}
			$i++;
		}
		$O->bdd->$BDD->query($sql);
	}
	
	public function select($table,$where = null,$champs = null) {
		$O = $GLOBALS['O'];
		$reflection = new ReflectionClass($this);
		$BDD = $reflection->getProperty('BDD');
		if (isset ($this->liens))
			$link = $reflection->getProperty('liens')->getValue($this);
		if (isset ($this->actionsLiens))
			$act = $reflection->getProperty('actionsLiens')->getValue($this);
		$sql = "SELECT ";
		if ($champs == null) $sql .= "* FROM " . $table;
		else				$sql .= $champs . " FROM " . $table;
		if (isset ($this->liens)) {
			if ($act['onSelect']) {
				foreach ($link as $key => $lin) {
					$tableName = explode('.',$lin);
					$Champ = explode('.',$key);
					if (isset ($_SESSION['table_mvc'])) {
						if ($tableName[0] != $_SESSION['table_mvc']) {
							$_SESSION['table_mvc'] = $tableName[0];
							$sql .= "," . $tableName[0];
						}
					} else {
						$_SESSION['table_mvc'] = $tableName[0];
						$sql .= "," . $tableName[0];
					}
				}
			}
		}
		
		if ($where != null) $sql .= " WHERE " . $where;
	
		if (isset ($this->liens)) {
			if ($act['onSelect']) {
				foreach ($link as $key => $lin) {
					$tableName = explode('.',$lin);
					$Champ = explode('.',$key);
					if ($where != null) {
						$sql .= " AND " . $key . "=" . $lin;
					} else {
						$where = "autre chose que null";
						$sql .= " WHERE " . $key . "=" . $lin;
					}
				}
			}
		}
		
		$BDD = $BDD->getValue($this);	
		
		unset($_SESSION['table_mvc']);
		return $O->bdd->$BDD->query($sql);
	}
	
	public function assoc($class,$type = 'POST') {
		// var_dump($class);
		$reflection = new ReflectionClass($class);
		$name = $reflection->getName();
		if ($type == 'POST') {
			foreach ($_POST as $key => $post) {
				// echo $key . "=>" . $post;
				// echo $name . '->' . $key . ' => ' . $post . '<br />';
                if (is_string($post)) addslashes($post);
                if (is_int($post)) intval($post);
				@$class->$key = $post;
			}
		} else {
			foreach ($_GET as $key => $post) {
                if (is_string($post)) addslashes($post);
                if (is_int($post)) intval($post);
				if (isset ($class->$key)) $class->$key = $post;
				// else $class->$key = null;
			}
		}
	}
        
	public function hydrate($class,$type = 'POST') {
		// var_dump($class);
		$reflection = new ReflectionClass($class);
		$name = $reflection->getName();
		if ($type == 'POST') {
                    foreach ($_POST as $key => $post) {
                        //echo $key . "=>" . $post . "<br />";
                        // echo $name . '->' . $key . ' => ' . $post . '<br />';
                        if (is_string($post)) addslashes($post);
                        if (is_int($post)) intval($post);
                        @$this->$key = $post;
                    }
		} else {
                    foreach ($_GET as $key => $post) {
                        if (is_string($post)) addslashes($post);
                        if (is_int($post)) intval($post);
                        if (isset ($class->$key)) $class->$key = $post;
                        // else $class->$key = null;
                    }
		}
	}        
}
?>