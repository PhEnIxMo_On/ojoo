<?php

/*
 * Short description : 
 * Author : 
 * Comments : 
 */

abstract class Manager {
    //put your code here
    private $objects;
    
    public function __get($name) {
        return $this->objects[$name];
    }
    
    public function getAll() {
        return $this->objects;
    }
    
    public function __set($name, $value) {
        $this->objects[$name] = $value;
    }
    
    public function exist($key) {
        if (isset ($this->objects[$key])) {
            return true;
        } else return false;
    }
    
    public function getByKey($name) {
        if (isset ($this->objects[$name])) {
            return $this->objects[$name];
        }
    }
    
    public function add($object,$name = "") {
        if ($name != '') {
             $this->objects[$name] = $object;
        } else {
             $this->objects[] = $object;
        }
    }
    
    public function delete($key) {
        if ($this->exist($key)) {
            unset($this->objects[$key]);
        }
    }
    
    public function searchByKey($name) {
        foreach ($this->objects as $key => $value) {
            if ($key == $name) {
                return $this->objects[$name];
            }
        }
        
        return false;
    }
    
    public function isEmpty() {
        if (count($this->objects) > 0) {
            return false;
        } else {
            return true;
        }
    }
    
    public function searchByValue($value) {
        foreach ($this->objects as $key => $Objvalue) {
            if ($Objvalue == $value) {
                return $this->objects[$key];
            }
        }
        
        return false;
    }
    
}

?>
