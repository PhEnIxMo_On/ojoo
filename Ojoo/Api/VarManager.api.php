<?php
    class VarManager {
        public $vars = array();
		
        public function VarManager($config,$vars = null) {
			$O = getOjoo();
            if ($vars != NULL) $this->vars = $vars;

            if ($config["autoCompletePost"]) {
                foreach ($_POST as $key => $value) {
                   if (is_string($value)) $this->vars[$key] = addslashes($value);
                }
            }

           if ($config["autoCompleteGet"]) {
                foreach ($_GET as $key => $value) {
                    if (is_string($value)) $this->vars[$key] = addslashes($value);
                }
            }

           if ($config["autoCompleteSessions"]) {
                foreach ($_SESSION as $key => $value) {
                   if (is_string($value)) $this->vars[$key] = addslashes($value);
                }
            }

        }

		public function addRouteVars() {
			$O = getOjoo();
			foreach ($O->route->params as $key => $value) {
				$this->vars[$key] = $value;
			}
		}

        public function add($key = NULL,$var) {
            if ($key == NULL) $this->vars[] = $var;
            else              $this->vars[$key] = $var;
        }

        public function __set($key,$value) {
            $this->vars[$key] = $value;
        }

        public function __get($key) {
            return $this->vars[$key];
        }

        public function exist($name) {
            if (isset ($_GET[$name])) return TRUE;
            else {
                if (isset ($_POST[$name])) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }

    }
?>
