<?php
	class FormValidator {
		public $contraintes = array();
		
		public function FormValidator() {
		}
		
		public function reset() {
			$this->contraintes = array();
		}
                
                public function getPostValue($c,$type = null) {
                    if ($this->checkTable($c)) {
                        $name = $this->getTableName($c);
                        $index = $c->index;
                        if (isset ($_POST[$name][$index]))
                            return $_POST[$name][$index];
                        else 
                            return null;
                    } else {
                        if ($type == null) {
                            if (isset ($_POST[$c->nomChamp]))
                                return $_POST[$c->nomChamp];
                            else
                                return null;
                        } else {
                            switch($type) {
                                case 'same_as':
                                    if (isset ($_POST[$c->same_as]))
                                        return $_POST[$c->same_as];
                                    else
                                        return null;
                                    break;
                            }
                        }
                    }
                    return null;
                }
                
                public function setPostValue($c,$value) {
                    if ($this->checkTable($c)) {
                        $name = $this->getTableName($c);
                        $index = $c->form->$name->getIndex();
                        if (isset ($_POST[$name][$index]))
                            $_POST[$name][$index] = $value;
                        else 
                            return false;
                    } else {
                        if (isset ($_POST[$c->nomChamp]))
                            $_POST[$c->nomChamp] = $value;
                        else
                            return false;
                    }
                    return null;                    
                }
                
                public function addForm($form) {
                    $O = getOjoo();
                    $reflection = new ReflectionClass($form);
                    $props   = $reflection->getProperties();
                    foreach ($props as $prop) {
                        $name = $prop->name;
                        if (is_object($form->$name)) {
                            if ($this->checkTable($name)) {
                                foreach ($form->$name->getInputs() as $input) {
                                    if (is_array($input->getContrainte())) {
                                        $this->add($input->getContrainte());
                                        $O->console->debug("Ajout de : " . $name . " en tant que tableau.","[ADDFORM]");
                                    }
                                        
                                }
                            } else {
                                if (is_array($form->$name->getContrainte())) {
                                    $this->add($form->$name->getContrainte());
                                     $O->console->debug("Ajout de : " . $name . " en tant que champ.","[ADDFORM]");
                                } 
                            }
                        }
                    }                  
                }
                
                public function exist($c) {
                    $O = getOjoo();                
                    $postValue = $this->getPostValue($c);
                    if ($postValue != null) {
                        if (empty ($postValue)) {
                            if ($c->required != null) {
                                $this->addError($c);
                                return false;
                            } else {
                                if ($this->defaultValue($c)) return true;
                                else                         return false;
                            }                            
                        }
                    }
                    else {
                        if ($c->required != null) {
                            $this->addError($c);
                            return false;
                        } else {
                            if ($this->defaultValue($c)) return true;
                            else                         return false;
                        }
                    }
                    return true;                        
                }
                
                public function getTableName($contrainte) {
                    return str_replace('[]','',$contrainte->nomChamp);
                }
                
                
                public function minAndMax($c) {
                    if (is_string($this->getPostValue($c))) {
                        if (isset ($c->min)) {
                            if (strlen($this->getPostValue($c)) < $c->min) {
                                $this->addError($c);
                                return false;
                            }
                        } 
                        if (isset ($c->max)) {
                            if (strlen($this->getPostValue($c)) > $c->max) {
                                $this->addError($c);
                                return false;
                            }
                        }
                    }
                    return true;                    
                }
                
                public function defaultValue($c) {
                    if ($c->default_value != null) {
                        $this->setPostValue($c, $c->default_value);
                        return true;
                    } else return false;
                        
                }
                
                public function addError($c) {
                    $O = getOjoo();
                    $c->state = false;
                    $O->addFlashMessage('auto',$c->erreurMsg);
                    return true;
                }
                
                public function sameAs($contrainte) {
                    if ($contrainte->same_as != null) {
                        $post = $this->getPostValue($contrainte,'same_as');
                        if (isset ($post)) {
                            if ($this->getPostValue($contrainte) != $this->getPostValue($contrainte,'same_as') ) {
                                $this->addError($contrainte);
                                return false;
                            }
                        } else {
                            $this->addError($contrainte);
                            return false;
                        }
                    }             
                    return true;
                }
                
                public function notIn($c) {                    
                    $O = getOjoo();
                    if ($c->not_in != null) {
                        if (is_array($c->not_in)) {
                            // Nouvelle méthode
                            $exampleArray = array(
                              "db" => "site",
                              "table" => "users",
                              "champ" => "pseudo",
                              "erreurMsg" => "Message d'erreur"
                            );
                            $db = $c->not_in['db'];
                            $table = $c->not_in['table'];
                            $champ = $c->not_in['champ'];
                            $erreurMsg = $c->not_in['erreurMsg'];
                            $donnees = $O->bdd->$db->queryR("SELECT COUNT(*) as nombre FROM " . $table . " WHERE " . $champ . "='" . $this->getPostValue($c) . "'");
                            if ($donnees['nombre'] > 0) {
                                $this->addError($contrainte);
                                return false;
                            }
                        } else {
                            // On gère quand même l'ancienne : 
                            $contrainte = $c;
                            if ($contrainte->not_in != null) {                                                    
                                $tableChamp = explode('.',$contrainte->not_in);
                                $donnees = $O->bdd->$tableChamp[0]->queryR("SELECT COUNT(*) as nombre FROM " . $tableChamp[1] . " WHERE " . $tableChamp[2] . "='" . $this->getPostValue($c) . "'");
                                if ($donnees['nombre'] > 0) {
                                    $O->addFlashMessage($contrainte->nomChamp,$tableChamp[3]);
                                    return false;
                                }
                            }                            
                        }
                    }
                    return true;
                }
                
                public function type($contrainte) {
                    if ($contrainte->type != null) {
                        switch ($contrainte->type) {
                            case 'string':
                                if (!is_string($this->getPostValue($contrainte) )) {
                                    $this->addError($contrainte);                                    
                                }                             
                                break;

                            case 'int':
								 if (!is_numeric($this->getPostValue($contrainte) )) {
                                    $this->addError($contrainte);                                    
                                }                             
                                break;
                                     
                        }
                    }              
                    return true;                    
                }
                
                public function banChar($contrainte) {
                    if ($contrainte->ban_char != NULL) {
                         $testBanChar = TRUE;
                         foreach ($contrainte->ban_char as $char) {
                             $test = strpos($this->getPostValue($contrainte) ,$char);
                             if ($test != FALSE) {
                                 $testBanChar = FALSE;
                                 break;
                             }
                         }
                         if ($testBanChar == FALSE) { 
                             $this->addError($contrainte);
                             return false;
                         }
                     }
                     return true;                    
                }
                
                
                public function banValue($contrainte) {
                    if ($contrainte->ban_value != NULL) {
                         $testBanChar = TRUE;
                         foreach ($contrainte->ban_value as $value) {
                             if ($this->getPostValue($contrainte)  == $value) $testBanChar = FALSE;
                         }
                         if ($testBanChar == FALSE) {
                             $this->addError($contrainte);
                             return false;
                         }
                     }
                     return true;                    
                }
                
                public function checkTable($contrainte) {
                    $O = getOjoo();
                    if (is_string($contrainte)) $nom = $contrainte;
                    elseif (is_object($contrainte)) $nom = $contrainte->nomChamp;
                    else {
                        $O->console->debug("Le format de la contrainte " . $contrainte . " n'est pas un objet.","[FORM_VALIDATOR/CHECK_TABLE]");
                        return false;
                    }
                    $nomChamp = str_replace('[]','',$nom);    
                    if (isset ($_POST[$nomChamp])) {
                        if (is_array($_POST[$nomChamp])) {
                            //$O->console->debug('Le champ ' . $nomChamp . ' est un tableau.');
                            return true;
                        } else {
                            //$O->console->debug('Le champ ' . $nomChamp . ' n\'est pas un tableau.');
                            return false;
                        }
                    }
                }
		
		public function isValid() {
                    /***************************************************
                    * Plusieurs traitement, en first on v�rifit : 
                    * Si le champ est requis
                    * 	Si oui, on effectue les traitements
                    * Sinon, on laisse.
                    ****************************************************/                    
                    $O = $GLOBALS['O'];
                    $O->resetFlashMessage();
                    foreach ($this->contraintes as $contrainte) {
                            if ($this->exist($contrainte)) {
                                $this->minAndMax($contrainte);
                                $this->sameAs($contrainte);
                                $this->notIn($contrainte);
                                $this->type($contrainte);
                                if ($contrainte->xmlError != NULL) {
                                        $O->userError->loadXml($contrainte->xmlError);
                                }
                                if ($contrainte->regex != NULL) {
                                    if (!preg_match($contrainte->regex,$_POST[$contrainte->nomChamp])) $this->addError($contrainte);
                                }     
                                $this->banChar($contrainte);
                                $this->banValue($contrainte);
                            }
                        
                       /* if (isset ($_POST[$contrainte->nomChamp])) {
                            if (!empty ($_POST[$contrainte->nomChamp])) {
                                if (is_string($_POST[$contrainte->nomChamp])) {
                                        if (isset ($contrainte->min)) {
                                                if (strlen($_POST[$contrainte->nomChamp]) < $contrainte->min) $O->addFlashMessage($contrainte->nomChamp,$contrainte->erreurMsg); 
                                        } 
                                        if (isset ($contrainte->max)) {
                                                if (strlen($_POST[$contrainte->nomChamp]) > $contrainte->max) $O->addFlashMessage($contrainte->nomChamp,$contrainte->erreurMsg);
                                        }
                                }
                                if ($contrainte->same_as != null) {
                                        if (isset ($_POST[$contrainte->same_as])) {
                                                if ($_POST[$contrainte->nomChamp] != $_POST[$contrainte->same_as]) $O->addFlashMessage($contrainte->nomChamp,$contrainte->erreurMsg);
                                        } else $O->addFlashMessage($contrainte->nomChamp,$contrainte->erreurMsg);
                                }
                                if ($contrainte->not_in != null) {                                                    
                                        $tableChamp = explode('.',$contrainte->not_in);
                                        $donnees = $O->bdd->$tableChamp[0]->queryR("SELECT COUNT(*) as nombre FROM " . $tableChamp[1] . " WHERE " . $tableChamp[2] . "='" . $_POST[$contrainte->nomChamp] . "'");
                                        if ($donnees['nombre'] > 0) $O->addFlashMessage($contrainte->nomChamp,$tableChamp[3]);
                                }
                                if ($contrainte->type != null) {
                                    switch ($contrainte->type) {
                                        case 'string':
                                            if (!is_string($_POST[$contrainte->nomChamp])) $O->addFlashMessage('auto',$contrainte->erreurMsg);
                                            break;

                                        case 'int':
                                            $test = $_POST[$contrainte->nomChamp];
                                            if (!preg_match('#^([0-9]+)$#i',$test)) {
                                                //echo "Le champ " . $contrainte->nomChamp . " est un int : " . $_POST[$contrainte->nomChamp];
                                                $O->addFlashMessage('auto',$contrainte->erreurMsg);
                                            }                                                           
                                            break;          
                                    }
                                }
                                if ($contrainte->xmlError != NULL) {
                                        $O->userError->loadXml($contrainte->xmlError);
                                }
                                if ($contrainte->regex != NULL) {
                                    if (!preg_match($contrainte->regex,$_POST[$contrainte->nomChamp])) $this->addError($contrainte);
                                }
                                if ($contrainte->ban_char != NULL) {
                                    $testBanChar = TRUE;
                                    foreach ($contrainte->ban_char as $char) {
                                        $test = strpos($_POST[$contrainte->nomChamp],$char);
                                        if ($test != FALSE) {
                                            $testBanChar = FALSE;
                                            break;
                                        }
                                    }
                                    if ($testBanChar == FALSE) $O->addFlashMessage($contrainte->nomChamp,$contrainte->erreurMsg);
                                }
                                if ($contrainte->ban_value != NULL) {
                                    $testBanChar = TRUE;
                                    foreach ($contrainte->ban_value as $value) {
                                        if ($_POST[$contrainte->nomChamp] == $value) $testBanChar = FALSE;
                                    }
                                    if ($testBanChar == FALSE) $O->addFlashMessage($contrainte->nomChamp,$contrainte->erreurMsg);
                                }
                            } else {
                                if ($contrainte->required) 
                                    $O->addFlashMessage($contrainte->nomChamp,$contrainte->erreurMsg);
                                else {
                                    if ($contrainte->default_value != NULL) {
                                        $_POST[$contrainte->nomChamp] = $contrainte->default_value;
                                    }
                                }
                            }
                        } else {
                            if ($contrainte->required) 
                                $O->addFlashMessage($contrainte->nomChamp,$contrainte->erreurMsg);
                            else {
                                if ($contrainte->default_value != NULL) {
                                    $_POST[$contrainte->nomChamp] = $contrainte->default_value;
                                }
                            }
                        }*/
                    }
                    if (count($O->flashMessage) > 0) {                        
                        $this->setClass();
                        return false;
                    } else {
                        $this->setClass();
                        return true;
                    }                            
                        
		}
                
                public function setClass() {
                    $O = getOjoo();
                    foreach ($this->contraintes as $c) {
                        if (is_object($c->form)) {
                            $name = $this->getTableName($c);
                            if (is_object($c->form->$name)) {                                
                               if ($c->state) {
                                    $input = $c->form->$name;       
                                    if ($input->getType() == 'InputTable') {
                                        $input2 = $c->form->$name->get($c->index);
                                        $input2->setAllowValidClass(true);
                                        $O->console->debug("Valide class pour : " . $name . "[" . $c->index . "]","[FORM_VALIDATOR]");
                                    } else {
                                        $input->setAllowValidClass(true);
                                        $O->console->debug("Valide class pour : " . $name,"[FORM_VALIDATOR]");
                                    }                                    
                                    
                               }                                   
                               else {
                                   $input = $c->form->$name;
                                   if ($input->getType() == 'InputTable') {
                                       $input2 = $c->form->$name->get($c->index);
                                       $input2->setAllowErrorClass(true);
                                       $O->console->debug("Error class pour : " . $name . "[" . $c->index . "]","[FORM_VALIDATOR]");
                                   } else {
                                       $c->form->$name->setAllowErrorClass(true);
                                       $O->console->debug("Error class pour : " . $name,"[FORM_VALIDATOR]");
                                   }                                                                      
                               }
                                   
                            } else $O->console->debug("Le champ :" . $name . " n'est pas un objet.");       
                        }
                    }                    
                }
		
		public function add($contrainte) {
			$this->contraintes[] = new Contrainte($contrainte);
			return $this;
		}
	}
?>