<?php
	class Api {
		public $api = array();
		
		public function Api() {
			foreach ($this->api as $fichier) {
				require_once "Ojoo/Api/" . $fichier . ".api.php";
				$this->api[$fichier] = new $fichier();
			}
		}

		public function __get($nom) {
			if (file_exists("Ojoo/Api/" . $nom . ".api.php")) {
				require_once "Ojoo/Api/" . $nom . ".api.php";
				$this->api[$nom] = new $nom();
				return $this->api[$nom];
			} else {
				$O = $GLOBALS['O'];
				$O->Erreur('#01');
			}
		}
		
	}
?>