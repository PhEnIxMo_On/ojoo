<?php
	class Sort {
		private $filters = array();
		private $columns = array();
		private $orders = array();
                private $type = array();
		private $limit = array();

		public function __construct() {
		}

		public function sqlByActiveFilter() {
			$sql = "";
			$first = FALSE;
			foreach ($this->filters as $filter) {
				if ($filter->getByColumn() != NULL) 
					$this->columns[] = $filter->getByColumn();
				if ($filter->getOrder() != NULL)
					$this->orders[] = $filter->getOrder();
                                if ($filter->getType() != NULL) 
                                    $this->type[] = $filter->getType();
			}


			$firstOrder = TRUE;
                        $i = 0;
			foreach ($this->columns as $column) {
				if ($firstOrder) {
					if ($column['type'] == 'LIKE')
						$sql .= "WHERE " . $column['nomChamp'] . " LIKE '%". $column['value'] . "%'";
					else 
						$sql .= "WHERE " . $column['nomChamp'] . $column['type'] . "'" . $column['value'] . "'";
					$firstOrder = FALSE;
				} else {
					if ($column['type'] == 'LIKE')
						$sql .= " " . $this->type[$i] . " " . $column['nomChamp'] . " LIKE '%". $column['value'] . "%'";
					else 
						$sql .= " " . $this->type[$i] . " " . $column['nomChamp'] . $column['type'] . "'" . $column['value'] . "'";
				}
                                $i++;
			}

			$firstOrder = TRUE;
			foreach ($this->orders as $order) {
				if ($firstOrder) {
					$sql.= " ORDER BY " . $order['nomChamp'];
					if (isset ($order['ORDER'])) $sql.= " " . $order['ORDER'];
					$firstOrder = FALSE;
				} else {
					$sql .= "," . $order['nomChamp'];
					if (isset ($order['ORDER'])) $sql.= " " . $order['ORDER'];
				}
			}

			if (isset ($this->limit['start'])) {
				$sql .= " LIMIT" . $this->limit['start'] . ',' . $this->limit['end'];
			}

			return $sql;
		}

		public function setLimit($start,$end) {
			$this->limit['start'] = $start;
			$this->limit['end'] = $end;
		}

		public function addFilter($filtre) {
			$this->filters[] = $filtre;
		}
	}
?>