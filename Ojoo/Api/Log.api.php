<?php
class Log {
	private $logName;
	
	public function Log($name) {
		$this->logName = $name;
	}

	public function getLogName() {
		return $this->logName;
	}
	
	public function setLog($name) {
		$this->logName = $name;
	}
	
	public function log_it($msg,$pre = true) {
		$handle = fopen('Log/' . $this->logName,'a+');
		if ($pre) {
		fwrite($handle,
		"
G�n�r� le " . date_fran() . "
------------------------------------
" . $msg . "
------------------------------------

		"
		);
		} else {
			fwrite($handle,$msg . "\n");
		}
		fclose($handle);
	}


	public function get_log($line = null) {
		$handle = fopen('Log/' . $this->logName,'a+');
		$array_line = array();
		if ($line == null) {
			$content = fread($handle,filesize('Log/' . $this->logName));
			return $content;
		} else {
			$i = 1;
			while ($i != $line) {
				$ligne = fgets($handle);
				$array_line[$i] = $ligne;
				$i++;
			}
			return $array_line;
		}
	}
}
?>