<?php

class FormManager {
    public $forms;

    public function __get($nom) {
        if (!isset ($this->forms[$nom])) {
                if (file_exists('Web/Form/' . $nom . '.php')) {
                        require_once 'Web/Form/' . $nom . '.php';
                        $this->forms[$nom] = new $nom();
                        return $this->forms[$nom];
                } else {
                    echo "Erreur";
                    $O = $GLOBALS['O'];
                    $O->erreur->set_erreur('#O1',12,'Modele.api.php');
                    $O->erreur->display();
                }
        } else return $this->forms[$nom];
    }
}
?>
