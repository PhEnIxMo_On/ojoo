<?php
class wowUser {
	private $accountId;
	private $username;
	private $email;
	private $gmlevel = -1; // Non connecté
	private $currentIp;
	private $webRight;
	private $last_ip;
	private $expansion; // Vanilla  = 0, BC = 1, TLK = 2, Cata = 3
    private $char;
    private $charGuid;
	private $locked; // Inutilisé par le core, je l'inclue tout de même ici. C'est toujours pratique :p
	private $failed_logins; // Si plus de 4 => Je lock le compte jusqu'à intervention d'un Mj pour le débloquer.
    public $nbVote;
    public $possibleVote;
    public $possibleDateVote;
    private $nbPoint;
	public $nbMoney;


	public function getAccountId() {
		return $this->accountId;
	}
	
	public function getCharGuid() {
		return $this->charGuid;
	}
        
    public function getCharName() {
        return $this->char;
    }
	
	public function getUsername() {
		return $this->username;
	}
	
	public function setGmlevel($gmlevel) {
		$this->gmlevel = $gmlevel;
	}

	public function getGmLevel() {
		return $this->gmlevel;
	}
        
        public function getAllIconName($charGuid) {
            $O = getOjoo();
            $char = $O->modele->characters->select_guid($charGuid)->fetch();
            $icons = array();
            if ($charGuid != 0) {
                if ($char['gender'] == 0) $gender ="Male";
                else                      $gender = "";

                switch ($char['race']) {

                    case 1:
                        $iconFaction = "alliance.gif";
                        $factionName = "Alliance";
                        $raceName = "Humain";
                        $iconName = "raceHumain" . $gender . ".png";
                        break;

                    case 2:
                         $iconFaction = "horde.gif";
                         $factionName = "Horde";
                         $raceName = "Orc";
                        $iconName = "raceOrc" . $gender . ".png";
                        break;

                    case 3:
                         $iconFaction = "alliancegifpng";
                         $factionName = "Alliance";
                         $raceName = "Nain";
                        $iconName = "raceNain" . $gender . ".png";
                        break;

                    case 4:
                         $iconFaction = "alliance.gif";
                         $factionName = "Alliance";
                         $raceName = "Elfe de la nuit";
                        $iconName = "raceNightElf" . $gender . ".png";
                        break;

                    case 5:
                         $iconFaction = "horde.gif";
                         $factionName = "Horde";
                         $raceName = "Mort-vivant";
                        $iconName = "raceUndead" . $gender . ".png";
                        break;

                    case 6:
                         $iconFaction = "horde.gif";
                         $factionName = "Horde";
                         $raceName = "Tauren";
                        $iconName = "raceTauren" . $gender . ".png";
                        break;

                    case 7:
                         $iconFaction = "alliance.gif";
                         $factionName = "Alliance";
                         $raceName = "Gnome";
                        $iconName = "raceGnome" . $gender . ".png";
                        break;

                    case 8:
                         $iconFaction = "horde.gif";
                         $factionName = "Horde";
                         $raceName = "Troll";
                        $iconName = "raceTroll" . $gender . ".png";
                        break;

                    case 10:
                         $iconFaction = "horde.gif";
                         $factionName = "Horde";
                         $raceName = "Elfe de sang";
                        $iconName = "raceBloodElf" . $gender . ".png";
                        break;

                    case 11:
                         $iconFaction = "alliance.gif";
                         $factionName = "Alliance";
                         $raceName = "Draeneï";
                        $iconName = "raceDraenei" . $gender . ".png";
                        break;       

                    default:
                         $iconFaction = "unknow.gif";
                         $factionName = "unknow";
                         $raceName = "unknow";
                        $iconName = "unknow" . $gender . ".png";
                        break;
                }

                switch ($char['class']) {
                    case 1:
                        $className = "Guerrier";
                        $iconClass = "classGuerrier.png";
                        break;

                    case 2:
                        $className = "Paladin";
                        $iconClass = "classPaladin.png";
                        break;

                    case 3:
                        $className = "Chasseur";
                        $iconClass = "classChasseur.png";
                        break;

                    case 4:
                        $className = "Voleur";
                       $iconClass = "classVoleur.png";
                        break;

                    case 5:
                        $className = "Prêtre";
                        $iconClass = "classPretre.png";
                        break;

                    case 6:
                        $className = "Chevalier de la mort";
                        $iconClass = "classDk.png";
                        break;

                    case 7:
                        $className = "Chaman";
                        $iconClass = "classChaman.png";
                        break;

                    case 8:
                        $className = "Mage";
                       $iconClass = "classMage.png";
                        break;

                    case 9:
                        $className = "Démoniste";
                        $iconClass = "classDemoniste.png";
                        break;

                    case 10:
                        $className = "Druide";
                       $iconClass = "classDruide.png";
                        break;        

                    default:
                        $className= "unknow";
                        $iconClass = "unknow.png";
                }
            } else {
                $char['level'] = '81';
                $char['gender'] = '0';
                $char['race'] = '0';
                $char['class'] = '0';
                $iconClass = null;
                $iconName = null;
                $iconFaction = null;
                $className = null;
                $raceName = null;
                $factionName = null;               
            }
            if($char['level'] >= 80 || $char['level'] == '??') $folder = '80';
            else                                               $folder = 'no80';
            
            if ($O->active_sub == 'oxygen') {
                if ($char['level'] >= 10) {
                    $marginLevel = '60';
                    $height = "135";
                } else {
                    $marginLevel = '54';
                    $height = "135";                    
                }
            } else {
                if ($char['level'] >= 10) {
                    $marginLevel = '60';
                    $height = "133";
                } else {
                    $marginLevel = '64';
                    $height = "133";                    
                }           
            }

            $style = 'color: white;font-size: 12px;margin-left: ' . $marginLevel . 'px;line-height: ' . $height . 'px;';
            
            if ($O->active_act == 'mpDetail') 
                $margin = 'margin-left: 20px;';
            else
                $margin = '';
            
            $html = 
'
<div style="' . $margin . 'width: 80px; height: 80px; background: url(\'Web/Images/avatars/' . $folder . '/' . $char['gender'] . '-' . $char['race'] . '-' . $char['class'] . '.gif\') no-repeat; background-position: 50% 50%">
    <div class="frame">
        <span style="' . $style . '">' . $char['level'] . '</span>
    </div>
</div>
';            
            
            $icons['class'] = $iconClass;
            $icons['race'] = $iconName;
            $icons['faction'] = $iconFaction;
            $icons['className'] = $className;
            $icons['raceName'] = $raceName;
            $icons['factionName'] = $factionName;
            $icons['charHtml'] = $html;
            
            return $icons;
            
        }

	public function makeFromId($id) {
		$O = getOjoo();
		$account = $O->modele->account->select_id($id)->fetch();
		$account_access = $O->modele->account_access->select_id($id)->fetch();
		if ($account_access['gmlevel'] != NULL) 
			$this->gmlevel = $account_access['gmlevel'];
		else $this->gmlevel = 0;
		$this->accountId = $id;
		$this->username = $account['username'];
		$this->email = $account['email'];
		
		$this->currentIp = $_SERVER['REMOTE_ADDR'];
		$this->last_ip = $account['last_ip'];
		$this->expansion = $account['expansion'];
		$this->locked = $account['locked'];
		$this->failed_logins = $account['failed_logins'];
        $char = $O->bdd->char->query("SELECT name,guid FROM characters WHERE account=" . $id . " ORDER BY totaltime DESC LIMIT 0,1")->fetch();
		if ($char != null) {
	        $this->char = $char['name'];
	        $this->charGuid = $char['guid'];		
		} else {
			$this->char = null;
			$this->charGuid = null;
		}

        $vote = $O->bdd->site->query("SELECT points,date,ip FROM votes WHERE idCompte=" . $id)->fetch();
        if ($vote != false) {
            $this->nbVote = $vote['points'];
            $this->possibleDateVote = $vote['date'] + 60*60*3; // On rajoute 3heures à la date
            if ($this->possibleDateVote <= time()) 
                $this->possibleVote = true;
            else
                $this->possibleVote = false;
        } else {
            $this->possibleVote = true;
            $this->possibleDateVote = null;
            $this->nbVote = 0;
        }
		
		$money = $O->bdd->site->query("SELECT nbMoney FROM money WHERE idCompte=" . $id)->fetch();
        if ($money != false)
            $this->nbMoney = $money['nbMoney'];
        else
            $this->nbMoney = 0;
                
	}
	
	public function crediter($nbPoints) {
		$O = getOjoo();
		$money = $O->bdd->site->query("SELECT nbMoney FROM money WHERE idCompte=" . $this->accountId)->fetch();
        if ($money != false)	 {	
			$O->modele->money->crediter($nbPoints,$this->accountId);
			$this->nbMoney += $nbPoints;
		} else {
			$O->modele->money->nbMoney = $nbPoints;
			$O->modele->money->idCompte = $this->accountId;
			$O->modele->money->ADD();
			$this->nbMoney = $nbPoins;
		}
	}

	public function __construct() {
		if (isset ($_SESSION['wowUser'])) {
			$wowUser = $_SESSION['wowUser'];
			$this->accountId = $wowUser->accountId;
			$this->username = $wowUser->username;
			$this->email = $wowUser->email;
			$this->gmlevel = $wowUser->gmlevel;
			$this->currentIp = $_SERVER['REMOTE_ADDR'];
			$this->last_ip = $wowUser->last_ip;
			$this->expansion = $wowUser->expansion;
			$this->locked = $wowUser->locked;
			$this->failed_logins = $wowUser->failed_logins;
			$this->char = $wowUser->char;
			$this->charGuid = $wowUser->charGuid;
			$this->possibleDateVote = $wowUser->possibleDateVote;
			$this->nbVote = $wowUser->nbVote;
			$this->possibleVote = $wowUser->possibleVote;
			$this->nbMoney = $wowUser->nbMoney;
			$_SESSION['Rang'] = $this->gmlevel;
		}
		else {
			$this->accountId = -1;
			$this->username = "Invité";
			$this->email = "invite@invite.com";
			$this->gmlevel = "-1";
			$this->currentIp = $_SERVER['REMOTE_ADDR'];
			$this->last_ip = null;
			$this->expansion = NULL;
			$this->locked = null;
			$this->char = "Invité";
			$this->charGuid = null;
			$this->failed_logins = null;

			$_SESSION['Rang'] = -1;
		}
	}

	public function connect($username,$pass) {
		$O = getOjoo();
		// Exception la requête se passe ici.
		$username = strtoupper($username);
		$sha_pass_hash = sha1($username . ':' . strtoupper($pass));
		$testConnexion = $O->bdd->auth->query("SELECT COUNT(*) AS test FROM account WHERE username='" . $username . "' AND sha_pass_hash='" . $sha_pass_hash . "'")->fetch();
		if ($testConnexion['test'] > 0) {
			$id = $O->bdd->auth->query("SELECT id FROM account WHERE username='" . $username . "' AND sha_pass_hash='" . $sha_pass_hash . "'")->fetch();
			$this->makeFromId($id['id']);

			$_SESSION['wowUser'] = $this;

			return TRUE;
		}
		else return FALSE;
	}

	public function __destruct() {
		$_SESSION['wowUser'] = $this;
	}

	public function logout() {
			$this->accountId = -1;
			$this->username = "Invité";
			$this->email = "invite@invite.com";
			$this->gmlevel = "-1";
			$this->currentIp = $_SERVER['REMOTE_ADDR'];
			$this->last_ip = null;
			$this->expansion = NULL;
			$this->locked = null;
			$this->failed_logins = null;

			$_SESSION['Rang'] = -1;
			unset($_SESSION['wowUser']);
	}



}
?>