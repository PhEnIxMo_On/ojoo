<?php
	class Console {
		const APP = "APPLICATION";
		const ROUTING = "ROUTING";
		const PHAR = "PHAR";
		const CONTROL = "CONTROL";
		
		private $messages = array();
		private $template;
        private $lastColor = "yellow";
        private $lastType = "";
		private $colors;
		private $icons;
		private $mask;
		private $totalEnv;

		public function __construct() {
			$this->template = new Template();
			$this->template->load_template("Ojoo/Console.tpl.php");
			$this->OjooDebug = get_defined_vars();
			$this->colors = array(
				"ROUTING" => "103473",
				"APPLICATION" => "04C497",
				"CONTROL" => "C4C404",
				"PHAR" => "C4047E"
			);			
			$this->icons = array(
				"APPLICATION" => "&#0098;"
			);
			$this->mask = array(
				"APPLICATION" => 0x00001,
				"ROUTING"     => 0x00010,
				"PHAR"        => 0x00100,
				"CONTROL"     => 0x01000
			);
		}
		
		public function get_color($color) {
			if (isset ($this->colors[$color])) return $this->colors[$color];
			else 							   return 'black';
		}
		
		public function get_mask($name) {
			return $this->mask[$name];
		}
		
		public function get_icon($icon) {
			if (isset ($this->icons[$icon])) return $this->icons[$icon];
			else 							   return 'none';
		}		

		public function display() {
			$html = '';
			foreach ($this->messages as $message) {
				$html .= $message->getFormatMessage();
			}
			$this->template->set_tampon(str_replace("{{CONTENT_CONSOLE}}",$html,$this->template->get_tampon()));
			$this->template->display();
		}

        public function debug($element,$message,$file,$method,$env) {
            $O = getOjoo();
            if ($O->getRunningMode() == Ojoo::DEBUG_MODE && $this->filter($element))
                $this->messages[] = new ConsoleMessage($element,$message,$file,$method,$env);
        } 
        
		public function filter($element) {
			//unset($_SESSION['filterConsole']);
			if (!isset ($_SESSION['filterConsole'])) {
				// On met juste le debug de l'application + control de base
				$_SESSION['filterConsole'] |= $this->mask['APPLICATION'];
				$_SESSION['filterConsole'] |= $this->mask['CONTROL'];
			}
				
			if ($this->mask[$element] & $_SESSION['filterConsole']) return true;
			else 													return false;
		}
                
		public function add(ConsoleMessage $message) {
			$this->messages[] = $message;
		}

		public function checkArgs($command,$arg2) {
			foreach ($arg2 as $argName => $needed) {
				if (!isset ($command['args'][$argName])) return consoleMessage("checkArgs",1,$needed);
			}
			return TRUE;
		}
		

		public function getFormatCommand($command) {
			/*
				Commande de la forme : 
				NomDuScript -arg Argument 1 -arg Argument 2
				Exemple : modele -table projets => Créé le modele en fonction de la table projets
			*/

			$tableCommande = explode('-',$command);

			$script = trim($tableCommande[0]);
			$args = array();
			foreach ($tableCommande as $key => $commande) {
				if ($key != 0) {
					$tableArg = explode(" ",$commande);
					$args[$tableArg[0]] = $tableArg[1];
				}
			}
			$finalCommand = array("script" => $script,"args" => $args);
			return $finalCommand;
			
		}

	}
?>