<?php
class DbInstance {
	private $_nom;
	private $_PDO;
	public function DbInstance($nom,$connexion = null) {
		$O = $GLOBALS['O'];
		if ($connexion != null) {
			$this->_nom = $nom;
			$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$this->_PDO = new PDO($connexion['string'],$connexion['user'],$connexion['pass'],$pdo_options);
			if ($O->config['DB_NAMES_UTF8'])
				$this->_PDO->query("SET NAMES UTF8"); 
		} else {
			$this->_nom = $nom;
			$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$this->_PDO = new PDO('mysql:host=mysql51-47.perso;dbname=evolyaseweb','evolyaseweb','08evoweb08',$pdo_options);
		}
	}

	
	public function last_id() {
		return $this->_PDO->lastInsertID();
	}
	public function query($sql) {
		/*$log = new Log("FormatBddHistory.txt");
		$texte = 
		"
			Requ�te : " . $sql . "
			Type    : normale
			Effectu�e le : " . date_fran() . "
		";
		$log->log_it($texte);
		$log->setLog("BddHistory.txt");
		$log->log_it($sql);*/
		$this->_PDO->query('SET NAMES utf8');
		return $this->_PDO->query($sql);
	}
	public function queryR($sql) {
		$log = new Log("FormatBddHistory.txt");
		$texte = 
		"
			Requ�te : " . $sql . "
			Type    : Rang�e.
			Effectu�e le : " . date_fran() . "
		";
		$log->log_it($texte);
		$log->setLog("BddHistory.txt");
		$log->log_it($sql);
		$this->_PDO->query('SET NAMES utf8');
		$reponse = $this->_PDO->query($sql);
		return $reponse->fetch();
	}
}
?>