<?php
    abstract class Form {

        public function getAsTable($except = null) {
            $reflection = new ReflectionClass($this);
            $props   = $reflection->getProperties();
            $output = "<table>";
            $hidden = "";
            foreach ($props as $prop) {
                $name = str_replace('[]','',$prop->name);
                              
               // var_dump($this->$name);
                if (is_object($this->$name)) {
                    if ($this->$name->getType() != 'InputTable') {
                        $this->$name->getAsHtml();
                        if ($this->$name->htmlData['input'] != '' && $this->$name->getType() != Input::INPUT_HIDDEN && !isset($except[$name])) {
                            $output .= "<tr>";
                                $output .= "<td>" . $this->$name->htmlData['icon'] . "</td>";
                                $output .= "<td>" . $this->$name->htmlData['input'] . "</td>";
                                $output .= "<td>" . $this->$name->htmlData['help'] . "</td>";
                            $output .= "</tr>";
                        } else {
                            if ($this->$name->getType() == Input::INPUT_HIDDEN && !isset($except[$name])) {
                                $hidden .= $this->$name->htmlData['input'];								
                            }
                        }
                    } else {
                        if (is_array($this->$name->getInputs())) {                            
                            foreach ($this->$name->getInputs() as $input) {
                                $input->getAsHtml();
                                if ($input->htmlData['input'] != '' && $input->getType() != Input::INPUT_HIDDEN && !isset($except[$name])) {
                                    $output .= "<tr>";
                                        $output .= "<td>" . $input->htmlData['icon'] . "</td>";
                                        $output .= "<td>" . $input->htmlData['input'] . "</td>";
                                        $output .= "<td>" . $input->htmlData['help'] . "</td>";
                                    $output .= "</tr>";
                                } else {
                                    if ($input->getType() == Input::INPUT_HIDDEN && !isset($except[$name])) {
                                        $hidden .= $input->htmlData['input'];
                                    }

                                }                            
                            }
                        }
                    }
                }
            }
            $output .= "</table>" . $hidden;
            
            return $output;
        }
        
        public function hydrate($modele) {
            $reflection = new ReflectionClass($this);
            $props   = $reflection->getProperties();
            foreach ($props as $prop) {
                $name = $prop->name;
                if (is_object($this->$name)) $this->$name->getAsHtml();
                if (is_object($this->$name)) {
                    if ($this->$name->htmlData['input'] != '' && $this->$name->getType() != Input::INPUT_HIDDEN) {
                        $this->$name->setValue($modele->$name);
                    } else {
                        if ($this->$name->getType() == Input::INPUT_HIDDEN) {
                            $this->$name->setValue($modele->$name);
                        }
                            
                    }
                }
            }         
        }
        
    }
?>
