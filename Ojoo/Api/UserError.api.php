<?php
class UserError {
	private $title;
	private $code;
	// Valeurs possibles : invisible, low, mid, hight
	private $level;
	private $log;
	private $line;
	private $template;
	private $message;
	private $trace = array();
	private $htmlDisplay = "";

	public function getLog() {
		return $this->log;
	}
	
	public function __toString() {
		return "UserError[" . $this->code . "] " . $this->title . "";
	}
	
	public function loadXml($code,$params = null) {
		$errors = simplexml_load_file('Erreur/customErrors.xml');
		$O = getOjoo();
		if (!Template::templateExists($O->config['Erreur']['userErrorTpl'])) {
			die('Template Error: Recursive not found. Prevent max execution error.');
		}		
		foreach ($errors->error as $error) {
			if ($error['code'] == $code) {
				$this->title = $error->title;
				$this->message = $error->message;
				$this->code = $error['code'];
				$this->level = $error->level;
				$this->template = new Template();
				if (isset ($error->template)) 
					$this->template->load_template($error->template);
				else
					$this->template->load_template($O->config['Erreur']['userErrorTpl']);


				$this->template->set_tampon(str_replace('{{TITRE}}',$error->title,$this->template->get_tampon()));
				$this->template->set_tampon(str_replace('{{CONTENT}}',$error->message,$this->template->get_tampon()));
				$this->htmlDisplay .= $this->template->get_tampon();
				$this->trace = array("sub" => $O->route->get_sub(), "mod" => $O->route->get_mod(),"act "=> $O->route->get_act());
				if ($params != NULL) $this->replaceParams($params);
				$this->htmlDisplay = $O->template->parseOjooCode($this->htmlDisplay);
				
				if (isset ($error['display'])) {
					if ($error['display'] == 'false') $this->htmlDisplay = "";
				}
				$O->app->addUserError($this);
				break;
			}
			elseif ($error->name == $code) {
				$this->title = $error->title;
				$this->message = $error->message;
				$this->code = $error['code'];
				$this->level = $error->level;
				$this->template = new Template();
				if (isset ($error->template)) 
					$this->template->load_template($error->template);
				else
					$this->template->load_template($O->config['Erreur']['userErrorTpl']);
					
				$this->template->set_tampon(str_replace('{{TITRE}}',$error->title,$this->template->get_tampon()));
				$this->template->set_tampon(str_replace('{{CONTENT}}',$error->message,$this->template->get_tampon()));
				$this->htmlDisplay .= $this->template->get_tampon();

				if ($error['log'] == 'true') {
					$this->log = new log('userError.txt');
					$this->log->log_it('UserError[' . $error['code'] . '/' . $error->level . '] : ' . $error->name . ' => ' . $error->message . '
Sub/Mod/Act :' . $O->route->sub . '/' . $O->route->mod . '/' . $O->route->act);
				}
				if ($params != NULL) $this->replaceParams($params);
				$this->htmlDisplay = $O->template->parseOjooCode($this->htmlDisplay);
				$O->app->addUserError($this);
				break;
			}
		}
	}

	public function addException($e) {
		$this->code = $e->getCode();
		$this->file = $e->getFile();
		$this->message = $e->getMessage();
		$this->line = $e->getLine();
		$this->trace = $e->getTrace();
		
		$params = array(
			"{{CODE}}" => $this->code,
			"{{FILE}}" => $this->file,
			"{{TRACE}}" => $this->trace,
			"{{MESSAGE}}" => $this->message,
			"{{ERRORLINE}}" => $this->line
		);

		$this->loadXml(6,$params);
	}

	public function replaceParams($params) {
		foreach ($params as $key => $param) {
			if (!is_array($param))
				$this->htmlDisplay = str_replace($key,$param,$this->htmlDisplay);
		}
	}

	public function getHtmlDisplay() {
		return $this->htmlDisplay;
	}

	public function __construct() {
	}

	public function getLevel() {
		return $this->level;
	}

	public function getTitle() {
		return $this->title;
	}

	public function getCode() {
		return $this->code;
	}
}
?>