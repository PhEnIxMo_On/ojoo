<?php
class Sub {
	public $config;
	public $name;
	public $path;
	public $mod;
	
	public function __construct($name) {
		if ($name != null) {
			$this->config = yaml_parse('Web/Sub/' . $name . '/' . $name . '.conf.yml');
			$this->name = $name;
			$this->path = 'Web/Sub/' . $name . '/';
		}
	}
	
	public function set_sub($nom) {
		$this->__construct($nom);
	}
	
	public function __get($nom) {
		if ($nom != 'path' && $nom != 'config' && $nom !='name')
			if (isset ($this->mod[$nom])) return $this->mod[$nom];
		else {
			if (is_dir($this->path . $nom)) {
				$this->mod[$nom] = new Mod($this->path . $nom . '/',$nom,$this->name);
				return $this->mod[$nom];
			} else {
				return null;
			}
		}
	}
	
	public function set_droit($droit) {
		$this->config['droit'] = $droit;
		$dump = yaml_dump($this->config);
		$O = $GLOBALS['O'];
		$O->file->delete('Web/Sub/' . $this->name . '/' . $this->name . '.conf.yml');
		$config = new File('Web/Sub/' . $this->name . '/' . $this->name . '.conf.yml','a+');
		$config->write($dump);
		$config->close();
	}
	
}
?>