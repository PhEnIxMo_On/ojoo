<?php

/*
 * Short description : 
 * Author : 
 * Comments : 
 */

class Route extends Request {
    //put your code here
    
    private $sub;
    private $mod;
    private $act;
    private $routeUrl;
    private $title;
    private $template;
    private $name;
    public $params;
    private $prefix;
    private $splitUrl;
	private $pageTitle;
	private $pageSubTitle;
    private $routeFound = false;
    
    public function __construct() {
		$config = yaml_parse('Ojoo/Ojoo.conf.yml');
        parent::__construct();
        $sitePath = str_split($config['SitePath']);
        $splitIndex = count($sitePath);
        $this->splitUrl = $this->getSplitUrl($this->url, $splitIndex);
		
    }
    
    public function getSub() {
        return $this->sub;
    }
    
    public function getMod() {
        return $this->mod;
    }
    
    public function getAct() {
        return $this->act;
    }

    
    public function getTitle() {
        return $this->title;
    }
    
    public function getSplitUrl($url,$splitIndex) {
        return substr($url,$splitIndex);
    }
    
    public function loadRoutes() {
        return include 'routing.php';
    }
    
    public function findRoute($routeName) {
        $routes = $this->loadRoutes();
        if (isset($routes[$routeName])) {
            return $routes[$routeName];
        } else return false;
    }
    
    public function makeUrl($routeName = null,$params = null)
	{
        if ($routeName == null)
		{
            // On travail avec la route courante
            return $this->prefix . $this->url;
        }
		else
		{
            // On souhaite cr?er l'URL d'une autre route
            $route = $this->findRoute($routeName);
            if ($route != false)
			{
                $prefix = (isset ($route['prefix'])) ? $route['prefix'] . '/' : '';
                if ($params != null)
				{
                    if (is_array($params))
					{
                        foreach ($params as $param)
						{
							$route['url'] = preg_replace('#\(\.\+\)#', $param, $route['url'], 1);
							$route['url'] = preg_replace('#\(\[0-9\]\+\)#', $param, $route['url'], 1);
                        }
                    }
					else
					{
						$route['url'] = preg_replace('#\(\.\+\)#', $params, $route['url'], 1);
						$route['url'] = preg_replace('#\(\[0-9\]\+\)#', $params, $route['url'], 1);
                    }
                }
                return $prefix . $route['url'];
            }
			else throw new Exception("[OjooRoute] Couldn't find any route with name " . $routeName);
        }
    }
    
    public function exist() {
        return $this->routeFound;
    }
	
	public function getPageTitle() {
		return $this->pageTitle;
	}
    
	public function getPageSubTitle() {
		return $this->pageSubTitle;
	}
	
	public function setPageTitle($t) {
		$this->pageTitle = $t;	
	}
	
	public function setPageSubTitle($t) {
		$this->pageSubTitle = $t;
	}
	
    public function getRoute() {
        $routes = $this->loadRoutes();
        $O = getOjoo();
		if ($O->config["Debug"]['Routing'])
        	$O->console->debug(Console::ROUTING,"Loading routes ...",__FILE__,"Route::getRoute()",get_defined_vars());
        foreach ($routes as $name => $route)
        {
                $vars = array();
                $prefixe = (isset ($route['prefix'])) ? $route['prefix'] . '/' : '';
				$O->console->debug(Console::ROUTING,"Trying with : " . $prefixe . $route['url'],__FILE__,"Route::getRoute()",get_defined_vars());
                if (preg_match('#^' . $prefixe . $route['url'].'$#', $this->splitUrl, $matches)) {					
					  
                        if(array_key_exists('vars', $route))
                        {
							$varsNames = explode(',', $route['vars']);
							foreach ($matches as $key => $match)
							{
								if ($key != 0) {
									$vars[$varsNames[$key-1]] = $match;
								}
							}
                        }
                        $this->routeUrl = $route['url'];
                        $this->prefix = (isset($route['prefix'])) ? $route['prefix'] . '/' : '';
                        $this->sub = $route['sub'];
                        $this->mod = $route['mod'];
                        $this->act = $route['act'];
						$this->pageTitle = (isset($route['title'])) ? $route['title'] : '';
						$this->pageSubTitle = (isset($route['subTitle'])) ? $route['subTitle'] : '';
                        $this->title = (isset($route['title'])) ? $route['title'] : '';
                        $subConf = yaml_parse('Web/Sub/' . $route['sub'] . '/' . $route['sub'] . '.conf.yml');
                        $this->template = (isset($route['template'])) ? $route['template'] : $subConf['design'];
                        $this->name = $name;
                        $this->params = $vars;
                        $this->routeFound = true;
						$O->console->debug(Console::ROUTING,"Route found : " . $name,__FILE__,"Route::getRoute()",get_defined_vars());
                        return APP_START;
              } 
        } 
       	$O->console->debug(Console::ROUTING,"No matches found !",__FILE__,"Route::getRoute()",get_defined_vars());
       return APP_NOT_FOUND;
    }
    
    public function getTemplate() {
        return $this->template;
    }
    
    public function makePathTo($name) {
        switch ($name) {
            
            case 'Act':
                return 'Web/Sub/' . $this->sub . '/' . $this->mod . '/Act/';
                break;
            
            case 'View':
                return 'Web/Sub/' . $this->sub . '/' . $this->mod . '/View/';
                break;
            
            case 'Template':
                return 'Web/Template/';
                break;
            
            case 'Api':
                return 'Ojoo/Api/';
                break;
        }
    }

	public function get_sub() {
      return $this->sub;
    }

	public function set_sub($sub) {
      $this->sub = $sub;
      return $this;
    }

	public function get_mod() {
      return $this->mod;
    }

	public function set_mod($mod) {
      $this->mod = $mod;
      return $this;
    }

	public function get_act() {
      return $this->act;
    }

	public function set_act($act) {
      $this->act = $act;
      return $this;
    }

	public function get_routeUrl() {
      return $this->routeUrl;
    }

	public function set_routeUrl($routeUrl) {
      $this->routeUrl = $routeUrl;
      return $this;
    }

	public function get_title() {
      return $this->title;
    }

	public function set_title($title) {
      $this->title = $title;
      return $this;
    }

	public function get_template() {
      return $this->template;
    }

	public function set_template($template) {
      $this->template = $template;
      return $this;
    }

	public function get_name() {
      return $this->name;
    }

	public function set_name($name) {
      $this->name = $name;
      return $this;
    }

	public function get_params() {
      return $this->params;
    }

	public function set_params($params) {
      $this->params = $params;
      return $this;
    }

	public function get_prefix() {
      return $this->prefix;
    }

	public function set_prefix($prefix) {
      $this->prefix = $prefix;
      return $this;
    }

	public function get_splitUrl() {
      return $this->splitUrl;
    }

	public function set_splitUrl($splitUrl) {
      $this->splitUrl = $splitUrl;
      return $this;
    }

	public function get_pageTitle() {
      return $this->pageTitle;
    }

	public function set_pageTitle($pageTitle) {
      $this->pageTitle = $pageTitle;
      return $this;
    }

	public function get_pageSubTitle() {
      return $this->pageSubTitle;
    }

	public function set_pageSubTitle($pageSubTitle) {
      $this->pageSubTitle = $pageSubTitle;
      return $this;
    }
}