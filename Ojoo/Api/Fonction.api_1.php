<?php
	
function yaml_parse($path) {
	$array = Spyc::YAMLLoad($path);
	return $array;
}
function yaml_dump($array) {
	return Spyc::YAMLDump($array);
}
function consoleMessage($api,$code,$message) {
	$msg = new ConsoleMessage($api,$code,$message);
	echo $msg->getFormatMessage();
}
function stripAccents($string){
	return strtr($string,'àáâãäßçèéêëìíîïñòóøôõöùúûüýÿÀÁÂÃÄÇÐÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
'aaaaabceeeeiiiinoooooouuuuyyAAAAACDEEEEIIIINOOOOOUUUUY');
}
function cleanCaracteresSpeciaux ($chaine)
{
	setlocale(LC_ALL, 'fr_FR');

	$chaine = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $chaine);

	$chaine = preg_replace('#[^0-9a-z]+#i', '-', $chaine);

	while(strpos($chaine, '--') !== false)
	{
		$chaine = str_replace('--', '-', $chaine);
	}

	$chaine = trim($chaine, '-');

	return $chaine;
}
function stateTask($level) {
	switch($level) {
		case 0: echo "En attente"; break;

		case 1: echo '<font color="green"> Ouvert </font>'; break;

		case 2: echo '<font color="red"> Fermee </fond>'; break;

		case 3: echo '<font color="grey"> Ancien </font>'; break;

		case 4: echo '<font color="orange"> En difficult� </font>'; break;
		
		case 5: echo '<font color="brown"> Fait </font>'; break;

		default: echo '<font color="red"> Erreur ! </font>';
	}
}
function date_fran() {
  $mois = array("Janvier", "Fevrier", "Mars",
                "Avril","Mai", "Juin", 
                "Juillet", "Ao�t","Septembre",
                "Octobre", "Novembre", "Decembre");
  $jours= array("Dimanche", "Lundi", "Mardi",
                "Mercredi", "Jeudi", "Vendredi",
                "Samedi");
  return $jours[date("w")]." ".date("j").(date("j")==1 ? "er":" ").
         $mois[date("n")-1]." ".date("Y");
}
 function formatUsernameWow($username) {
	$username = strtolower($username);
	$username[0] = strtoupper($username[0]);
	return $username;
 }
function assoc($modele) {
	$reflection = new ReflectionClass($modele);
	$props   = $reflection->getProperties();
	foreach(@$props as $prop) {
		$nameProp = $prop->getName();
		if (isset ($_POST[$nameProp]))
			$modele->$nameProp = $_POST[$nameProp];
	}
}
function formatDate($timestamp) {
	return date_fr('l j F',$timestamp);
}
function date_fr($format, $timestamp=false) {
	if ( !$timestamp ) $date_en = date($format);
	else               $date_en = date($format,$timestamp);

	$texte_en = array(
		"Monday", "Tuesday", "Wednesday", "Thursday",
		"Friday", "Saturday", "Sunday", "January",
		"February", "March", "April", "May",
		"June", "July", "August", "September",
		"October", "November", "December"
	);
	$texte_fr = array(
		"Lundi", "Mardi", "Mercredi", "Jeudi",
		"Vendredi", "Samedi", "Dimanche", "Janvier",
		"F&eacute;vrier", "Mars", "Avril", "Mai",
		"Juin", "Juillet", "Ao&ucirc;t", "Septembre",
		"Octobre", "Novembre", "D&eacute;cembre"
	);
	$date_fr = str_replace($texte_en, $texte_fr, $date_en);

	$texte_en = array(
		"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun",
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
		"Aug", "Sep", "Oct", "Nov", "Dec"
	);
	$texte_fr = array(
		"Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim",
		"Jan", "F&eacute;v", "Mar", "Avr", "Mai", "Jui",
		"Jui", "Ao&ucirc;", "Sep", "Oct", "Nov", "D&eacute;c"
	);
	$date_fr = str_replace($texte_en, $texte_fr, $date_fr);

	return $date_fr;
}
function ojoo_exception_handler($e) {
	$O = getOjoo();
	$O->userError->addException($e);

	return TRUE;
}
function ojoo_error_handler($level,$message,$file,$line,$context) {
	$O = getOjoo();
	$error = new PhpError($message,$file,$line,$level,$context);
	return TRUE;
}
function ojoo_shutdown_function() {
}
function error($texte) {
	echo '
		<div class="alert alert-error">
			<span class="label label-important"> Erreur ! </span>&nbsp;
			' . $texte . '
		</div>
	';
}function formatError($texte) {
	echo '
		<div class="erreur">
			<span class="label label-important"> Erreur ! </span>&nbsp;
			' . $texte . '
		</div>
	';
}
function getProjectType($slug) {
	switch ($slug) {
		case 'web':
			return 'Projet web (PHP,HTML,CSS,Javascript etc ...)';
		break;

		case 'webApplet':
			return 'Projet web (applet Java, C++ etc ..)';
		break;

		case 'appClient':
			return 'Application client (C++, JAVA, C# etc .. )';
		break;

		case 'webGame':
			return 'Jeu par navigateur';
		break;

		case 'appGame2D':
			return 'Jeu client 2D';
		break;

		case 'appGame3D':
			return 'Jeu client 3D';
		break;

		case 'autre':
			return 'Autre';
		break;
	}
}
function getOjoo() {
	$O = $GLOBALS['O'];
	return $O;
}
function validForm($msg) {
	$O = $GLOBALS['O'];
	$O->resetFlashMessage();
	$O->addFlashMessage("validForm",TRUE);
	$O->addFlashMessage("ok",$msg);
}
function errorForm() {
	$O = $GLOBALS['O'];
    if (isset ($O->flashMessage["validForm"])) {
        ?>
            <div class="validBox"><?php echo $O->flashMessage["ok"]; ?></div><br />
        <?php
    } else {
        foreach ($O->flashMessage as $message) {
            ?>
                <div class="errorBox"><?php echo $message; ?></div><br />
            <?php
        }
    }
}
function info($texte) {
	echo '
		<div class="info">
			<span class="label label-info"> Info ! </span>&nbsp;
			' . $texte . '
		</div>
	';
}
?>