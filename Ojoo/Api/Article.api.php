<?php
class Article {
	private $id;
	private $qte;
	private $fetchData;
	private $prixVote;
	private $prixMoney;
	// 0 => Vote 1 => Money
	private $typePrice;

	public function __construct($id = null,$qte = 1,$type = 0) {
		if ($id != null) {
			$O = getOjoo();
			$article = $O->modele->web_boutique_article->select_id($id)->fetch();
			if ($article != null) {
				$this->id = $article['id'];
				$this->qte = $qte;
				$this->fetchData = $article;
				$this->prixVote = $article['prixVote'];
				$this->prixMoney = $article['prixMoney'];
				$this->typePrice = $type;
			}
		}
	}
	
	public function itsTimeToPay() {
		$O = getOjoo();
		if ($this->typePrice == 0) {
			$O->wowUser->nbVote -= $this->prixVote * $this->qte;
			$O->modele->votes->setVote($O->wowUser->nbVote,$O->wowUser->getAccountId());
		} else {
			if ($this->id == 763)
				$O->wowUser->nbMoney -= $this->prixMoney;
			else
				$O->wowUser->nbMoney -= $this->prixMoney * $this->qte;
			$O->modele->money->setMoney($O->wowUser->nbMoney,$O->wowUser->getAccountId());
		}
		$_SESSION['wowUser'] = $O->wowUser;
	}
	
	public function isSpecial() {
		if ($this->getData('idWowhead') < 0) return true;
		else                                 return false;
	}
	
	public function getPrixVote() {
		return $this->prixVote;
	}
	
	public function getPrixMoney() {
		return $this->prixMoney;
	}
	
	public function getName() {
		return $this->fetchData['name'];
	}
	
	public function getPriceType() {
		if ($this->typePrice == 0) return " point vote";
		else 					   return 'bulle d\'Oxygen';
	}
	
	public function getStock() {
		return $this->fetchData['stock'];
	}
	
	public function setQte($qte) {
		$this->qte = $qte;
	}
	
	public function getTotalPrice() {
		if ($this->canBuy()) {
			if ($this->typePrice == 0) {
				return $this->prixVote * $this->qte;
			} else {
				return $this->prixMoney * $this->qte;
			}
		} else return false;
	}
	
	public function addQte($qte) {
		$this->qte += $qte;
	}
	
	public function getData($name) {
		if (isset ($this->fetchData[$name])) return $this->fetchData[$name];
		else 								 return false;
	}
	
	public function getQte() {
		return $this->qte;
	}
	
	public function getType() {
		return $this->typePrice;
	}
	
	public function canBuy() {
		$O = getOjoo();
		$article = $O->modele->web_boutique_article->select_id($this->id)->fetch();
		if ($article != null) {
			$cat = $O->modele->web_boutique_cat->select_id($article['idCat'])->fetch();
			if ($cat['state'] & web_boutique_cat::IS_OPEN) {
				// Catégorie ouverte, on check les moyens de paiement. 
				if ($this->typePrice == 0) {
					if ($cat['state'] & web_boutique_cat::IS_VOTE) {
						if ($article['state'] & web_boutique_cat::IS_OPEN && $article['state'] & web_boutique_cat::IS_VOTE) return true;
						else return false;
					}
				} else {					
					if ($cat['state'] & web_boutique_cat::IS_FREE_OR_REAL_MONEY) {
						if ($article['state'] & web_boutique_cat::IS_OPEN && $article['state'] & web_boutique_cat::IS_FREE_OR_REAL_MONEY) return true;
						else return false;
					}					
				}
			} else return false;
		}		
	}

}
?>