<?php
	class PhpError {
		private $level;
		private $file;
		private $line;
		private $message;
		private $customErrorName;
		private $template;
		private $log;

		public function getFile() {
			return $this->file;
		}

		public function getLine() {
			return $this->line;
		}

		public function getMessage() {
			return $this->message;
		}

		public function getLog() {
			return $this->log;
		}

		public function getContext() {
			return $this->context;
		}

		public function __construct($message,$file,$line,$level,$context,$log = true,$customErrorName = "") {
			$O = getOjoo();
			$this->level = $level;
			$this->message = $message;
			$this->file = $file;
			$this->line = $line;
			$this->context = $context;
			$this->customErrorName = $customErrorName;
			if ($log) {
				$this->log = new Log("phpError.txt");
				$this->log->log_it($this->formatLogError());	
			}
			if ($O->config['Dev']['autoDisplay']) {
				if ($this->customErrorName == "")
					$O->userError->loadXml(4,$this->formatErrorParams());
				else
					$O->userError->loadXml($customErrorName,$this->formatErrorParams());
			}
			$O->app->addPhpError($this);
		}

		public function getErrorLevel() {
			return $this->level;
		}

		public function formatErrorParams($titre = "PHP Error !") {
			return array(
				"{{TITLE}}" => $titre,
				"{{FILE}}" => $this->file,
				"{{ERRORLINE}}" => $this->line,
				"{{MESSAGE}}" => $this->message,
				"{{LEVEL}}" => $this->level
			);
		}

		public function formatLogError() {
			$error = "Généré le : " . date('l jS \of F Y h:i:s A');
			$error .= "\n Level : " . $this->level;
			$error .= "\n File : " . $this->file;
			$error .= "\n Line : " . $this->line;
			$error .= "\n Message : " . $this->message;

			return $error;
		}

	}
?>