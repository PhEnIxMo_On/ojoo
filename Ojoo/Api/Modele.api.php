<?php
class Modele {
	public $modeles = array();
	
	public function __get($nom) {
		if (!isset ($this->modeles[$nom])) {
			if (file_exists('Web/Modele/' . $nom . '.php')) {
				require_once 'Web/Modele/' . $nom . '.php';
				$this->modeles[$nom] = new $nom();
				return $this->modeles[$nom];
			} else {
				$O = $GLOBALS['O'];
				$O->erreur->set_erreur('#O1',12,'Modele.api.php');
				$O->erreur->display();
			}
		} else return $this->modeles[$nom];
	}
}
?>