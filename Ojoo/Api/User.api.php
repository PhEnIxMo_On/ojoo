<?php
	class User {
		private $userName;
		private $rang;
		private $email;
		private $id;
		private $connect = false;
        private $pays;
        private $adresse;
        private $ville;
        private $skype;
        private $hotmail;
        private $avatar;
        private $recherche;
        private $privacyData;
        private $OjooConfig;
		
		public function __construct($id = null,$rang = 'invite',$config) {
            $this->OjooConfig = $config;
            if ($id == null) {
                $this->userName = "Invité";
                $this->rang = $config['Rang'][$rang];
                $this->id = NULL;
                $this->email = NULL;
                $this->avatar = "Web/Images/CP/avatar_unknow.png";
                // Ce n'est pas un véritable utilisateur, il est ici en tant qu'invité ou autre, dans tout les cas il n'est pas connecté
                $this->connect = false;
            } else {
                $this->makeFromId($id);
            }
		}
		
		public function makeFromId($id) {
			$O = $GLOBALS['O'];
			$donnees = $O->modele->CPuser->select_id($id)->fetch();
            if ($donnees['pseudo'] != NULL) {
			    $this->userName = $donnees['pseudo'];
			    $this->rang = $donnees['rang'];
                $this->privacyData = $O->modele->userprivacy->select_userID($id)->fetch();
			    $this->email = $donnees['email'];
                $this->pays = $donnees['pays'];
                $this->adresse = $donnees['adresse'];
                $this->ville = $donnees['ville'];
                $this->skype = $donnees['skype'];
                $this->avatar = $donnees['avatar'];
                if ($this->avatar == '' or $this->avatar == NULL) 
                     $this->avatar = "Web/Images/CP/avatar_unknow.png";
                $this->recherche = $donnees['recherche'];
                $this->hotmail = $donnees['hotmail'];
			    $this->id = $donnees['id'];
			    $this->connect = true;
            }
		}
		
		public function logout() {
            $this->userName = "Invité";
            $this->rang = $this->OjooConfig['Rang']['invite'];
            $this->id = NULL;
            $this->email = NULL;
            $this->connect = FALSE;
            $this->avatar = "Web/Images/CP/avatar_unknow.png";
            unset($_SESSION['user']);
		}
		
		public function isConnect() {
			if ($this->connect) return true;
			else {
				return false;
			}
		}

        public function destructSession() {
            unset($_SESSION['user']);
        }
		
		public function __destruct() {
			$_SESSION['user'] = $this;
		}

		public function get_userName() {
	      return $this->userName;
	    }
	
		public function set_userName($userName) {
	      $this->userName = $userName;
	      return $this;
	    }
	
		public function get_rang() {
	      return $this->rang;
	    }
	
		public function set_rang($rang) {
	      $this->rang = $rang;
	      return $this;
	    }
	
		public function get_email() {
	      return $this->email;
	    }
	
		public function set_email($email) {
	      $this->email = $email;
	      return $this;
	    }
	
		public function get_id() {
	      return $this->id;
	    }
	
		public function set_id($id) {
	      $this->id = $id;
	      return $this;
	    }
	
		public function get_pays() {
	      return $this->pays;
	    }
	
		public function set_pays($pays) {
	      $this->pays = $pays;
	      return $this;
	    }
	
		public function get_adresse() {
	      return $this->adresse;
	    }
	
		public function set_adresse($adresse) {
	      $this->adresse = $adresse;
	      return $this;
	    }
	
		public function get_ville() {
	      return $this->ville;
	    }
	
		public function set_ville($ville) {
	      $this->ville = $ville;
	      return $this;
	    }
	
		public function get_skype() {
	      return $this->skype;
	    }
	
		public function set_skype($skype) {
	      $this->skype = $skype;
	      return $this;
	    }
	
		public function get_hotmail() {
	      return $this->hotmail;
	    }
	
		public function set_hotmail($hotmail) {
	      $this->hotmail = $hotmail;
	      return $this;
	    }
	
		public function get_avatar() {
	      return $this->avatar;
	    }
	
		public function set_avatar($avatar) {
	      $this->avatar = $avatar;
	      return $this;
	    }
	
		public function get_recherche() {
	      return $this->recherche;
	    }
	
		public function set_recherche($recherche) {
	      $this->recherche = $recherche;
	      return $this;
	    }
	
		public function get_privacyData() {
	      return $this->privacyData;
	    }
	
		public function set_privacyData($privacyData) {
	      $this->privacyData = $privacyData;
	      return $this;
	    }
	
		public function get_OjooConfig() {
	      return $this->OjooConfig;
	    }
	
		public function set_OjooConfig($OjooConfig) {
	      $this->OjooConfig = $OjooConfig;
	      return $this;
	    }
}