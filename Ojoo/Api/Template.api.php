<?php
class Template {
	private $tampon;
	private $active_tpl;
	private $is_display = false;
	
	public function load_template($template) {
		$O = $GLOBALS['O'];
		if (file_exists('Web/Template/' . $template)) {
			ob_start();
				include 'Web/Template/' . $template;
				$this->tampon = ob_get_contents();
			ob_end_clean();
			$this->active_tpl = $template;
			$this->parse($this->tampon);
		} else {
			$O->userError->loadXml(12,array('{{FILE}}' => $template));
		}
	}
	
	public static function templateExists($template) {
		if (file_exists('Web/Template/' . $template)) return true;
		else return false;
	}
	
	public function load_act($path) {
		$O = $GLOBALS['O'];
		ob_start();
			include $path;
			$this->tampon = ob_get_contents();
		ob_end_clean();
		$this->parse($this->tampon);
	}
	
	public function route($matches) {
		$O = getOjoo();
		return '<a href="' . $O->route->makeUrl($matches[1]) . '">' . $matches[2] . '</a>';
	}

	public function parseOjooCode($text) {
		$text = preg_replace('#\{\{color:(.+)\}\}(.+){{/color}}#i','<span style="color: $1">$2</span>', $text);
		$text = preg_replace('#\{\{bold\}\}(.+){{/bold}}#i','<strong>$1</strong>', $text);
		$text = preg_replace('#\{\{line\}\}#i','<br />', $text);
		$text = preg_replace('#\{\{lien:(.+)\}\}(.+){{/lien}}#i','<a href="$1">$2</a>', $text);
		$text = preg_replace('#\{\{italic\}\}(.+){{/italic}}#i','<i>$1</i>', $text);
		$text = preg_replace_callback('#\{\{route:(.+)\}\}(.+)\{\{/route\}\}#imU',array(get_class($this),'route'), $text);
		return $text;
	}
	
	public function parse() {
		$this->tampon = preg_replace_callback('#\{\{(.+).view.php\}\}#i',array(get_class($this),'parse_view'),$this->tampon);
		$this->tampon = preg_replace_callback('#\{\{(.+).tpl.php\}\}#i',array(get_class($this),'parse_tpl'),$this->tampon);
		$this->tampon = preg_replace_callback('#\{\{redirect:(.+):(.+):(.+)\}\}#i',array(get_class($this),'redirect'),$this->tampon);
		$this->tampon = preg_replace_callback('#\{\{url:(.+):(.+):(.+)\}\}#i',array(get_class($this),'url'),$this->tampon);
		$this->tampon = preg_replace_callback('#\{\{route:(.+)\}\}#i',array(get_class($this),'route'),$this->tampon);
		$this->tampon = preg_replace_callback('#\{\{account:(.+)\}\}#i',array(get_class($this),'account'),$this->tampon);
		$this->tampon = preg_replace('#\{\{(.+).css\}\}#i','<link rel="stylesheet" href="Web/Css/$1.css" type="text/css" media="screen">', $this->tampon);
		$this->tampon = preg_replace('#\{\{(.+).js\}\}#i','<script src="Web/Js/$1.js" type="text/javascript"></script>', $this->tampon);
		$this->tampon = preg_replace('#\{\{img:(.+)\.(.+)\}\}#i','<img src="Web/Images/$1.$2" alt="" class="img_align" title="" />', $this->tampon);
		$this->tampon = preg_replace('#\{\{checkbox check name="(.+)" content="(.+)"\}\}#i','<div class="ok_checkbox" name="$1" title="check" onClick="ojooPanel.checkbox(this);">$2</div><input type="checkbox" style="display: none;" name="$1" checked="checked"/>', $this->tampon);
		$this->tampon = preg_replace('#\{\{checkbox name="(.+)" content="(.+)"\}\}#i','<div class="checkbox" name="$1" onClick="ojooPanel.checkbox(this);">$2</div>', $this->tampon);
		
		$this->tampon = str_replace('{*TABLE_EDIT*}','<img src="Web/Images/Ojoo/Design/edit.png" />',$this->tampon);
		$this->tampon = str_replace('{*TABLE_DELETE*}','<img src="Web/Images/Ojoo/Design/delete.png" />',$this->tampon);
		$this->tampon = str_replace('{*TABLE_LOOK*}','<img src="Web/Images/Ojoo/Design/look.png" />',$this->tampon);
		$this->tampon = str_replace('{*TABLE_ADD*}','<img src="Web/Images/Ojoo/Design/add.png" />',$this->tampon);
	}
	
	public function redirect($matches) {
		header('Location: index.php?sub=' . $matches[1] . '&mod=' . $matches[2] . '&act=' . $matches[3]);
	}
	
	public function url($matches) {
		$url = 'index.php?sub=' . $matches[1] . '&mod=' . $matches[2] . '&act=' . $matches[3];
		return $url;
	}
	
	public function account($matches) {
		$O = getOjoo();
		$account = $O->modele->account->getAccountName($matches[1]);
		if ($account != false) {
			$url = '<a href="' . $O->route->makeUrl('editAccount',$matches[1]) . '">' . $account . '</a>';
			return $url;
		} else return "Compte inconnu";			
	}
	
	public function parse_view($matches) {
		$O = $GLOBALS['O'];
		ob_start();
			if (file_exists('Web/Sub/' . $O->active_sub . '/' . $O->active_mod . '/View/' . $matches[1] . '.view.php')) 
				include 'Web/Sub/' . $O->active_sub . '/' . $O->active_mod . '/View/' . $matches[1] . '.view.php';
			else echo '<div class="alert alert-error"> Le fichier demand� : ' . $matches[1] . '.view.php n\'a pu �tre trouv� � l\'adresse suivante : Web/Sub/' . $O->active_sub . '/' . $O->active_mod . '/View/' . $matches[1] . '.view.php</div>';
			$buffer = ob_get_contents();
		ob_end_clean();
		
		
		$buffer = preg_replace_callback('#\{\{(.+).view.php\}\}#i',array(get_class($this),'parse_view'),$buffer);
		$buffer = preg_replace_callback('#\{\{(.+).tpl.php\}\}#i',array(get_class($this),'parse'),$buffer);
		$buffer = preg_replace_callback('#\{\{redirect:(.+):(.+):(.+)\}\}#i',array(get_class($this),'redirect'),$buffer);
		$buffer = preg_replace_callback('#\{\{url:(.+):(.+):(.+)\}\}#i',array(get_class($this),'url'),$buffer);
		$buffer = preg_replace_callback('#\{\{route:(.+)\}\}#i',array(get_class($this),'route'),$buffer);
		$buffer = preg_replace_callback('#\{\{account:(.+)\}\}#i',array(get_class($this),'account'),$buffer);
		$buffer = preg_replace('#\{\{(.+).css\}\}#i','<link rel="stylesheet" href="Web/Css/$1.css" type="text/css" media="screen">', $buffer);
		$buffer = preg_replace('#\{\{(.+).js\}\}#i','<script src="Web/Js/$1.js" type="text/javascript"></script>', $buffer);
		$buffer = preg_replace('#\{\{img:(.+).(.+)\}\}#i','<img src="Web/Images/$1.$2" alt="" class="img_align" title="" />', $buffer);
		$buffer = preg_replace('#\{\{checkbox check name="(.+)" content="(.+)"\}\}#i','<div class="ok_checkbox" name="$1" title="check" onClick="ojooPanel.checkbox(this);">$2</div><input type="checkbox" style="display: none;" name="$1" checked="checked"/>', $buffer);
		$buffer = preg_replace('#\{\{checkbox name="(.+)" content="(.+)"\}\}#i','<div class="checkbox" name="$1" onClick="ojooPanel.checkbox(this);">$2</div>', $buffer);
		$buffer = str_replace('{*TABLE_EDIT*}','<img src="Web/Images/Ojoo/Design/edit.png" />',$buffer);
		$buffer = str_replace('{*TABLE_DELETE*}','<img src="Web/Images/Ojoo/Design/delete.png" />',$buffer);
		$buffer = str_replace('{*TABLE_LOOK*}','<img src="Web/Images/Ojoo/Design/look.png" />',$buffer);
		$buffer = str_replace('{*TABLE_ADD*}','<img src="Web/Images/Ojoo/Design/add.png" />',$buffer);
		return $buffer;
	}
	
	public function parse_tpl($matches) {
		$O = $GLOBALS['O'];
		ob_start();
			include 'Web/Template/' . $matches[1] . '.tpl.php';
			$buffer = ob_get_contents();
		ob_end_clean();
		
		
		$buffer = preg_replace_callback('#\{\{(.+).view.php\}\}#i',array(get_class($this),'parse_view'),$buffer);
		$buffer = preg_replace_callback('#\{\{(.+).tpl.php\}\}#i',array(get_class($this),'parse'),$buffer);
		$buffer = preg_replace_callback('#\{\{redirect:(.+):(.+):(.+)\}\}#i',array(get_class($this),'redirect'),$buffer);
		$buffer = preg_replace_callback('#\{\{url:(.+):(.+):(.+)\}\}#i',array(get_class($this),'url'),$buffer);
		$buffer = preg_replace_callback('#\{\{route:(.+)\}\}#i',array(get_class($this),'route'),$buffer);
		$buffer = preg_replace_callback('#\{\{account:(.+)\}\}#i',array(get_class($this),'account'),$buffer);
		$buffer = preg_replace('#\{\{(.+).css\}\}#i','<link rel="stylesheet" href="Web/Css/$1.css" type="text/css" media="screen">', $buffer);
		$buffer = preg_replace('#\{\{(.+).js\}\}#i','<script src="Web/Js/$1.js" type="text/javascript"></script>', $buffer);
		$buffer = preg_replace('#\{\{img:(.+).(.+)\}\}#i','<img src="Web/Images/$1.$2" alt="" class="img_align" title="" />', $buffer);
		$buffer = preg_replace('#\{\{checkbox check name="(.+)" content="(.+)"\}\}#i','<div class="ok_checkbox" name="$1" title="check" onClick="ojooPanel.checkbox(this);">$2</div><input type="checkbox" style="display: none;" name="$1" checked="checked"/>', $buffer);
		$buffer = preg_replace('#\{\{checkbox name="(.+)" content="(.+)"\}\}#i','<div class="checkbox" name="$1" onClick="ojooPanel.checkbox(this);">$2</div>', $buffer);
		$buffer = str_replace('{*TABLE_EDIT*}','<img src="Web/Images/Ojoo/Design/edit.png" />',$buffer);
		$buffer = str_replace('{*TABLE_DELETE*}','<img src="Web/Images/Ojoo/Design/delete.png" />',$buffer);
		$buffer = str_replace('{*TABLE_LOOK*}','<img src="Web/Images/Ojoo/Design/look.png" />',$buffer);
		$buffer = str_replace('{*TABLE_ADD*}','<img src="Web/Images/Ojoo/Design/add.png" />',$buffer);
		return $buffer;
	}
	
	public function display() {
		$O = $GLOBALS['O'];
		$this->is_display = true;
		$this->tampon = str_replace('{{ERRORS}}',$O->userError->getHtmlDisplay(),$this->tampon);
		if ($O->request->getMode() == 'ajax') {
			if ($O->config['Template']['utf8_decode_ajax']) 
				echo utf8_encode($this->tampon);
			else
				echo $this->tampon;
		} else {
			if ($O->config['Template']['utf8_decode_page']) 
				echo utf8_decode($this->tampon);
			else
				echo $this->tampon;
		}
	}
	
	public function inject($tpl) {
		ob_start();
			if (file_exists('Web/Template/' . $tpl)) 
				include 'Web/Template/' . $tpl;
			else echo '<div class="alert alert-error"> Le fichier demand� (' . $tpl . ') n\'a put �tre trouv� � l\'adresse suivante : Web/Template/' . $tpl . '</div>';
			$tampon = ob_get_contents();
		ob_end_clean();
		$this->tampon = str_replace("{{CONTENT}}",$tampon,$this->tampon);
		$this->parse();
	}

	public function get_tampon() {
      return $this->tampon;
    }

	public function set_tampon($tampon) {
      $this->tampon = $tampon;
      return $this;
    }

	public function get_active_tpl() {
      return $this->active_tpl;
    }

	public function set_active_tpl($active_tpl) {
      $this->active_tpl = $active_tpl;
      return $this;
    }
	
	public function is_display() {
		if ($this->is_display) return true;
		else 				   return false;
	}
}