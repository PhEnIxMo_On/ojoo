<?php
	class Notification {
		public $titre;
		public $contenu;
		public $secure;
		public $isRead;
		public $userId;
		public $id;
		public $date;

		public function getUserNotifications($id) {
			$O = $GLOBALS['O'];
			//return $O->bdd->cp->query("SELECT * FROM usernotifications WHERE userId='" . $id . "' AND isRead=0")->fetchAll();
		}

		public function getNotification($id) {
			$O = $GLOBALS['O'];
			return $O->bdd->cp->query("SELECT * FROM usernotifications WHERE id='" . $id . "'")->fetch();
		}

		public function addNotification($titre,$contenu,$userId,$secure = 0) {
			$O = $GLOBALS['O'];
			$contenu = str_replace("{{currentUser}}",'<a href="profil-' . $O->user->id . '">' . $O->user->userName . '</a>',$contenu);
			$contenu = preg_replace_callback('#\{\{user:(.+)\}\}#i',array(get_class($this),'parseUser'),$contenu);
			$contenu = preg_replace_callback('#\{\{equipe:(\d)\}\}#i',array(get_class($this),'parseEquipe'),$contenu);
			$contenu = preg_replace_callback('#\{\{projet:(\d)\}\}#i',array(get_class($this),'parseProjet'),$contenu);
			$O->bdd->cp->query("INSERT INTO usernotifications VALUES('','" . addslashes($titre) . "','" . addslashes($contenu) . "','" . $secure . "',0,'" . $userId . "','" . time() . "')");
		}

		public function setNotificationAsRead($id) {
			$O = $GLOBALS['O'];
			$O->bdd->cp->query("UPDATE usernotifications SET isRead=1 WHERE id='" . $id . "'");
		}

		public function parseUser($params) {
			$O = $GLOBALS['O'];
			$profil = '<a href="profil-' . $params[1] . '">' . $O->modele->cpuser->id_to_pseudo($params[1]) . '</a>';
			return $profil;
		}

		public function parseEquipe($params) {
			$O = $GLOBALS['O'];
			$equipe = $O->modele->equipe->select_id($params[1])->fetch();
			$equipeChaine = '<a href="equipe-' . $params[1] . '">' . $equipe['nom'] . '</a>';
			return $equipeChaine;			
		}

		public function parseProjet($params) {
			$O = $GLOBALS['O'];
			$equipe = $O->modele->projets->select_id($params[1])->fetch();
			$equipeChaine = '<a href="projet-' . $params[1] . '">' . $equipe['titre'] . '</a>';
			return $equipeChaine;			
		}
	}
?>
