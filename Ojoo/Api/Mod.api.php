<?php
class Mod {
	private $name;
	private $config;
	private $path;
	private $act;
	private $sub;
	
	public function __construct($path,$nom,$sub) {
		$this->path = $path;
		$this->name = $nom;
		$this->sub = $sub;
		$this->config = yaml_parse($this->path . '/' . $nom . '.conf.yml');
	}
	
	public function __get($nom) {
		if ($nom != 'name' && $nom != 'config' && $nom != 'act' && $nom != 'path') 
			if (isset ($this->act[$nom])) return $this->act[$nom];
		else {
			if (file_exists($this->path . 'Act/' . $nom)) {
				$this->act[$nom] = new Act($this->path . 'Act/' . $nom . '.php',$nom,$this->name);
			} else return null;
		}
	}
	
	
	
	public function get_name() {
      return $this->name;
    }

	public function set_name($name) {
      $this->name = $name;
      return $this;
    }

	public function get_config() {
      return $this->config;
    }

	public function set_config($config) {
      $this->config = $config;
      return $this;
    }

	public function get_path() {
      return $this->path;
    }

	public function set_path($path) {
      $this->path = $path;
      return $this;
    }

	public function get_act() {
      return $this->act;
    }

	public function set_act($act) {
      $this->act = $act;
      return $this;
    }

	public function get_sub() {
      return $this->sub;
    }

	public function set_sub($sub) {
      $this->sub = $sub;
      return $this;
    }
}