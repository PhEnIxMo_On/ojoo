<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Input {
    const INPUT_TEXT = 1;
    const INPUT_SELECT = 2;
    const INPUT_PASSWORD = 3;
    const INPUT_CHECKBOX = 4;
    const INPUT_RADIO = 5;
    const INPUT_TEXTAREA = 6;
    const INPUT_HIDDEN = 7;
    
    private $name;
    private $type;
    private $id;
    private $placeholder;
    private $autobound;
    private $value;
    private $options;
    private $label;
    private $icon;
    private $class;
    private $iconClass;
    private $labelClass;
    private $help;
    private $helpClass;
    private $disabled;
    private $checked;
    private $selected;
    private $cols;
    private $rows;
    private $style;
    private $validClass;
    private $errorClass;
    private $allowValidClass;
    private $index;
    private $allowErrorClass;
    private $arrayParams;
    
    private $contrainte;
    public $htmlData;
    /*
      *  public $nomChamp;
      *  public $type;
      *  public $required;
      *  public $regex = NULL;
      *  public $min;
      *  public $max;
      *  public $ban_char = NULL;
      *  public $same_as = null;
      *  public $not_in = null;
      *  public $ban_value = NULL;
      *  public $default_value = NULL;
      *  public $erreurMsg;
      * public $xmlError = NULL;
     */
    
    public function __construct($data) {
        /*$this->name = $name;
        $this->type = $type;
        $this->placeholder = $placeholder;
        $this->label = $label;
        $this->value = $value;
        $this->contrainte = $contrainte;
        $this->contrainte['nomChamp'] = $name;
        $this->id = $id;
        $this->autobound = $autobound;
        $this->options = $options;
        $this->icon = $icon;
        $this->class = $class;
        $this->iconClass = $iconClass;
        $this->labelClass = $labelClass;
        $this->help = $help;
        $this->helpClass = $helpClass;*/
        $this->autobound = true;
        foreach ($data as $key => $value) {
           $this->$key = $value;
        }
        $this->allowValidClass = false;
        $this->allowErrorClass = false;
        $this->arrayParams = $data;    
        $this->autoValue();
    }
    public function setClass($class) {
        $this->class = $class;
    }
    public function getArrayParams() {
        return $this->arrayParams;
    }

    public function setArrayParams($arrayParams) {
        $this->arrayParams = $arrayParams;
    }

    public function getIndex() {
        return $this->index;
    }

    public function setIndex($index) {
        $this->index = $index;
    }

    public function setIconClass($iconClass) {
        $this->iconClass = $iconClass;
    }

    public function setLabelClass($labelClass) {
        $this->labelClass = $labelClass;
    }

    public function setHelp($help) {
        $this->help = $help;
    }

    public function setHelpClass($helpClass) {
        $this->helpClass = $helpClass;
    }

    public function setDisabled($disabled) {
        $this->disabled = $disabled;
    }
    public function getValidClass() {
        return $this->validClass;
    }
    public function setValidClass($validClass) {
        $this->validClass = $validClass;
    }

    public function setErrorClass($errorClass) {
        $this->errorClass = $errorClass;
    }

    public function setAllowValidClass($allowValidClass) {
        $this->allowValidClass = $allowValidClass;
    }

    public function setAllowErrorClass($allowErrorClass) {
        $this->allowErrorClass = $allowErrorClass;
    }

        public function getErrorClass() {
        return $this->errorClass;
    }

    public function getAllowValidClass() {
        return $this->allowValidClass;
    }

    public function getAllowErrorClass() {
        return $this->allowErrorClass;
    }

        public function setChecked($checked) {
        $this->checked = $checked;
    }

    public function setSelected($selected) {
        $this->selected = $selected;
    }

    public function setCols($cols) {
        $this->cols = $cols;
    }

    public function setRows($rows) {
        $this->rows = $rows;
    }

    public function setStyle($style) {
        $this->style = $style;
    }

    public function setHtmlData($htmlData) {
        $this->htmlData = $htmlData;
    }

    public function getClass() {
        return $this->class;
    }

    public function getIconClass() {
        return $this->iconClass;
    }

    public function getLabelClass() {
        return $this->labelClass;
    }

    public function getHelp() {
        return $this->help;
    }

    public function getHelpClass() {
        return $this->helpClass;
    }

    public function getDisabled() {
        return $this->disabled;
    }

    public function getChecked() {
        return $this->checked;
    }

    public function getSelected() {
        return $this->selected;
    }

    public function getCols() {
        return $this->cols;
    }

    public function getRows() {
        return $this->rows;
    }

    public function getStyle() {
        return $this->style;
    }

    public function getHtmlData() {
        return $this->htmlData;
    }

    public function getRealType() {
        switch ($this->type) {
            case Input::INPUT_TEXT:
                return "text";
                break;
            
            case Input::INPUT_PASSWORD:
                return "password";
                break;
            
            case Input::INPUT_CHECKBOX:
                return "checkbox";
                break;
            
            case Input::INPUT_RADIO:
                return "radio";
                break;
            
            case Input::INPUT_SELECT:
                return "select";
                break;    
            
            case Input::INPUT_TEXTAREA:
                return "textarea";
                break;         
            
            case Input::INPUT_HIDDEN:
                return "hidden";
                break;
        }
    }
    
    public function getAsHtml() {
        $html = "";
        switch ($this->type) {
            
            case Input::INPUT_TEXT:
                $att = $this->getHtmlAttribute();
                $html = $att['icon'] . '<input ' . $att['name'] . ' ' . $att['type'] . ' ' . $att['class'] . ' ' . $att['id'] . ' ' . $att['autobound'] . ' ' . $att['placeholder'] . ' ' . $att['value'] . ' ' . $att['disable'] . ' ' . $att['style'] . ' ' . $att['validClass'] . ' ' . $att['errorClass'] . '/>';
                $this->htmlData['input'] = '<input ' . $att['name'] . ' ' . $att['type'] . ' ' . $att['class'] . ' ' . $att['id'] . ' ' . $att['autobound'] . ' ' . $att['placeholder'] . ' ' . $att['value'] . ' ' . $att['disable'] . ' ' . $att['style'] . ' ' . $att['validClass'] . ' ' . $att['errorClass'] . '/>';
                $this->htmlData['icon'] = $att['icon'];
                $this->htmlData['help'] = $att['help'];
                $this->htmlData['label'] = $att['label'];
                break;
            
            case Input::INPUT_HIDDEN:
                $att = $this->getHtmlAttribute();
                $html = $att['icon'] . '<input ' . $att['name'] . ' ' . $att['type'] . ' ' . $att['class'] . ' ' . $att['id'] . ' ' . $att['autobound'] . ' ' . $att['placeholder'] . ' ' . $att['value'] . ' ' . $att['disable'] . ' ' . $att['style'] . ' ' . $att['validClass'] . ' ' . $att['errorClass'] . '/>';
                $this->htmlData['input'] = '<input ' . $att['name'] . ' ' . $att['type'] . ' ' . $att['class'] . ' ' . $att['id'] . ' ' . $att['autobound'] . ' ' . $att['placeholder'] . ' ' . $att['value'] . ' ' . $att['disable'] . ' ' . $att['style'] . ' ' . $att['validClass'] . ' ' . $att['errorClass'] . '/>';
                $this->htmlData['icon'] = $att['icon'];
                $this->htmlData['help'] = $att['help'];
                $this->htmlData['label'] = $att['label'];
                break;
            
            case Input::INPUT_PASSWORD:
                $att = $this->getHtmlAttribute();
                $html = $att['icon'] . '<input ' . $att['name'] . ' ' . $att['type'] . ' ' . $att['class'] . ' ' . $att['id'] . ' ' . $att['autobound'] . ' ' . $att['placeholder'] . ' ' . $att['value'] . ' ' . $att['disable'] . ' ' . $att['style'] . ' ' . $att['validClass'] . ' ' . $att['errorClass'] . '/>';
                $this->htmlData['input'] = '<input ' . $att['name'] . ' ' . $att['type'] . ' ' . $att['class'] . ' ' . $att['id'] . ' ' . $att['autobound'] . ' ' . $att['placeholder'] . ' ' . $att['value'] . ' ' . $att['disable'] . ' ' . $att['style'] . ' ' . $att['validClass'] . ' ' . $att['errorClass'] . '/>';
                $this->htmlData['icon'] = $att['icon'];
                $this->htmlData['help'] = $att['help'];
                $this->htmlData['label'] = $att['label'];
                break;                

           case Input::INPUT_CHECKBOX:
                $att = $this->getHtmlAttribute();
                $html = $att['icon'] . '<input ' . $att['name'] . ' ' . $att['type'] . ' ' . $att['class'] . ' ' . $att['id'] . ' ' . $att['autobound'] . ' ' . $att['checked'] . ' ' . $att['disable'] . ' ' . $att['style'] . ' ' . $att['validClass'] . ' ' . $att['errorClass'] . '/>' . $att['label'];
                $this->htmlData['input'] = '<input ' . $att['name'] . ' ' . $att['type'] . ' ' . $att['class'] . ' ' . $att['id'] . ' ' . $att['autobound'] . ' ' . $att['checked'] . ' ' . $att['disable'] . ' ' . $att['style'] . ' ' . $att['validClass'] . ' ' . $att['errorClass'] . '/>';
                $this->htmlData['icon'] = $att['icon'];
                $this->htmlData['help'] = $att['help'];
                $this->htmlData['label'] = $att['label'];
                break;    
            
           case Input::INPUT_RADIO:
                $att = $this->getHtmlAttribute();
                $html = $att['icon'] . '<input ' . $att['name'] . ' ' . $att['type'] . ' ' . $att['class'] . ' ' . $att['id'] . ' ' . $att['autobound'] . ' ' . $att['checked'] . ' ' . $att['disable'] . ' ' . $att['style'] . ' ' . $att['validClass'] . ' ' . $att['errorClass'] . '/>' . $att['label'];
                $this->htmlData['input'] = '<input ' . $att['name'] . ' ' . $att['type'] . ' ' . $att['class'] . ' ' . $att['id'] . ' ' . $att['autobound'] . ' ' . $att['checked'] . ' ' . $att['disable'] . ' ' . $att['style'] . ' ' . $att['validClass'] . ' ' . $att['errorClass'] . '/>';
                $this->htmlData['icon'] = $att['icon'];
                $this->htmlData['help'] = $att['help'];
                $this->htmlData['label'] = $att['label'];
                break;      
            
           case Input::INPUT_TEXTAREA:
                $att = $this->getHtmlAttribute();
                $html = '<textarea ' . $att['name'] . ' ' . $att['class'] . ' ' . $att['id'] . ' ' . $att['checked'] . ' ' . $att['disable'] . ' ' . $att['placeholder'] . ' ' . $att['cols'] . ' ' . $att['rows'] . ' ' . $att['style'] . ' ' . $att['validClass'] . ' ' . $att['errorClass'] . '>' . $att['rawValue'] . '</textarea>';
                $this->htmlData['input'] = '<textarea ' . $att['name'] . ' ' . $att['class'] . ' ' . $att['id'] . ' ' . $att['checked'] . ' ' . $att['disable'] . ' ' . $att['placeholder'] . ' ' . $att['style'] . ' ' . $att['validClass'] . ' ' . $att['errorClass'] . '>' . $att['rawValue'] . '</textarea>';
                $this->htmlData['icon'] = $att['icon'];
                $this->htmlData['help'] = $att['help'];
                $this->htmlData['label'] = $att['label'];
                break;            
            
            case Input::INPUT_SELECT:
                $att = $this->getHtmlAttribute();
                $html = "<select " . $att['name'] . " " . $att['id'] . " " . $att['class'] . " " . $att['validClass'] . " " . $att['errorClass'] . ">";
                $select = '';
				if (!empty($this->options)) {
	                foreach ($this->options as $key => $option) {
	                    $select = "";
	                    if (!empty($this->value)) {
	                        //echo $this->value . "=>" . $key . "<br />";
	                        if ($this->value == $key) {    
	                            //echo "value => " . $this->name . "<br />";
	                            //echo "Soit : " . $this->value . " => " . $key . "<br />";
	                            $select = 'selected="selected"';
	                        }
	                    } else {
	                        if (isset ($_POST[$this->name])) {
	                            if ($_POST[$this->name] == $key) $select = 'selected="selected"';
	                            else                             $select = '';
	                        }
	                    }
	                    $html .= '<option value="' . $key . '" ' . $select . '>' . $option . '</option>';
	                }
				}
                $html .= "</select>";
                $this->htmlData['input'] = $html;
                $html = $att['icon'] . $html;
                $this->htmlData['icon'] = $att['icon'];
                $this->htmlData['help'] = $att['help'];
                $this->htmlData['label'] = $att['label'];
                break;                     
                
            default:
                throw new Exception("Votre formulaire est invalide.");
        }
        return $html;
    }
    
    public function autoValue() {
        $name = str_replace('[]','',$this->name);
        if (isset ($_POST[$name])) {
            if (is_array($_POST[$name])) {
                if (isset ($_POST[$name][$this->index])) $this->value = $_POST[$name][$this->index];
            } else {
                $this->value = $_POST[$name];
            }
        }
    }
    
    public function getHtmlAttribute() {
        $O = getOjoo();
        $attribute = array();
        $attribute['name'] = ($this->name != null) ? 'name="' . $this->name . '"' : '';
        $attribute['type'] = ($this->type != null) ? 'type="' . $this->getRealType() . '"' : '';
        $attribute['id'] = ($this->id != null) ? 'id="' . $this->id . '"' : '';
        $attribute['autobound'] = ($this->autobound != null) ? 'value="<?php if (isset ($_POST[\'' . $this->name . '\'])) echo $_POST[\'' . $this->name . '\']; ?>"' : ''; 
        $rawValue = "";
        if ($this->autobound != null && $this->autobound) {            
            if (isset ($_POST[$this->name])) { $value = 'value="' . $_POST[$this->name]. '"'; $rawValue = $_POST[$this->name];  }
            else                               $value = "";
            
            $testTable = str_replace('[]','',$this->name);
            if (isset ($_POST[$testTable])) {
                if (is_array($_POST[$testTable])) {
                    if (isset ($_POST[$testTable][$this->index])) { $value = 'value="' . $_POST[$testTable][$this->index]. '"'; $rawValue = $_POST[$testTable][$this->index]; $O->console->debug("CHAMP TABLE " . $testTable . "[" . $this->index . "] " . $value,"[FORM/AUTOBOUND]"); }
                    else                                          $value = '';
                }
            }
            
        } else $value = "";
        
        $attribute['autobound'] = $value;
        
        if (!empty ($this->value)) {
            $rawValue = $this->value;
            $attribute['autobound'] = null;
        }
        $attribute['labelClass'] = ($this->labelClass != null) ? 'class="' . $this->labelClass . '"' : 'ojooLabel';
        $attribute['iconClass'] = ($this->iconClass != null) ? $this->iconClass : 'ojooIconClass';
        $attribute['helpClass'] = ($this->helpClass != null) ? $this->helpClass : 'ojooHelpClass';
        $attribute['validClass'] = ($this->validClass != null && $this->allowValidClass) ? 'class="' . $this->validClass . '"' : '';
        $attribute['errorClass'] = ($this->errorClass != null && $this->allowErrorClass) ? 'class="' . $this->errorClass . '"' : '';
        $attribute['icon'] = ($this->icon != null) ? '<img src="' . $this->icon . '" class="' . $this->iconClass . '" alt="Icon" />': '';
        $attribute['label'] = ($this->label != null) ? '<span class="' . $attribute['labelClass'] . '">' . $this->label . '</span>' : '';
        $attribute['help'] = ($this->help != null) ? '<span class="' . $attribute['helpClass'] . '">' . $this->help . '</span>' : '';
        $attribute['value'] = ($this->value != null) ? 'value="' . $this->value . '"' : '';
        $attribute['rawValue'] = $rawValue;
        $attribute['placeholder'] = ($this->placeholder != null) ? 'placeholder="' . $this->placeholder . '"' : '';
        $attribute['class'] = ($this->class != null) ? 'class="' . $this->class . '"' : '';
        $attribute['disable'] = ($this->disabled != null) ? 'disabled="disabled"' : '';
        $attribute['checked'] = ($this->checked != null) ? 'checked="checked"' : '';
        $attribute['selected'] = ($this->selected != null) ? 'selected="selected"' : '';
        $attribute['cols'] = ($this->cols != null) ? 'cols="' . $this->cols . '"' : '';
        $attribute['rows'] = ($this->rows != null) ? 'rows="' . $this->rows . '"' : '';
        $attribute['style'] = ($this->rows != null) ? 'style="' . $this->style . '"' : '';
        return $attribute;
    }
    
    public function getName() {
        return $this->name;
    }

    public function getType() {
        return $this->type;
    }

    public function getId() {
        return $this->id;
    }

    public function getPlaceholder() {
        return $this->placeholder;
    }

    public function getAutobound() {
        return $this->autobound;
    }

    public function getValue() {
        return $this->value;
    }

    public function getOptions() {
        return $this->options;
    }

    public function getLabel() {
        return $this->label;
    }

    public function getIcon() {
        return $this->icon;
    }

    public function getContrainte() {
        return $this->contrainte;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setPlaceholder($placeholder) {
        $this->placeholder = $placeholder;
    }

    public function setAutobound($autobound) {
        $this->autobound = $autobound;
    }

    public function setValue($value) {
        $this->value = $value;
    }

    public function setOptions($options) {
        $this->options = $options;
    }

    public function setLabel($label) {
        $this->label = $label;
    }

    public function setIcon($icon) {
        $this->icon = $icon;
    }

    public function setContrainte($contrainte) {
        $this->contrainte = $contrainte;
    }

    public function __destruct() {
        unset($_SESSION['indexTable']);
    }
    
}
?>
