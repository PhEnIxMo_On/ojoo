<?php
	class PharManager {
		private $phar;
		private $sub;
		private $mod;
		
		public function compressMod($sub,$mod) {
			if (is_dir('Web/Sub/' . $sub)) {
				$this->phar = new PHAR('Web/Sub/' . $sub . '/' . $mod . '.phar.php');	
				$this->phar->buildFromDirectory('Web/Sub/' . $sub . '/' . $mod);
				$this->phar->stopBuffering();
			} else {
				$O = getOjoo();
				if ($O->config["Debug"]["Phar"])
					$O->console->debug("Le Sub : " . $sub . "n'existe pas.","[PHAR]");
			}
			
		}
		
	}
?>