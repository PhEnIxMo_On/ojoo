<?php
	class Filter {
		private $byColumn = array("nomChamp" => "","type" => "LIKE","value" => "");
		private $order = array("nomChamp" => "","ORDER" => "DESC");
                private $type;
		
		public function __construct($byColumn = NULL,$order = NULL,$type = "AND") {
			$this->byColumn = $byColumn;
			$this->order = $order;
                        $this->type = $type;
		}

		public function getByColumn() {
			return $this->byColumn;
		}
		
		public function getOrder() {
			return $this->order;
		}
                
                public function getType() {
                    return $this->type;
                }

	}
?>