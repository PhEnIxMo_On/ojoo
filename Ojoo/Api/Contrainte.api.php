<?php
	class Contrainte {
        const TEXT = 1;
        const INT = 2;
        const STRING = 3;
        const VARCHAR = 4;
        const URL = 5;
        const EMAIL = 6;
        const PASSWORD = 7;
                
            
		public $nomChamp;
		public $type;
		public $required = NULL;
		public $regex = NULL;
		public $min;
		public $max;
        public $ban_char = NULL;
		public $same_as = null;
		public $not_in = null;
        public $ban_value = NULL;
   	 	public $default_value = NULL;
		public $erreurMsg;
		public $xmlError = NULL;
        public $form;
        public $state;
        public $index;
		
		public function Contrainte($contraintes) {
			foreach ($contraintes as $key => $contrainte) {
				$this->$key = $contrainte;                                
			}
                        $this->state = true;
		}
                
                
        static public function getC($name,$erreur,$type,$form = null,$required = true,$options = null) {
           $contrainte =  array(
              "nomChamp" => $name,
              "erreurMsg" => $erreur,
              "required" => $required,
              "form" => $form
            );
           
           switch ($type) {
               case Contrainte::TEXT:
                   $contrainte["type"] = 'string';
                   $contrainte["min"] = 3;                           
                   break;
               
               case Contrainte::INT:
                   $contrainte["type"] = 'int';                           
                   break;
               
               case Contrainte::STRING:
                   $contrainte["type"] = 'string';
                   $contrainte["min"] = 3;  
                   $contrainte["max"] = 255;
                   break;
               
               case Contrainte::VARCHAR:
                   $contrainte["min"] = 3;  
                   $contrainte["max"] = 255;                           
                   break;
               
               case Contrainte::URL:
                   $contrainte["regex"] = '|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i';
                   break;
           }
           if ($options != null) $contrainte = array_merge($contrainte,$options);
           
           return $contrainte;
        }
	}
?>