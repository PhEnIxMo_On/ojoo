<?php
class Control {
	public function loadPage($sub,$mod,$act) {
		$O = $GLOBALS['O'];
		if (file_exists('Web/Sub/' . $sub . '/' . $mod . '/Act/' . $act . '.php')) {
			
			$O->sub->set_sub($sub);
			if (isset ( $O->sub->config['rang']['global']))
				$droit_sub = Rights::format($O->sub->config['rang']['global']);
			else
				$droit_sub = 'none';
				
			
			if (isset($O->sub->config['rang'][$mod]['global']))
				$droit_mod = Rights::format($O->sub->config['rang'][$mod]['global']);
			else
				$droit_mod = 'none';
			
			if (isset( $O->sub->config['rang'][$mod][$act]))
				$droit_act = Rights::format($O->sub->config['rang'][$mod][$act]);
			else 
				$droit_act = 'none';
		
			if (is_int($droit_act)) {
                if ($_SESSION['Rang'] & $droit_act) {                	
					$O->console->debug(Console::CONTROL,"User right OK with act right. (" . $_SESSION['Rang'] . " & " . $droit_act . ")",__FILE__,"loadPage(\$sub,\$mod,\$act)",get_defined_vars());
					return true;
				} else {					
					$O->console->debug(Console::CONTROL,"User right NOK with act right. (" . $_SESSION['Rang'] . " & " . $droit_act . ")",__FILE__,"loadPage(\$sub,\$mod,\$act)",get_defined_vars());
					return false;
				}
			}
			if (is_int($droit_mod)) {
	            if ($_SESSION['Rang'] & $droit_mod) {	            
					$O->console->debug(Console::CONTROL,"User right OK with mod right. (" . $_SESSION['Rang'] . " & " . $droit_mod . ")",__FILE__,"loadPage(\$sub,\$mod,\$act)",get_defined_vars());
					return true;
	            } else {
					$O->console->debug(Console::CONTROL,"User right NOK with mod right. (" . $_SESSION['Rang'] . " & " . $droit_mod . ")",__FILE__,"loadPage(\$sub,\$mod,\$act)",get_defined_vars());
					return false;
	            }
			}
			if (is_int($droit_sub)) {
                if ($_SESSION['Rang'] & $droit_sub) {
				    $O->console->debug(Console::CONTROL,"User right OK with sub right. (" . $_SESSION['Rang'] . " & " . $droit_sub . ")",__FILE__,"loadPage(\$sub,\$mod,\$act)",get_defined_vars());
                	return true;
                }
			}
			$O->console->debug(Console::CONTROL,"No rights found",__FILE__,"loadPage(\$sub,\$mod,\$act)",get_defined_vars());
            return false;
		} else {
			$O->userError->loadXml(2);
		}
	}
}
?>