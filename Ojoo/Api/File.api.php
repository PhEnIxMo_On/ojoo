<?php
class File {
	private $name;
	private $droits;
	private $path;
	private $content;
	private $extension;
	private $handle;
	private $mode;
	private $size;
	
	
	
	public function __construct($path,$mode = 'r') {
		if ($path != null) {
			$O = $GLOBALS['O'];
			$test = false;
			if (file_exists($path)) $test = true;
			else {
				if ($mode == 'a+') $test = true;
			}
			if ($test) {
				$name = explode('.',$path);
				$nb = count($name);
				$name2 = explode('/',$path);
				$total = count($name2);
				$this->name = $name2[$total - 1];
				$this->extension = $name[$nb - 1];
				$this->droits = null;
				$this->mode = $mode;
				$this->path = $path;
				$this->handle = fopen($path,$this->mode);
				$this->size = filesize($path);
				if ($this->size > 0)
					$this->content = fread($this->handle,$this->size);
			} else {
				$O->erreur->set_erreur("#O1");
				$O->erreur->display();
			}
		}
	}
	
	public function delete($path) {	
		unlink($path);
	}
	
	public function set_file($path,$mode = 'r') {
		$this->__construct($path,$mode);
	}
	
	public function chmod($droit) {
		chmod($this->path,$droit);
	}
	
	public function write($content) {
		fwrite($this->handle,$content);
	}
	
	public function close() {
		fclose($this->handle);
	}

	public function get_name() {
      return $this->name;
    }

	public function set_name($name) {
      $this->name = $name;
      return $this;
    }

	public function get_droits() {
      return $this->droits;
    }

	public function set_droits($droits) {
      $this->droits = $droits;
      return $this;
    }

	public function get_path() {
      return $this->path;
    }

	public function set_path($path) {
      $this->path = $path;
      return $this;
    }

	public function get_content() {
      return $this->content;
    }

	public function set_content($content) {
      $this->content = $content;
      return $this;
    }

	public function get_extension() {
      return $this->extension;
    }

	public function set_extension($extension) {
      $this->extension = $extension;
      return $this;
    }

	public function get_handle() {
      return $this->handle;
    }

	public function set_handle($handle) {
      $this->handle = $handle;
      return $this;
    }

	public function get_mode() {
      return $this->mode;
    }

	public function set_mode($mode) {
      $this->mode = $mode;
      return $this;
    }

	public function get_size() {
      return $this->size;
    }

	public function set_size($size) {
      $this->size = $size;
      return $this;
    }
}