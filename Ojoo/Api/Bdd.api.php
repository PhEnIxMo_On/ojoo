<?php
class Bdd {
	private $instances = array();
	public function connect($nom = 'none',$connexion = null) {
		if ($connexion != null) {
			$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$DB = new DbInstance($nom,$connexion);
			$this->instances[(string) $DB->_nom] = $DB;
		} else {
			$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$DB = new DbInstance($nom);
			$this->instances[(string) $DB->_nom] = $DB;
		}
	}
	public function Bdd() {
	}
	public function __get($nom) {
		if(isset($this->instances[$nom])) // Si la connexion existe
			return $this->instances[$nom];
		else {
			
			$O = $GLOBALS['O'];
			$config = $O->config;
			if (isset ($config['Database'][$nom])) {
				$connexion = array();
				$connexion['string'] = $config['Database'][$nom]['pilote'];
				$connexion['user'] = $config['Database'][$nom]['user'];
				$connexion['pass'] = $config['Database'][$nom]['pass'];
				$DB = new DbInstance($nom,$connexion);
				$this->instances[(string) $DB->_nom] = $DB;
				return $this->instances[$nom];
			}
		}
	}
}
?>