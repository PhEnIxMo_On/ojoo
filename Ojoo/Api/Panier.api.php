<?php
	class Panier {
		private $articles;
		
		public function __construct() {
			if (isset ($_SESSION['panier'])) {
				$panier = unserialize($_SESSION['panier']);
				$this->articles = $panier->articles;
			}
			else {
				$this->articles = array();
			}
		}
		
		public function add($id,$qte = 1,$type = 1) {
			if (!isset ($this->articles[$id . '|' . $type])) {
				$this->articles[$id . '|' . $type] = new Article($id,$qte,$type);
			} else {
				if (!$this->articles[$id . '|' . $type]->isSpecial())
					$this->articles[$id . '|' . $type]->addQte($qte);
				else 
					return false;
			}
			$O = getOjoo();
			$O->modele->web_boutique_panier->MAJPanier();
			return true;
		}
		
		public function getArticle($id,$type = 1) {
			if (isset ($this->articles[$id . '|' . $type]))
				return $this->articles[$id . '|' . $type];
			else
				return false;
		}
		
		public function getArticles() {
			return $this->articles;
		}
		
		public function delete($id,$type) {
			unset($this->articles[$id . '|' . $type]);
			$O = getOjoo();
			$O->modele->web_boutique_panier->MAJPanier();			
		}
		
		public function setQte($id,$qte,$type = 1) {
			if (!isset ($this->articles[$id . '|' . $type]))
				$this->articles[$id . '|' . $type] = new Article($id,$qte);
			else {
				$this->articles[$id . '|' . $type]->setQte($qte);
			}			
			$O = getOjoo();
			$O->modele->web_boutique_panier->MAJPanier();			
		}
		
		public function __destruct() {
			//unset($_SESSION['panier']);
			$_SESSION['panier'] = serialize($this);
		}
		
	}
?>