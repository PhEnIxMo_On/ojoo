<?php	
	/***************************************************
	* Si vous souhaitez ajouter une execution ( ou un �v�nement )sur un �v�nement quelconque : 
	* Allez dans le fichier Event.conf.yml � la racine du dossier Ojoo.
	****************************************************/
	
	class Event {
		private $event = array();
		private $execute = array();
		private $params = array();
		
		public function Event() {
			// On charge les events et execution de ceux-ci :
			$events = yaml_parse('Ojoo/Event.conf.yml');
			foreach ($events as $key => $event) {
				$this->event[$key] = false;
				$this->execute[$key] = $event;
			}
		}
		
		public function trigEvent($search) {
			$O = $GLOBALS['O'];
			if (is_int($search))
				$reponseEvent = $O->modele->event_template->select_id($search);
			else
			$reponseEvent = $O->modele->event_template->select_nom($search);
			$donneesEvent = $reponseEvent->fetch();
			if ($donneesEvent != null) {
				$reponseEvent = $O->modele->onevent->select_event_id($donneesEvent['id']);
				// $O->modele->event_template->set_is_trig(true,$donneesEvent['id']);
				while ($donnees = $reponseEvent->fetch()) {
					if ($donnees['script_path'] != null) {
						if (file_exists($donnees['script_path']))
							require_once $donnees['script_path'];
						else throw new FileNotFoundException();
					}
					else {
						$test = false;
						if ($donnees['script_php'] != null) {
							$test = true;
							eval($donnees['script_php'] . ';');
						}
						if ($donnees['script_js_html'] != null) {
							$test = true;
							?>
								<?php echo $donnees['script_js_html']; ?>
							<?php
						}
						if (!$test) throw new WrongEventException();
					}
				}
			} else throw new EventNotFoundException();
		}
	
		
		public function return_param($matches) {
			if (isset ($this->params["PARAM" . $matches[1]])) {
				return "'" . addslashes($this->params["PARAM" . $matches[1]]) . "'";
			}
			else {
				$O = new Ojoo();
				$O->erreur->set_erreur("#O4",34,'Event.api.php');
				$O->erreur->display();
			}
		}
		
	}
?>