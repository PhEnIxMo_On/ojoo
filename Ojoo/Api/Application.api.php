<?php
	class Application {
		private $code = APP_WAIT;
		private $name;
		private $sessionId;
		private $appId;
		private $sub;
		private $mod;
		private $act;
		private $stop = false;
		private $errors = array();
		private $exceptions = array();
		private $userError = array();
		private $stopContent = false;

		public function __construct($name = "Ojoo Application",$code = APP_WAIT) {
			$this->name = $name;
			$this->code = $code;
			$this->sessionId = session_id();
			$this->appId = uniqid();
		}

		public function setPage($array) {
			$this->sub = $page["sub"];
			$this->mod = $page["mod"];
			$this->act = $page["act"];
		}

		public function getStateName($code) {
			switch($code) {
				case -1:
					return "APP_WAIT";
				break;

				case 0:
					return "APP_START";
				break;

				case 1:
					return "APP_PROCESS";
				break;

				case 2:
					return "APP_ERROR_NOTICE";
				break;

				case 3:
					return "APP_ERROR_STOP";
				break;

				case 4:
					return "APP_ERROR_CRITICAL";
				break;

				case 5:
					return "APP_ERROR_UNKNOW";
				break;

				case 6:
					return "APP_NOT_FOUND";
				break;

				case 7:
					return "APP_NOT_ALLOWED";
				break;

				case 8:
					return "APP_END";
				break;

			}
		}

		public function startApp() {
			$O = getOjoo();
			$this->returnCode(APP_PROCESS);
			if ($O->request->getMode() == 'ajax') {
				// Traitement pour une requête Ajax
				$O->sub->set_sub($O->route->get_sub());
				$O->loadPage_ajax($O->route->get_sub(),$O->route->get_mod(),$O->route->get_act());
			} else {
				// Sinon fonctionnement normal : 
				$O->sub->set_sub($O->route->get_sub());
				$O->loadPage($O->route->get_sub(),$O->route->get_mod(),$O->route->get_act());
			}
			if (!$O->template->is_display() && !$this->stop) 
				$O->template->display();
		}

		public function returnCode($code) {
			$O = $GLOBALS['O'];
			if ($this->code != APP_ERROR_STOP && $this->code != APP_ERROR_CRITICAL && $this->code != APP_END) {
				if ($O->config['Debug']['Application'])
					$O->console->debug(Console::APP,"Application state set to " . $this->getStateName($code),__FILE__,'returnCode(\$code)',get_defined_vars());
				switch ($code) {
					case -1:
						throw new AppException($this->appId,"The mode APP_WAIT is used only by Ojoo for give you information about the application state.",11,$this->code);
					break;

					case 0:
						if ($this->code == APP_WAIT) {
							$this->code = APP_START;
							$this->startApp();
						} else throw new AppException($this->appId,"You've tried to run an application who was already in process",10,$this->code);
					break;

					case 1:
						if ($this->code == APP_START) $this->code = APP_PROCESS;
						else
							throw new AppException($this->appId,"The mode APP_PROCESS is used only by Ojoo for giving you information about the application state.",11,$this->code);
					break;

					case 2:
						$this->code = APP_ERROR_NOTICE;
					break;

					case 3:
						$this->code = APP_ERROR_STOP;
						$this->stop = TRUE;
						$this->appErrorStop();
						exit();
					break;

					case 5:
						$this->code = APP_ERROR_UNKNOW;
						$this->stop = TRUE;
						exit("Ojoo Application have encoutered an unknow error.");
					break;

					case 4:
						$this->code = APP_ERROR_CRITICAL;
						$this->stop = TRUE;
						exit($O->config["Erreur"]["CriticalMessage"]);
					break;

					case 6:
						$this->code = APP_NOT_FOUND;
						$this->appNotFound();
						$this->stopContent = true;				
					break;

					case 7:
						$this->code = APP_NOT_ALLOWED;
						$this->appNotAllowed();
						$this->stopContent = true;
					break;

					case 8:
						$this->stop = TRUE;
						if (empty($this->errors) && empty($this->userError) && empty($this->exceptions)) {
							if ($O->config['Debug']['Application'])
								$O->console->debug(Console::APP,"Application end without any errors. Congratulations !",__FILE__,'returnCode(\$code)',get_defined_vars());
						} else {
							if ($O->config['Debug']['Application'])
								$O->console->debug(Console::APP,"The application have encoutered somes errors who haven't stop her. You should check the logs ;)",__FILE__,'returnCode(\$code)',get_defined_vars());
						}
					break;

					case 9:
						$this->stop = TRUE;
						$this->appException();
					break;
				}
			}

		}

		public function appException() {
			if (!$this->stopContent) {
				$O = getOjoo();
				if ($O->request->getMode() == 'normal' && !$O->template->is_display) {	
					$O->template->set_tampon('');
					$O->template->load_template($O->config['Erreur']['404']);
					$O->template->tampon = str_replace('{{CONTENT}}','',$O->template->tampon);
					$O->template->display();
				} else {
					echo $O->userError->getHtmlDisplay();
				}
			}
		}

		public function appErrorStop() {
			$O = getOjoo();
			if ($O->request->getMode() == 'normal' && !$O->template->is_display) {
				$O->template->set_tampon('');
				$O->template->load_template($O->config['Erreur']['404']);
				$O->userError->loadXml(5);
				$O->template->tampon = str_replace('{{CONTENT}}','',$O->template->tampon);
				$O->template->display();
				$O->console->display();
			} else {
				$O->userError->loadXml(5);
				echo $O->userError->getHtmlDisplay();
			}

		}

		public function appNotAllowed() {
			if (!$this->stopContent) {
				$O = getOjoo();
				$O->route->setPageTitle($O->config['Erreur']['pageTitle403']);
				$O->route->setPageSubTitle($O->config['Erreur']['pageSubTitle403']);			
				if ($O->request->getMode() == 'normal') {
					$O->template->set_tampon('');
					$O->template->load_template($O->config['Erreur']['403']);
					$O->userError->loadXml(3);
					$O->template->set_tampon(str_replace('{{CONTENT}}',$O->userError->getHtmlDisplay(),$O->template->get_tampon()));
					$O->template->display();
					//$O->console->display();
				} else {
					$O->userError->loadXml(3);
					echo $O->userError->getHtmlDisplay();
				}
			}
		}

		public function appNotFound() {
			$O = getOjoo();
			$O->route->setPageTitle($O->config['Erreur']['pageTitle404']);
			$O->route->setPageSubTitle($O->config['Erreur']['pageSubTitle404']);
			$O->template->load_template($O->config['Erreur']['404']);
			$O->userError->loadXml('error404');
			$O->template->set_tampon(str_replace('{{CONTENT}}',$O->userError->getHtmlDisplay(),$O->template->get_tampon()));
			$O->template->display();
		}

		public function getAppId() {
			return $this->appId;
		}

		public function getName() {
			return $this->name;
		}

		public function addPhpError($error) {
			if (!$this->stop) {
				$O = getOjoo();
				$O->console->add(new ConsoleMessage("App",8,"PHP error have been added : " . $error->getErrorLevel() . "[" . $error->getMessage() . "]"));
				if (is_object($error->getLog())) $O->console->add(new ConsoleMessage("App",7,"PHP error have been log in " . $error->getLog()->getLogName()));
				   if (!(error_reporting() & $error->getErrorLevel())) {
						// Ce code d'erreur n'est pas inclus dans error_reporting()
						return;
					}
					switch ($error->getErrorLevel()) {
						case E_ERROR:
							$this->returnCode(APP_ERROR_CRITICAL);
						break;

						case E_WARNING:
							$this->returnCode(APP_ERROR_STOP);
						break;

						case E_NOTICE:
							$this->returnCode(APP_ERROR_NOTICE);
						break;

						default:
							$this->returnCode(APP_ERROR_UNKNOW);
						break;
					}

					$this->errors[] = $error;	
			}
		}

		public function addUserError($error) {
			if (!$this->stop) {
				$O = getOjoo();
				$O->console->debug(Console::APP,"User error have been added : " . $error->getTitle() . "[" . $error->getCode() . "] => level : " . $error->getLevel(),__FILE__,'returnCode(\$code)',get_defined_vars());
				if (is_object($error->getLog())) $O->console->add(new ConsoleMessage("App",7,"User error have been log in " . $error->getLog()->getLogName()));
				switch ($error->getLevel()) {
					case 'low':
						$this->returnCode(APP_ERROR_NOTICE);
					break;

					case 'mid':
						$this->returnCode(APP_ERROR_STOP);
					break;

					case 'hight':
						$this->returnCode(APP_ERROR_CRITICAL);
					break;

					case 'exception':
						$this->returnCode(APP_EXCEPTION);
					break;

					default:
						$this->returnCode(APP_ERROR_UNKNOW);
					break;
				}
				$this->userError[] = $error;	
			}
		}

	}
?>