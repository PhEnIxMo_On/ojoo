<?php
class OxyNotification {
	const FLAG_SECURE = 0x001;
	const FLAG_GM = 0x002;
	const FLAG_NORMAL = 0x004;
	const FLAG_OJOO = 0x008; // Notification venant du site quoi
	const FLAG_MULTIPLE_RESPONSE = 0x010;
	const FLAG_NO_DELETE = 0x020;
	const FLAG_SAVE_DB = 0x040;
	const FLAG_SAVE_FILE = 0x080;
	const FLAG_IS_READ = 0x100;
	
	const TYPE_QUESTION = 0x1;
	const TYPE_AFFIRMATION = 0x2;
	const TYPE_SONDAGE = 0x4;
	const TYPE_NORMAL = 0x8;
	
	
	
	private $id;
	private $sender;
	private $receiver;
	private $flags;
	private $title;
	private $content;
	private $type;
	private $idQuestion;

	public function __construct($id = null,$title = null) {
		$isConstruct = false;
		$O = getOjoo();
		if($id != null) {
			$not = $O->modele->notifications->select_id($id)->fetch();
			if ($not != null) {
				$this->id = $not['id'];
				$this->sender = $not['sender'];
				$this->receiver = $not['receiver'];
				$this->flags = $not['flags'];
				$this->title = $not['title'];
				$this->content = $not['content'];
				$this->type = $not['type'];
				$this->idQuestion = $not['idQuestion'];
				$isConstruct = true;
			}
		}
		if ($title != null && !$isConstruct) {
			$not = $O->modele->notifications->selec_title($title)->fetch();
			if ($not != null) {
				$this->id = $not['id'];
				$this->sender = $not['sender'];
				$this->receiver = $not['receiver'];
				$this->flags = $not['flags'];
				$this->title = $not['title'];
				$this->content = $not['content'];
				$this->type = $not['type'];
				$this->idQuestion = $not['idQuestion'];
				$isConstruct = true;
			}		
		}
		if (isset ($_POST['NOTIFICATION_RESPONSE'])) {
			if ($this->hasType(self::TYPE_QUESTION) && $this->idQuestion != null) {
				if (isset ($_POST['RESPONSE_NOT'])) {
					$reponse = strip_tags(htmlentities(addslashes($_POST['RESPONSE_NOT'])));
					$O = getOjoo();
					if ($this->hasFlag(self::FLAG_MULTIPLE_RESPONSE)) 
						$test = true;						
					else
						$test = $O->modele->notifications_response->testQuestion($this->idQuestion,$O->wowUser->getAccountId());
					
					if ($test) {
						$O->modele->notifications_response->idQuestion = $this->idQuestion;
						$O->modele->notifications_response->responseText = $reponse;
						$O->modele->notifications_response->responeInt1 = null;
						$O->modele->notifications_response->accountId = $O->wowUser->getAccountId();
						$O->modele->notifications_response->ip = $_SERVER['REMOTE_ADDR'];
						$O->modele->notifications_response->ADD();					
						if ($this->hasType(self::TYPE_QUESTION)) {
							$O->modele->notifications->setAsRead($this->id);
							$this->setFlag(self::FLAG_IS_READ,false);
						}
					}							
				}
			}
		}
	}

	public function sendNotificationToGM($title,$content) {
		$O = getOjoo();
		$acc = $O->modele->account->getAccountByGmLevel(9);
		foreach ($acc as $a) {
			$this->sendNotification($a['id'],$title,$content);
		}
	}

	public function sendNotification($receiver,$title,$content,$sender = 'Ojoo',$flags = 0,$type = 0) {
		$O = getOjoo();
		$O->modele->notifications->sender = $sender;
		$O->modele->notifications->receiver = $receiver;
		$O->modele->notifications->title = $title;
		$O->modele->notifications->content = $content;
		if ($sender == 'Ojoo') {
			$flag = self::FLAG_IS_READ | self::FLAG_OJOO | $flags;
		} else $flag = self::FLAG_IS_READ | $flags;
		$O->modele->notifications->flags = $flag;
		$O->modele->notifications->type = self::TYPE_NORMAL | $type;
		$O->modele->notifications->idQuestion = 0;
		$O->modele->notifications->ADD();
	}

	public function setFlag($flag,$type) {
		if ($type) {
			$this->flags = $this->flags | $flag;
		} else {
			$this->flags = $this->flags & (~ $flag ); 
		}		
	}
		
	public function hasFlag($flag,$type = true) {
		if ($type) {
			if ($this->flags & $flag) 
				return true;
			else
				return false;
		} else {
			if ($this->flags & ( ~ $flag)) 
				return true;
			else
				return false;
		}
	}
		
	public function hasType($flag,$type = true) {
		if ($type) {
			if ($this->type & $flag) 
				return true;
			else
				return false;
		} else {
			if ($this->type & (~$flag)) 
				return true;
			else
				return false;
		}			
	}
	
	public function addFlag($flag) {
		$this->flags = $this->flags | $flag;
	}
	
	public function parse() {
		$O = getOjoo();
		$c = $this->content;
		$c = str_replace('{{currentUser}}',$O->wowUser->getCharName(),$c);
		$c = preg_replace_callback('#\{\{char:(.+)\}\}#i',array(get_class($this),'parseUser'),$c);
		$this->content = $c;
	}
	
	public function parseUser($params) {
		if (is_numeric($params[1])) {
			$O = getOjoo();
			$char = $O->modele->characters->getMainChar($params[1]);
			return $char;
		}
	}
	
	public function getIcone() {
		switch ($this->type) {
			case $this->hasType(self::TYPE_NORMAL):
				return 'notifications/not_normal.png';
				break;
			
		}
	}
	
	public function getSender() {
		if ($this->hasFlag(self::FLAG_OJOO))
			return 'Oxygen';
		else {
			if ($this->sender != null) {
				$O = getOjoo();
				$char = $O->modele->characters->getMainChar($this->sender);
				if ($char != null)
					return $char;
				else
					return 'Inconnu';
			} else return 'Inconnu';
		}
	}
	
	public function isGM() {
		if ($this->hasFlag(self::FLAG_GM))
			return '<td><img src="Web/Images/icons/16x16/gm.gif" style="vertical-align: middle;" /> Message officiel de l\'équipe </td>';
		else 
			return false;		
	}
	
	public function question() {
		if ($this->hasType(self::TYPE_QUESTION)) {
			if ($this->idQuestion != null) {				
				$O = getOjoo();
				$question = $O->modele->notifications_questions->select_id($this->idQuestion)->fetch();
				if ($question != null) {
					$question =
					'
							<tr>
								<td colspan="2" style="height: 10px;"></td>
							</tr>
							<tr>
								<td colspan="2"> ' . $question['question'] . ' </td>
							</tr>
							<tr>
								<td><img src="Web/Images/notifications/not_question.png" width="24" height="24" alt="Icône notification"/></td>
								<td><form action="" method="POST"><input type="text" size="45" name="RESPONSE_NOT"/><input type="submit" value="Ok" style="width: 40px;" name="NOTIFICATION_RESPONSE"/></form></td>
							</tr>
					';	
					return $question;			
				}
			}
		}
	}
	
	public function getAsHtml($parsing = true) {
		if ($parsing) 
			$this->parse();
		$html = 
'
<div class="notification" title="' . $this->title . '">
	<table>
		<tr>
			<td><img src="Web/Images/' . $this->getIcone() . '" width="54" height="54" alt="Icône notification"/></td>
			<td>' . $this->content . '</td>
		</tr>
		' . $this->question() . '
	</table>	
	<br />
	<div class="enteteTicket">
		<table>
			<tr>
				<td><img src="Web/Images/icons/16x16/user.png" style="vertical-align: middle;" />' . $this->getSender() . '</td>
				<td style="width: 50px;"></td>
				' . $this->isGM() . '
			</tr>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(".notification").dialog({width: \'600px\'});
</script>
';
	if ($this->hasFlag(self::FLAG_IS_READ))
		return $html;
	else
		return '';
	}
	
	public function setAsRead() {
		$O = getOjoo();
		if ($this->hasType(self::TYPE_QUESTION,false))
			$O->modele->notifications->setAsRead($this->id);
		return true;
	}
	
	static function sendUnreadNotifications() {
		$O = getOjoo();
		$nots = $O->modele->notifications->getUnreadNotifications();
		foreach ($nots as $not) {
			$n = new OxyNotification($not['id']);
			echo $n->getAsHtml();
			$n->setAsRead();
		}
	}

				
	
}
?>