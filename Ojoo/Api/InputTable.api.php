<?php

class InputTable extends Input {
    
    private $patternInput;
    private $inputs;
    private $contrainte;    
    private $occurence;
    private $indexTotal;
    
    public function getType() {
        return "InputTable";
    }
    
    public function __construct($input, $occurence) {
        $this->indexTotal = -1;
        if (is_object($input) && is_int($occurence)) {
            $this->patternInput = $input;
            $this->occurence = $occurence;
            for($i = 0; $i < $this->occurence;$i++) {
                $params = $input->getArrayParams();
                $params['index'] = $i;
                $this->add(new Input($params),$i);                
            }
        } else {
            $O = getOjoo();
            $O->console->debug("Les paramètres donnés sont de mauvais type : __construct(Array \$inputs, int \$occurence);","[INPUT_TABLE]");
        }

    }
    
    public function getInputs() {
        return $this->inputs;
    }
    
    public function add($input,$i = 'GROSNAZE§') {
        $O = getOjoo();
        if (is_object($input)) {
            if ($i != 'GROSNAZE§') {
                //$O->console->debug("I : " . $i);
                $this->inputs[$i] = $input;            
                $input->setIndex($i);
                $c = $input->getContrainte();                
                if (is_array($c))  $c['index'] = $i;
                $input->setContrainte($c);
                $this->indexTotal++;                
                $O->console->debug("Ajout d'un champ (" . $input->getName() . ") d'index : " . $i,"[INPUT_TABLE]");
            } else {
                $this->indexTotal++; 
                $this->inputs[$this->indexTotal] = $input;            
                $input->setIndex($this->indexTotal);     
                $c = $input->getContrainte();
                if (is_array($c))  $c['index'] = $this->indexTotal;
                $input->setContrainte($c);
                $O->console->debug("Ajout d'un champ (" . $input->getName() . ") d'index : " . $input->getIndex(),"[INPUT_TABLE]");
            }
            
        }
        else
            $O->console->debug("add(Input \$input) 'Le paramètre donnée n'est pas un objet de Type Input.' ","[INPUT_TABLE]");
    }
    
    public function get($nb) {
        if (isset ($this->inputs[$nb]))
            return $this->inputs[$nb];
    }
    
}
?>
