<?php
	class Notify {
		public $notifications = array();

		public function add($notification,$key) {
			$this->notifications[$key] = $notification;
		}
		
		public function __construct() {
			if (isset ($_SESSION['notification']))
				$this->notifications = $_SESSION['notification'];
		}

		public function __get($name) {
			return $this->notifications[$name];
		}

		public function __destruct() {
			$_SESSION['notification'] = $this->notifications;
		}

		public function display() {
			foreach($this->notifications as $notification) {
				$notification->display();
				break;
			}
		}

	}
?>