<?php
/******************************************************
* Les rangs sont assez compliqu� et dur � g�rer . 
* Je vous demanderai de bien vouloir utiliser cette classe pr�vue � cette effet,
* Comme �a personne ne va toucher mon code d�j� bien assez horrible comme sa 
* Donc utilisez simplement cette classe qui vous dira tout ;)
* Les rangs des groupes sont sup�rieurs � ceux des utilisateurs
*******************************************************/


class Rights {
	private $droits;
	private $userRight;
	private $compareRight;
	
	public function Rights($userRight = null,$compareRight = null) {
		$rang = yaml_parse('Ojoo/Ojoo.conf.yml');
		$this->droits = $rang["Rang"];
		if ($userRight != null)
			$this->userRight = $userRight;
		if ($userRight != null) 
			$this->compareRight = $compareRight;
	}
	
	public static function format($right) {
		if (is_string($right)) {
			$O = getOjoo();
			foreach ($O->config['Rang'] as $name => $hex) {
				if ($right == $name) return $hex;
			}
			// On renvoit un droit invite en cas de droits non trouvés
			return 0x0000000001001;
		} else return $right; // Déjà en hexa
	}
	
	public function get_droit($nom) {
		return $this->droits[$nom];
	}
	public function isCurrentUserAllowTo($droit) {
		$O = $GLOBALS['O'];
		if ($this->isAllowTo($droit,$O->user->rang)) return true;
		else										 return false;
	}
	
	public function isAllowTo($droitAComparer = null,$rang = null) {
	if (is_string($droitAComparer)) {
		$O = $GLOBALS['O'];
		if (isset ($O->config["Rang"][$droitAComparer])) $droitAComparer = $O->config["Rang"][$droitAComparer];
	}
		if ($droitAComparer == null)
			$droitAComparer = $this->compareRight;
		if ($rang == null)
			$rang = $this->userRight;
		// echo $droitAComparer . ': le droit � comparer s<br />';
		// echo $rang . ' : Le rang de l\'utilisateur <br />';
		$secuRang = array();
		$secuRangActions = array();
		$rang2 = explode("/",$droitAComparer);
		$rang3 = explode(".",$rang2[0]);
		$rang4 = explode("/",$rang);
		$rang5 = explode(".",$rang4[0]);
		for ($i = 0;$i < count($rang3);$i++) {
			// echo $i . ' ceci est $i <br />';
			// echo $rang3[$i] . ' -> 1 et 0 du droit � comparer : $rang3 <br />';
			// echo $rang5[$i] . ' -> 1 et 0 de l\'utilisateur : $rang5 <br />';
			// echo $rang2[$i + 1] . ' -> Les permissions d\'�crire etc ... : $rang2[] -> droit a comparer <br />';
			// echo $rang4[$i + 1] . ' -> Les permissions d\'�crire etc ... : $rang4[] -> utilisateur <br />';
			if ($rang3[$i] == $rang5[$i]) {
				$secuRang[$i] = true;
			}
			else {
				if ($rang5[$i] == 1)  $secuRang[$i] = true; 
				else				$secuRang[$i] = false;
			}
		}
		for ($i = 0;$i < 3;$i++) {
			if ($rang2[$i] == $rang4[$i]) $secuRangActions[$i] = true;
			elseif($rang4[$i] > $rang2[$i]) $secuRangActions[$i] = true;
			else 					              $secuRangActions[$i] = false;
		}
		foreach ($secuRang as $secu) {
			if ($secu == false) return false; // Si � un moment c'est faux, on renvoit faux
		}
		foreach ($secuRangActions as $secu) {
			// var_dump($secuRangActions);
			if (!$secu) return false;
		}
		return true; // Sinon tout a continu� donc on peut renvoyer vrai
	}
}
?>