<?php
	class Lib {
		public $lib = array();
		
		public function Lib() {
			foreach ($this->lib as $fichier) {
				require_once "Ojoo/Lib/" . $fichier . ".lib.php";
				$this->lib[$fichier] = new $fichier();
			}
		}

		public function __get($nom) {
			if (file_exists("Ojoo/Lib/" . $nom . ".lib.php")) {
				require_once "Ojoo/Lib/" . $nom . ".lib.php";
				$this->lib[$nom] = new $nom();
				return $this->lib[$nom];
			} else {
				$O = $GLOBALS['O'];
				$O->Erreur('#01');
			}
		}
		
	}
?>