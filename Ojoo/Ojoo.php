<?php
class Ojoo {
    const DEBUG_MODE = 1;
    const PROD_MODE = 2;
    
	public $api;
	public $lib;
	public $sub;
	public $modele;
	public $config;
	public $event;
	public $erreur;
	public $template;
	public $bdd;
	public $log;
	public $rang;
	public $folder;
	public $file;
	public $route;
	public $notify;
	public $user;
	public $path;
	public $request;
	public $const;
	public $app;
    public $form;
	public $console;
    private $runningMode;
	public $wowUser; // Ajout pour Elevennmanagers
	public $formValidator;
    public $userError;
	public $security;
	public $panier;
	public $flashMessage = array();
	
	public $active_sub;
	public $active_mod;
	public $active_act;
	public $active_url;
	
	public $request_type;

    public $data;
	
	public function Ojoo($runMode = Ojoo::PROD_MODE) {
		$this->init();
                $this->runningMode = $runMode;
	}
        
    public function getRunningMode() {
        return $this->runningMode;
    }
	
	public function addFlashMessage($key,$content) {
            if ($key == 'auto')
                $this->flashMessage[] = $content;
            else
		$this->flashMessage[$key] = $content;
	}
	
	public function init() {
		// Chargement de la config : 
		$this->config = $this->parse_config();
		$this->convertRightToDec();
	
		// Initialisation de touts les composants :
		$this->console = new Console();
		$this->app = new Application();
		$this->api = new Api(); // Fait
		$this->lib = new Lib(); // Fait
		$this->sub = new Sub(null);
		$this->userError = new UserError();
        $this->form = new FormManager();
		$this->modele = new Modele();
		$this->request = new Request("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		$this->route = new Route();
		$this->event = new Event();
		$this->path = yaml_parse('Ojoo/Path.conf.yml');
		$this->template = new Template();
		$this->bdd = new Bdd();
		$this->control = new Control();
		$this->log = new Log('');
		$this->folder = new Folder(null);
		$this->file = new File(null);
		$this->formValidator = new formValidator();
		$this->rang = new Rights($this->config['Rang']['invite'],$this->config['Rang']['invite']); // On place les rangs directement en invit�.
		$this->security = new Security();
		$this->panier = new Panier();
		
		// Inclusion des exceptions
		$this->include_exceptions();
		
        // On ins�re les posts et get :
        $this->data = new VarManager($this->config);
		

        // Cr�ation de l'utilisateur : 
        if (isset ($_SESSION['user'])) $this->user = $_SESSION['user'];
        else                           $this->user = new User(NULL,'invite',$this->config);


	}
	
	public function include_exceptions() {
		$this->folder->set_folder('Erreur/Ojoo/');
		foreach ($this->folder->get_files() as $file) {
			require_once $file->get_path();
		}
	}
	
	public function loadPage($sub,$mod,$act) {		
		if ($this->control->loadPage($sub,$mod,$act)) {
			$this->active_url = $this->request->getUrl();
			$this->active_sub = $sub;
			$this->active_mod = $mod;
			$this->active_act = $act;
			$this->sub->set_sub($this->active_sub);
			// $this->event->trigEvent('onPageLoad');		
			$contentTemplate = new Template();
			$contentTemplate->load_act('Web/Sub/' . $sub . '/' . $mod . '/Act' . '/' . $act . '.php');
			$this->template->load_template($this->sub->config['design']);
			$this->template->set_tampon(str_replace('{{CONTENT}}',$contentTemplate->get_tampon(),$this->template->get_tampon()));
			
		} else 
			$this->app->returnCode(APP_NOT_ALLOWED);
	}
	
	public function resetFlashMessage() {
		$this->flashMessage = array();
	}
	
	public function convertRightToDec() {
		foreach ($this->config['Rang'] as $key => $rang) {
			$this->config['Rang'][$key] = hexdec($rang);
		}
	}
	
	public function loadPage_ajax($sub,$mod,$act) {
		if ($this->control->loadPage($sub,$mod,$act)) {
			$this->active_url = $this->request->getUrl();
			$this->active_sub = $sub;
			$this->active_mod = $mod;
			$this->active_act = $act;
			// $this->event->trig_event('onPageLoad');
			$contentTemplate = new Template();
			$contentTemplate->load_act('Web/Sub/' . $sub . '/' . $mod . '/Act' . '/' . $act . '.php');
			$contentTemplate->display();
		} else
			$this->app->returnCode(APP_NOT_ALLOWED);
	}
	
	public function parse_config() {
		// Parsage de la config via SpycYML
		return yaml_parse('Ojoo/Ojoo.conf.yml');
	}
}
?>