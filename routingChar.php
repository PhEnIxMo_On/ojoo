<?php
return array(
  "detailChar" => array(
      "sub" => "EM",
      "mod" => "char",
      "act" => "detailChar",
      "vars" => "guid",
      "prefix" => "manager",
      "url" => "char-detail-([0-9]+)"
  ) 
);
?>
