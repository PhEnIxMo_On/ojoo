<?php
	return array(
		"doc-id" => array(
			"url" => "doc-id",
			"sub" => "EM",
			"mod" => "docId",
			"act" => "docId"
		),
		"doc-id-add" => array(
			"url" => "doc-id-add",
			"sub" => "EM",
			"mod" => "docId",
			"act" => "docIdAdd"
		),
		"doc-id-detail-ID" => array(
			"url" => "doc-id-detail-(.+)",
			"sub" => "EM",
			"mod" => "docId",
			"act" => "docIdDetail",
			"vars" => "id"
		),
		"doc-id-edit-ID" => array(
			"url" => "doc-id-edit-(.+)",
			"sub" => "EM",
			"mod" => "docId",
			"act" => "docIdEdit",
			"vars" => "id"
		),
		"notes" => array(
			"url" => "notes",
			"sub" => "EM",
			"mod" => "docId",
			"act" => "accueilNotes"
		),
		"charteNinja" => array(
			"url" => "charteNinja",
			"sub" => "EM",
			"mod" => "docId",
			"act" => "charteNinja"
		),
		'docAccueil' => array(
			'url' => 'doc-accueil',
			'prefix' => 'documentation',
			'sub' => 'Ojoo',
			'mod' => 'doc',
			'act' => 'docAccueil'
		)
	);
?>