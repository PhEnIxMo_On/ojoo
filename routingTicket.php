<?php
    return array(
        "voirTickets" => array(
            'url' => 'ticket-liste',
            "prefix" => "manager",
            "sub" => "EM",
            "mod" => "ticket",
            "act" => "voirTickets",
        ),
        "voirTicketsPage" => array(
            'url' => 'ticket-liste-([0-9]+)',
            "prefix" => "manager",
            "sub" => "EM",
            "mod" => "ticket",
            "act" => "voirTickets",
            "vars" => "page"
        ),            
        "detailTicket" =>array(
            'url' => 'ticket-detail-(.+)',
            "prefix" => "manager",
            "sub" => "EM",
            "mod" => "ticket",
            "act" => "detailTicket",
            "vars" => "id",
        ),
        "rechercheTicket" => array(
            'url' => 'ticket-recherche',
            "prefix" => "manager",
            "sub" => "EM",
            "mod" => "ticket",
            "act" => "rechercheTicket"
        ),
        "editerTicket" => array (
            'url' => 'ticket-edition-(.+)',
            "prefix" => "manager",
            "sub" => "EM",
            "mod" => "ticket",
            "act" => "editerTicket",
            "vars" => "id",
        )
    );
?>