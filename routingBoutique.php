<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
     "boutiqueAccueil" => array(
         "sub" => "oxygen",
         "mod" => "boutique",
         "act" => "boutique",
         "url" => "boutique"
     ),  
     "boutiqueAdminList" => array(
         "sub" => "EM",
         "prefix" => "manager",
         "mod" => "boutique",
         "act" => "boutiqueList",
         "url" => "boutique-cat-liste"
     ),
     "boutiqueAdminCatEdit" => array(
     	"sub" => "EM",
     	"prefix" => "manager",
     	"mod" => "boutique",
     	"act" => "boutiqueEdit",
     	"url" => "boutique-cat-edit-([0-9]+)",
     	"vars" => "id"
	 ),
     "boutiqueAdminCatOrdre" => array(
     	"sub" => "EM",
     	"prefix" => "manager",
     	"mod" => "boutique",
     	"act" => "boutiqueList",
     	"url" => "boutique-cat-ordre-(.+)-([0-9]+)",
     	"vars" => "type,id"
	 ),
     "boutiqueAdminCatAdd" => array(
     	"sub" => "EM",
     	"prefix" => "manager",
     	"mod" => "boutique",
     	"act" => "boutiqueAdd",
     	"url" => "boutique-cat-add",
	 ),
	 "boutiqueAdminCatDel" => array(
	 	"sub" => "EM",
	 	"prefix" => "manager",
	 	"mod" => "boutique",
	 	"act" => "boutiqueDel",
	 	"vars" => "id",
	 	"url" => "boutique-delete-(.+)"
	 ),  
     "boutiqueAdminArtList" => array(
         "sub" => "EM",
         "prefix" => "manager",
         "mod" => "boutique",
         "act" => "boutiqueArtList",
         "url" => "boutique-art-liste"
     ),  
     "boutiqueAdminArtCatList" => array(
         "sub" => "EM",
         "prefix" => "manager",
         "mod" => "boutique",
         "act" => "boutiqueArtList",
         "url" => "boutique-art-cat-([0-9]+)-liste",		 
     	"vars" => "idCat"
     ),
     "boutiqueAdminArtEdit" => array(
     	"sub" => "EM",
     	"prefix" => "manager",
     	"mod" => "boutique",
     	"act" => "boutiqueArtEdit",
     	"url" => "boutique-art-edit-([0-9]+)",
     	"vars" => "id"
	 ),
     "boutiqueAdminArtAdd" => array(
     	"sub" => "EM",
     	"prefix" => "manager",
     	"mod" => "boutique",
     	"act" => "boutiqueArtAdd",
     	"url" => "boutique-art-add",
	 ),
	 "boutiqueAdminArtDel" => array(
	 	"sub" => "EM",
	 	"prefix" => "manager",
	 	"mod" => "boutique",
	 	"act" => "boutiqueArtDel",
	 	"vars" => "id",
	 	"url" => "boutique-art-delete-(.+)"
	 ),
     "boutiqueDetailCat" => array(
     	"sub" => "oxygen",
     	"mod" => "boutique",
     	"act" => "detailCat",
     	"url" => "boutique-detail-(.+)",
     	"vars" => "id"
     ),
     "boutiqueDetailPanier" => array(
     	"sub" => "oxygen",
     	"mod" => "boutique",
     	"act" => "detailPanier",
     	"url" => "panier-detail"
     ),
     "boutiqueDetailPanierAdd" => array(
     	"sub" => "oxygen",
     	"mod" => "boutique",
     	"act" => "detailPanier",
     	"url" => "panier-detail-add-([0-9]+)-([0-9]+)",
		"vars" => "addPanier,addPanier2"
     ),
     "boutiqueDetailPanierDel" => array(
     	"sub" => "oxygen",
     	"mod" => "boutique",
     	"act" => "detailPanier",
     	"url" => "panier-detail-del-([0-9]+)-([0-9]+)",
     	"vars" => "delPanier,delPanier2"
     ),
     "boutiqueDetailPanierQte" => array(
     	"sub" => "oxygen",
     	"mod" => "boutique",
     	"act" => "detailPanier",
     	"url" => "panier-detail-qte-([0-9]+)-([0-9]+)-([0-9]+)",
     	"vars" => "qtePanier,qtePanier2,idArticle"
     ),
     "boutiqueCommande" => array(
     	"sub" => "oxygen",
     	"mod" => "boutique",
     	"act" => "commande",
     	"url" => "commande"
     ),
     "boutiqueCrediter" => array(
	 	"sub" => "oxygen",
	 	"mod" => "boutique",
	 	"act" => "boutiqueCrediter",
	 	"url" => "boutique-crediter"
	 ),
	 "boutiqueOk" => array(
	 	"sub" => "oxygen",
	 	"mod" => "boutique",
	 	"act" => "boutiqueOk",
	 	"url" => "boutique-ok",
	 	"vars" => "data"
	 ),
	 "boutiqueNOk" => array(
	 	"sub" => "oxygen",
	 	"mod" => "boutique",
	 	"act" => "boutiqueOk",
	 	"url" => "boutique-nok"
	 ),
	 "boutiqueAdminAcceuil" => array(
	 	"sub" => "EM",
	 	"prefix" => "manager",
	 	"mod" => "boutique",
	 	"act" => "boutiqueAccueil",
	 	"url" => "boutique-accueil"
	 )
);
?>
