<?php
	return array(
        "accueilTest" => array(
			"url" => "accueil",
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "accueil"
		),
		"inscription" => array(
			'url' => 'inscription',
			"sub" => "EM",
            "prefix" => "manager",
			"mod" => "user",
			"act" => "inscription",
		),
		"migration" => array(
		   "url" => "migration",
		   "sub" => "oxygen",
		   "mod" => "user",
		   "act" => "migration"
		  ),		
		"inscriptionOxy" => array(
			'url' => 'inscription',
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "inscription",
		),		
		"telecharger" => array(
			'url' => 'telecharger',
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "telecharger",
		),            
		"profilOxy" => array(
			"url" => 'profil',
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "profil"
		),
		"don" => array(
			"url" => "donner",
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "don"
		),
		"connexion" => array(
			"url" => "connexion",
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "connexion"
		),
		"connexionM" => array(
			"url" => "connexion",
			"prefix" => "manager",
			"sub" => "EM",
			"mod" => "user",
			"act" => "connexion"
		),		
		"deconnexion" => array(
			"url" => "deconnexion",
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "deconnexion"
		),
		"recherche" => array(
			"url" => "recherche",
            "prefix" => "manager",
			"sub" => "EM",
			"mod" => "user",
			"act" => "recherche"
		),
		"editAccountOk" => array(
			"url" => "compte-editer-(ok)-(.+)",
			"prefix" => "manager",
			"sub" => "EM",
			"mod" => "user",
			"act" => "editAccount",
			"vars" => "ok,id"
		),
		"editAccount" => array(
			"url" => "compte-editer-(.+)",
            "prefix" => "manager",
			"sub" => "EM",
			"mod" => "user",
			"act" => "editAccount",
			"vars" => "id"
		),
		"vote" => array(
			"url" => "vote",
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "vote"
		),
		"mp" => array(
			"url" => "mp",
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "mp"
		),
		"mpPartir" => array(
			"url" => "mp-partir-([0-9]+)",
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "mp",
			"vars" => "id"
		),            
		"mpAddConv" => array(
			"url" => "mp-creer",
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "mpAddConv"
		),
		"detailMp" => array(
			"url" => "mp-detail-([0-9]+)",
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "mpDetail",
			"vars" => "id"
		),
		"infosServer" => array(
			"url" => "infos-serveur",
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "infosServeur"
		),
		"statistiques" => array(
			"url" => "statistiques",
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "statistiques"
		),
		"classements" => array(
			"url" => "classements",
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "classements"
		),
		"bigdump" => array(
			"url" => "bigdump",
			"sub" => "oxygen",
			"mod" => "user",
			"act" => "bigdump"
		),
		"login" => array(
			"url" => "login",
			"prefix" => "manager",
			"sub" => "manager",
			"mod" => "user",
			"act" => "login"
		)
		
	);
?>