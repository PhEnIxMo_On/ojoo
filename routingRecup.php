<?php

return array(
    "recupCreate" => array(
        "sub" => "oxygen",
        "mod" => "recuperation",
        "act" => "createRecup",
        "url" => "recup-creer-(.+)",
        "vars" => "type"
    ),
    "accueilRecup" => array(
      "sub" => "oxygen",
      "mod" => "recuperation",
       "act" => "accueilRecup",
       "url" => "recup-accueil"
    ),
    "recupCreate2" => array(
        "sub" => "EM",
        "prefix" => "manager",
        "mod" => "recup",
        "act" => "create",
        "url" => "recup-creer",
        "vars" => "type"
    ),    
    "managerUserRecup2" => array(
        "sub" => "EM",
		"prefix" => "manager",
        "mod" => "recup",
        "act" => "managerRecupUser",
        "url" => "mes-recup"
    ),
    "doubleRecup" => array(
        "sub" => "EM",
        "mod" => "recup",
        "act" => "doubleRecup",
        "url" => "check-recup-(.+)",
        "prefix" => "manager",
        "vars" => "id"
    ),
    "doubleClassRecup" => array(
        "sub" => "EM",
        "mod" => "recup",
        "act" => "doubleClass", 
         "prefix" => "manager",
        "url" => "check-class-(.+)",
        "vars" => "id"
    ),
    "managerUserRecup" => array(
        "sub" => "oxygen",
        "mod" => "recuperation",
        "act" => "managerRecupUser",
        "url" => "mes-recup"
    ),	
    "recupDetail" => array(
        "sub" => "oxygen",
        "mod" => "recuperation",
        "act" => "recupDetail",
        "vars" => "id",
        "url" => "recup-detail-([0-9]+)"
    ),
    "recupEdit" => array(
        "sub" => "oxygen",
        "mod" => "recuperation",
        "act" => "recupEdit",
        "vars" => "id",
        "url" => "recup-edit-([0-9]+)"
    ),
    "adminRecup" => array(
        "sub" => "EM",
        "mod" => "recup",
        "prefix" => "manager",
        "act" => "admin",
        "url" => "recup-admin"
    ),
    "adminListRecup" => array(
        "sub" => "EM",
        "mod" => "recup",
        "prefix" => "manager",
        "act" => "adminListRecup",
        "url" => "recup-admin-list-([0-9]+)",
        "vars" => "state"
    ),
    "adminDetailRecup" => array(
        "sub" => "EM",
        "mod" => "recup",
        "prefix" => "manager",
        "act" => "recupDetailAdmin",
        "url" => "recup-admin-detail-([0-9]+)-([0-9]+)",
        "vars" => "id,cat"
    ),
    "adminSearch" => array(
        "sub" => "EM",
        "mod" => "recup",
        "prefix" => "manager",
        "act" => "adminSearch",
        "url" => "recup-admin-recherche"
    ),
    "demandeGuilde" => array(
        "sub" => "oxygen",
        "mod" => "recuperation",
        "act" => "demandeGuilde",
        "url" => "recup-demande-guilde"
    ),
    "managerGuilde" => array(
        "sub" => "oxygen",
        "mod" => "recuperation",
        "act" => "managerGuilde",
        "url" => "recup-manage-guilde"        
    ),
    "manageGuildeDelete" => array(
        "sub" => "oxygen",
        "mod" => "recuperation",
        "act" => "managerGuilde",
        "url" => "manage-guilde-delete-([0-9]+)",
        "vars" => "idDelete"
    ),
    "manageGuildeAccept" => array(
        "sub" => "oxygen",
        "mod" => "recuperation",
        "act" => "managerGuilde",
        "url" => "manage-guilde-accept-([0-9]+)",
        "vars" => "idAccept"
    ),    
    "adminGuilde" => array(
        "sub" => "EM",
        "prefix" => "manager",
        "mod" => "recup",
        "act" => "adminGuilde",
        "url" => "recup-admin-guilde"        
    ),    
    "listGuilde" => array(
        "sub" => "EM",
        "prefix" => "manager",
        "mod" => "recup",
        "act" => "listGuilde",
        "url" => "admin-guilde-liste-([0-9]+)",
        "vars" => "state"
    ),
    "editGuilde" => array(
        "sub" => "EM",
        "prefix" => "manager",
        "mod" => "recup",
        "act" => "editGuilde",
        "url" => "admin-guilde-edit-(.+)",
        "vars" => "id"
    ),    
);
?>
