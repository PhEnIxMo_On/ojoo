<?php
	// Dщfinitions : 
	define('APP_WAIT',-1);
	define('APP_START',0);
	define('APP_PROCESS',1);
	define('APP_ERROR_NOTICE',2);
	define('APP_ERROR_STOP',3);
	define('APP_ERROR_CRITICAL',4);
	define('APP_ERROR_UNKNOW',5);
	define('APP_NOT_FOUND',6);
	define('APP_NOT_ALLOWED',7);
	define('APP_EXCEPTION',9);
	define('APP_END',8);
	
	ini_set('display_errors',true);
	
	
	// Inclusion des щlements systems libs :
	include 'Ojoo/Lib/spyc.lib.php';
	include 'Ojoo/Lib/Lib.lib.php';
    include 'Ojoo/Lib/telnet.lib.php';

	
	// Inclusion des щlements systems api :
	include 'Ojoo/Api/secure.api.php';
	include 'Ojoo/Api/Application.api.php';
	include 'Ojoo/Api/Request.api.php';
    include 'Ojoo/Api/Manager.api.php';
    include 'Ojoo/Api/Input.api.php';
    include 'Ojoo/Api/InputTable.api.php';
    include 'Ojoo/Api/Form.api.php';
    include 'Ojoo/Api/FormManager.api.php';
	include 'Ojoo/Api/Route.api.php';
	include 'Ojoo/Api/Fonction.api.php';
	include 'Ojoo/Api/Sort.api.php';
	include 'Ojoo/Api/PhpError.api.php';
	include 'Ojoo/Api/Contrainte.api.php';
	include 'Ojoo/Api/ConsoleMessage.api.php';
	include 'Ojoo/Api/Console.api.php';
	include 'Ojoo/Api/UserError.api.php';
	include 'Ojoo/Api/FormValidator.api.php';
	include 'Ojoo/Api/Api.php';
	include 'Ojoo/Api/Event.api.php';
	include 'Ojoo/Api/Modele.api.php';
	include 'Ojoo/Api/Act.api.php';
	include 'Ojoo/Api/User.api.php';
	include 'Ojoo/Api/View.api.php';
	include 'Ojoo/Api/Mod.api.php';
	include 'Ojoo/Api/Sub.api.php';
	include 'Ojoo/Api/Filter.api.php';
	include 'Ojoo/Api/Template.api.php';
	include 'Ojoo/Api/Bdd.api.php';
	include 'Ojoo/Api/DbInstance.api.php';
	include 'Ojoo/Api/Log.api.php';
	include 'Ojoo/Api/mvc_bdd.api.php';
	include 'Ojoo/Api/Control.api.php';
	include 'Ojoo/Api/Rights.api.php';
	include 'Ojoo/Api/Folder.api.php';
	include 'Ojoo/Api/File.api.php';
    include 'Ojoo/Api/VarManager.api.php';   
	include 'Ojoo/Api/Article.api.php'; 
	include 'Ojoo/Api/Panier.api.php';
	include 'Ojoo/Api/PharManager.api.php';
	

	// On set le fuseau horraire sur Paris : 
	date_default_timezone_set('Europe/Paris');
	
	// Session expire : 1h00
	session_cache_expire(10800);
	
	// Session start
    session_start();
    //session_destroy();
    
	
	// Gestion des exceptions :
	//set_exception_handler('ojoo_exception_handler');
	
	// Gestion des erreurs :
	//set_error_handler('ojoo_error_handler');
	
	// Shutdown fonction :
	//register_shutdown_function('ojoo_shutdown_function');

	if(!isset ($_SESSION['Rang'])) $_SESSION['Rang'] = -1;

	$_SESSION['Rang'] = 5000000000;
	//echo $_SESSION['Rang'];
	
	// Inclusion d'Ojoo
	include 'Ojoo/Ojoo.php';
	global $O;
	$O = new Ojoo(Ojoo::DEBUG_MODE);
	
	if ($O->config["Debug"]["Phar"] && $O->getRunningMode() == Ojoo::DEBUG_MODE)
		ini_set('phar.readonly','Off');
	else
		ini_set('phar.readonly','On');

	// On nofitie que l'application a bien ?t? cr??e;
	if ($O->config["Debug"]["Application"])
		$O->console->debug(Console::APP,"Ojoo application have been successfully created.",__FILE__,"none",get_defined_vars());
	$O->app->returnCode($O->route->getRoute($O->request->getUrl()));
	$O->app->returnCode(APP_END);
	if ($O->request->getMode() == 'normal' && $O->getRunningMode() == Ojoo::DEBUG_MODE && $O->config["Debug"]["ActiveConsole"])
		$O->console->display();

?>