<?php
	return array(
		"compress" => array(
			"url" => "compress-([0-9+])",
			"prefix" => "module",
			"sub" => "Ojoo",
			"mod" => "Module",
			"act" => "compress",
			"title" => "Module",
			"subTitle" => "Compression au format PHAR",
			"vars" => "stape"
		)
	);
?>