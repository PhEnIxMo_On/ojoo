<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
     "faqAdminAccueil" => array(
         "sub" => "EM",
		 "prefix" => "manager",
         "mod" => "faq",
         "act" => "faqList",
         "url" => "faq-Liste"
     ),  
     "faqAdminList" => array(
         "sub" => "EM",
         "prefix" => "manager",
         "mod" => "faq",
         "act" => "faqList",
         "url" => "faq-Liste"
     ),
     "faqAdminCatEdit" => array(
     	"sub" => "EM",
     	"prefix" => "manager",
     	"mod" => "faq",
     	"act" => "faqCatEdit",
     	"url" => "faq-cat-edit-([0-9]+)",
     	"vars" => "id"
	 ),
     "faqAdminCatOrdre" => array(
     	"sub" => "EM",
     	"prefix" => "manager",
     	"mod" => "faq",
     	"act" => "faqList",
     	"url" => "faq-cat-ordre-(.+)-([0-9]+)",
     	"vars" => "type,id"
	 ),
     "faqAdminCatAdd" => array(
     	"sub" => "EM",
     	"prefix" => "manager",
     	"mod" => "faq",
     	"act" => "faqCatAdd",
     	"url" => "faq-cat-add",
	 ),
	 "faqAdminCatDel" => array(
	 	"sub" => "EM",
	 	"prefix" => "manager",
	 	"mod" => "faq",
	 	"act" => "faqDel",
	 	"vars" => "id",
	 	"url" => "faq-delete-(.+)"
	 ),  
     "faqAdminQRtList" => array(
         "sub" => "EM",
         "prefix" => "manager",
         "mod" => "faq",
         "act" => "faqQRList",
         "url" => "faq-qr-liste"
     ),  
     "faqAdminQRCatList" => array(
         "sub" => "EM",
         "prefix" => "manager",
         "mod" => "faq",
         "act" => "faqQRList",
         "url" => "faq-qr-cat-([0-9]+)-liste",		 
     	"vars" => "idCat"
     ),
     "faqAdminQREdit" => array(
     	"sub" => "EM",
     	"prefix" => "manager",
     	"mod" => "faq",
     	"act" => "faqQREdit",
     	"url" => "faq-qr-edit-([0-9]+)",
     	"vars" => "id"
	 ),
     "faqAdminQROrdre" => array(
     	"sub" => "EM",
     	"prefix" => "manager",
     	"mod" => "faq",
     	"act" => "faqList",
     	"url" => "faq-qr-ordre-(.+)-([0-9]+)",
     	"vars" => "type,id"
	 ),
     "faqAdminQRAdd" => array(
     	"sub" => "EM",
     	"prefix" => "manager",
     	"mod" => "faq",
     	"act" => "faqQRAdd",
     	"url" => "faq-qr-add",
	 ),
	 "faqAdminQRDel" => array(
	 	"sub" => "EM",
	 	"prefix" => "manager",
	 	"mod" => "faq",
	 	"act" => "faqQRDel",
	 	"vars" => "id",
	 	"url" => "faq-qr-delete-(.+)"
	 ),
     "faq" => array(
     	"sub" => "oxygen",
     	"mod" => "faq",
     	"act" => "faq",
     	"url" => "FAQ"
     )
);
?>
