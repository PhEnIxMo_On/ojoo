<?php
	return array(
		'accueil' => array(
			'url' => 'taches-accueil',
			'sub' => 'EM',
			'mod' => 'task',
			'act' => 'accueil'
		),
		'creer' => array(
			'url' => 'taches-creer',
			'sub' => 'EM',
			'mod' => 'task',
			'act' => 'createTask'
		),
		'state' => array(
			'url' => 'taches-state-(.+)-(.+)',
			'sub' => 'EM',
			'mod' => 'task',
			'act' => 'accueil',
			'vars' => 'state,id'
		),
		'edit' => array(
			'url' => 'taches-edit-(.+)',
			'sub' => 'EM',
			'mod' => 'task',
			'act' => 'editTask',
			'vars' => 'id'
		),
		'detail' => array(
			'url' => 'taches-detail-(.+)',
			'sub' => 'EM',
			'mod' => 'task',
			'act' => 'detailTask',
			'vars' => 'id'
		),
		'viewAll' => array(
			'url' => 'taches-all',
			'sub' => 'EM',
			'mod' => 'task',
			'act' => 'taskAll',
			'vars' => 'id'
		)
	);
?>